---
title: "Download and use the AppImage version"
layout: page
sources:
  - name: Stable version
    pipelineTxt: download page showing the stable version
    pipelineUrl: https://cdn.kde.org/ci-builds/office/kmymoney/5.1/linux/
    imageName: kmymoney-5.1-####-linux-gcc-x86_64.AppImage
    description: >
        AppImage packages are available for Linux users who want to use a version newer than the one available in their Software Center.<br>
        They offer the most recent state of the stable version  including all the latest fixes and are generated whenever the stable branch
        receives fixes in the source code repository. This process is called Continuous Integration and Continuous Deployment (or simply CI/CD).

    note:

  - name: Development version
    pipelineTxt: download page showing the development version
    pipelineUrl: https://cdn.kde.org/ci-builds/office/kmymoney/master/linux/
    imageName: kmymoney-master-####-linux-gcc-x86_64.AppImage
    description: >
        For the more adventurous Linux users, the development versions might be interesting. They contain the latest development state but
        might be unstable and eat your data. Make sure to have backups of your data before you use this software.
        To download it, the following steps need to be performed:

    note:

---

<h1>How to download and use the AppImage version</h1>

Page last updated: 2024-03-10

<div class="highlight">
    <p><strong>Important:</strong> The images referenced on this page are build as part of KDE's CI/CD process and are automatically generated when
    the repository receives changes. They contain a number in the build which is changed with each build. Please replace the placeholder #### with
    the actual number in the instructions below.</p>

{% for source in page.sources %}
    <h2>{{ source.name }}</h2>
    {{ source.description  | markdownify }}

    <ul>
    <li>Visit the <a href="{{ source.pipelineUrl }}" target="_blank">{{ source.pipelineTxt }}</a></li>
    <li>Click on the link which links to the AppImage file. This starts downloading the file to your local disk.</li>
    <li>Make the downloaded file executable using the following command:<br><br><pre>chmod +x {{ source.imageName }}</pre></li>
    <li>Execute the command <strong>{{ source.imageName }}</strong><br>(Don't forget to provide the path if the download directory is not in your search path)</li>
    </ul>

    <p><em>{{ source.note | markdownify }}</em></p>
{% endfor %}
</div>
