<?php

/*
 * Copyright 2020-2024 Ralf Habacker <ralf.habacker@freenet.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) version 3, or any
 * later version accepted by the membership of KDE e.V. (or its
 * successor approved by the membership of KDE e.V.), which shall
 * act as a proxy defined in Section 6 of version 3 of the license.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

 /*
  * online quotes test web service for alkimia
  */

function dump_single_data($a, $type, $dtype = "")
{
    $date = date("H:i:s(CE\S\T) d/m/Y");
    $value = floatval(rand(300, 600)) / 10;
    # https://query1.finance.yahoo.com/v8/finance/chart/USD?events=history&includeAdjustedClose=true&period1=1730070000&period2=1730313114&interval=1d
    if ($type == "json") {
?>
{"chart":{"result":[{"meta":{"currency":"USD","symbol":"<?php echo $a ?>","exchangeName":"PCX","fullExchangeName":"NYSEArca","instrumentType":"ETF","firstTradeDate":1170340200,"regularMarketTime":1730316162,"hasPrePostMarketData":true,"gmtoffset":-14400,"timezone":"EDT","exchangeTimezoneName":"America/New_York","regularMarketPrice":136.05,"fiftyTwoWeekHigh":137.872,"fiftyTwoWeekLow":133.12,"regularMarketDayHigh":137.872,"regularMarketDayLow":133.12,"regularMarketVolume":326619,"longName":"ProShares Ultra Semiconductors","shortName":"ProShares Ultra Semiconductors","chartPreviousClose":138.32,"priceHint":2,"currentTradingPeriod":{"pre":{"timezone":"EDT","start":1730275200,"end":1730295000,"gmtoffset":-14400},"regular":{"timezone":"EDT","start":1730295000,"end":1730318400,"gmtoffset":-14400},"post":{"timezone":"EDT","start":1730318400,"end":1730332800,"gmtoffset":-14400}},"dataGranularity":"1d","range":"","validRanges":["1d","5d","1mo","3mo","6mo","1y","2y","5y","10y","ytd","max"]},"timestamp":[1730122200,1730208600,1730316162],"indicators":{"quote":[{"low":[137.0,135.47999572753906,133.1199951171875],"close":[137.3000030517578,141.80999755859375,136.0500030517578],"high":[140.16000366210938,143.30999755859375,137.8719940185547],"open":[140.16000366210938,137.17999267578125,137.0],"volume":[276500,343300,326619]}],"adjclose":[{"adjclose":[137.3000030517578,141.80999755859375,136.0500030517578]}]}}],"error":null}}
<?php
    } else if ($type == "csv") {
        echo "symbol;value;date\n";
        echo "$a;$value;$date\n";
    } else {
        if ($dtype == "javascript") {
            echo <<<EOD
<script type="text/javascript">
document.body.onload = addQuote("$value", "$date");

function addQuote(value, date) {
  const newDiv = document.createElement("span");
  document.createTextNode(value);
  newDiv.appendChild(document.createTextNode(value));
  newDiv.appendChild(document.createTextNode(" updated "));
  newDiv.appendChild(document.createTextNode(date));
  const currentDiv = document.getElementById("div");
  document.body.insertBefore(newDiv, currentDiv);
}
</script>
<div/>
EOD;
        } else {
            echo "$value $date</br>";
        }
    }
}

function dump_double_data($a, $b, $type)
{
    $date = date("H:i:s(CE\S\T) d/m/Y");
    $value = rand(7900, 8100);
    # from https://query1.finance.yahoo.com/v8/finance/chart/BTC-USD?events=history&includeAdjustedClose=true&period1=1730070000&period2=1730313114&interval=1d
    if ($type == "json") {
?>
{"chart":{"result":[{"meta":{"currency":"USD","symbol":"<?php echo "$a-$b" ?>","exchangeName":"CCC","fullExchangeName":"CCC","instrumentType":"CRYPTOCURRENCY","firstTradeDate":1410912000,"regularMarketTime":1730312940,"hasPrePostMarketData":false,"gmtoffset":0,"timezone":"UTC","exchangeTimezoneName":"UTC","regularMarketPrice":71983.44,"fiftyTwoWeekHigh":72707.63,"fiftyTwoWeekLow":71462.375,"regularMarketDayHigh":72707.63,"regularMarketDayLow":71462.375,"regularMarketVolume":45133393920,"longName":"Bitcoin USD","shortName":"Bitcoin USD","chartPreviousClose":67929.3,"priceHint":2,"currentTradingPeriod":{"pre":{"timezone":"UTC","end":1730246400,"start":1730246400,"gmtoffset":0},"regular":{"timezone":"UTC","end":1730332740,"start":1730246400,"gmtoffset":0},"post":{"timezone":"UTC","end":1730332740,"start":1730332740,"gmtoffset":0}},"dataGranularity":"1d","range":"","validRanges":["1d","5d","1mo","3mo","6mo","1y","2y","5y","10y","ytd","max"]},"timestamp":[1729987200,1730073600,1730160000,1730312940],"indicators":{"quote":[{"close":[67929.296875,69907.7578125,72720.4921875,71983.4375],"volume":[16721307878,38799856657,58541874402,45133393920],"open":[67023.4765625,67922.671875,69910.046875,72707.6328125],"high":[68221.3125,70212.265625,73577.2109375,72707.6328125],"low":[66847.2265625,67535.1328125,69729.9140625,71462.375]}],"adjclose":[{"adjclose":[67929.296875,69907.7578125,72720.4921875,71983.4375]}]}}],"error":null}}
<?php
    } else if ($type == "csv") {
        echo "symbol1;symbol2;rate;date;notes\n";
        echo "$a;$b;$value;$date\n";
    } else { ?>
<h1 style="margin-top:5px;color:#ec1b1b;"><span style="font-size:15px;color:#000;">1 Bitcoin =</span><br /> <?php echo $value ?> British Pound</h1>
<span class="datetime" style="display:inline-block; margin-top:10px; text-align:right; align:right; font-size:12px; color:#9c9c9c">updated <?php echo $date ?></span>
<?php }
}

# show errors
$debug = isset($_REQUEST['debug']);
if ($debug) {
    error_reporting(E_ALL);
    ini_set('display_errors', '1');
}

$isPost = $_SERVER['REQUEST_METHOD'] == "POST";

$self = ($_SERVER['SERVER_PORT'] == '443' ? 'https://' : 'http://').$_SERVER['SERVER_NAME'].$_SERVER['SCRIPT_NAME'];

# detect number of parameters
$params = $_REQUEST;

# return server error
$servererror= isset($params['servererror']) ? strtolower($params['servererror']) : "";
if ($servererror == "1") {
    http_response_code(500);
    exit(0);
}

$redirect= isset($params['redirect']) ? strtolower($params['redirect']) : "";
if ($redirect == "1") {
    unset($params['redirect']);
    $url = $self.'?'.http_build_query($params);
    header("Location: ".$url);
    http_response_code(301);
    exit(0);
}

$timeout= isset($params['timeout']) ? strtolower($params['timeout']) : "";
if ($timeout != "") {
    sleep(intval($timeout));
    header("Content-Type: text/plain; charset=utf-8");
    http_response_code(504);
    exit(0);
}

$type = isset($params['type']) ? strtolower($params['type']) : "html";
$dtype = isset($params['dtype']) ? strtolower($params['dtype']) : "";

$file_date = date("Y-m-d-H-i-s");
if ($type == "json") {
    header("Content-Type: application/json");
    header("Content-Disposition: inline; filename=\"data-$file_date.$type\"");
} else if ($type == "csv") {
    header("Content-Type: text/csv");
    header("Content-Disposition: inline; filename=\"data-$file_date.$type\"");
} else {
    header("Content-Type: text/html; charset=utf-8");
    echo "<html><body>";
}

# default output
if (count($params) == 0) {
?>
<p>This service is intended for access by KMyMoney, the online quote editor or any other application using the online quote support provided by alkimia.</p>
<p>To use this service, enter one of the following URL's in the URL field of the quote details widget:
<ul>
<li> with one parameter as html output:<pre><?php echo $self ?>?a=%1</pre></li>
<li> with one parameter as json output:<pre><?php echo $self ?>?a=%1&type=json</pre></li>
<li> with one parameter as csv output:<pre><?php echo $self ?>?a=%1&type=csv</pre></li>
<li> with two parameters as html output:<pre><?php echo $self ?>?a=%1&b=%2</pre></li>
<li> with two parameters as json output:<pre><?php echo $self ?>?a=%1&b=%2&type=json</pre></li>
<li> with two parameters as csv output:<pre><?php echo $self ?>?a=%1&b=%2&type=csv</pre></li>
<li> with one parameter as html output generated by javascript:<pre><?php echo $self ?>?a=%1&dtype=javascript</pre></li>
</ul>
<?php
}

# handle requests
if (isset($params['a']) && isset($params['b'])) {
    dump_double_data($params['a'], $params['b'], $type);
} else if (isset($params['a'])) {
    dump_single_data($params['a'], $type, $dtype);
}

# debug output
if ($debug)
    echo "<p>Debug Information</p>"
        ."<pre>".print_r($GLOBALS, true)."</pre>\n";

if ($type == "html")
    echo "</body></html>";
