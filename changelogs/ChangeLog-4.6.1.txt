2011-08-11 15:14  yurchor

	* CMakeLists.txt, doc, doc/CMakeLists.txt, doc/account_edit.png,
	  doc/account_options.png, doc/accountdropdown.png,
	  doc/accounts_view.png, doc/accwiz1.png, doc/accwiz10.png,
	  doc/accwiz2.png, doc/accwiz6.png, doc/accwiz7.png,
	  doc/accwiz8.1.png, doc/accwiz8.2.png, doc/accwiz8.3.png,
	  doc/accwiz8.4.png, doc/accwiz8.5.png, doc/accwiz9.png,
	  doc/budgets_view.png, doc/calendarview.png, doc/categories.png,
	  doc/categories_view.png, doc/category_widget.png,
	  doc/categoryedit.png, doc/cleared_state.png,
	  doc/confirmenter.png, doc/credits.docbook,
	  doc/currency_newpriceentry.png, doc/currency_priceeditor.png,
	  doc/date_widget.png, doc/details-accounts.docbook,
	  doc/details-budgets.docbook, doc/details-categories.docbook,
	  doc/details-currencies.docbook, doc/details-database.docbook,
	  doc/details-forecast.docbook, doc/details-formats.docbook,
	  doc/details-impexp.docbook, doc/details-institutions.docbook,
	  doc/details-investments.docbook, doc/details-ledgers.docbook,
	  doc/details-loans.docbook, doc/details-payees.docbook,
	  doc/details-reconciliation.docbook, doc/details-reports.docbook,
	  doc/details-schedules.docbook, doc/details-search.docbook,
	  doc/details-settings.docbook, doc/details-widgets.docbook,
	  doc/enterschedule.png, doc/faq.docbook, doc/find-account.png,
	  doc/find-amount.png, doc/find-category.png, doc/find-date.png,
	  doc/find-details.png, doc/find-payee.png, doc/find-text.png,
	  doc/firsttime.docbook, doc/forecast_view.png,
	  doc/generate_sql.png, doc/gnucash-import_options.png,
	  doc/gnucash-report.png, doc/gnucash-select_price_source.png,
	  doc/home_view.png, doc/index.docbook, doc/installation.docbook,
	  doc/institution_options.png, doc/institution_view.png,
	  doc/institutions_view.png, doc/introduction.docbook,
	  doc/investment-currencywarning.png,
	  doc/investment-exchangerateeditor.png,
	  doc/investment-onlineupdate.png,
	  doc/investment-transactionform.png,
	  doc/investments_summarytab.png, doc/investments_view.png,
	  doc/ledger_more.png, doc/ledgers_view.png,
	  doc/ledgerview-numbered.png, doc/mainwindow_numbered.png,
	  doc/makemostof.docbook, doc/man-kmymoney.1.docbook, doc/new.png,
	  doc/newacct.png, doc/newfile-2.png, doc/newfile-3.png,
	  doc/newfile-4.png, doc/newfile-5.png, doc/newfile-6.png,
	  doc/newfile.png, doc/newinst.png, doc/newsched_numbered.png,
	  doc/outbox_view.png, doc/payee_history.png, doc/payee_info.png,
	  doc/payee_match.png, doc/payee_widget.png, doc/payees_view.png,
	  doc/payeeview.png, doc/qif_report.png, doc/qifimport-export.png,
	  doc/qifimport-qifprofiledate.png,
	  doc/qifimport-qifprofileeditor.png, doc/qifimportverify.png,
	  doc/qifopen.png, doc/reconcile-redo.png, doc/reconcile.png,
	  doc/reconcile1.png, doc/reconcile2.png, doc/reconcile3.png,
	  doc/reconcile4.png, doc/reconcile5.png, doc/reference.docbook,
	  doc/report_configuration-reporttab.png,
	  doc/report_configuration-rowscolumns.png,
	  doc/reports_view-all.png, doc/reports_view.png,
	  doc/schedcaltypes.png, doc/schedmonthday.png, doc/schedpopup.png,
	  doc/schedule_view.png, doc/scheduleview.png,
	  doc/select_currency.png, doc/select_database.png,
	  doc/split_transaction.png, doc/tipofday.png,
	  doc/transaction_find.png, doc/transactionform-off.png,
	  doc/transactionform.png, doc/translist.png, doc/value_widget.png,
	  doc/whatsnew.docbook: Add documentation to the stable branch of
	  KMyMoney, enable its compilation.

2011-08-11 18:43  conet

	* kmymoney/mymoney/CMakeLists.txt,
	  kmymoney/mymoney/storage/CMakeLists.txt: Fix tests link
	  directives. Patch by José Arthur Benetasso Villanova.
	  Backported to the stable branch.

2011-08-13 10:52  conet

	* kmymoney/mymoney/mymoneyscheduled.cpp: BUG: 262945
	  Consider the adjusted dates (both start and end) of a schedule
	  when computing the payment dates in a given period.
	  Backported to the stable branch.

2011-08-13 18:47  ostroffjh

	* doc/index.docbook: Added French translator entities so French
	  documentation can be compiled with kdelibs < 4.7

2011-08-13 22:23  conet

	* kmymoney/mymoney/mymoneyscheduletest.cpp,
	  kmymoney/mymoney/mymoneyscheduletest.h: BUG: 262945
	  Added a testcase for this scenario.
	  Backported to the stable branch.

2011-08-14 12:56  conet

	* kmymoney/kmymoney.cpp: BUG: 280070
	  Refactored the closeFile code of the KMyMoney application which
	  takes care to also reset the selections and use it instead of the
	  plain MyMoneyView::closeFile which leaves the selections at the
	  KMyMoneyApplication invalid and causing this uncaught exception.
	  Still todo is to investigate the other usages of
	  MyMoneyView::closeFile in kmymoney.cpp.
	  Backported to the stable branch.

2011-08-15 11:08  conet

	* kmymoney/widgets/kmymoneyaccountselector.cpp: BUG: 249438
	  Instead of disabling a parent item which does not matches the
	  filter but has children that do match the filter mark it as not
	  selectable.
	  If we would disable the item then all it's children will be drawn
	  as disabled even if they would be selectable. Disabling and
	  selectability are two aspects and it seems that disabling applies
	  to a whole subtree.
	  Backported to the stable branch.

2011-08-15 20:53  conet

	* kmymoney/converter/mymoneygncreader.cpp: BUG: 280068
	  Make sure the storage is attached while using the
	  MyMoneyFileTransaction. I don't know when did this break but it
	  seems that GNUCash import was not working for a long time.
	  Backported to the stable branch.

2011-08-15 21:09  conet

	* kmymoney/converter/mymoneygncreader.cpp: Make sure the string
	  puzzle is properly split.
	  Backported to the stable branch.

2011-08-15 21:09  conet

	* kmymoney/dialogs/kgncimportoptionsdlg.cpp: BUG: 257501
	  Remove the cancel button since at the point the dialog is
	  displayed the import process can't be canceled anymore. If the
	  options dialog is closed with the 'x' some defaults are used (the
	  same as previously pressing Cancel but at least it does not
	  suggest that the process can be canceled).
	  Backported to the stable branch.

2011-08-15 21:43  conet

	* kmymoney/dialogs/kmymoneysplittable.cpp: BUG: 241544
	  It's pretty obvious, the base class implementation was not
	  called. Now resizing is very smooth with the table, I don't know
	  what took us so long.
	  Backported to the stable branch.

2011-08-16 10:40  allananderson

	* kmymoney/plugins/csvimport/investprocessing.cpp: Bug 280117
	  
	  While testing an unreleased version of the CSV plugin, a crash
	  occurred during
	  import of an investment file. The crash occurred when selecting a
	  column for
	  the memo field, when that column had already been allocated.
	  
	  The cause was an uninitialised variable.
	  
	  The same problem exists in the stable version of the plugin.

2011-08-16 12:40  allananderson

	* kmymoney/plugins/csvimport/investprocessing.cpp: Bug 280117
	  SVN_SILENT

2011-08-16 21:01  conet

	* kmymoney/kmymoney.cpp: BUG: 280172
	  Fix an infinite loop in ReportAccount::calculateAccountHierarchy
	  caused by an invalid budget that was obtained by deleting a
	  category and assigning it to another category which was not
	  budgeted. In that case MyMoneyBudget::account will return the
	  empty account group for the new category when calling
	  'MyMoneyBudget::AccountGroup toBudget = b.account(categoryId);'
	  thus if you add toBudget with toBudget.id() instead of toBodget
	  with categoryId then an account group with an empty id is
	  created.
	  Backported to the stable branch.

2011-08-17 17:18  conet

	* kmymoney/kmymoney.cpp, kmymoney/mymoney/mymoneyfile.cpp,
	  kmymoney/reports/objectinfotable.cpp: BUG: 280071
	  A payee could be referenced by a loan account. When a payee is
	  deleted consider this reference also and update it according to
	  the user's reassignment choice.
	  
	  Fixed the payee deletion code and added a step in the consistency
	  check to fix files that where affected by this issue.
	  Backported to the stable branch.

2011-08-18 08:05  yurchor

	* kmymoney/mymoney/mymoneyfile.cpp: Backport commit 1247919
	  
	  fix typo: romved->removed

2011-08-21 11:59  scripty

	* kmymoney/kmymoney.desktop,
	  kmymoney/plugins/csvimport/kmm_csvimport.desktop,
	  kmymoney/plugins/icalendarexport/kcm_kmm_icalendarexport.desktop,
	  kmymoney/plugins/icalendarexport/kmm_icalendarexport.desktop,
	  kmymoney/plugins/kbanking/kmm_kbanking.desktop: SVN_SILENT made
	  messages (.desktop file)

2011-08-21 19:37  conet

	* kmymoney/mymoney/mymoneyfile.cpp, kmymoney/mymoney/mymoneyfile.h,
	  kmymoney/mymoney/mymoneyfiletest.cpp,
	  kmymoney/mymoney/mymoneyfiletest.h,
	  kmymoney/views/kmymoneyview.cpp: Create the unique ID only when
	  necessary
	  
	  BUG: 280067
	  
	  Backported to the stable branch.

2011-08-21 21:29  conet

	* kmymoney/kmymoney.cpp: BUG: 279206
	  
	  Process KMyMoneyApp::slotTransactionsEnter(void) only once per
	  triggered action.
	  
	  Backported to the stable branch.

2011-08-25 20:30  conet

	* libkgpgfile/kgpgfile.cpp: Properly initialize the GpgMe library
	  before we use it.
	  Backported to the stable branch.

2011-08-26 17:50  conet

	* kmymoney/mymoney/mymoneyfile.cpp: BUG: 280740
	  During the 'Consistency check' check that the child accounts are
	  consistent (the parent they know is the account that contains
	  them in their child list).
	  Backported to the stable branch.

2011-08-26 18:00  conet

	* kmymoney/dialogs/transactioneditor.cpp: BUG: 280795
	  If editing multiple transactions and the date is valid mark the
	  editing as complete.
	  Backported to the stable branch.

2011-08-27 07:52  conet

	* doc/details-accounts.docbook: Fixed typo.
	  Backport to the stable branch.

2011-08-28 11:49  scripty

	* kmymoney/kmymoney.desktop: SVN_SILENT made messages (.desktop
	  file)

2011-08-28 15:59  conet

	* kmymoney/kmymoney.cpp, kmymoney/plugins/viewinterface.h,
	  kmymoney/views/kaccountsview.cpp, kmymoney/views/kaccountsview.h,
	  kmymoney/views/kbudgetview.cpp, kmymoney/views/kbudgetview.h,
	  kmymoney/views/kcategoriesview.cpp,
	  kmymoney/views/kcategoriesview.h,
	  kmymoney/views/kforecastview.cpp, kmymoney/views/kforecastview.h,
	  kmymoney/views/kgloballedgerview.cpp,
	  kmymoney/views/khomeview.cpp,
	  kmymoney/views/kinstitutionsview.cpp,
	  kmymoney/views/kinstitutionsview.h,
	  kmymoney/views/kinvestmentview.cpp,
	  kmymoney/views/kinvestmentview.h,
	  kmymoney/views/kmymoneyview.cpp, kmymoney/views/kmymoneyview.h,
	  kmymoney/views/kpayeesview.cpp, kmymoney/views/kpayeesview.h,
	  kmymoney/views/kreportsview.cpp,
	  kmymoney/views/kscheduledview.cpp,
	  kmymoney/views/kscheduledview.h: Fixed action enable/disable when
	  changing views.
	  
	  Backported to the stable branch.

2011-09-04 11:44  scripty

	* kmymoney/kmymoney.desktop: SVN_SILENT made messages (.desktop
	  file)

2011-09-14 12:58  conet

	* kmymoney/reports/objectinfotable.cpp,
	  kmymoney/views/kreportsview.cpp: Make some untranslatable user
	  visible strings in reports translatable.
	  Backport it to the stable branch.

2011-09-26 19:18  conet

	* kmymoney/kmymoneyutils.cpp: Use link color from color scheme
	  Backported to the stable branch.

2011-10-01 13:10  scripty

	* kmymoney/plugins/icalendarexport/kmm_icalendarexport.desktop:
	  SVN_SILENT made messages (.desktop file)

2011-10-02 12:13  conet

	* kmymoney/dialogs/kequitypriceupdatedlg.cpp: Allow online price
	  update during account creation
	  
	  Backported to stable branch.
	  
	  BUG: 283163

2011-10-02 12:29  conet

	* kmymoney/plugins/kbanking/widgets/kbaccountlist.cpp,
	  kmymoney/plugins/kbanking/widgets/kbaccountlist.h: Removed unused
	  code
	  Make strings translatable
	  
	  Backported to the stable branch.
	  
	  BUG: 282901

2011-10-02 12:31  conet

	* kmymoney/kmymoney.cpp: Don't open the institution editor if
	  selected institution does not have an ID
	  
	  Backported to the stable branch.
	  
	  BUG: 282578

2011-10-20 12:20  scripty

	* developer-doc/phb/kmymoney-developer.desktop,
	  kmymoney/kmymoney.desktop,
	  kmymoney/plugins/csvimport/kmm_csvimport.desktop,
	  kmymoney/plugins/icalendarexport/kcm_kmm_icalendarexport.desktop,
	  kmymoney/plugins/icalendarexport/kmm_icalendarexport.desktop,
	  kmymoney/plugins/kbanking/kmm_kbanking.desktop,
	  kmymoney/plugins/kmymoneyimporterplugin.desktop,
	  kmymoney/plugins/kmymoneyplugin.desktop,
	  kmymoney/plugins/ofximport/kmm_ofximport.desktop,
	  kmymoney/plugins/printcheck/kcm_kmm_printcheck.desktop,
	  kmymoney/plugins/printcheck/kmm_printcheck.desktop,
	  kmymoney/plugins/reconciliationreport/kmm_reconciliationreport.desktop:
	  SVN_SILENT made messages (.desktop file)

2011-10-22 07:55  conet

	* kmymoney/views/kpayeesview.cpp, kmymoney/views/kpayeesview.h: Fix
	  to make changes to payee data persistent upon selection of
	  another payee
	  
	  Backported to stable branch.
	  
	  BUG: 283205

2011-10-22 07:57  conet

	* kmymoney/dialogs/kequitypriceupdatedlg.cpp,
	  kmymoney/widgets/register.cpp: Marked strings so that they are
	  picked up for translation
	  
	  Backported to stable branch.
	  
	  BUG: 283558

2011-10-22 08:04  conet

	* kmymoney/widgets/register.cpp: Fix page up/down cursor movement.
	  
	  Backported to stable branch.
	  
	  BUG: 283799

2011-10-22 08:15  conet

	* kmymoney/views/kpayeesview.cpp: Use price information to
	  calculate the balance over all transactions shown for a payee in
	  the payees view.
	  
	  Backported to stable branch.
	  
	  BUG: 284122

2011-10-22 08:20  conet

	* kmymoney/converter/webpricequote.cpp: Allow dots as thousand
	  separators (as per mail from Jan Ritzerfeld dated Fri, 29 Apr
	  2011 18:12:15 +0200 on kmymoney2-user@lists.sourceforge.net)
	  
	  Backported to stable branch.

2011-10-22 11:08  conet

	* kmymoney/views/kbudgetview.cpp: Make the budgets view current
	  splitter size persistent.
	  
	  Backported to the stable branch.

2011-10-22 20:22  lueck

	* doc/details-accounts.docbook, doc/details-categories.docbook,
	  doc/faq.docbook, doc/firsttime.docbook: backport doc fixes to
	  4.6.1
	  CCMAIL:kde-i18n-doc@kde.org

2011-10-23 19:58  conet

	* kmymoney/widgets/kmymoneyselector.cpp: Reset the focused widget
	  or else we'll have a crash later
	  caused by an already free'd focus widget in the input context
	  
	  Backported to the stable branch.
	  
	  BUG: 283538

2011-10-28 18:45  tbaumgart

	* kmymoney/mymoney/storage/CMakeLists.txt: Fix link order and
	  spelling of SQL library
	  
	  Backported from devel branch

2011-10-29 15:37  tbaumgart

	* kmymoney/kmymoney.cpp: Avoid problem with unknown file protocol
	  during 'Save as'

2011-10-29 16:35  tbaumgart

	* astyle-kmymoney.sh: Add usage of normalize as in trunk

2011-10-29 16:37  tbaumgart

	* kmymoney/converter/mymoneyqifreader.cpp,
	  kmymoney/converter/webpricequote.cpp,
	  kmymoney/dialogs/investtransactioneditor.cpp,
	  kmymoney/dialogs/kchooseimportexportdlg.cpp,
	  kmymoney/dialogs/kcurrencycalculator.cpp,
	  kmymoney/dialogs/kcurrencyeditdlg.cpp,
	  kmymoney/dialogs/keditscheduledlg.cpp,
	  kmymoney/dialogs/kenterscheduledlg.cpp,
	  kmymoney/dialogs/kequitypriceupdatedlg.cpp,
	  kmymoney/dialogs/kexportdlg.cpp,
	  kmymoney/dialogs/kfindtransactiondlg.cpp,
	  kmymoney/dialogs/kgpgkeyselectiondlg.cpp,
	  kmymoney/dialogs/kimportdlg.cpp,
	  kmymoney/dialogs/kmymoneypricedlg.cpp,
	  kmymoney/dialogs/kmymoneysplittable.cpp,
	  kmymoney/dialogs/knewaccountdlg.cpp,
	  kmymoney/dialogs/knewbankdlg.cpp,
	  kmymoney/dialogs/knewequityentrydlg.cpp,
	  kmymoney/dialogs/kselectdatabasedlg.cpp,
	  kmymoney/dialogs/kselecttransactionsdlg.cpp,
	  kmymoney/dialogs/ksplittransactiondlg.cpp,
	  kmymoney/dialogs/mymoneyqifprofileeditor.cpp,
	  kmymoney/dialogs/settings/ksettingsgeneral.cpp,
	  kmymoney/dialogs/settings/ksettingsgpg.cpp,
	  kmymoney/dialogs/settings/ksettingshome.cpp,
	  kmymoney/dialogs/settings/ksettingsonlinequotes.cpp,
	  kmymoney/dialogs/settings/ksettingsregister.cpp,
	  kmymoney/dialogs/settings/ksettingsreports.cpp,
	  kmymoney/dialogs/settings/ksettingsschedules.cpp,
	  kmymoney/dialogs/transactioneditor.cpp, kmymoney/kmymoney.cpp,
	  kmymoney/mymoney/mymoneyfile.cpp,
	  kmymoney/mymoney/mymoneyfiletest.cpp,
	  kmymoney/plugins/csvimport/investprocessing.cpp,
	  kmymoney/plugins/csvimport/redefinedlg.cpp,
	  kmymoney/plugins/interfaces/kmmviewinterface.cpp,
	  kmymoney/plugins/ofximport/dialogs/kofxdirectconnectdlg.cpp,
	  kmymoney/plugins/ofximport/dialogs/konlinebankingsetupwizard.cpp,
	  kmymoney/plugins/ofximport/ofximporterplugin.cpp,
	  kmymoney/plugins/ofximport/ofxpartner.cpp,
	  kmymoney/plugins/pluginloader.cpp,
	  kmymoney/plugins/printcheck/kcm_printcheck.cpp,
	  kmymoney/plugins/printcheck/printcheck.cpp,
	  kmymoney/plugins/reconciliationreport/reconciliationreport.cpp,
	  kmymoney/views/kaccountsview.cpp, kmymoney/views/kbudgetview.cpp,
	  kmymoney/views/kcategoriesview.cpp,
	  kmymoney/views/kgloballedgerview.cpp,
	  kmymoney/views/khomeview.cpp,
	  kmymoney/views/kinstitutionsview.cpp,
	  kmymoney/views/kinvestmentview.cpp,
	  kmymoney/views/kmymoneyview.cpp, kmymoney/views/kpayeesview.cpp,
	  kmymoney/views/kreportsview.cpp,
	  kmymoney/views/kscheduledview.cpp,
	  kmymoney/widgets/kbudgetvalues.cpp,
	  kmymoney/widgets/kguiutils.cpp,
	  kmymoney/widgets/kmymoneyaccounttreeview.cpp,
	  kmymoney/widgets/kmymoneycalendar.cpp,
	  kmymoney/widgets/kmymoneycategory.cpp,
	  kmymoney/widgets/kmymoneycombo.cpp,
	  kmymoney/widgets/kmymoneycompletion.cpp,
	  kmymoney/widgets/kmymoneydateinput.cpp,
	  kmymoney/widgets/kmymoneyedit.cpp,
	  kmymoney/widgets/kmymoneymvccombo.cpp,
	  kmymoney/widgets/kmymoneyscheduledcalendar.cpp,
	  kmymoney/widgets/kmymoneyscheduleddatetbl.cpp,
	  kmymoney/widgets/kmymoneyselector.cpp,
	  kmymoney/widgets/register.cpp,
	  kmymoney/widgets/registersearchline.cpp,
	  kmymoney/widgets/transactionsortoptionimpl.cpp,
	  kmymoney/wizards/endingbalancedlg/checkingstatementinfowizardpage.cpp,
	  kmymoney/wizards/endingbalancedlg/interestchargecheckingswizardpage.cpp,
	  kmymoney/wizards/endingbalancedlg/kendingbalancedlg.cpp,
	  kmymoney/wizards/newaccountwizard/knewaccountwizard.cpp,
	  kmymoney/wizards/newinvestmentwizard/kinvestmentdetailswizardpage.cpp,
	  kmymoney/wizards/newinvestmentwizard/kinvestmenttypewizardpage.cpp,
	  kmymoney/wizards/newinvestmentwizard/knewinvestmentwizard.cpp,
	  kmymoney/wizards/newinvestmentwizard/konlineupdatewizardpage.cpp,
	  kmymoney/wizards/newloanwizard/durationwizardpage.cpp,
	  kmymoney/wizards/newloanwizard/effectivedatewizardpage.cpp,
	  kmymoney/wizards/newloanwizard/firstpaymentwizardpage.cpp,
	  kmymoney/wizards/newloanwizard/interesteditwizardpage.cpp,
	  kmymoney/wizards/newloanwizard/knewloanwizard.cpp,
	  kmymoney/wizards/newloanwizard/loanamountwizardpage.cpp,
	  kmymoney/wizards/newloanwizard/namewizardpage.cpp,
	  kmymoney/wizards/newloanwizard/paymenteditwizardpage.cpp,
	  kmymoney/wizards/newloanwizard/schedulewizardpage.cpp: Ran astyle
	  (first time with normalize)

2011-10-30 14:33  tbaumgart

	* kmymoney/kmymoney.cpp: BUG: 285310
	  
	  Make sure not to try to delete the same transaction twice in case
	  of multiple selected transactions.
	  Don't allow to delete the empty transaction at the end of the
	  ledger.
	  
	  Backported to stable branch

