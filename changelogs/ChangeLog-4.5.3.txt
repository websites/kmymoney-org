2010-12-24 10:38  tbaumgart

	* CMakeLists.txt: setup default CMAKE_INSTALL_PREFIX before calling
	  FIND_PACKAGE(KDE4)
	  
	  Backported to stable branch
	  
	  BUG: 261132

2011-01-01 20:02  tbaumgart

	* kmymoney/converter/webpricequote.cpp: Updated regexp to extract
	  prices from "Gielda Papierow Wartosciowych"
	  
	  Backport to stable branch.
	  
	  BUG: 261803

2011-01-17 18:42  tbaumgart

	* kmymoney/dialogs/kmymoneysplittable.cpp,
	  kmymoney/dialogs/kmymoneysplittable.h,
	  kmymoney/widgets/kmymoneylineedit.cpp,
	  kmymoney/widgets/kmymoneylineedit.h: Don't select the character
	  that starts the split editor
	  
	  Backported to stable branch.
	  
	  BUG: 263319

2011-01-19 19:39  tbaumgart

	* kmymoney/templates/de_CH/Makefile.am: Removed file from ancient
	  build system

2011-01-21 07:16  tbaumgart

	* cmake/modules/FindAqBanking.cmake,
	  cmake/modules/FindGwenhywfar.cmake,
	  kmymoney/plugins/kbanking/CMakeLists.txt,
	  kmymoney/plugins/kbanking/banking.cpp,
	  kmymoney/plugins/kbanking/banking.hpp,
	  kmymoney/plugins/kbanking/dialogs/CMakeLists.txt,
	  kmymoney/plugins/kbanking/dialogs/kbaccountsettings.cpp,
	  kmymoney/plugins/kbanking/dialogs/kbaccountsettings.h,
	  kmymoney/plugins/kbanking/dialogs/kbaccountsettings.ui,
	  kmymoney/plugins/kbanking/dialogs/kbmapaccount.cpp,
	  kmymoney/plugins/kbanking/dialogs/kbmapaccount.h,
	  kmymoney/plugins/kbanking/dialogs/kbmapaccount.ui,
	  kmymoney/plugins/kbanking/dialogs/kbpickstartdate.cpp,
	  kmymoney/plugins/kbanking/dialogs/kbpickstartdate.h,
	  kmymoney/plugins/kbanking/dialogs/kbpickstartdate.ui,
	  kmymoney/plugins/kbanking/dialogs/kbsettings.cpp,
	  kmymoney/plugins/kbanking/dialogs/kbsettings.h,
	  kmymoney/plugins/kbanking/kbanking.cpp,
	  kmymoney/plugins/kbanking/kbanking.h,
	  kmymoney/plugins/kbanking/kmm_kbanking.desktop,
	  kmymoney/plugins/kbanking/mymoneybanking.cpp,
	  kmymoney/plugins/kbanking/mymoneybanking.h,
	  kmymoney/plugins/kbanking/views/kbjobview.cpp,
	  kmymoney/plugins/kbanking/views/kbjobview.h,
	  kmymoney/plugins/kbanking/views/kbjobview.ui,
	  kmymoney/plugins/kbanking/widgets/CMakeLists.txt,
	  kmymoney/plugins/kbanking/widgets/kbaccountlist.cpp,
	  kmymoney/plugins/kbanking/widgets/kbaccountlist.h: Switch to
	  usage of AqBanking5

2011-01-21 10:44  tbaumgart

	* libkgpgfile/kgpgfile.cpp: Backported fix to KGPGFile to use
	  KSaveFile instead of a simple QFile when writing

2011-01-21 12:08  tbaumgart

	* kmymoney/plugins/ofximport/ofximporterplugin.cpp,
	  kmymoney/views/kbudgetview.cpp,
	  kmymoney/widgets/kmymoneylineedit.cpp,
	  kmymoney/widgets/register.cpp, libkgpgfile/kgpgfile.cpp:
	  SVN_SILENT ran astyle

2011-01-21 21:24  tbaumgart

	* kmymoney/plugins/kbanking/dialogs/kbmapaccount.ui,
	  kmymoney/plugins/kbanking/dialogs/kbpickstartdate.ui: Fix problem
	  with connections in KBanking dialogs. Backport to stable branch.

2011-01-23 07:51  conet

	* cmake/modules/FindGwenhywfar.cmake,
	  kmymoney/plugins/kbanking/CMakeLists.txt: Changed KBanking link
	  dependencies order so that the plugin will link on mingw also
	  (where library order seems to matter). Backported to the stable
	  branch.

2011-02-05 14:54  tbaumgart

	* kmymoney/converter/convertertest.cpp,
	  kmymoney/converter/mymoneygncreader.cpp,
	  kmymoney/dialogs/investtransactioneditor.cpp,
	  kmymoney/dialogs/knewaccountdlg.cpp,
	  kmymoney/dialogs/ksplittransactiondlg.cpp,
	  kmymoney/dialogs/transactioneditor.cpp, kmymoney/kmymoney.cpp,
	  kmymoney/models/accountsmodel.cpp,
	  kmymoney/mymoney/mymoneyfiletest.cpp,
	  kmymoney/mymoney/mymoneyforecast.cpp,
	  kmymoney/mymoney/mymoneyforecasttest.cpp,
	  kmymoney/mymoney/mymoneymoney.cpp,
	  kmymoney/mymoney/mymoneymoney.h,
	  kmymoney/mymoney/mymoneymoneytest.cpp,
	  kmymoney/mymoney/mymoneysplittest.cpp,
	  kmymoney/mymoney/mymoneytransaction.cpp,
	  kmymoney/mymoney/mymoneytransactiontest.cpp,
	  kmymoney/mymoney/storage/mymoneydatabasemgr.cpp,
	  kmymoney/mymoney/storage/mymoneydatabasemgrtest.cpp,
	  kmymoney/mymoney/storage/mymoneyseqaccessmgr.cpp,
	  kmymoney/mymoney/storage/mymoneyseqaccessmgrtest.cpp,
	  kmymoney/mymoney/storage/mymoneystorageanon.cpp,
	  kmymoney/reports/pivotgridtest.cpp,
	  kmymoney/reports/pivottabletest.cpp,
	  kmymoney/reports/querytabletest.cpp,
	  kmymoney/reports/reportstestcommon.cpp,
	  kmymoney/views/kbudgetview.cpp, kmymoney/views/kpayeesview.cpp,
	  kmymoney/wizards/newaccountwizard/knewaccountwizard.cpp,
	  kmymoney/wizards/newloanwizard/additionalfeeswizardpage.cpp,
	  kmymoney/wizards/newloanwizard/keditloanwizard.cpp,
	  kmymoney/wizards/newloanwizard/knewloanwizard.cpp: Fixed a bunch
	  of MyMoneyMoney usage issues as preparation for the libalkimia
	  integration
	  
	  Backported from trunk

2011-02-05 15:21  tbaumgart

	* kmymoney/mymoney/CMakeLists.txt,
	  kmymoney/mymoney/mymoneybalancecache.cpp,
	  kmymoney/mymoney/mymoneybalancecache.h,
	  kmymoney/mymoney/mymoneybalancecachetest.cpp,
	  kmymoney/mymoney/mymoneybalancecachetest.h,
	  kmymoney/mymoney/mymoneyfile.cpp, kmymoney/mymoney/mymoneyfile.h,
	  kmymoney/mymoney/storage/imymoneystorage.h,
	  kmymoney/mymoney/storage/mymoneyseqaccessmgr.cpp,
	  kmymoney/mymoney/storage/mymoneyseqaccessmgr.h,
	  kmymoney/mymoney/storage/mymoneyseqaccessmgrtest.cpp: Implement
	  balance cache at MyMoneyFile layer.
	  
	  Backported from trunk

2011-02-05 15:46  tbaumgart

	* kmymoney/models/accountsmodel.cpp,
	  kmymoney/mymoney/storage/mymoneyseqaccessmgr.cpp,
	  kmymoney/mymoney/storage/mymoneyseqaccessmgr.h,
	  kmymoney/views/khomeview.cpp: Improve performance with investment
	  account balance calculation
	  
	  Backported from trunk

2011-02-10 14:30  tbaumgart

	* kmymoney/mymoney/mymoneyaccount.cpp,
	  kmymoney/mymoney/mymoneyinstitution.cpp,
	  kmymoney/views/kforecastview.cpp,
	  kmymoney/views/kreportsview.cpp: Added more improvements from
	  trunk version (e.g. pixmap cache)

