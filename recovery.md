---
title: "The KMyMoney recovery key"
layout: page
---

# The KMyMoney recovery key

KMyMoney has the ability to encrypt your data using GPG.
Besides using your own key for encryption, KMyMoney supports the
ability to encrypt your data with a recover key only available to a
small set of core developers.

This key will have no effect if you always have access to
your own key, but if you lose your key or the passphrase for
it, the KMyMoney recover key comes in handy as now the core
developers can help you to recover your financial data. The only
thing you have to do upfront is to encrypt your data with the
recover key. If you don't do that, There is nothing we can do to help you.

**Please read the details about the different key versions below.**

## Was it useful to KMyMoney users in the past?

**For the second time I received an email with a request for help to decrypt a
KMyMoney data encrypted with GPG where the user has lost his/her
GPG-Key(ring). Unfortunately, in both cases KMyMoney's recover key feature had not been
used and I could not recover the data.** If I had the ability to help in
these cases (without the recover key) it would mean that I can successfully
break any GPG encryption. Trust me, this is not the case.

**The *only* thing that allows me to help a user in this situation is the
KMyMoney recover key.** Using this feature, you encrypt your data with your
key ***and*** the recover key. In case you lose yours, you can send me the
file and I can use the secret part of the recover key pair to decrypt it.
I will ask you some questions about the contents of the file to make sure
that the person who sends me the file is the legal owner of the file. You
will also have to provide me with your new (public) key, which I will use
to encrypt the file to send it back to you. This way, your data is never
travelling over the internet in readable form.

BTW: I encrypt my data using my own key (the same I use to sign all my
e-mails) and the recover key just for this purpose. I have stored a
printed copy of the recover key as well as a CD containing it in a safe at
my bank. This way, I can always reconstruct it and it is not getting lost.

**Now, don't ever tell me you have not used the recover key feature... you
have been warned.**

## Different key versions

Key versions: KMyMoney versions up to version 5.1.3 use a key that was created
in 2005. The length of this key (1024 bits) is meanwhile considered unsafe.

Therefore, newer KMyMoney versions will use a new key with a larger
key size (4096 bits).

Import the recover key for your version of KMyMoney into your
GPG keyring before you can use it with KMyMoney. Select encryption
against this key in the Security settings dialog. Save your data. That's it.
BTW: both keys can co-exist in your keyring without problems.

## Key for KMyMoney versions > 5.1.3 aka *master*

This key be found on a [public key server](https://keys.openpgp.org/vks/v1/by-fingerprint/A6429AEB9EFFB2F9C434FC0221F247011D0BADF5).

Fingerprint: `A642 9AEB 9EFF B2F9 C434 FC02 21F2 4701 1D0B ADF5`

    -----BEGIN PGP PUBLIC KEY BLOCK-----
    Comment: Recover key for KMyMoney version > 5.1.3

    mQINBGajQZQBEADgLwuwdjTi+2djuTao8EtsP7S2QtxY5clk7N776J9txBeO5U2c
    AGTNugNpZToM3bVAOlf+uVb7s7Japh0i8R+CndEXf1ylQXO5h9oCyflUsILghntF
    qo4RdsHBmgL3BAthIRlwu4gAZ6vwRbSvUC0LQ+NQTiqEOxHAXFhcWbGrtTj9uRO3
    /WpB/N2rUPp+GlNv9bkogo5F3aideCd6kSM+2C7GPwQ6Bf3NomGkKjKo60gxte+J
    TWn+lwi+3HPr26c7g2byQE64wVOmVlowUT9qj8EuYDJNAgGgz1izG10Ot/jlVuBQ
    Q/izk6FEPlnkpfL8CXdTEpBFtrfH7AV9YaM2/jJxMp2oUQK2ur663UjcHQDfTkUu
    OwND958om5Cej8F87j50g0k+5Fd0zlMYf23J4Mkfp2v8C0wP89r8K/McMswodV/v
    BtMPaFi39ChkgczuiGIhvfIB14eeLvRc8q5H5mM/M49KVRtOaL4/pzid7GjXgT4h
    ELvAc/oV35xQY+5rVrkZAHMqHFW+DpTJxMk4Mg54svyD1Odln6W50EajETK+6o2O
    ULPHHpoVOptCgejXs/QScDoK064GexKlKEWOisZpy7u23LHtFV3R8adva80qOmut
    YFR/FKYzfB3iO99xyO+oGMyCUyNArlJioTZtNG28um5MyIoY24F0bdLwWQARAQAB
    tDdLTXlNb25leSBlbWVyZ2VuY3kgZGF0YSByZWNvdmVyeSA8cmVjb3ZlckBrbXlt
    b25leS5vcmc+iQJUBBMBCgA+FiEEpkKa657/svnENPwCIfJHAR0LrfUFAmajQZQC
    GwMFCQOpjIwFCwkIBwMFFQoJCAsFFgIDAQACHgECF4AACgkQIfJHAR0LrfVq6w/+
    MufsPJwx+l7CGdzy6ctc1ptPNwT0tSRXstc3H27jDw49ZKhuxsqWl2ONvlJRcw8G
    P4yZ/e/1cKJ4Oia7ZXZLqy2wy8U7djxfU2FyrwE/9iWgEiEUXZOYI+wUUE39icGa
    RyMrtR5KTP0V2D/9BZoKzkl1YGzrqK8WFzDgC9fmVaCvkYHyEgRCKKunuO2/I0nN
    hfjfHLT+a4wyVaP+YGkqB4UcaqZ3KZM2EJvExNCbuozZh0KmKfVVzCMubTvuyYyN
    0IK4/sdrDfUrqJlRjk2LQn0TCkHxE3+cUgZp9t8JsmFsZvULSG4YegE02Q34bvI4
    A4oXsJfZrA5qadAjv2fP/5xJDiiyvghh/7vtFXcUphrwoXaczucg2azCNRkN06qT
    vZ6XyN0oVxP0/QVrLkFT78+jxq74lfYQo9waeyaYcDFoih7SwZGaDChGDdhREmfw
    cwj6gK/74ctq6oEVEySj/QjtMK9Cv7ulIDdjX39VCC5aw2/QswbZ48gEhqi/ksR1
    xn0nvfclRhElqjoUEIS5rEdIo0658TrSGDXZZOJqBLXozlQCaXclsL9X2epPW/kc
    AlrXyGMEpYFGw/ZnTnICM9PFffDas9uVK8v0rEiV+qOuTTBUBIHuknFG6d1sq7m0
    JIT+DprCTY4QDmfJwGPpdZ0IT/H11lrJRsBWQqGDAlSIXQQTEQoAHRYhBC4bGbve
    SprTtRvEGFmw+CbSsIRABQJmpJ6CAAoJEFmw+CbSsIRAxMkAn2RnQrGX4UsleenV
    gNPuPl3Bj8mGAJ9f9D41ap5tcVGBinR6q46UauVgD4kCMwQQAQgAHRYhBNaadFpV
    Mx9E9ATYJY1N4GKqLrAcBQJmpJ9LAAoJEI1N4GKqLrAc65QP+wVCinwu2pGzl0bw
    kLfNzXEv4jyzmiteVcz/3T5Y0o5/0SI6JdYTxULyEdwyVrgov7TVOxTr5+OHJVY3
    99dsFN5idBFuY/yXg7MNTvGx+/lVW0WCwNdWlPLz+PYf5HZ+c/SwsVi2JIpUKXav
    ByMCtoBIrOMaPHiAeyO2/vonqnC8VFnEXaV0teXdE7SajTaCWScMEddEhUVEj6md
    3pUmeCmHNS7Jtze/nOIIthGw6CxXQGS3IKLPQrCQNeRyyIbV/GIlLLd1T7CoxX9d
    wZNt79zDyionJsD188j3eb88akOmnVYCxbecK/p3ee+guV1fRgPBJtOXl6NNlS25
    b33QF+44ccziMqIhX+rN8UpcDb/xsCLWk+eBJW5kZN8/HDukij5clgHzIKs2fwpV
    hTmBNlzGFgD1qA/xN6DoLAZmsLvcsyydRdanoSgPUv3FmwpdnPSlGQz2s/iKb939
    KOUEebSguUePR/oKsWyK3dqy6GFA4sJxkSpca8mPc23sTp+qfu2AtwqxOnwEpXjH
    dowsYOTStnHBCRFhJh02T8XFtgGNrCg122GYfe2+/vrK2rCaQ6vEz6FCKN7vC2U6
    XvQk6/QpxS93aMCdLn3omotmI9Ejb+DphCGGZnZ86M/RQHB1jd7CknyV9IGkxX5x
    PbLcR2erPihvMMzSwvEQC5BpHujNuQINBGajQZQBEADP7aDZO5wLDAKL0qc/z8yE
    u8kQbUR6QZCGN3Qcf43Cnxl0noETX3C6V6gDrKYeWjSQW+8F5FS72rvkRz+lZyWf
    7tbsZ1qW4mXfcl2CjQtxgCAXI52yeJERCFwmYwZpUPw0/Z1HenP8Ksgg2h4fwsii
    ypS2OMrCjYzHhEzk2/V6YRjkTTFygSEGKRlgjlg7F5SJe5Fk2Ld1kSlo20WOAInW
    KaViYImC0rzPg4XyL7QcqMKlT5ClNasnF/bj/srrDbWgAbdFGjbliPMK4vvxThk7
    4ZopH1hpqbwvbY49wOsubiQ3DtG812ovXf/PHIQuMi63TsXk2mO8Uxz2ba3wmM8A
    QHMX6WxRClk71/8cgVb+rca+1qCNMb1MR6BmjsGyva56rvsE6RSv7Rn5ULhCe2Es
    /gnm9/kjpkOh+yIKRAaEdi1YUfZ9Ar5Ss/qvUcVcpYBOXqkNQQJ7D/JBsOpoQ7Az
    8GTKuYd/GZQvXMzavaHlKoUp7r0EqNy09oatyXjCXUCIw2Ci4/VN6fd8Rtzn2DpW
    QVV+1Yv0F24A5a/TxuYeekkGW/aHJA7PRf1uUeZT8crr45XdocEPLtgoVOya/u3t
    oyaAkl3qTIRRIq5GkEJHuZMPgJFS2zKM0Vjmzvd5zIA1fte8yr4kQvFQl6AMks6V
    Fj7JwkDaUkIsYsjpkRkfRwARAQABiQI8BBgBCgAmFiEEpkKa657/svnENPwCIfJH
    AR0LrfUFAmajQZQCGwwFCQOpjIwACgkQIfJHAR0LrfXIaw/+PLPMzFZFDkvHQDAj
    JfuDyED26bc0H+/OMjLFqILhh1AQjjIS6KF67gbFwAHnoHvfKxpUH8a7cTxA0wV+
    3hPmRcfwDuuJwrQ9l8e49k8+2g2z0Y5xrP64cvTqcbMJc6+f6oyXBc1P7c49tTH/
    7z0SzR1Bns+C5cvii5nB4tErZ3nTjWqFHPsPWFPhbW/RCfxeEWiInN51pWlE+DHp
    n2K+EYn98agEFgPyzBpye5pVPYBTQe8EMOJASo+ldbTZfZEboZPw5IrGTIN0I5SB
    1k8NRHPzmP/jIqMXegB0U05JXlwAlTLshY2ilg5pDvfkvMfYAAqb0GBUFtQ9VFl+
    JuykSc+/NpsYnTkROGAa1AsX8WOIVdwJr1UJnr4rgpZV2SZYOrR/FJ3P+FYspzYV
    VcsOx9Dvy6YPh8I5U/pTMCPiLdx6TD1fv7xtGPAuUCnTDVzeT1KdHRmqwib0tzoH
    3XP2NZDXbmB/L5mFHRvn/3SF1O3ytrH1fkba04yy7n76pYynm9+iqt+c3J7rWvwY
    1dFJ3Su5h/26nB3bpC3o1Ruz2Bdqk40qT8EIXY7R4uV3AzvTc5wlrxdzdwHL3Ara
    RPqvJwuyNXXYbGl4cW7AQBs9UXuR/0bHqXqF98PE3cjcDUZII34cMY08hJVHa+7Z
    sF1wokYJHE1Z0m2Qa8vjWO1BfEU=
    =9bfv
    -----END PGP PUBLIC KEY BLOCK-----

The ew key is signed with the old
recover key and my own personal key (the numbers in parenthesis are the
fingerprints of these keys):

Old KMyMoney recover key: [2E1B 19BB DE4A 9AD3 B51B C418 59B0 F826 D2B0 8440](https://keys.openpgp.org/vks/v1/by-fingerprint/2E1B19BBDE4A9AD3B51BC41859B0F826D2B08440)

[My](/team/) private key: [D69A 745A 5533 1F44 F404 D825 8D4D E062 AA2E B01C](https://keys.openpgp.org/vks/v1/by-fingerprint/D69A745A55331F44F404D8258D4DE062AA2EB01C)


## Key for KMyMoney versions <= 5.1.3

**Please keep in mind that the usage of this key will expire on
January 7th, 2025 and will not be extended.**

This key be found on a [public key server](https://keys.openpgp.org/vks/v1/by-fingerprint/2E1B19BBDE4A9AD3B51BC41859B0F826D2B08440).

Fingerprint: `2E1B 19BB DE4A 9AD3 B51B C418 59B0 F826 D2B0 8440`

    -----BEGIN PGP PUBLIC KEY BLOCK-----
    Comment: Recover key for KMyMoney version <= 5.1.3

    mQGiBEHZVEsRBADT6dNItEHaZjpa9OOZ24mbemfwrVbCzH1EuV6mE+7ID8yH37Va
    B5chb3qg44nAc4GY8F9Y/mggTQsG/Lvp6fIj1ADFHygHxeyzr7rh323TLZqaDbkc
    rTQX0mzOK1I1Crp8AaRu3+Rs6J6WMC94QcMBNYyRppg0AN4Hxi3aPdXggwCgjd7a
    wlFkKK4tXubzoXDOH06HDT0EAITZQdcZvlRMXx3XI4B/Qll3hgN55OVVhrV+xY34
    E6XBFY1aaqLbussFCZOOcjUELymcD/eK/qt+mYLdkvmA0IYTk+W3E2NXvewphQuJ
    X9h7kl5/Cvtm/0HLBIeRLEC4TVEruqc2ml7wni24HOC/Ez+vU4Zzk4HZVyS3akRl
    xFnFBAC4fNyrupnWlWPd2UJNyggpd40rUYzy/yK+4W0b/3coHP1hOSFf1RBhhgC3
    D/W8QEAWnCih15IXNNI4om86Bz+p9cnoDVELIjeq5BvafpPGGkz84Z7YU1+q9Lhc
    oXabxZdzI/dC6EjOLThqQL0J/xlRaDyfdWXlHA9Kpf58Qz8w17RJS015TW9uZXkg
    ZW1lcmdlbmN5IGRhdGEgcmVjb3ZlcnkgPGtteW1vbmV5LXJlY292ZXJAdXNlcnMu
    c291cmNlZm9yZ2UubmV0PohkBBMRAgAkBQJB2VRLAhsDBQkJZgGABgsJCAcDAgMV
    AgMDFgIBAh4BAheAAAoJEFmw+CbSsIRATLUAoISpsD7HywfBRxxtTGcoCddFvyot
    AJ9iFoySYwXVz6/Q8W9yqxEGCuhaBYhMBBMRAgAMBQJB2V9SBYMJZfZ5AAoJEJxZ
    20C3XdO6Ka4AoKV1dfZLAfs6IMjBdLlqK4yXwboyAJ4pzfrXOC+5EWnvLWr1jjbU
    ceEkfYhGBBARAgAGBQJGin/0AAoJEDfVOyfVAUYWzbkAoLLFoH5qpTqX4DHqPLhA
    DZhFHRa8AKDjCDTqbEUfMxHwvDol70rB616DqIhGBBARAgAGBQJHdE5/AAoJECm6
    cuUpyqIU1ZwAn0dsvIE+qLRYmTJT9vNNMS5oKviaAKC0ljkJDgf63LwYT8li+C8a
    x1wYGIh7BBMRCgA7AhsDBgsJCAcDAgMVAgMDFgIBAh4BAheAFiEELhsZu95KmtO1
    G8QYWbD4JtKwhEAFAmV9e7gFCSWjrW0ACgkQWbD4JtKwhEBiNQCfX+RhFeR25zFP
    UFn1v+ndudhWlwAAn2UAFVN86cSSJCrYNYQ9IEGbISReiQEcBBABAgAGBQJGfULB
    AAoJEHJCAEvq1TJwKTIH/05RnEECu63km8NZVx8H7CjVkl8jc+kg2xPfz6zdeV53
    fcdHKLrZJEbgdrCdSPg8iiMwD9dklw/o1I9USMcSypk6IUpTJoaK7NoFP+h2eoMT
    28HocWd4fhL/kS+Lz3yElD59ejLUtdmI7IoC6EcWnr4t4WoEkTtJ9BL8nDCyoeOt
    kdBQpUsuLoO5tASg9Ydu/a3sLvxueQF006x1yfkcUw+xZ/WE62xIDV01yVXLZUw3
    xYaZI2Alye0qQ4PJk6nPQGgJArACbRfssERgbOAi9KneToMQmJ7UEkWH4UZz0zo+
    P7zns9JvHQ4o6NfcQUrQDdgq8OSVjp+NCK4IiR2apkWIRgQQEQgABgUCTFQy2AAK
    CRDWp10Ysds7X33FAJ9Ip++rUruA6hPbV7TsEayh/Mg7iQCgh1QtLB7kh1TSWM/C
    KEqGOdmbRnWIZAQTEQIAJAIbAwYLCQgHAwIDFQIDAxYCAQIeAQIXgAUCS0JB+AUJ
    Es7vLQAKCRBZsPgm0rCEQMZZAJ93gkHTFoYpbx0nCc/pvXoLxLelUgCfTYrD9WUY
    6oSUihlTjEELz4DD+cOIZAQTEQIAJAIbAwYLCQgHAwIDFQIDAxYCAQIeAQIXgAUC
    VDE/kgUJHDsrRwAKCRBZsPgm0rCEQMeCAKCMBK0zaqs2L23isA9or+BwIWh1NQCe
    IWOSS+75n3moWF2eM71MVZIv1ji0N0tNeU1vbmV5IGVtZXJnZW5jeSBkYXRhIHJl
    Y292ZXJ5IDxyZWNvdmVyQGtteW1vbmV5Lm9yZz6IgAQTEQoAQAIbAwcLCQgHAwIB
    BhUIAgkKCwQWAgMBAh4BAheAFiEELhsZu95KmtO1G8QYWbD4JtKwhEAFAmV9e7gF
    CSWjrW0ACgkQWbD4JtKwhEDh7gCfTzp85/DMES097yRAqHfs6I1MtpgAn06RW0st
    aBFabV/JwGOa8DgpsAYHiEYEExECAAYFAlQxROIACgkQnFnbQLdd07olPgCgkHT6
    /bpLV3lqHy84MK/Drkk6ZlcAoNrTzg9BvRg4K+7U8GoTTPBqlJnYiGkEExECACkF
    AlQxRKECGwMFCRw7K0cHCwkIBwMCAQYVCAIJCgsEFgIDAQIeAQIXgAAKCRBZsPgm
    0rCEQPzpAJ9hDz7is4YlUlQeyR3/xwPjto19pQCdElFQhAV9xFM6gIt042chQpkB
    VcO5Ag0EQdlUVhAIANVkbMJOK9SPu/qJmU1NV+uOZni6uJUjm1nyLqkQPG0+Uv6A
    Ykcd8jjAOjoXVboHglI2l/aACGhuO/EN9IUaK9BK0C+B1XDGKzyXy+Qudepq4y1Q
    SHkaT63FMcHDtaOMff3i1qFBm4GLuQ5icllEHZjZgw8T0TUrOsgbRZsHajoqFmVB
    KoFFvABigqqksNmPU2hcnveHURHg2oNFn3hIstchZJ9ntkeaBEXp+Y+shx2qBBTA
    kWQEHlahwt0x4hFctWr0hCjVLnIcBQtGRK3jBQ413VZNGHlKBrt/436Sw8tgr3W6
    ws1ip/hzhEZpJq6JYeThKNnUScpzHRX3OuEHIp8AAwcH/Rw3xL0Y872XFxyLBAhS
    xFutftFCaVYIEMdNXv9gkp6/sz/LLWXeiRBP4E2YuPYhxXTfZgm/so/Ceyk43hky
    JqRvpHzQOwl3IuIQHplXv98JAWC7+Dtk9r9N783jlUaAoc3Z8TiKBIHJDcj3CbLm
    rMZghyXAAnsUxv2FUMzKTq61JLWO33KlFa3q6k+FTWx8xUEb9TRjoHCSQmWmqLpo
    cP0LH2eRY0CB4I0JrrZSYTZL/0YFfogFD1vM60bUDeQ3DZZPKWDTBuFiBgAbEiRc
    NbiNkj/61V3Y7Sxp4nn4TTW9qClNYjyqAqepot7cXmeazgFnP+1PQ+vIqMDnNEQv
    lxKITwQYEQIADwUCQdlUVgIbDAUJCWYBgAAKCRBZsPgm0rCEQKixAJwKc1lQN9f2
    IeWqe91qlv7NT4radACfXqgAN8VL3w2NuoCxpdQR6nWkdfOIZgQYEQoAJgIbDBYh
    BC4bGbveSprTtRvEGFmw+CbSsIRABQJlfXwGBQklo62wAAoJEFmw+CbSsIRAbngA
    n0r5NEY0UWHy5ypjS+tom0f1bO0VAJ95wySuz84rskrLPxEkS5isIr9LjA==
    =x1Zc
    -----END PGP PUBLIC KEY BLOCK-----
