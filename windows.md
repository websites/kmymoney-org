---
title: "Download and use the Windows version"
layout: page
sources:
  - name: Stable version
    pipelineTxt: download page showing the stable version
    pipelineUrl: https://cdn.kde.org/ci-builds/office/kmymoney/5.1/windows/
    imageName: kmymoney-5.1-####-windows-cl-msvc2019-x86_64.exe
    description: >
        The stable version for Windows. To download it, the following steps need to be performed:

    note:

  - name: Development version
    pipelineTxt: download page showing the development version
    pipelineUrl: https://cdn.kde.org/ci-builds/office/kmymoney/master/windows/
    imageName: kmymoney-master-####-windows-cl-msvc2019-x86_64.exe
    description: >
        For the more adventurous Windows users, the development versions might be interesting. They contain the latest development state but
        might be unstable and eat your data. Make sure to have backups of your data before you use this software.
        To download it, the following steps need to be performed:

    note:
---

<h1>How to download and use the Windows version</h1>

Page last updated: 2024-03-10

<div class="highlight">
    <p><strong>Important:</strong> The images referenced on this page are build as part of KDE's CI/CD process and are automatically generated when
    the repository receives changes. They contain a number in the build which is changed with each build. Please replace the placeholder #### with
    the actual number in the instructions below.</p>

{% for source in page.sources %}
    <h2>{{ source.name }}</h2>
    {{ source.description  | markdownify }}

    <ul>
    <li>Visit the <a href="{{ source.pipelineUrl }}" target="_blank">{{ source.pipelineTxt }}</a></li>
    <li>Click on the link for the installer named <strong>{{ source.imageName }}</strong> to start downloading the file to your local disk.</li>
    <li>Execute the command <strong>{{ source.imageName }}</strong> to start the installation process.</li>
    </ul>

    <p><em>{{ source.note | markdownify }}</em></p>
{% endfor %}
</div>
