<?php

/*
 * Copyright 2017,2019 Ralf Habacker <ralf.habacker@freenet.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) version 3, or any
 * later version accepted by the membership of KDE e.V. (or its
 * successor approved by the membership of KDE e.V.), which shall
 * act as a proxy defined in Section 6 of version 3 of the license.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

 /*
 * ofx test web service for kmymoney login test
 *
 * based on http://www.ofx.net/downloads/OFX%202.2.pdf
 */

class Debug
{
	const Display = 1;
	const RawData = 2;
	const Lines = 4;
	const KeyValues = 8;
	const Dates = 16;
	const DataArray = 32;
	const PHPVersion = 64;
	const InRange = 128;
	const Response = 256;
	const Info = 512;
}

# show errors
$debug = isset($_REQUEST['debug']) ? $_REQUEST['debug'] : 0;
if ($debug)
	error_reporting(E_ALL);
if ($debug & Debug::Display)
	ini_set('display_errors', '1');

$hasOffsetSupport = version_compare(phpversion(), '5.5.10', '>=');
/*
  use namespace for validating response against xsd file https://www.ofx.net/downloads/OFX%202.2.0%20schema.zip

  wget --post-file=ofxrequest.xml -O response.xml "https://kmymoney.org/ofxtest.php?debug=1&useNameSpace=1"
  xmllint --noout --schema "OFX 2.2.0 schema\OFX2_Protocol.xsd" response.xml
*/
$useOFXNameSpace = isset($_REQUEST['useNameSpace']);

/**
 * convert an ofx date string into a timestamp
 */
function strToDate($s)
{
	global $debug;

	$i = strpos($s, "[");
	if ($i !== false) {
		$dt = substr($s, 0, $i);
		$tzs = substr($s, $i+1, strlen($s)-$i-2);
		try {
			if (is_numeric($tzs[0]))
				$tz = new DateTimeZone('+'.$tzs);
			else
				$tz = new DateTimeZone($tzs);
		} catch (Exception $e) {
			echo '<!-- could not create time zone: '.$e->getMessage()."-->";
			error_log("'$s' could not create time zone: ".$e->getMessage());
			return 0;
		}
	} else {
		$dt = $s;
		$tz = new DateTimeZone("GMT");
	}
	if (strlen($dt) == 8)
		$d = DateTime::createFromFormat("YmdHis", $dt."000000", $tz);
	elseif (strlen($dt) == 14)
		$d = DateTime::createFromFormat("YmdHis", $dt, $tz);
	elseif (strlen($dt) == 18)
		$d = DateTime::createFromFormat("YmdHis#u", $dt, $tz);
	else
		$d = 0;
	if (!$d) {
		error_log("'$s' invalid date format");
		return 0;
	}
	$t = $d->getTimestamp();
	if ($debug & Debug::Dates)
		error_log("$dt '".$tz->getName()."' -> $t");
	return $t;
}

function _checkDate($mode, $s="")
{
	if ($mode == 0) {
		echo "<table>\n"
		."<tr><th>ofx date</th><th>timestamp</th><th>formatted output</th></tr>\n";
		return;
	}
	elseif ($mode == 1) {
		$d = strToDate($s);
		$date = new DateTime("@$d");
		$tzs = "CET";
		$date->setTimezone(new DateTimeZone($tzs));
		$s2 = $date->format("Y-m-d H:i:s.u");
		//$s2 = date("Y-m-d H:i:s.u", $d);
		echo "<tr><td>$s</td><td>$d</td><td>$s2 [$tzs]</td></tr>\n";
	}
	elseif ($mode == 2)
		echo "</table>";
}

if (isset($_REQUEST['debug']) && $_REQUEST['debug'] == 'date') {
	$data = array(
		"20181201",
		"20181201000000",
		"20181201000000.000",
		"20181201000000[GMT]",
		"20181201000000[EST]",
		"20181201000000[MAGT]",
		"20181201000000[SST]",
		"20181201000000[12]",
		"20181201000000[-11]",
		"20181201000000[+12]",
		"20181201000000[12.00]",
		"20181201000000[-11.50]",
		"20181201000000[+10.50]",
		"20181201000000[+12:MAGT]",
		"20181201000000[12:MAGT]",
		"20181201000000[-11:SST]",
		# from spec section 3.2.8.2 Date and Datetime
		"19961005132200.124[-5:EST]",
	);

	_checkDate(0);
	foreach($data as $s) {
		_checkDate(1, $s);
	}
	_checkDate(2);
	exit(1);
}

/**
 * check that datetime string $a is in range between $start and $end
 */
function isInRange($a, $start, $end)
{
	global $hasOffsetSupport;
	global $debug;

	if ($hasOffsetSupport) {
		$t = strToDate($a);
		$r = (empty($start) || $t >= strToDate($start)) && (empty($end) || $t <= strToDate($end));
	} else {
		$r = true;
	}
	if ($debug & Debug::InRange)
		error_log("isInRange($a, $start, $end) -> $r");
	return $r;
}

$templates = array(
    "INVSTMTMSGSRSV1" => array(
        "INVSTMTTRNRS" => array(
            "INVSTMTRS" => array(
                "INVTRANLIST" => array(
                    "BUYSTOCK" => "
<BUYSTOCK>
    <INVBUY>
        <INVTRAN>
            <FITID>23321</FITID>
            <DTTRADE>20050825</DTTRADE>
            <DTSETTLE>20050828</DTSETTLE>
        </INVTRAN>
        <SECID>
            <UNIQUEID>123456789</UNIQUEID>
            <UNIQUEIDTYPE>CUSIP</UNIQUEIDTYPE>
        </SECID>
        <UNITS>100</UNITS>
        <UNITPRICE>50.00</UNITPRICE>
        <COMMISSION>25.00</COMMISSION>
        <TOTAL>-5025.00</TOTAL>
        <SUBACCTSEC>CASH</SUBACCTSEC>
        <SUBACCTFUND>CASH</SUBACCTFUND>
    </INVBUY>
    <BUYTYPE>BUY</BUYTYPE>
</BUYSTOCK>
",
                    "INVBANKTRAN" => "
<INVBANKTRAN>
    <STMTTRN>
        <TRNTYPE>CREDIT</TRNTYPE>
        <DTPOSTED>20050825</DTPOSTED>
        <DTUSER>20050825</DTUSER>
        <TRNAMT>1000.00</TRNAMT>
        <FITID>12345</FITID>
        <NAME>Customer deposit</NAME>
        <MEMO>Your check #1034</MEMO>
    </STMTTRN>
    <SUBACCTFUND>CASH</SUBACCTFUND>
</INVBANKTRAN>
",
                ),
                "INVPOSLIST" => array(
                    "POSSTOCK" => "
<POSSTOCK>
    <INVPOS>
        <SECID>
            <UNIQUEID>123456789</UNIQUEID>
            <UNIQUEIDTYPE>CUSIP</UNIQUEIDTYPE>
        </SECID>
        <HELDINACCT>CASH</HELDINACCT>
        <POSTYPE>LONG</POSTYPE>
        <UNITS>200</UNITS>
        <UNITPRICE>49.50</UNITPRICE>
        <MKTVAL>9900.00</MKTVAL>
        <DTPRICEASOF>20050827010000</DTPRICEASOF>
        <MEMO>Next dividend payable Sept 1</MEMO>
    </INVPOS>
</POSSTOCK>
",
                    "POSOPT" => "
<POSOPT>
    <INVPOS>
        <SECID>
            <UNIQUEID>000342222</UNIQUEID>
            <UNIQUEIDTYPE>CUSIP</UNIQUEIDTYPE>
        </SECID>
        <HELDINACCT>CASH</HELDINACCT>
        <POSTYPE>LONG</POSTYPE>
        <UNITS>1</UNITS>
        <UNITPRICE>5</UNITPRICE>
        <MKTVAL>500</MKTVAL>
        <DTPRICEASOF>20050827010000</DTPRICEASOF>
        <MEMO> Option is in the money</MEMO>
    </INVPOS>
</POSOPT>
",
                ),
                "INVBAL" => "
<INVBAL>
    <AVAILCASH>200.00</AVAILCASH>
    <MARGINBALANCE>-50.00</MARGINBALANCE>
    <SHORTBALANCE>0</SHORTBALANCE>
    <BALLIST>
        <BAL>
        <NAME>Margin Interest Rate</NAME>
        <DESC>Current interest rate on margin balances</DESC>
        <BALTYPE>PERCENT</BALTYPE>
        <VALUE>7.85</VALUE>
        <DTASOF>20050827010000</DTASOF>
        </BAL>
    </BALLIST>
</INVBAL>
",
            ),
        ),
    ),
);

$today = date('Ymd');

$config = array(
	'OFXHEADER' => '200',
	'VERSION' => '220',
	'USERID' => 'test',
	'USERPASS' => 'test1',
	'ORG' => 'test',
	'FID' => 'test',
	'DESC' => 'Power Checking',
	'PHONE' => '0088224482',
	'BANKID' => '123456789',
	'CURDEF' => 'USD',
	'accounts' => array(
		'00001234' => array(
			'ACCTID' => '00001234',
			'ACCTTYPE' => 'CHECKING',
			# non standard field
			'MEMO' => 'account with check and atm statements',
			'BANKTRANLIST' => array(
				'20171001' => array(
					'TRNTYPE' => 'CHECK',
					'DTPOSTED' => $today,
					'TRNAMT' => -200.00,
					'FITID' => '00002',
					'NAME' => 'Test1',
					'CHECKNUM' => '1000'
				),
				'20171002' => array(
					'TRNTYPE' => 'ATM',
					'DTPOSTED' => $today,
					'DTUSER' => '20051020',
					'TRNAMT' => -300.00,
					'FITID' => '00003',
					'NAME' => 'Test2',
				),
			),
			'LEDGERBAL' => array(
				'BALAMT' => '200.29',
				'DTASOF' => $today,
			),
			'AVAILBAL' => array(
				'BALAMT' => '200.29',
				'DTASOF' => $today,
			),
		),
		'00001235' => array(
			'ACCTID' => '00001235',
			'ACCTTYPE' => 'CHECKING',
			# non standard field
			'MEMO' => 'another account with check statement',
			'BANKTRANLIST' => array(
				'20171001' => array(
					'TRNTYPE' => 'CHECK',
					'DTPOSTED' => $today,
					'TRNAMT' => 200.00,
					'FITID' => '00001',
					'NAME' => 'Test3',
				),
			),
			'LEDGERBAL' => array(
				'BALAMT' => '100.29',
				'DTASOF' => $today,
			),
			'AVAILBAL' => array(
				'BALAMT' => '100.29',
				'DTASOF' => $today,
			),
		),
		'00001236' => array(
			'ACCTID' => '00001236',
			'ACCTTYPE' => 'CHECKING',
			# non standard field
			'MEMO' => 'account for testing dates with different time zones',
			'BANKTRANLIST' => array(
				'1' => array(
					'TRNTYPE' => 'XFER',
					'DTPOSTED' => '20181201',
					'TRNAMT' => -140.00,
					'FITID' => '0001',
					'NAME' => 'Landlord-1',
					'MEMO' => 'short date - 12:00 AM (the start of the day), GMT',
				),
				'2' => array(
					'TRNTYPE' => 'XFER',
					'DTPOSTED' => '20181201000000',
					'TRNAMT' => -140.00,
					'FITID' => '0002',
					'NAME' => 'Landlord-2',
					'MEMO' => 'date with time without tz',
				),
				'3' => array(
					'TRNTYPE' => 'XFER',
					'DTPOSTED' => '20181201000000.000',
					'TRNAMT' => -140.00,
					'FITID' => '0003',
					'NAME' => 'Landlord-3',
					'MEMO' => 'date with time and msecs without tz',
				),
				'4' => array(
					'TRNTYPE' => 'XFER',
					'DTPOSTED' => '20181201000000[GMT]',
					'TRNAMT' => -140.00,
					'FITID' => '0004',
					'NAME' => 'Landlord-4',
					'MEMO' => 'date with time and default tz',
				),
				'5' => array(
					'TRNTYPE' => 'XFER',
					'DTPOSTED' => '20181201000000.000[GMT]',
					'TRNAMT' => -140.00,
					'FITID' => '0005',
					'NAME' => 'Landlord-5',
					'MEMO' => 'date with time, msecs and default tz',
				),
				'6a' => array(
					'TRNTYPE' => 'XFER',
					'DTPOSTED' => '20181201000000[-11]',
					'TRNAMT' => -140.00,
					'FITID' => '0006a',
					'NAME' => 'Landlord-6a',
					'MEMO' => 'date with time, tz offset without tz identifier (-11)',
				),
				'6b' => array(
					'TRNTYPE' => 'XFER',
					'DTPOSTED' => '20181201000000[-11:SST]',
					'TRNAMT' => -140.00,
					'FITID' => '0006b',
					'NAME' => 'Landlord-6b',
					'MEMO' => 'date with time, tz offset and identifier (-11:SST)',
				),
				'7a' => array(
					'TRNTYPE' => 'XFER',
					'DTPOSTED' => '20181201000000[12]',
					'TRNAMT' => -140.00,
					'FITID' => '0007a',
					'NAME' => 'Landlord-7a',
					'MEMO' => 'date with time, tz offset without tz identifier (12)',
				),
				'7b' => array(
					'TRNTYPE' => 'XFER',
					'DTPOSTED' => '20181201000000[12:MAGT]',
					'TRNAMT' => -140.00,
					'FITID' => '0007b',
					'NAME' => 'Landlord-7b',
					'MEMO' => 'date with time, tz offset and identifier (12:MAGT)',
				),
				'8a' => array(
					'TRNTYPE' => 'XFER',
					'DTPOSTED' => '20181201000000[+12]',
					'TRNAMT' => -140.00,
					'FITID' => '0008a',
					'NAME' => 'Landlord-8a',
					'MEMO' => 'date with time, tz offset without tz identifier (+12)',
				),
				'8b' => array(
					'TRNTYPE' => 'XFER',
					'DTPOSTED' => '20181201000000[+12:MAGT]',
					'TRNAMT' => -140.00,
					'FITID' => '0008b',
					'NAME' => 'Landlord-8b',
					'MEMO' => 'date with time, tz offset and identifier (+12:MAGT)',
				),
			),
			'LEDGERBAL' => array(
				'BALAMT' => '-980',
				'DTASOF' => $today,
			),
			'AVAILBAL' => array(
				'BALAMT' => '-980',
				'DTASOF' => $today,
			),
		),
		'00001237' => array(
			'ACCTID' => '00001237',
			'ACCTTYPE' => 'CHECKING',
			# non standard field
			'MEMO' => 'account with several check statements',
			'BANKTRANLIST' => array(
				'20171001' => array(
					'TRNTYPE' => 'CHECK',
					'DTPOSTED' => '20171001',
					'TRNAMT' => -123.00,
					'FITID' => '00001',
					'CHECKNUM' => '123',
					'NAME' => 'Test1',
				),
				'20171002' => array(
					'TRNTYPE' => 'CHECK',
					'DTPOSTED' => '20171002',
					'TRNAMT' => -123.00,
					'FITID' => '00002',
					'CHECKNUM' => '000123',
					'NAME' => 'Test2',
				),
				'20171003' => array(
					'TRNTYPE' => 'CHECK',
					'DTPOSTED' => '20171003',
					'TRNAMT' => -124.00,
					'FITID' => '00003',
					'CHECKNUM' => '124',
					'NAME' => 'Test3',
				),
				'20171004' => array(
					'TRNTYPE' => 'CHECK',
					'DTPOSTED' => '20171004',
					'TRNAMT' => -125.00,
					'FITID' => '00004',
					'CHECKNUM' => '125',
					'NAME' => 'Test4',
				),
				'20171005' => array(
					'TRNTYPE' => 'CHECK',
					'DTPOSTED' => '20171005',
					'TRNAMT' => -126.00,
					'FITID' => '00005',
					'CHECKNUM' => '126',
					'NAME' => 'Test5',
				),
			),
			'LEDGERBAL' => array(
				'BALAMT' => '-621.29',
				'DTASOF' => $today,
			),
			'AVAILBAL' => array(
				'BALAMT' => '-621.29',
				'DTASOF' => $today,
			),
		),
		'00001238' => array(
			'ACCTID' => '00001238',
			'ACCTTYPE' => 'OTHER',
			'DTASOF' => $today,
			# non standard field
			'MEMO' => 'investment account for buying shares',
			'INVTRANLIST' => array(
				'1' => array(
					'BUYSTOCK' => array(
						'INVBUY' => array(
							'INVTRAN' => array(
								'FITID' => '00001',
								'DTTRADE' => '20220825',
								'DTSETTLE' => '20220828',
							),
							'SECID' => array(
								'UNIQUEID' => '123456789',
								'UNIQUEIDTYPE' => 'CUSIP',
							),
							'UNITS' => 100,
							'UNITPRICE' => 50.00,
							'COMMISSION' => 25.00,
							'TOTAL' => -5025.00,
							'SUBACCTSEC' => 'CASH',
							'SUBACCTFUND' => 'CASH',
						),
						'BUYTYPE' => 'BUY',
					),
					'INVBANKTRAN' => array(
						'STMTTRN' => array(
							'TRNTYPE' => 'CREDIT',
							'DTPOSTED' => '20220825',
							'DTUSER' => '20220825',
							'TRNAMT' => '1000.00',
							'FITID' => '12345',
							'NAME' => 'Customer deposit',
							'MEMO' => 'Your check #1034',
						),
						'SUBACCTFUND' => 'CASH',
					),
				),
			),
		),
		'bug290051' => array(
			'ACCTID' => 'bug290051',
			'ACCTTYPE' => 'CHECKING',
			# non standard field
			'MEMO' => 'account for testing bug 290051 (it contains 4 checks with different check numbers, but same amount',
			'BANKTRANLIST' => array(
				'1' => array(
					'TRNTYPE' => 'CHECK',
					'DTPOSTED' => '20171001',
					'TRNAMT' => -123.00,
					'FITID' => '00001',
					'CHECKNUM' => '1',
					'NAME' => 'Test1',
				),
				'2' => array(
					'TRNTYPE' => 'CHECK',
					'DTPOSTED' => '20171001',
					'TRNAMT' => -123.00,
					'FITID' => '00002',
					'CHECKNUM' => '2',
					'NAME' => 'Test2',
				),
				'3' => array(
					'TRNTYPE' => 'CHECK',
					'DTPOSTED' => '20171001',
					'TRNAMT' => -123.00,
					'FITID' => '00003',
					'CHECKNUM' => '3',
					'NAME' => 'Test3',
				),
				'4' => array(
					'TRNTYPE' => 'CHECK',
					'DTPOSTED' => '20171001',
					'TRNAMT' => -123.00,
					'FITID' => '00004',
					'CHECKNUM' => '4',
					'NAME' => 'Test4',
				),
			),
			'LEDGERBAL' => array(
				'BALAMT' => '-492,00',
				'DTASOF' => $today,
			),
			'AVAILBAL' => array(
				'BALAMT' => '-492,00',
				'DTASOF' => $today,
			),
		),
		'bug458847' => array(
			'ACCTID' => 'bug458847',
			'ACCTTYPE' => 'CHECKING',
			# non standard field
			'MEMO' => 'account for testing bug bug458847 (higher precision than 1/100)',
			'BANKTRANLIST' => array(
				'1' => array(
					'TRNTYPE' => 'CHECK',
					'DTPOSTED' => '20220828',
					'TRNAMT' => -281.543,
					'FITID' => '00001',
					'CHECKNUM' => '1',
					'NAME' => 'Test1',
				),
				'2' => array(
					'TRNTYPE' => 'CHECK',
					'DTPOSTED' => '20220829',
					'TRNAMT' => -10.019997,
					'FITID' => '00002',
					'CHECKNUM' => '2',
					'NAME' => 'Test2',
				),
			),
			'LEDGERBAL' => array(
				'BALAMT' => '-291.562997',
				'DTASOF' => $today,
			),
			'AVAILBAL' => array(
				'BALAMT' => '-291.562997',
				'DTASOF' => $today,
			),
		),
		'bug458447-2' => array(
			'ACCTID' => 'bug458447-2',
			'ACCTTYPE' => 'OTHER',
			'DTASOF' => $today,
			# non standard field
			'MEMO' => 'investment account for test selling a mutual fund',
			'INVTRANLIST' => array(
				'1' => array(
					'SELLMF' => array(
						'INVSELL' => array(
							'INVTRAN' => array(
								'FITID' => '20220607TB190801450137952',
								'DTTRADE' => '20220607110000.000[-5:EST]',
								'DTSETTLE' => '20220608110000.000[-5:EST]',
								'MEMO' => 'Sale: PUTNAM ULTRA SHORT DURATION INCOME FD CL Y VSP 01/04/21 FR',
							),
							'SECID' => array(
								'UNIQUEID' => '74676P698',
								'UNIQUEIDTYPE' => 'CUSIP',
							),
							'UNITS' => '-281.543',
							'UNITPRICE' => '10.019997',
							'MARKDOWN' => '0',
							'COMMISSION' => '0',
							'FEES' => '0',
							'TOTAL' => '2821.06',
							'SUBACCTSEC' => 'CASH',
							'SUBACCTFUND' => 'CASH',
						),
						'SELLTYPE' => 'SELL',
					),
				),
			),
		),
		'bug458847invest' => array(
			'ACCTID' => 'bug458847invest',
			'ACCTTYPE' => 'MONEYMRKT',
			'BROKERID' => '1234',
			# non standard field
			'MEMO' => 'account for testing bug bug458847 (higher precision than 1/100)',
			'INVTRANLIST' => array(
				'1' => array(
					'TRNTYPE' => 'CHECK',
					'DTPOSTED' => '20220828',
					'TRNAMT' => -281.543,
					'FITID' => '00001',
					'CHECKNUM' => '1',
					'NAME' => 'Test1',
				),
				'2' => array(
					'TRNTYPE' => 'CHECK',
					'DTPOSTED' => '20220829',
					'TRNAMT' => -10.019997,
					'FITID' => '00002',
					'CHECKNUM' => '2',
					'NAME' => 'Test2',
				),
			),
			'LEDGERBAL' => array(
				'BALAMT' => '-291.562997',
				'DTASOF' => $today,
			),
			'AVAILBAL' => array(
				'BALAMT' => '-291.562997',
				'DTASOF' => $today,
			),
		),
	),
);

$bank_config = &$config;
$accounts_config = &$config['accounts'];
$a = explode('.', $_SERVER['SERVER_NAME']);
array_shift($a);
$domain = implode('.', $a);

function recursiveArrayToXml($array, &$return=""){
	foreach ($array as $key => $subarray){
		if(empty($key)) {continue;}
		$key = preg_replace('/[^\da-z]/i', '', $key);
		if(preg_match('/[0-9]/i',$key[0])){
			$key = 'x'.$key;
		}
		$return .= "<".$key.">";
		if(!is_array($subarray)){
			$return .= htmlentities($subarray);
		}else{
			recursiveArrayToXml($subarray, $return);
		}
		$return .= "</".$key.">\n";
	}
	return $return;
}

function addTag($key, $value)
{
	return "<$key>$value</$key>";
}

function addTagFromArray($key, &$data)
{
	return "<$key>".$data[$key]."</$key>";
}

function addBankAccountFrom(&$account)
{
	global $bank_config;

	return addTag('BANKACCTFROM',
			addTagFromArray('BANKID', $bank_config)
			.addTagFromArray('ACCTID', $account)
			.addTagFromArray('ACCTTYPE', $account)
		)
		;
}

function addInvestAccountFrom(&$account)
{
	global $domain;

	return addTag('INVACCTFROM',
			addTag('BROKERID', $domain)
			.addTagFromArray('ACCTID', $account)
		)
		;
}

function addAcountInfo(&$account)
{
	global $bank_config;

	if ($account['ACCTTYPE'] == 'CHECKING') {
		$a = addTag('BANKACCTINFO',
			addBankAccountFrom($account)
			.addTag('SUPTXDL', 'Y')
			.addTag('XFERSRC', 'Y')
			.addTag('XFERDEST', 'Y')
			.addTag('SVCSTATUS', 'ACTIVE')
		)
		;
	} else {
		$a = addTag('INVACCTINFO',
			addInvestAccountFrom($account)
			.addTag('USPRODUCTTYPE', 'N')
			.addTag('CHECKING', 'N')
			.addTag('SVCSTATUS', 'ACTIVE')
		)
		;
	}
	$s = addTag('ACCTINFO',
		addTagFromArray('DESC', $bank_config)
		.addTagFromArray('PHONE', $bank_config)
		.$a)
		;
	return $s;
}

function addInvestListData(&$requestData, &$account, $key)
{
	global $today;

	if (isset($account[$key])) {
		$dtstart = $requestData['DTSTART'];
		$dtend = isset($requestData['DTEND']) ? $requestData['DTEND'] : $today;
		$ds = $key == 'INVTRANLIST' ?
			addTag('DTSTART', $dtstart)
			.addTag('DTEND', $dtend) : ''
			;
		$ts = "";
		foreach ($account[$key] as $t) {
			$ts .= recursiveArrayToXml($t)
				;
		}
		$s = addTag($key,
				$ds
				.$ts
				)
			;
		return $s;
	}
	return "";
}

function getStatusCode(&$requestData)
{
	global $bank_config;

	$code = 0;
	if ($requestData['USERID'] != $bank_config['USERID'] || $requestData['USERPASS'] != $bank_config['USERPASS'])
		$code = 15500;
	return $code;
}

function getStatusResponse($code)
{
	if ($code != 0)
		return addTag('STATUS',
				addTag('CODE', $code)
				.addTag('SEVERITY', 'ERROR')
			)
		;
	return
		addTag('STATUS',
			addTag('CODE', '0')
			.addTag('SEVERITY', 'INFO')
		)
	;
}

function getSignOnMessageService(&$requestData, $code)
{
	global $requestData;

	$s = addTag('SIGNONMSGSRSV1',
		addTag('SONRS',
			getStatusResponse($code)
			.addTag('DTSERVER','20051029101003')
			.addTag('LANGUAGE', 'ENG')
			.addTag('DTPROFUP','19991029101003')
			//.'<DTACCTUP>'.$requestData['DTACCTUP'].'</DTACCTUP>'
			.addTag('FI',
				addTagFromArray('ORG', $requestData)
				.addTagFromArray('FID', $requestData)
			)
		)
	)
	;
	return $s;
}

function accountInformationRequest(&$requestData)
{
	global $bank_config, $accounts_config;

	$code = getStatusCode($requestData);
	$s = "";
	if (isset($requestData['SIGNONMSGSRQV1']))
		$s .= getSignOnMessageService($requestData, $code);
	if (isset($requestData['SIGNUPMSGSRQV1']))
		$s .= '<SIGNUPMSGSRSV1>';
	$s .= '<ACCTINFOTRNRS>'
		.addTagFromArray('TRNUID', $requestData)
		.getStatusResponse($code)
		;
	if (!$code) {
		$a = "";
		foreach ($accounts_config as $account) {
			$a .= addAcountInfo($account)
				;
		}
		$s .= addTag('ACCTINFORS',
			addTagFromArray('DTACCTUP', $requestData)
			.$a);
			;
	}
	$s .= '</ACCTINFOTRNRS>'
		;
	if (isset($requestData['SIGNUPMSGSRQV1']))
		$s .= '</SIGNUPMSGSRSV1>';

	return $s;
}

function statementDownloadRequest(&$requestData, $h)
{
	global $bank_config, $accounts_config, $today;

	$code = getStatusCode($requestData);
	$s = getSignOnMessageService($requestData, $code);
	if ($code)
		return $s;
	$s .= "<$h[0]>"
	."<$h[1]>"
	.addTagFromArray('TRNUID', $requestData)
	;

	if (!isset($accounts_config[$requestData['ACCTID']])) {
		$code = 2003; // account not found
	}
	$s .= getStatusResponse($code);
	// account has been found
	if (!$code) {
		$account = &$accounts_config[$requestData['ACCTID']];
		$dtstart = $requestData['DTSTART'];
		$dtend = isset($requestData['DTEND']) ? $requestData['DTEND'] : $today;
		$s .= "<$h[2]>"
		.addTagFromArray('CURDEF', $bank_config)
		.addTag('BANKACCTFROM',
			addTagFromArray('BANKID', $bank_config)
			.addTagFromArray('ACCTID', $account)
			.addTagFromArray('ACCTTYPE', $account)
			.(isset($account['BROKERID']) ? '<BROKERID>'.$account['BROKERID'].'</BROKERID>' : '')
		)
		.''
		.addTag($h[4],
			addTag('DTSTART', $dtstart)
			.addTag('DTEND', $dtend)
		)
		;
		// add transactions
		foreach ($account[$h[4]] as $t) {
			if (!isInRange($t['DTPOSTED'], $dtstart, $dtend))
				continue;
			$s .= '<STMTTRN>';
			foreach ($t as $key => $value) {
				$s .= addTag($key, $value);
			}
			$s .= '</STMTTRN>'
			;
		}
		$s .= addTag($h[4],
			addTag('LEDGERBAL',
				addTagFromArray('BALAMT', $account['LEDGERBAL'])
				.addTagFromArray('DTASOF', $account['LEDGERBAL'])
			)
			.addTag('AVAILBAL',
				addTagFromArray('BALAMT', $account['AVAILBAL'])
				.addTagFromArray('DTASOF', $account['AVAILBAL'])
			)
		)
		."<$h[2]>";
	}
	$s .= "</$h[1]>"
	."</$h[0]>"
	;
	return $s;
}

function investDownloadRequest(&$requestData)
{
	global $bank_config, $accounts_config, $domain, $today;

	$code = getStatusCode($requestData);
	$s = getSignOnMessageService($requestData, $code);
	if ($code)
		return $s;

	if (!isset($accounts_config[$requestData['ACCTID']])) {
		$code = 2003; // account not found
	}

	$sa = "";
	// account has been found
	if (!$code) {
		$account = &$accounts_config[$requestData['ACCTID']];
		$sa = addTag('INVSTMTRS',
				addTagFromArray('DTASOF', $account)
				.addTagFromArray('CURDEF', $bank_config)
				.addInvestAccountFrom($account)
				.addInvestListData($requestData, $account, 'INVTRANLIST')
				.addInvestListData($requestData, $account, 'INVPOSLIST'))
			;
	}
	$s .= addTag('INVSTMTMSGSRSV1',
			addTag('INVSTMTTRNRS',
				addTagFromArray('TRNUID', $requestData)
				.getStatusResponse($code)
				.$sa)
			)
		;
	return $s;
}

function bankStatementDownloadRequest(&$data)
{
    $h = array('BANKMSGSRSV1', 'STMTTRNRS', 'STMTRS', 'BANKACCTFROM', 'BANKTRANLIST');
    return statementDownloadRequest($data, $h);
}

function investStatementDownloadRequest(&$data)
{
    $h = array('INVSTMTMSGSRSV1','INVSTMTTRNRS','INVSTMTRS','INVACCTFROM', 'INVTRANLIST');
    return statementDownloadRequest($data, $h);
}

$validOFXRecord = false;
$rawPostData = file_get_contents("php://input");
if ($debug & Debug::RawData)
	error_log($rawPostData);
$postData = explode("\r\n", $rawPostData);
if ($debug)
	error_log(print_r($postData,true));

// parse input
foreach($postData as $line) {
	$l = trim($line);
	if ($debug & Debug::Lines)
		error_log($l);
	if (strpos($l, ':') !== false) {
		list($key, $value) = explode(":", $l);
		$headerData[$key] = $value;
		continue;
	} if (strcmp($l, "<OFX>") === 0) {
		$validOFXRecord = true;
	}
	if (0 === strpos($l, '<')) {
		$a = explode(">", $l);
		$b = explode("<", $a[0]);
		$key = $b[1];
		$value = $a[1];
		$requestData[$key] = $value;
		if ($debug & Debug::KeyValues)
			error_log("$key $value");
	}
}

if ($validOFXRecord) {
	header("Content-Type: application/x-ofx");
	$code = getStatusCode($requestData);
	$s = '<?xml version="1.0"?>';
	if ($debug & Debug::PHPVersion)
		$s .= '<!-- php version: '.phpversion() .' -->';
	if ($debug & Debug::DataArray)
		error_log(var_export($requestData, true));
	$responseVersion = version_compare($headerData['VERSION'], $bank_config['VERSION']) > 0 ? $bank_config['VERSION'] : $headerData['VERSION'];
	$s .= '<?OFX OFXHEADER="'.$bank_config['OFXHEADER'].'" VERSION="'.$responseVersion.'" SECURITY="NONE" OLDFILEUID="NONE" NEWFILEUID="NONE"?>'
	.""
	. ($useOFXNameSpace ? '<ofx:OFX xmlns:ofx="http://ofx.net/types/2003/04">' : '<OFX>');
	if ($code) {
		$s .= getStatusResponse($code);
	} else if (isset($requestData['ACCTINFOTRNRQ'])) {
		$s .= accountInformationRequest($requestData);
	} else if (isset($requestData['BANKMSGSRQV1'])) {
		$s .= statementDownloadRequest($requestData);
	} else if (isset($requestData['INVSTMTMSGSRQV1'])) {
		$s .= investDownloadRequest($requestData);
	} else
		$s .= getStatusResponse(2028);
	$s .= ($useOFXNameSpace ? '</ofx:OFX>' : '</OFX>');
	echo $s;
	if ($debug & Debug::Response)
		error_log($s);
} else {
	header("Content-Type: text/html");
	echo "<pre>"
	."This service is intended for access from KMyMoney only\n\n"
	."It supports assigning an ofx online account to a KMyMoney account\n"
	."and updating the account from the online account.\n\n"
	."---------------------- access data ----------------------\n"
	."Choose adding manual account with the following settings:\n\n"
	."Org: ".$bank_config['ORG']."\n"
	."Fid: ".$bank_config['FID']."\n"
	."Url: "."http".(($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://").$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']."\n"
	."User: ".$bank_config['USERID']."\n"
	."Password: ".$bank_config['USERPASS']."\n"
	."\n"
	."---------------------- bank data ----------------------\n"
	."bank identification:".$bank_config['BANKID']."\n"
	."number of accounts: ".sizeof($accounts_config)."\n"
	."\n"
	."---------------------- accounts ----------------------\n"
	." account  \t type   \t notes\n";
	foreach($accounts_config as $account) {
		echo $account['ACCTID']."\t". $account['ACCTTYPE']."\t". $account['MEMO']."\n";
	}
	echo "\n"
	."---------------------- limitations ----------------------\n";
	if (!$hasOffsetSupport)
		echo "- The limitation of statements by date does not work, caused by the PHP version used\n";
	echo "</pre>";
}
?>
