---
title: 'KMyMoney 0.3.5 Release Notes'
date: 2001-07-12 00:00:00
layout: post
---
2001-07-12 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Fixed small bug i introduced in the import dialog.
* Released 0.3.5.

2001-07-12 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Improved import &amp; export process. Now prompts for more than
one type of export (at the moment, QIF and CSV).

2001-07-11 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Improved reconciliation process, mostly GUI improvements.
* Backup dialog remembers the state of the mount check box.
* Fixed a bug in MyMoneyFile:: assignment operator and the copy constructor.
* MyMoneyFile::isInitialised() should work properly now.
* Fixed bug in reconcile dlg dealing with editing transactions during the
process. (It opened the window again even if you've cancelled when you
next edit/create a transaction).

2001-07-09 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Updated the parent() methods to have descriptive names and added missing
call in MyMoneyTransaction.
* Changed the backup dialog to incorporate a choose folder button.
* Fixed bug in backup dialog dealing with automount systems.
* Backup files now have the date appended to them, for proper backup
restoration, and a test exists to check overwriting files.

2001-07-08 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Start dialog only shows files that exist.
* Fixed bug in MyMoneyFile class that didn't delete the payee list
and didn't delete the category list properly on close/reset.
* Removed subdirs, configure.in and configure.files from cvs because
they are generated. Updated .cvsignore to ignore those files.
* Temporarily hidden all the not implemented stuff ready for the next
0.4 stable release. It's still there but the user can't see it.

2001-07-08 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Standardized most of the dialogs in respect to the OK and
Cancel buttons.
* MyMoneyFile now updates the dirty flag when needed.
* All classes now have a 'parent' pointer and update the
dirty flag when needed. (e.g transactions now know which
account/bank they are in).

2001-07-06 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Added the ability to customize the key definitions.
* Fixed the ability to load the last file edited automatically.
This is done in the configure dialog and bypasses the start
dialog if needed. Beware: the random crashes on exit mean
it doesn't always update the options properly. But you can
hand edit $HOME/.kde/share/config/kmymoney2rc.
* Removed KMyMoneySettings class and changed all references to
use the KConfig class.

2001-07-06 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Improved the payee editor, adding a delete button and improving
a lot of the logic.
* Fixed a bug in the MyMoneyFile class that didn't update the
dirty flag when adding a payee.

2001-07-06 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Improved the transaction search process.
* Changed acinclude.m4.in to enable debug by default. We'll
change it back for the stable release.

2001-07-06 Felix Rodriguez &lt;frodriguez@mail.wesleyan.edu&gt;
* Optimized TransactionView Refresh

2001-07-05 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Fixed the category list item class so you can use
--enable-final in the configure process.
* Changed the search dialog to be ready for the new features.

2001-07-05 Felix Rodriguez &lt;frodriguez@mail.wesleyan.edu&gt;
* Added Memo Display
* Fixed numerous transactionview bugs.

2001-06-29 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Added ability to search for transactions and view them in
register view.
* Fixed resize issues with transaction view.
* Added the Back icon onto the toolbar.

2001-06-23 Felix Rodriguez &lt;frodriguez@mail.wesleyan.edu&gt;
* Payee ComboBox now populates from PayeeList also

2001-06-22 Felix Rodriguez &lt;frodriguez@mail.wesleyan.edu&gt;
* Fixed Payee List Bug
* Added Edit Transaction Button to Reconcile Dialog

2001-06-21 Felix Rodriguez &lt;frodriguez@mail.wesleyan.edu&gt;
* Fixed Transaction Deletion Bug

2001-06-21 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Fixed missing addPayee call in reading qif files.
* Fixed bug in filename handling code when closing/opening
files.
* Added missing File New action.
* Added new action - Account open and changed behaviour
of account list view. It now lets you select an account
and then open it or perform other operations.
* You can now perform menu operations on the account, (not
just by right clicking on the account).

2001-06-20 Felix Rodriguez &lt;frodriguez@mail.wesleyan.edu&gt;
* Fixed Reconciliation Bugs when editing transactions
during reconciliation.
* Removed Vertical Header

2001-06-19 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* GUI improvements to the New Category dialog.
* Bug fixed in MyMoneyFile in dealing with categories.
* README and AUTHORS updated.
* GUI improvements to the Payee dialog.

2001-06-18 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* KAbout data updated.
* More GUI improvements focusing on the Categories dialog.
* Transaction view shows numbers instead of the currency prefix.
* Fixed bug in KMyMoney2App that didn't prompt to save file
on exit.
* BUGS file updated.

2001-06-17 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Fixed a bug in the MyMoneyAccount copy constructor.
* Changed the 'up' icon to use a standard back action but
the toolbar action has gone ? You can access it by going
to the Go menu or by pressing Alt-Left.
* More GUI improvements especially to the banks/accounts list,
and to the dialogs New File, New Institution, New Account.
* Fixed bug in MyMoneyMoney class where isZero() returned true
for numbers &lt; 0.
* Added a BUGS file to the project.

2001-06-16 Javier Campos Morales &lt;javi_cms@terra.es&gt;
* New icons added.
* Remove sprintf in ktransactionview. "The %s escape sequence expects a utf8() encoded string.". This is not
good for internacional support.

2001-06-16 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Removed 'name' input field from KNewFileDlg class.
* Removed m_moneyName from MyMoneyFile and made the
file reading code detect the older version and convert.
* Removed hide/show input box.
* Some minor GUI improvements, (focus etc).
* Added some opening balance fields to MyMoneyAccount.
* Better version control in file format. Can convert
between versions now.
* The Bank list view is now open by default.
* Fixed bug in MyMoneyFile::resetAllData().
* Changed behaviour of File|New to open a new window because
the user can create a new file through the start dialog.
(Maybe we should change the open icon to something to
represent the start dialog ?).

2001-06-16 Javier Campos Morales &lt;javi_cms@terra.es&gt;
* Only one settings dialog is needed so added new general settings dialog -&gt; ksettingsdlg.
* Remove klistsettings and old settings variables.
* Modify to find correct kmy icon in startup dialog.
* Changes some toolbar icons.

2001-06-15 Felix Rodriguez &lt;frodriguez@mail.wesleyan.edu&gt;
* Fixed Year bug in QIF import
* Can now edit transactions during reconciliation

2001-06-07 Felix Rodriguez &lt;frodriguez@mail.wesleyan.edu&gt;
* QIF import and export now provide feedback
* Added backup function to File menu

2001-05-29 Felix Rodriguez &lt;frodriguez@mail.wesleyan.edu&gt;
* Widgets now goes to next transaction after pressing enter.

2001-05-28 Felix Rodriguez &lt;frodriguez@mail.wesleyan.edu&gt;
* Fixed problems caused working with QT in RedHat 7.1

2001-05-25 Felix Rodriguez &lt;frodriguez@mail.wesleyan.edu&gt;
* Added QIF export of categories and transactions

2001-05-20 Felix Rodriguez &lt;frodriguez@mail.wesleyan.edu&gt;
* Added QIF import of categories and transactions
* Fixed bugs in ReconcileDialogBox
* Fixed bug in MyMoneyMoney class in determining whether amount is zero.

2001-05-15 Felix Rodriguez &lt;frodriguez@mail.wesleyan.edu&gt;
* Fixed bug where numeric keypad enter key would not work
* Now saving Payee information in transaction's payee field

2001-05-09 Felix Rodriguez &lt;frodriguez@mail.wesleyan.edu&gt;
* Can now Enter a transaction by pressing enter from a
data entry widget

2001-05-08 Felix Rodriguez &lt;frodriguez@mail.wesleyan.edu&gt;
* Implemented automatic cheque numbering
* Transactions now autocompletes after selecting Payee/Memo

2001-05-03 Felix Rodriguez &lt;frodriguez@mail.wesleyan.edu&gt;
* Implemented Update of transfer transactions

2001-05-02 Felix Rodriguez &lt;frodriguez@mail.wesleyan.edu&gt;
* Optimized refresh of TransactionView

2001-05-01 Felix Rodriguez &lt;frodriguez@mail.wesleyan.edu&gt;
* Implemented Deletion of transfer transactions

2001-04-30 Felix Rodriguez &lt;frodriguez@mail.wesleyan.edu&gt;
* Implemented Transfer of one Account to another within the same bank

2001-04-28 Felix Rodriguez &lt;frodriguez@mail.wesleyan.edu&gt;
* Alphabetized Categories

2001-04-13 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Removed some of the old code in ktransactionview.cpp.
* Removed all of the unneeded Table Item classes in
ktransactionview.cpp and added a single KTransactionTableItem
class.

2001-03-25 Felix Rodriguez &lt;frodriguez@wesleyan.edu&gt;
* Added Cheque Number KLineEdit box

2001-03-24 Felix Rodriguez &lt;frodriguez@wesleyan.edu&gt;
* Added Payees as selections to Payee ComboBox

2001-03-23 Felix Rodriguez &lt;frodriguez@wesleyan.edu&gt;
* Fixed Category Combo Box to include Minor Categories
* Fixed Category Dialog Box ListView WidthMode to Maximum

2001-03-22 Felix Rodriguez &lt;frodriguez@wesleyan.edu&gt;
* Moved the Reconcile Column from column 4 to column 3 of the table
* Fixed problem after clicking cancel if you click on same transaction
row, Input widgets did not appear.
* Moved Ending Balance Box to appear directly under balance column

2001-03-21 Felix Rodriguez &lt;frodriguez@wesleyan.edu&gt;
* Added two lines per transaction in the transaction view
* Added Data Entry Widgets and buttons to appear on
transaction rows.
* Removed tabbed data entry views at the bottom of the
transaction view.
* Added Balance Label to show the account's current balance
* The last transaction is now always visible when the transactions
are updated

2001-03-13 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Ran KFileReplace to change my email address because I won't
always be at university.
* Remove all #include &lt;x.moc&gt; from the source files.
* Updated project to install the ui file in the correct place.
* Cleaned up the startup code so the default colour settings aren't black on black.
* Changed addCredit to addAuthor in main.cpp.
* Project file 16x16 mimetype kmy icon changed to kmy not kmy2 in install options.
* Changed web address reference in main.cpp to kmymoney2.sourceforge.net.
* Updated README file.
* Removed some unused list items from the project.
* Added files to .cvsignore (make-ui.sh, *~)

2001-03-11 Javier Campos Morales &lt;javi@DarkStar&gt;
* Insert new startuplogo

2001-03-08 Javier Campos Morales &lt;javi@DarkStar&gt;
* Fixed kmymoney2 repository
* Remove compile.sh and mkui.sh
* Modify the project to complain with kdevelop 1.4
* Added directory icons with a few icons for the future
* Added readConfig and writeConfig to KStartDlg
* Delete KMyMoney2App::openDocumentFile(const KURL&amp; ulr) - Method not used
* Added kstartdlg recent action. Now works.
* KMyMoney2App::slotFileOpen now open the start dialog.
* Fixed errors with compile.
* Added .cvsignore in all project.
        