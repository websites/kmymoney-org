---
title: '        KMyMoney 4.6.0 Release Notes
      '
date: 2012-08-11 00:00:00 
layout: post
---

<p>We are happy to announce the release of KMyMoney version 4.6.0, the next stable version, after 11 months of development from the first release of the <a href="release-notes.php#itemKMyMoney450ReleaseNotes">4.5 series</a>.</p><p>Some of the highlights since the <a href="release-notes.php#itemKMyMoney453ReleaseNotes">latest stable version</a>:
          <ul>
            <li>New CSV import plugin</li>
            <li>The application is translated in 36 languages (Bosnian and Uyghur added)</li>
            <li>The user documentation is available in 7 languages (French translation added)</li>
            <li>Many fixes for the OS X and Windows platform</li>
            <li>Performance improvements (although we know there's still work to do in this regard)</li>
            <li>Many fixes in the import and online banking modules</li>
            <li>Over 100 bugs have been fixed since the latest bugfix release alone</li>
            <li>Since the OFX institution listing service run by Microsoft (TM) stopped working, we switched to the service run on <a href="http://www.ofxhome.com">www.ofxhome.com</a> which is provided on a voluntarily basis by Jesse Liesch. With his support it was easy to make the switch in KMyMoney</li>
            <li>Added Serbian Dinar as a standard currency</li>
            <li>Allow entering an interest rate with three decimal places when editing a loan</li>
            <li>Improved wording when transaction editing is canceled by selecting another transaction in the ledger</li>
            <li>Make FID an optional field during definition of an OFX account mapping</li>
            <li>Allow to base the payee name from either the PAYEEID, NAME or MEMO field in an OFX transaction</li>
          </ul>
        </p><p>For a full list of the changes since the <a href="release-notes.php#itemKMyMoney453ReleaseNotes">latest stable version</a> please check out the <a href="changelogs/ChangeLog-4.6.0.txt">changelog</a></p><p>The KMyMoney Development Team</p>