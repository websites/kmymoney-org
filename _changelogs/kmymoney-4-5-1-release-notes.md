---
title: '        KMyMoney 4.5.1 Release Notes
      '
date: 2010-11-12 00:00:00 
layout: post
---

<p>The KMyMoney Team is pleased to announce the release of KMyMoney version
          4.5.1. This version contains several fixes for bugs found in <a href="release-notes.php#itemKMyMoney450ReleaseNotes">4.5.0</a> since it was
          released almost three months ago as the stable release for the KDE Platform 4.</p><p>Here is a list of the most important changes since the last stable release:
          <ul>
            <li>fixed several crashes like when adding a default category to a payee <a href="https://bugs.kde.org/show_bug.cgi?id=248448">#248448</a>,
              when activating the schedules calendar on certain languages <a href="https://bugs.kde.org/show_bug.cgi?id=232757">#232757</a>, when
              back-tabbing in the split editor <a href="https://bugs.kde.org/show_bug.cgi?id=244962">#244962</a></li>
            <li>fixed some report configuration bugs <a href="https://bugs.kde.org/show_bug.cgi?id=255532">#255532</a> and
              <a href="https://bugs.kde.org/show_bug.cgi?id=249119">#249119</a></li>
            <li>improved some small user interface issues <a href="https://bugs.kde.org/show_bug.cgi?id=252598">#252598</a>,
              <a href="https://bugs.kde.org/show_bug.cgi?id=249609">#249609</a>,
              <a href="https://bugs.kde.org/show_bug.cgi?id=241044">#241044</a>,
              <a href="https://bugs.kde.org/show_bug.cgi?id=255433">#255433</a>,
              <a href="https://bugs.kde.org/show_bug.cgi?id=248448">#251212</a></li>
            <li>added the new Bulgarian Lev as a currency <a href="https://bugs.kde.org/show_bug.cgi?id=255491">#255491</a></li>
          </ul>
        </p><p>For a full list of the changes please check out the <a href="changelogs/ChangeLog-4.5.1.txt">changelog</a>. If you
          are using the KDE4 version and where affected by one of the closed bugs or wish
          for a few improvements we recommend upgrading to 4.5.1.</p><p>The KMyMoney Development Team</p>