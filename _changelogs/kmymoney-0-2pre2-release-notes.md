---
title: 'KMyMoney 0.2pre2 Release Notes'
date: 2000-08-22 00:00:00
layout: post
---

0.2pre2

- Fixed the KStartupLogo class to actually find the picture to display on startup.
- Stopped using the DateInput class from calendar-0.13 and am now using my own widget, with
        some code temporarily used from KDatePicker until that widget gets updated in KDE (if it
        ever does, Iv'e mailed the maintainer requesting a change to the widgets code (16/08/00) ).
- Made the register view show two different background colours depending upon position.
        The colours will be user degined in a future version.
- Made the register view list refreshment a little bit quicker.  This will be looked at again
        in a future version.
- Removed all the labels from the *View dialogs and am now using QdbtTabulars to provide
        column alignment for numbers etc.
- Removed the *View dialogs and changed a little code in their previous files to reflect the
        changes.
- Made sure that ALL widgets resize properly.
- When adding a transaction and refreshing the transaction list the last transaction is shown
        at the bottom so the user does not have to scroll.
- Changed the Reconciliation dialog to show the new colours and bordering.
- Fixed bug in reconciliation dialog where the current date was written where it should have
        been the ending date.
- Cleaned up the reconciliation code and it now works ! (hopefully).

  