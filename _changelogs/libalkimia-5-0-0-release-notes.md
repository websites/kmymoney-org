---
title: '        LibAlkimia 5.0.0 Release Notes
      '
date: 2015-12-01 00:00:00 
layout: post
---

<p><a href="http://download.kde.org/stable/alkimia/5.0.0/src/libalkimia-5.0.0.tar.xz.mirrorlist">LibAlkimia version 5.0.0</a> was released in preparation for the release of KMyMoney version 4.8.0. This version contains build system updates and optimizations that will allow KMyMoney to run faster:
          <ul>
            <li>add support for newer versions of cmake (&gt;=2.8.7)</li>
            <li>use implicit sharing in AlkValue</li>
          </ul>
        For a full list of the changes please check out the <a href="changelogs/alkimia/ChangeLog-5.0.0.txt">changelog</a>.</p><p>The KMyMoney Development Team</p>