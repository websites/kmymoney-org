---
title: 'KMyMoney 0.7.1 Release Notes'
date: 2005-05-21 00:00:00
layout: post
---
2005-05-21 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed drag&amp;drop of accounts between institutions
* Fixed build system for documentation
* Released 0.7.1

2005-05-20 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed dependencies and install instructions for documentation
* Added initial doc on currencies
* Removed outbox view from documentation as it is not part of 0.8

2005-05-15 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Avoid warning about unknown macro in kmymoney2.cpp
* Fixed escape sequence in kmymoneyedit.cpp
* Allow --enable-final to work properly with prior modifications
* Don't include the docbook source files in the installation
* Added new docbook files provided by Ace
* Added patch provided by Laurent Montel to fix some cvsignore files
* Fixed some more .cvsignore problems

2005-05-14 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added investment handling to QIF importing. Handles !Type:Invst
!Type:Price and !Type:Security.
* Started "KMyMoney Files" help file

2005-05-14 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Gnucash Reader
* Infrastructure for Price Source fix

        