---
title: KMyMoney 5.1.0 Release Notes
date: 2020/06/14
layout: post
---
<p>The KMyMoney development team is proud to present <a href="https://download.kde.org/stable/kmymoney/5.1.0/src/kmymoney-5.1.0.tar.xz.mirrorlist">version 5.1.0</a> of its open source Personal Finance Manager.</p>

<p>Here is the list of bugfixes:
  <ul>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=350360">350360</a> OFX import leaves brokerage account field blank for nested accounts</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=396286">396286</a> KF5 ofximporter "Map account" fails on MS Windows</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=399261">399261</a> report's chart mess with data if there are too many data</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=416534">416534</a> Some ui files are not compilable after editing with designer</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=416577">416577</a> Message Box Doesn't Size</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=416621">416621</a> Import a QFX file fails on MacOS</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=416711">416711</a> Missing german translation</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=416746">416746</a> Summary values are not updated for investment transaction of type interest income</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=416827">416827</a> libofx dtd files are not found in AppImage</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=416902">416902</a> Investment reports should ignore setting for "Show equity accounts"</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=416963">416963</a> After the migration to aq6 the change of views takes a long time</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=417142">417142</a> cannot find yahoo finance under online quotes</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=418334">418334</a> Request: Use latest values to fill in transaction</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=418823">418823</a> Script based online quotes do not work in the AppImage version</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=419082">419082</a> Backup</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=419113">419113</a> Data displayed in scheduled transaction and home page are sometimes not consistent</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=419554">419554</a> Balance of budget is shown incorrect</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=419974">419974</a> MyMoneyStatementReader uses base currency instead brokerage account's own when adding new price</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=419975">419975</a> When importing transactions, we're matching against the other transactions also being imported</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=420056">420056</a> BUY/SELL information ignored when importing OFX investment transactions</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=420082">420082</a> Startlogo not translated in french</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=420422">420422</a> Indian Rupee has new symbol since 7 years,it is ₹</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=420584">420584</a> QIF importer ignores new investments</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=420593">420593</a> A sum of multiple rows selected is incorrect for securities with fraction > 100</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=420683">420683</a> Inaccurate decimal precision of South Korean Won (KRW)</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=420761">420761</a> After upgrade from Fedora 31 to 32, one of my checking accounts shows a huge negative "Cleared" balance</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=420767">420767</a> Incorrect ordinate axis labels when zooming a chart</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=420931">420931</a> Crash in "Edit loan Wizard"</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=421056">421056</a> Freeze: logarithmic vertical axis and negative data range From value</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=421105">421105</a> Logarithmic vertical axis has multiple zero labels</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=421126">421126</a> Securities Dialog "Market" field not populated with existing data on edit</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=421260">421260</a> Networth "account balances by institution" provides incorrect results</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=421307">421307</a> Account context menu's Reconcile option opens incorrect ledger</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=421569">421569</a> New Account Wizard throws exception on empty payment method selected</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=421691">421691</a> New Account Wizard is not asking if the user wants to add a new payee</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=421750">421750</a> Scheduled monthly transaction will only change first date</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=421757">421757</a> Anonymised files are no longer created</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=421900">421900</a> SEGFAULT occurring when marking an account as preferred</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=422012">422012</a> Incorrect account hierarchy if an account is marked as preferred</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=422196">422196</a> Budget view displays all account types</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=422200">422200</a> KMyMoney crashes when navigating backwards through CSV import wizard</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=422480">422480</a> Search widget in the Budgets view ignores user input</li>
  </ul>
</p>
<p>Here is the list of the enhancements which have been added:
  <ul>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=416279">416279</a> Add option "Reverse charges and payments" to OFX import</li>
  </ul>
</p>
<p>A complete description of all changes can be found in the <a href="/changelogs/ChangeLog-5.1.0.txt">changelog</a></p>.
