---
title: 'KMyMoney 0.7.4 Release Notes'
date: 2005-07-18 00:00:00
layout: post
---
2005-07-18 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed tab order in new investment wizard
* Added help button to reconcile wizard
* Released 0.7.4
* Bumped version to 0.7.5

2005-07-18 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* GnuCash Reader - stand-alone anonymizer fixes
- fix memory leak when deferred interval present

2005-07-18 Robert Wadley &lt;robntina@users.sourceforge.net&gt;
* Added quite a few more screenshots to the user manual.

2005-07-17 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Minor layout changes (use tags where possible)
* Added Ctrl-Ins to create new transactions/splits
* Use $(MAKE) instead of 'make' to be able to switch make program
* Removed unused code enclosed with "#if __WORDSIZE == 32"
* Removed unused declarations in mymoneymoney.h
* Allow to compile mymoneymoneytest on FreeBSD

2005-07-16 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* QIF Importer improvements for investments
- Supports O and L fields (transaction fees and account transfer)
- Disallow transfers INTO an investment account
* Major revision of the handbook
- Pared down 'firsttime' section significantly
- Moved most 'firsttime' information down into the details
- Merged MTE's 'firsttime' information with Roger's sections
- Added Roger's latest: Menus reference
- Removed "Views" reference
- Removed "Integrity Checks"
- Removed "Other documentation"
- Removed my "Files" section. Tom's is better.
- Added links for the 2 remaining "whats new" items without links

2005-07-16 Robert Wadley &lt;robntina@users.sourceforge.net&gt;
* Added screenshots for details-widgets.docbook in the user manual.

2005-07-15 Robert Wadley &lt;robntina@users.sourceforge.net&gt;
* Applied patches submitted by Darin Strait for the user manual.

2005-07-15 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Allow modification of due date for newly created schedules based on
existing transactions
* Fixed a nasty problem when two-byte characters across a 512 byte
boundary in a GPG encrypted file causing a false EOF report in KGPGFile.

2005-07-11 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Destroy objects in MyMoneyStorageXML::readFile even if file is not
parsable to avoid memory leaks
* Added capability to modify fraction of securities/investments
ie. Fixed #1175904 (Edit investment shares precision)
* Use precision setting for price fields in investment handling
* Fixed email link in documentation
* Suppress engine notifications during auto entering due schedules
after file load

2005-07-13 Robert Wadley &lt;robntina@users.sourceforge.net&gt;
* added screenshots submitted by Darin Strait for the user manual.

2005-07-10 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* When saving an anonymous file, retain the linkage from investment account
to brokerage account
* QIF Importer improvements for investments
- Fixed reversed sign on dividend importing
- Fixed price importing. Previously, it only imported prices for the
current account. But really it should import them for any prices you have
in your KMM file.
- Added line number to warnings, to make it easier to track them down
- Converted many dialog-box warnings to debug output. They weren't that
useful in dialogs
- Removed "QIF/" from bank ID's. It was a bad design decision to include
that. If you import the same transaction 2 different ways, it will
seem different to KMM, but it should look like the same transaction.
- Warn when importing an investment transaction without a stock
- Warn when ignoring a transaction because the stock was not found
- Warn when ignoring a stock split

2005-07-10 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Applied patch provided by Christian Nolte &lt;noltec@users.sourceforge.net&gt;
* Use QFile as base class instead of QIODevice
* Bumped version number to 0.7.4

2005-07-10 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* GnuCash Reader - improve formula detection in schedules
- minor stand-alone anonymizer changes

2005-07-09 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Updated ledger docs from Roger
* Small fixes here and there in the docs
* Modified home view to show total value of favorite accounts
(this includes balances of sub-accounts, and correctly handles currency)

        