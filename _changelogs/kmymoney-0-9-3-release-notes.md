---
title: 'KMyMoney 0.9.3 Release Notes'
date: 2009-02-24 00:00:00
layout: post
---
2009-02-24 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated README.ofx
* Released 0.9.3

2009-02-23 Robert Wadley &lt;robntina@users.sourceforge.net&gt;
* Updated Summary view style.
* Updated "What's new" page for upcoming release

2009-02-23 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed register column resizing algorithm
* Show database options in File menu

2009-02-22 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Load addressbook for new user wizard in background
* Fixed bko#185212 (Default year prefix)

2009-02-21 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Load data for account template widget in the background
* Added patch provided by Colin Wright (fix3months_removefq_occurencePeriodToString.diff)

2009-02-20 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added patch provided by Holger
(0001-cmake-QtSqlite-and-small-tweaks.patch)
(0002-Installation-Versioning-and-Documentation-improvem.patch)

2009-02-19 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated regular expression for Financial Express price source

2009-02-18 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed problem in case additional fees for loans referenced another
asset or liability account
* Fixed bko#118285 (kmymoney online quotes are cached by KDE)
* Assigned icons to dialog buttons

2009-02-17 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed creation of unique bankID in MyMoneyStatementReader per statement
* Fixed unnecessary popup of dialog box in payees view
* Fixed calculator widget to use same decimal symbol as edit widget
* Fixed #1962055 (Crash in case AqBanking failed) Fix also requires
an updated KBanking package

2009-02-16 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed some issues around modification of addtional loan fees
* Removed Makefile.in
* Added patch provided by Colin Wright (stringToOccurence.diff)
* Added patches provided by Holger
(0001--non-fancy-sqlite3-support.patch,
0002-make-install-and-make-kmymoney-unstable_rpm.patch,
0003-added-fancy-qsqlite3-support.patch)
* Removed Makefile
* Added updated Russian default account template from Andrey Cherepanov
* Renamed ru_SU into ru_RU for templates
* Change tab between deposit/withdrawal if sign of amount changes

2009-02-15 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Updated Spanish translation

2009-02-14 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added Argentinian account templates
* Fixed typos in Spanish templates

2009-02-14 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
- Happy Valentine's Day
* Added patch provided by Colin Wright (oops.diff)
* Added testcase for the above correction
* Pass on plugin name to createInstanceFromLibrary()
* Use const reference for getters in plugin info object

2009-02-13 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added patch provided by Cristian Onet (patch_plugins.patch)

!!!!FILE FORMAT CHANGE!!!! The previous change causes the file format
to change slightly. Be prepared, that schedules may show wrong
payment periods when loaded in earlier versions of the program.

2009-02-13 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added patch provided by Colin Wright (use_compoundOccurence.diff)

2009-02-11 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Reverted parts of yesterdays patch
* Added patch provided by Colin Wright (addHalfMonths.diff)
* Added contrib subdirectory
* Fixed typos also in .pot and in .po files

2009-02-11 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Updated Argentinian translation
* Fixed typos in kmymoneygpgconfigdecl.ui and ksettingsgpgdecl.ui

2009-02-10 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Updated Argentinian translation
* Do not show investments accounts in detailAll charts

2009-02-10 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added patch provided by Colin Wright (use_occurencePeriod.diff)
* Added "don't ask again" feature to balance warning dialog

2009-02-09 Fernando Vilas &lt;fvilas@iname.com&gt;
* Fixed date calculation in SQL that was missed in my last patch
* Speed improvement in readSplit()

2009-02-09 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added patch provided by Colin Wright (schedules_multiplier_prep.diff)
* Remove link to payee from second split if name is removed from transaction

2009-02-08 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated Italian translation from Vincenzo Reale
* Updated Brazilian translation from Marcus Gama
* Fixed French translation
* Updated Galician translation from Marce Villarino

2009-02-07 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Updated translation documentation to merge outdated po files
* Updated Spanish translation
* Updated Argentinian translation

2009-02-07 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed a sign issue with investment sell/buy imports from OFX
* Allow higher precision for price and share information from OFX
* Updated translations for de, pt and fr

2009-02-06 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed Balance After calculation in Home Page Payments
* Fixed one-time schedules duplicating in Home Page Payments

2009-02-06 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed CMake based build system (still not 100% compatible w/ autotools)
* Fixed auto detection of variable sizes for CMake based build system
* Fixed some more CMake related things (mostly cleanup)
* Added missing files
* Fixed typos in two messsages

2009-02-05 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Updated Spanish translation

2009-02-05 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added update for CMake build environment (a bit broken for me atm)

2009-02-04 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added trace functionality to HTTPS connections for OFX

2009-02-04 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed loan amortization calculation on reports when there
are multiple amortization splits

2009-02-03 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Updated Spanish translation

2009-02-03 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Show correct amount for multi-currency schedules
* Updated Portuguese translation from José Jorge
* Added home_pt.html from José Jorge
* Fixed #2558627 (Account entry widget mess about splits)
* Updated Romanian translation by Cristian Onet
* Fixed an OFX issue when the bank_id field was not filled

2009-02-02 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Removed dependancy to libxml2-devel

2009-02-02 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Updated Spanish translation

2009-02-01 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Code cleanup of calculateAutoLoan in kmymoneyutils
* Added interestSplit method to MyMoneyTransaction
* Added isInterestSplit method to MyMoneySplit

2009-02-01 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added patch provided by Colin Wright (occurenceToString.diff)
* Regenerated POT file and merged all PO files
* Updated German translation

2009-01-31 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed loan interest calculation in pivot reports when the loan
account is not included in the report - #2541605

2009-01-31 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added #2551345 (Maintain geometry of split transaction editor
* Added #2551417 (Deletion of the zero-value split records)
* Added #2551242 (ENTER focus to next split if is is already defined)
* Added patch provided by Colin Wright (halfmonth_200901241601.diff)

2009-01-30 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed loan calculation when the loan account itself is not
part of the report. More work needed on that area

2009-01-30 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Allow overriding header version for OFX requests

2009-01-29 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added icon to close button of balance chart dialog

2009-01-28 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added merge button to split transaction dialog (#2538561)
* Improved path detection for QT in sqlite.m4

2009-01-27 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Patch provided by Thomas Baumgart to show currency conversion
dialog when entering schedules involving foreign currencies
* Fixed schedule forecast calculation - bug #2507699

2009-01-26 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Anonymize budget values while saving as anonymized file

2009-01-25 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Removed dependency to libcurl and libxml++ from OFX plugin

2009-01-24 Fernando Vilas &lt;fvilas@iname.com&gt;
* Reworked balance calculations
* Standardized date and MyMoneyMoney formatted storage
* Fixed date calculations to work with SQLite
* Added (commented) code to support future MMSchedule KVP.

2009-01-24 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed #2530475 (Calculator (Value entry widget) bug!)
* Updated French translation from Patrick Petit

2009-01-23 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added 'Force SSLv3' switch to OFX banking setup wizard
* Removed some unused files

2009-01-22 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Applied patch provided by Colin Wright to clean up schedule frequencies

2009-01-19 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed sqlite detection problems with older autoconf versions

2009-01-18 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed problems reported by Doxygen

2009-01-17 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added Quicken 2004 and Quicken 2003 to the OFX plugin

2009-01-16 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Use KDE locale setting for date format in report subtitles
* Collect all statement reports and show them together in a single window
* Automatically detect mapped acount during OFX statement download
* Show the transaction selection dialog for autofill even if there
is only a single matching transaction
* Show warning if user wants to map a brokerage account to an online account
* Added icons to buttons on plugin selection dialog

2009-01-12 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added missing include in kguiutils.cpp
* Added date to pivot reports subtitle
* Added date to transaction reports subtitle

2009-01-14 Tony Bloomfield &lt;gandalf1189@users.sourceforge.net&gt;
* Fix #2248020 - Allow passworded databases
* Remove redundant code
* Update database documentation

2009-01-14 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Improved OFX logging during setup
* Fixed layout for select database dialog
* Fixed slot for OK button in select database dialog
* Fixed translation issue
* Fixed MyMoneyMoney formatting problem with precision -1 that caused
14.04 to be shown as 14.4
* Added testcase for this problem
* Removed const qualifier from formatMoney() returnvalue

2009-01-13 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixes to the build system (make distcheck did not work due
to the changes for SQLITE3 support)

2009-01-12 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added CashFlow Report

2009-01-11 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated Romanian translation by Cristian Onet
* Added QT SQLITE3 support from
http://www.kde-apps.org/content/show.php/qt-sqlite3?content=23011
to the build system
* Building OFX support is now automatic if required libs are installed
* Allow arbitrary precision interest rates for loan accounts
* Fixed a bunch of warnings

2009-01-09 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Refactored the usage of the 'Private * d' construct

2009-01-08 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added chart detail setting to forecast view
* Fixed sign of expected liabilities on cash flow summary
* Cleaned old code in querytable.cpp

2009-01-08 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated German translation

2009-01-07 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added Finnish translation from Raimo Ikonen
* Regenerated POT file and merged all PO files

2009-01-06 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Allow to load details for new investments if same security is used

2009-01-05 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added chart to forecast view

2009-01-04 Tony Bloomfield &lt;gandalf1189@users.sourceforge.net&gt;
* Fix # 2217233 - Add option to import GnuCash notes as memos

2009-01-04 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added display of number of splits in file info dialog

2009-01-01 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* We welcome Slovakia as a new member of the Eurozone
(http://en.wikipedia.org/wiki/Eurozone)

2008-12-29 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Make menu item 'File information' available if build w/o debug support
* Added differentiation between QIF bank statement and QIF application import

2008-12-29 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Import GnuCash notes as transaction memo

2008-12-27 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Don't include invalid account types in split editor account selection

2008-12-25 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added moving average to report charts
* Cleanup of the charts code
* Added configuration of moving average days
* Added Moving Average vs Actual report

2008-12-23 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added testcases for the loader functions in MyMoneySeqAccessMgr
* Fixed last id detection in loader functions

2008-12-21 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed an uninitialized variable bug in the QIF importer
* Improved const-ness passing of parameters in Gnc importer
* Use QString for ids rather than QCString

2008-12-20 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Assign base currency to account if none is present during creation

2008-12-19 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Show different intro when editing an investment with the wizard

2008-12-18 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Reworked matching logic so that in case of multiple payees match,
the one with the largest match is taken. This returns the name
matching to the 'old' functionality of partial matching.

2008-12-17 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed MyMoneyQifProfile::possibleDateFormats so that it
always delivers at least one possible format

2008-12-15 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed display of total rows in charts

2008-12-15 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added OFX trace functionality

2008-12-14 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated Czech translation from Marek Stopka
* Removed blank in front of synopsis
* Added mandatory price entry for new foreign account
* Added online quote button to new account wizard

2008-12-14 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added moving average reports for investments
* Fixed crash in forecast view when displaying stock in foreign currency

2008-12-12 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* More changes to the QIF importer

2008-12-09 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Don't discard final line during QIF import if it does not contain a LF
* Categories don't override account information during QIF import
* Don't create duplicate opening balance transaction if same file
is imported more than once. Present a warning if amount differs and let
the user manually correct it.
* Mark imported opening balance transactions as imported (diff. background)
* Fixed QIF Buy/Sell action if given an account but not BuyX/SellX action
* Enhanced price recording such that security can also be referenced
by name by the importer

2008-12-08 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed some print issues in the CSS file
* Fixed some more QIF importer and statement reader problems
* Fixed payee matching for name matching mode

2008-12-07 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added more debug output to statement reader
* Added patch provided from Greg Darke to add more settings to OFX plugin

2008-12-04 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added support for manual OFX configuration
* Fixed a sign problem when importing investment sales transations via QIF
* The transaction matcher now uses transaction matching on the same date
in favor over those that match on different dates.
* More fixes to the QIF importer

2008-12-03 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed some more QIF importer problems

2008-12-02 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Remove transfers to/from investment accounts in split transactions
during QIF import

2008-12-01 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed createBudget forecast unit tests to check dates correctly

2008-12-01 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Revisited fix for bko #175949. Using the tab directly to create
a new transaction did not honor the pressed tab
* Updated Czech translation from Marek Stopka

2008-11-30 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Use QT_LDFLAGS and X_LDFLAGS when building the widget library
* Keep position of schedule view if item was deleted

2008-11-29 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Improved QIF date format detection
* Detect dividend transactions as investment transactions even if they
don't have a reference to a stock account

2008-11-28 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Don't create multiple payees if the name contains meta characters
of regular expression (e.g. parans)
* Fixed bko #176316 (kmymoney2 crashes when right clicking in a
categories tax-editing area)

2008-11-27 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* A merged transaction has precedence over an imported transaction
in the ledger
* Fixed more QIF importer issues
* If an imported transaction is also matched, then accepting the
transaction will remove both flags at once
* Allow asset/liability accounts to receive VAT splits

2008-11-26 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Sort accounts by name in home page forecast

2008-11-25 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed starting balance price in investment performance report

2008-11-24 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Store multiple XML statement files for QIF import (debug feature)
* Fixed bko #175949 ('New' transaction does not honor the selected tab)
* Improved detection of existing accounts in QIF importer

2008-11-22 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Reworked QIF import with lots of improvements (incl. patches from
Allan Anderson)
Needs thorough testing and a lot of code cleanup
* Adapted statement reader to work with new QIF importer
* Allow dividend payments w/o reference to security
* Renamed a bunch of i18n-ed html files
* Display correct number of prices in file info dialog

2008-11-22 Fernando Vilas &lt;fvilas@iname.com&gt;
* Added db index from user manual to schema
* Fixed db not logging out when KMM closes (fixes open warning annoyance)
* Fixed transactions query to properly ignore schedule splits (fixes
calculations on homepage)

2008-11-09 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Prevent user from selecting an inconsistent column type for budgets

2008-11-15 Fernando Vilas &lt;fvilas@iname.com&gt;
* Updated SQLite driver detection to work with Qt3 and Qt4. Patch from Tony
Bloomfield

2008-11-12 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated simplified Chinese translation by Roy Qu

2008-11-09 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed budget reports when using non-monthly columns

2008-11-08 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added summary to budget in Home Page
* Added gaps to tables in Home Page

2008-11-08 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added new homepage html file from mvillarino

2008-11-05 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed price calculation in investment performance report

2008-11-05 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated Dutch translation from Bert Keuter
* Added support for imperial prices to MyMoneyMoney ctor
* Added testcase for the above

2008-11-04 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed budget reports when month-by-month are displayed
for non-monthly columns
* Fixed Cashflow summary schedules when multiple occurences
happen during the month
* Added properties initialization to MyMoneyForecast constructor

2008-11-02 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added patch provided by Cristian Onet which fixes a
crash in the the accounts icon view
* Ensure unique IDs within the same statement downloaded
* Removed unnecessary code

2008-10-31 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed http://bugs.kde.org/show_bug.cgi?id=173908

2008-10-30 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Changed "Ok" --&gt; "OK"
* Use KStdGuiItem where appropriate
* Fixed placement of search bar in payees view
* Fixed http://bugs.kde.org/show_bug.cgi?id=173907

2008-10-29 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added signal transactionsSelected() to plugins' viewinterface
* Added signal accountReconciled to plugins' viewinterface
* Updated Romanian translation from Cristian Onet

2008-10-28 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed Forecast unit test

2008-10-28 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated Romanian translation from Cristian Onet
* Added patch by Cristian Onet to solve a ledger sorting problem

2008-10-27 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed sign problem in display of loan information when
sum of additional fees was negative
* Added information when turning on GPG encryption mode
* Added option to turn off title bar of the views

2008-10-26 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed Budget reports when columns longer than a month
* Added Linear Regression method to Forecast History Methods

2008-10-26 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated Italian translation from Vincenzo Reale
* Added patch by Cristian Onet to fix statement marker problem

2008-10-24 Robert Wadley &lt;robntina@users.sourceforge.net&gt;
* Fixed some issues with the html when kmm is used on wide screens
and other small fixes.

2008-10-22 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Make sure to show the correct balance in the ledger view if no
transactions are displayed

2008-10-20 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added default report Income and Expenses By Year

2008-10-19 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated French translation from Patrick Petit

2008-10-19 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed multi currency calculation of transaction reports
* Fixed spelling mistake in Home View
* Changed current Summary to Assets and Liabilities Summary

2008-10-18 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed a problem during online price update when neither the security
nor the currency is the base currency
* Show all categories in both selectors during reconciliation start

2008-10-18 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed calculation of assets and liabilities in CashFlow summary

2008-10-17 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Update total field when changing a budget
* Fixed sign problem when postponing reconciliation of liability accounts

2008-10-15 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Updated Brazilian Portuguese translation by Marcus Gama
* Updated Galician translation by Marcelino Villarino

2008-10-13 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed some Krazy warnings in forecast
* Added CashFlow Summary to Home Page
* Fixed Schedule calculation in CashFlow Summary

2008-10-09 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed balance of general information reports when converting to
base currency

2008-10-07 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed calculation of forecast for stocks which caused an overflow
of MyMoneyMoney members

2008-10-07 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated Dutch translation from Bert Keuter
* Added feature to update all online accounts at once
* Dump some more values
* Fix problem with duplicate dividend transactions

2008-10-06 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated Portuguese translation from José Jorge

2008-10-05 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed multi currency calculation for incomes and expenses in the summary
* Updated Spanish translation

2008-10-04 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated Dutch translation from Bert Keuter
* Tranport price in imported transaction
* Detect previously used fee and dividend categories while importing
investment statements
* Added Romanian HTML files from Cristian Onet
* Fixed failing translation of strings containing an ampersand
character to identify the keyboard shortcut
* Regenerated POT file and merged PO files

2008-10-03 Fernando Vilas &lt;fvilas@iname.com&gt;
* Allow SQLite databases to upgrade

2008-10-03 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added 'Ignore' and 'Skip' buttons to EnterSchedule dialog and show
them during reconciliation start and auto entry during appl startup
* Move icon draw logic from StdTransaction to Transaction to have it
for investment transactions as well
* Display imported investment transaction with the correct background
* Updated French translation from Patrick Petit

2008-10-02 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Show correct message if statement balance is not included in statement

2008-09-30 Wolfgang Rohdewald &lt;wolfgang@rohdewald.de&gt;
* fix valgrind warning about conditional jump depending on
uninitialized variable

2008-09-30 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated Brazilian translation from Marcus Gama
* Make sure to update local variables from engine after chanages
* Removed unused variables

2008-09-29 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Assign id to budget accountgroup if missing or different
* Updated french what's new page from Patrick Petit

2008-09-28 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added Romanian account templates from Onet Cristian

2008-09-27 Fernando Vilas &lt;fvilas@iname.com&gt;
* Implemented patch for occurrenceMultiplier in schedules
provided by Colin Wright
* Added db upgrade path to support the above patch

2008-09-26 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated Italian what's new file provided by Vincenzo Reale

2008-09-25 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed a problem with schedule preview in ledger if end of schedule
falls into preview period. This could cause a lockup of the application.
* Show cleared transactions during reconciliation in a dimmed fashion
* Updated Romanian translation by Chrstian Onet

2008-09-22 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added blank column to Summary
* Added missing file for Home Settings

2008-09-22 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed some typos in German translation, shortened a few strings

2008-09-21 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added option to select most often used transaction for autofill

2008-09-21 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added setting to hide limit column on Home page

2008-09-20 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Show closed accounts in reports and find transactions dialog
if the user has chosen to do so
* Changed the order and rearranged the tables of Summary
* Fix the style divs provided by Robert Wadley

2008-09-16 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed a transaction matcher issue with similar transactions

2008-09-15 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Convert fractions in KMyMoneyAccountTreeForecastItem to make
sure it does not overflow when handling multiple currencies
* Reinitialize m_forecast in Home Page on change

2008-09-14 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added generic code in PivotTable to support multiple
columns while preventing duplication of code

2008-09-12 Fernando Vilas &lt;fvilas@iname.com&gt;
* Updated MMStorageSql to throw MyMoneyExceptions

        