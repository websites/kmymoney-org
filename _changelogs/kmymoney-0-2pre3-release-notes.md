---
title: 'KMyMoney 0.2pre3 Release Notes'
date: 2000-08-23 00:00:00
layout: post
---

0.2pre3

This release fixes the 'bug' where I forgot to include the file opening/
saving code that uses KFileDialog::getOpenFileName instead of the hard link
to a file in my home directory.

When I try using the KFileDialog under KDE 1.1.2 it complains that dcopserver
isn't running and then load the server.  When the program is finished I have
to manually kill dcopserver (and kdeinit) so I can get control back to
KDevelop.

By having the hard link to /home/mte/kmymoney2.kmy I can circumnavigate the
KFileDialog code rather than having to kill dcopserver every time I run the
program.  (I tried running dcopserver from a startup script but it didn't
work.).

If anyone can help me on using KFileDialog within KDE 1.1.2 please email me
at mte@users.sourceforge.net.

(I use KDE 1.1.2 because I use KDevelop 1.2)

Any Help appreciated...
Michael.
  