---
title: '        KMyMoney 5.0.0 Release Notes
      '
date: 2018-02-06 00:00:00 
layout: post
---

<p>The KMyMoney development team is proud to present <a href="http://download.kde.org/stable/kmymoney/5.0.0/src/kmymoney-5.0.0.tar.xz.mirrorlist">version 5.0.0</a> of its open source Personal Finance Manager.</p><p>As with every release, the KMyMoney development team has been working hard
        to make this release better and easier to use in every way. We have also made
        quite a few improvements. We are confident you will like what you see.</p><p>The largest amount of work has gone towards basing this version on KDE
        Frameworks. Many of the underlying libraries used by the application have
        been reorganized and improved, but most of that is behind the scenes, and not
        directly visible to the end user. Some of the general look and feel may have
        changed, but the basic functionality of the program remains the same, aside
        from intentional improvements and additions.</p><p>In addition to adapting to new and updated libraries, there have also been
        numerous bug fixes, as well as several new features and improved
        functionality. Although this means there have been some major changes to the
        underlying code, this version has actually been used in production by many of
        the developers, so it has actually had a significant amount of testing.</p><p>Here are some of the new features found in this release:
          <ul>
            <li>Multiple improvements to reports, including better performance.</li>
            <li>Allow logarithmic axes in report graphs.</li>
            <li>When deleting a security, automatically delete its prices.</li>
            <li>Allow separate beginning balance accounts for different currencies.</li>
            <li>Added support for several new currencies</li>
          </ul>
        </p><p>Here are some of the major bugs which have been fixed:
          <ul>
            <li>Elimination of several crashes.</li>
            <li>Fix problems with report graphs using older data.</li>
            <li>Fix rounding errors in some investment transactions.</li>
            <li>Removed Yahoo from stock and currency price sources, as they no longer provide this service</li>
          </ul>
        </p><p>There are a some changes which will need some adjustments on the user's side if you are upgrading from 4.8.x or an older version
        The program searches for the templates for the printcheck plugin in
        different locations. The user needs to account for this in the plugins
        setting dialog.</p><p>Known issues
          <ul>
            <li>Executing SEPA online transfers are not working. Transaction download is still possible</li>
            <li>Italian documentation requires KDE Frameworks newer. than 5.30.0. In case you run into problems compiling the Italian documentation because you have an older version of the KDE Frameworks, simply remove the file po/it/docs/kmymoney/index.docbook from the released source, re-run cmake and build the application.</li>
          </ul>
        </p><p>For a full list of the changes please check out the <a href="changelogs/ChangeLog-5.0.0.txt">changelog</a>. We highly recommend upgrading to 5.0.0.</p><p>The KMyMoney Development Team</p>