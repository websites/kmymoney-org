---
title: '        KMyMoney 4.6.3 Release Notes
      '
date: 2012-09-03 00:00:00 
layout: post
---

<p>The KMyMoney Team is pleased to announce the immediate availability of KMyMoney version
          4.6.3. This version contains quite a few fixes for bugs found in <a href="release-notes.php#itemKMyMoney462ReleaseNotes">4.6.2</a> since it was
          released seven months ago.</p><p>Here is a list of the most important changes since the last stable release:
          <ul>
            <li>The online statement balance is highlighted if it's different from current file balance <a href="https://bugs.kde.org/show_bug.cgi?id=287494">#287494</a></li>
            <li>Correct the post date of the opening balance transaction when the opening date of the account changes <a href="https://bugs.kde.org/show_bug.cgi?id=293430">#293430</a></li>
            <li>The header state (adjusted column sizes) is now always restored correctly <a href="https://bugs.kde.org/show_bug.cgi?id=294093">#294093</a></li>
            <li>Can be built with Qt 4.8 without patching KDChart <a href="https://bugs.kde.org/show_bug.cgi?id=293448">#293448</a></li>
            <li>Fix a crash on opening SQLite databases <a href="https://bugs.kde.org/show_bug.cgi?id=295235">#295235</a></li>
            <li>MZN as new Mozambique Metical as well as an entry for MZM as the old Mozambique Metical were added</li>
            <li>Attempt to reconnect to the database (once) on accidental disconnects <a href="https://bugs.kde.org/show_bug.cgi?id=286503">#286503</a> and <a href="https://bugs.kde.org/show_bug.cgi?id=294046">294046</a></li>
            <li>Allow the creation of 'Equity' accounts when "Show equity accounts" is checked <a href="https://bugs.kde.org/show_bug.cgi?id=295483">#295483</a></li>
            <li>In the 'Find transactions' dialog trigger a search when return is pressed and there is text entered in the filter <a href="https://bugs.kde.org/show_bug.cgi?id=299525">#299525</a></li>
            <li>Reports related fixes <a href="https://bugs.kde.org/show_bug.cgi?id=267990">#267990</a>, <a href="https://bugs.kde.org/show_bug.cgi?id=296723">#296723</a>, <a href="https://bugs.kde.org/show_bug.cgi?id=290737">#290737</a> and <a href="https://bugs.kde.org/show_bug.cgi?id=283294">#283294</a></li>
            <li>Fixed a large minimum ledger size when using some fonts <a href="https://bugs.kde.org/show_bug.cgi?id=295883">#295883</a></li>
            <li>Fixed SQL sitax in a certain usecase <a href="https://bugs.kde.org/show_bug.cgi?id=250300">#250300</a></li>
            <li>CSV importer plugin related fixes <a href="https://bugs.kde.org/show_bug.cgi?id=302181">#302181</a>, <a href="https://bugs.kde.org/show_bug.cgi?id=287786">#287786</a>, <a href="https://bugs.kde.org/show_bug.cgi?id=295162">#295162</a>, <a href="https://bugs.kde.org/show_bug.cgi?id=296550">#296550</a>, <a href="https://bugs.kde.org/show_bug.cgi?id=296676">#296676</a> and <a href="https://bugs.kde.org/show_bug.cgi?id=296977">#296977</a></li>
          </ul>
        </p><p>For a full list of the changes please check out the <a href="changelogs/ChangeLog-4.6.3.txt">changelog</a>. We highly recommend upgrading to 4.6.3 as soon as possible.</p><p>The KMyMoney Development Team</p>