---
title: 'KMyMoney 0.7.5 Release Notes'
date: 2005-07-29 00:00:00
layout: post
---
2005-07-29 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Released 0.7.5
* Regenerated strings for translations
* Added updated Italian and German translation
* Fixed #1241831 (Encryption/Decryption problem with 0.7.4)
Many thanks to Thomas Schlesinger for his help to resolve this problem

2005-07-29 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* GnuCash Reader
- fix account type error message
- fix sloppy coding errors which I've been getting away with for too long
- update documentation

2005-07-27 Robert Wadley &lt;robntina@users.sourceforge.net&gt;
* Added some screenshots for the user manual for Tony.

2005-07-26 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed problem with total value not including fees when editing
investment transactions (reported by Peter Pointner)
* Fixed #1243365 (Wrong symbol of foreign currency on homepage)
* Hide unused widget in currency editor

2005-07-25 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Enter and show reconciliation values for liability accounts as
positive values

2005-07-24 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* More handbook changes
- Added &amp; edited Roger's Settings chapter
- Retooled introduction chapter
- Added lots of links between various sections
- Added more details on the price editor
- Added authorinfo tags to chapters where they were missing
- Wrote instructions on how to write high-quality bugs
- Various edits throughout the manual

2005-07-24 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed misdetection of EOF in KGPGFile
* Make sure edit session ends when switching from investment transaction tab
to investment summary tab

2005-07-23 Robert Wadley &lt;robntina@users.sourceforge.net&gt;
* Added some investment screenshots to the user manual.

2005-07-23 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* GnuCash Reader - handle Currency account type
- update documentation

2005-07-22 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed problem when opening an encrypted file w/o using the gpg-agent
This was broken due to the changes added on 2005-07-15

2005-07-21 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed crash when selecting print with no report selected

2005-07-20 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Fixed QIF importer to deal with reinvest dividend transactions that have
fees associated with them.

2005-07-20 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed auto increment of check number field to work with non-numeric
entries

2005-07-18 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* More handbook changes
- Explained the price editor
- Finished investment documentation
- Added lots of "Screen Shot" cues for Rob
- Added Rob to the manual credits

2005-07-18 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Allow opening the split editor on new transactions when the
transaction form is not visible

        