---
title: 'KMyMoney 0.7.2 Release Notes'
date: 2005-05-31 00:00:00
layout: post
---
2005-05-31 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Released 0.7.2

2005-05-30 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added detection of html2ps and ps2pdf14 during configure
* Added conditional conversion of project handbook into pdf if
the necessary tools are present
* Don't compress file if named .xml
* Fixed warning about missing argument to QString::arg() in KMyMoneyView

2005-05-29 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Pickup i18n version of whats_new.html
* Added KMyMoneyUtils::findResource() which searches different
filenames to find a translated file
* Added some more Galician files provided by Marcellino
* Added German translation to home page and what's new page
* Added (partial) italian translation provided by Samuel Algisi

2005-05-27 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Calculator widget will clear display upon first digit/comma/period when
opened via the button
* Fixed payee rename to happen immediately
* Don't crash if splash screen is not correctly installed
* Renamed gl_ES.po and es_ES.po into gl.po and es.po
* Updated -fvisibility detection and usage
* Added 'make preview' to online documentation
* Generate PHB using meinproc

2005-05-25 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added support for unsermake (updated to current versions of
cvs.sh and detect-autoconf.sh in the admin subdir)
* Added support for -fvisibility (slightly modified patch
provided by Laurent Montel)
* Fixed build-system in kgncpricesourcedlg.h (I made this change
already for 0.7.1 but it got lost/overwritten in the meantime)

2005-05-23 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Applied patches provided by Laurent Montel
* Added entry for Laurent on the about page
* Added shortcut Ctrl-Ins to start a new transaction/split
* Changed copy split to only work with Ctrl-C

2005-05-23 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Updated help files for reports, investments, and qif/ofx import

2005-05-23 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Gnucash Reader
* Add dialog for Price Source
* Fix stock transfer crash
* Stand-alone anonymizer fix

2005-05-22 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fix selection of investment accounts when account is selected in
another view

2005-05-21 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* First draft of help files for reports, online quotes, and qif/ofx import

2005-05-21 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Bumped version to 0.7.2
* Added == and != operators to MyMoneySecurity
* Added testcases for the above
* Fixed new investment wizard to store modified security information

        