---
title: 'KMyMoney 0.8.0 Release Notes'
date: 2005-08-12 00:00:00
layout: post
---
2005-08-12 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* If auto enter is selected for a schedule, enter it if it's due today
not only if it is overdue
* Fixed #1256431 (tx switch from Dep. to Withdr.)
* Released 0.8

2005-08-09 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added progress to the progress bar when importing a KMM statement
* In KMM statement importer, post dividend income to "_Dividend" account
* "By institution" reports now group by institution &amp; topaccount
* Accounts with no institution now get their institution from the topaccount
* Investment accounts are not included in account-based query reports

2005-08-09 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Make sure the Finish button is enabled on the last page of
the edit loan wizard
* Update schedule view when a new loan account has been added
* Don't show error if schedule has no payee assigned
* Fixed a problem in KFilterDev by providing our own version
of QIODevice::readAll() in mymoneystoragexml.cpp This caused
the program to lock up on certain compressed files

2005-08-08 Robert Wadley &lt;robntina@users.sourceforge.net&gt;
* Changed my email address as listed in the docs
* Assorted image resolution tweeks to improve PDF output.

2005-08-06 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Send out notification when report is added to engine

2005-08-06 Robert Wadley &lt;robntina@users.sourceforge.net&gt;
* Added the 0.8 splash screen and titlebar image.

2005-08-06 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Edited loan docs

2005-08-06 Robert Wadley &lt;robntina@users.sourceforge.net&gt;
* Adjusted image sizes for PDF output.

2005-08-05 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Spelling fixes in loans docs from Darin

2005-08-05 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added --enable-pdf-docs to configure which controls the generation
of the PDF version of the developer handbook and the user manual.
This setting defaults to 'no'
* Fixed a problem around creating symbolic links to header files in
VPATH build environments
* Added online help to
- settings dialog
- QIF profile editor
- Schedule dialog

2005-08-03 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed #1250608 (Calculator calculates wrong)
* Fixed crash when using option 'Goto other side of transfer transaction'

2005-08-03 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Spelling fixes in loans docs from Darin

2005-08-02 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Don't show the 'ancient' currencies in the online update dialog

2005-08-01 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Handle 'yield' transactions in reports. Previously, they were
universally ignored.
* New Loans chapter for manual from Darin.
* Updated search chapter for manual from Darin.

2005-08-01 Robert Wadley &lt;robntina@users.sourceforge.net&gt;
* Added more screenshots for the manual
* Clarified some text in the manual (Schedules)
* Made some small changes in the manual for consistency.(First time)

2005-07-31 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added a "continue/cancel" dialog when there is a problem importing
a single transaction. Otherwise, the user will keep getting the
error dialog continuously in a large file.
* Fixes a bug in the investment performance report: "shares bought on
the report start day are included in the starting balance and in buys.
The solution is to take the starting balance from the day before the
start of the report."
As submitted by Peter Pointner &lt;pml1@wuzel.de&gt;

2005-07-31 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* GnuCash Reader - implement (and document) ancient currency support

2005-07-31 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added auto loading of 'ancient' currencies to
KMyMoneyView::loadAncientCurrencies
* Added all currencies that were converted to EURO to this list
* Maintain setting of 'update price history' between sessions
* Correctly interpret the amount entered for foreign currency
transactions (required a TAB to update the values before pressing OK)
* Do not allow to modify ancient currency prices

2005-07-30 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Maintain identification entered for investments
* Modified home view to catch exceptions when information is somewhat
inconsistent

2005-07-30 Robert Wadley &lt;robntina@users.sourceforge.net&gt;
* fixed some screenshot mistakes

2005-07-30 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* GnuCash Reader - handle transactions without a currency

        