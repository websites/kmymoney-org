---
title: 'KMyMoney 0.9.2 Release Notes'
date: 2008-09-12 00:00:00
layout: post
---
2008-09-12 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed sign problem with paid dividends
* Updated German Whats New page
* Updated Portuguese translation as provided by Jose Jorge
* Updated French Whats New page
* Released 0.9.2

2008-09-11 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed calculation of paid dividends for return of investment
* Updated Spanish Whats New page

2008-09-10 Robert Wadley &lt;robntina@users.sourceforge.net&gt;
* Updated the "whats new in this release" page.

2008-09-09 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added skip icon for schedules to home view
* Moved icons in front of schedule names
* Added tooltips to the icons and schedule links
* Fixed bug in transaction matcher when two or more identical transactions
are carried out on the same day and are manually entered already.

2008-09-08 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed edit loan wizard to keep changes made to interest rate

2008-09-07 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added Budget, Budget Difference and Forecast to exported
report CSV

2008-09-07 Fernando Vilas &lt;fvilas@iname.com&gt;
* Fixed primary key error when adding first items to database
* More database documentation updates
* Added simple test cases for budgets
* Fixed budget test cases for the case when a database is not installed

2008-09-07 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Display information why Financial Summary link is not working

2008-09-06 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Changed internal structure of PivotGrid to work as a QValueList
* Modified PivotTable and unit tests to accomodate to the new PivotGrid
design
* Implemented new methods for drawing charts by level and avoid duplicating
code

2008-09-06 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed Wolfgang's modification

2008-09-06 Wolfgang Rohdewald &lt;wolfgang@rohdewald.de&gt;
* Show the file name in the passphrase dialog

2008-09-05 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Don't remove statement marker if it's the last entry in the ledger
* Don't present balance warnings when entering schedules that will be matched
* Check 'Type' keyword in QIF case independant
* Updated German translation

2008-09-04 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Initial code for drag and drop for attachments (not functional yet)
* Added documentation update provided by Colin Wright
* Updated screen shots for documentation of new account wizard
as received in patch #2038235

2008-09-02 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Prevent numeric overflow when calculating value of
stocks traded in different currency
* Delete StatementKey during account unmap
* Show last online balance in ledger
* Added Debian/Ubuntu specifc build instructions to README
* Improved sorting in ledger for items with same post date
* Don't allow create schedule option for investment accounts

2008-09-01 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed a false popup dialog when editing a schedule from within the ledger
* Added name based detection of brokerage account to QIF Importer and
statement reader
* Preset statement balance to autoCalc to be able to detect if the
balance was filled in by the importer/online module
* Don't remove account from selector if it has children. Rather make
it not selectable
* Fix sortorder of scheduled and non-scheduled transactions in
ledger when they have the same post date

2008-08-31 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added settings to exclude future or scheduled-transactions from forecast

2008-08-31 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added Romanian translation provided by Cristian One
* Regenerated POT file
* Merged message files

2008-08-30 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Don't show overdue payments in the past

2008-08-29 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Reset the last payment date when duplicating a transaction

2008-08-28 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added Spanish translation of Home and Whats New pages

2008-08-28 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added functionality to wizard's help button
* Set standard context for new file setup
* Disabled warning about deprecated fileformat of version 0.4

2008-08-27 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Don't select scheduled transactions during Ctrl+A processing
* Wrap networth graph on homepage into frame
* Reload widget when delting splits in the split editor
* Updated Italian translation from Vincenzo Reale

2008-08-25 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Changed styles of Home Page - Patch provided by David Houlden

2008-08-26 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed a problem with syncing the hierarchy tab in the account edit dialog
* Added possibility to select scheduled transaction in register and
open the schedule context menu for them

2008-08-25 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Reload forecast on Home Page when file is changed
* Set the title of legend on report charts

2008-08-25 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Allow direct editing and entering of schedules from Home page
* Don't allow deletion of transactions referencing closed accounts
* Fixed endless loop in transaction preview for one time schedules

2008-08-24 Fernando Vilas &lt;fvilas@iname.com&gt;
* Updated OCI values in db code based on comments from Colin Wright.
(Untested)
* Removed some unused db code.
* Changed closing behavior of db to make it less likely to remain open.
(More to come on this one.)

2008-08-24 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed format of value and column name in Account Info Reports
* Fixed display of AutoCalc values in Schedule Info Reports
* Do not show closed account in Loan Info Reports
* Fixed display of value in Schedule Reports
* Fixed initialization of PivotCell to check m_cellUsed - #2070446
* Added a flag to MyMoneyForecast to tell when forecast is done
* Added forecast balance after payments to Home Page
* Changed alignment of totals on Summary

2008-08-24 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed sign of equity balance in accounts and ledger view
* Fixed some stuff in the anonymous file writer

2008-08-23 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added borders to tables on Home Page
* Fixed alignment on Home Page
* Use the currency symbol rather than the name in Information reports
* Don't display closed accounts in Account Information reports
* Show balance of subaccounts of an investment in account information
* Show 'next due date' only in the first row of the schedule information
report
* Changed style of intermediate column in Summary - patch provided by
David Houlden
* Code cleanup in budget forecast tree
* Fixed html tag in Summary - patch provided by David Houlden

2008-08-23 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Store name of new file in 'recent files' list

2008-08-22 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Add type of Information Reports when reading and writing reports
* Fixed style of report column headers
* Added account to payments on Home Page

2008-08-22 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Turn off internal memory leak checker by default
* Added option to display scheduled transactions in ledger
* Don't allow creation of transactions in closed accounts
* Got rid of 'type qualifiers ignored on function return type' warning
* Use occurenceToString() from MyMoneySchedule instead of KMyMoneyUtils
* Disable the split button of the category until the account is known
* Don't keep elements from file that are not properly read
* Fixed a problem with the investment value display in the ledger view

2008-08-21 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Branches of forecast trees now show the total of subaccounts if closed,
and revert if opened
* Fixed running sum of schedule-based forecast when creating a budget -
Reported by Marko Käning

2008-08-21 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Allow creation of Equity additional accounts
* Allow editing transactions in Equity accounts
* Renamed TransactionMatcher::scheduledTransaction() into
KMyMoneyUtils::scheduledTransaction()

2008-08-20 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Be somewhat relaxed when searching an account based on information
provided by online banking providers

2008-08-19 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added web price quote for Financial Express as provided by P. Hargreaves
* Fixed crash on shift-select

2008-08-19 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Make branches in forecast trees open by default

2008-08-18 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed styles in Home Page. Reported by Pallavi Damera.

2008-08-18 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Don't allow creation of transactions when no account is selected
* Allow application override before initial OFX connection with bank
* Fixed a duplicate i18n call as pointed out by Alexander Kireev
* Reduced min height of the KAccountTemplateSelector widget
* Don't allow online account update if provider is not present

2008-08-17 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added style to headers in Home Page. Reported by Pallavi Damera.
* Added hierarchy to summary and detail forecast. Accounts are now shown
even if not in the forecast when child accounts are in forecast.
* Changed budget forecast to be shown in a tree

2008-08-17 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Actually use override value for OFX app id and version
* Don't allow override for libOFX &lt; 0.9

2008-08-16 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed schedule loader to not forget data about finished items
* Removed extranous EXTRA_DIST entry in templates
* Fixed precedence of multiplication over addition in calculator widget
* Show the error message provided by the bank when importing an OFX file

2008-08-16 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed Forecast Summary. The tree was being shown in a different tab.

2008-08-15 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added application override to OFX plugin
* Added search filter to GUI of OFX institution setup wizard

2008-08-14 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Fix sqlite logout problem

2008-08-14 Wolfgang Rohdewald &lt;wolfgang@rohdewald.de&gt;
* globally catch all uncaught kmymoney exceptions

2008-08-14 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Improvement for fix of #1866881: keep focus item
* Applied patch provided by Alexis Lahouze
* Fixed testcases
* Added status display for statement import
* Keep changes to category when changing parent account in same edit session

2008-08-13 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed endless loop when canceling the entry of a schedule during
reconciliation startup
* Make sure to have the current selected split as first split in the schedule
* Fixed #1866881 (Show Transaction Detail:scroll view to current record)

2008-08-11 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed consistency check to modify share instead of value field if they
have to contain equal values.
* Updated Brazilian translation from Marcus Gama
* Updated Simplified Chinese translation from Roy Qu
* Fixed a payee assignment and display problem with schedules

2008-08-09 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Forecast Summary and Detail are shown in a tree
* Code cleanup in KForecastView
* Added method to show negative numbers in red to
KMyMoneyAccountTreeBaseItem

2008-08-09 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated German translation provided by yllohy
* Fixed reload of accounts and categories view after changes when
a quick filter was active

2008-08-08 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Allow entering of overdue schedules before reconciliation

2008-08-06 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed payee matching (was a bit broken by change on 2008-08-03)
* Added MyMoneySeqAccessMgr::m_priceList to transaction based handling
* Modified testcases to accompany the above change
* Updated Portuguese translation by José Jorge

2008-08-05 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added logic to remove payees that were created during statement
import but are unused.

2008-08-04 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Allow to move transactions to different category in category ledger
* Updated pot file and merged into message files
* Fixed override for account type during OFX statement requests
* Added feature to unmap an online account

2008-08-03 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Cleaned code in forecast and reports
* Added check in QueryTable to prevent infinite loops in cases
of splits data inconsistency

2008-08-03 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Removed override for OFX money market accounts for LibOFX 0.8
* Use the payee as stored with a manually entered transaction in
favor of the imported transaction after matching
* If the manually transaction does not have a payee assigned
use the one found in the imported transaction
* If multiple payees match use the one that is found most often
in the account that is imported

2008-08-02 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Avoid sending out dataChanged() signal when no changes were
made to the data but a transaction ends.

2008-08-01 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added link to ledger to budget overruns in home page

2008-08-01 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed OFX override for LibOFX 0.8
* Renamed MyMoneyTransactionFilter::untilToday
into MyMoneyTransactionFilter::asOfToday
* Added patch provided by Colin Wright

2008-07-31 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Include transactions with the same date as the statement date
in reconciliation
* Show information about deposits and payments during reconciliation
* Added OFX override for money market accounts

2008-07-30 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Updated Argentinian translation
* Added report of transactions by reconciliation status
* Fixed order of schedules info report
* Fixed grouping of account and loan info reports
* Removed category column of schedule summary info report
- Patch provided by Marko Käning
* Fixed interest rate in loan info report

2008-07-30 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Respect preferred account setting from new account wizard
* Updated French translation from Patrick Petit
* Added patch provided by Alexis Lahouze with small modifications
* Added StatementGroupMarker object

2008-07-29 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed reload of payees and schedule view after changes when
a quick filter was active
* Added transaction matcher for schedules
* Cleanup statement reader call in application
* Catch exception thrown by user abort during import

2008-07-28 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed balance column in info reports
* Changed schedule info report to sort by next due date

2008-07-27 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added report for loan information
* Fixed duplicate column in account information report

2008-07-27 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated the online price update dialog and allow to quit out
of the whole operation

2008-07-26 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Only create imported user once

2008-07-25 Fernando Vilas &lt;fvilas@iname.com&gt;
* Fixed compilation issue from previous commit

2008-07-25 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Comment generation of request.ofx in OFX plugin
* Added auto transaction matcher to statement reader
* Added quick filter for matched transactions
* Renamed a button in the new user wizard

2008-07-23 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed memo filter to schedule report
* Added account information report

2008-07-23 Joerg Rodehueser &lt;wampbier@users.sourceforge.net&gt;
* Did some code cleaning to avoid compiler warnings
* Added the kdchar source to the cmake build
* Added missing files to CMakeLists.txt

2008-07-23 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Escape special characters in payee names before adding to match list
* Prevent crashes in MyMoneyMoney::price() in case m_shares is 0
which it should not ever be
* Code cleanup (performed after Joerg's above)

2008-07-22 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Workaround to get price for starting balance in performance
report even when there is no price
* In performance report add splits without action to buy or sell

2008-07-22 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Automatically add copy of deleted payee names to match list of
replacement payee
* Default payee matching to ignore case when first turned on

2008-07-21 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed investtransaction editor to work with newly created categories
and securities

2008-07-20 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Renamed 'Amount' column in investment ledger to 'Quantity'
* Fixed a compile problem with libofx &lt; 0.9 being used
* Show multiple accounts with the same name in account icon view
as multiple icons
* Fixed placement of calculator to make sure it's not outside the desktop

2008-07-19 Fernando Vilas &lt;fvilas@iname.com&gt;
* Added db support for new file fix version
* Fixed how file fix version is handled in db code
* Fixed incorrect balance when a transaction zeroes an account
in the db
* Added db test case for adding a transaction to zero an account

2008-07-19 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Replaced IRR with XIRR from KOffice by Sascha Pfau,
modified to fit the way querytable works

2008-07-18 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated German translation from yllohy

2008-07-18 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added Turkish translation from Serdar Soytetir
* Keep memo for category memos in sync with the one visible
* Added code for autofix after load (but not yet active)
Needs further testing, esp. with the database backend
* Updated Italian translation from Vincenzo Reale

2008-07-16 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed text filtering of splits in transaction reports

2008-07-15 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed dependencies for some xxxToString() methods

2008-07-15 Fernando Vilas &lt;fvilas@iname.com&gt;
* Added KVPs to splits in the db
* Updated (some) documentation of db functions
* Fixed MMInstitution to properly store KVP in the db

2008-07-15 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated Italian translation from Vincenzo Reale
* Added option to specify the default reconciliation state for
transactions entered during reconciliation
* Added capability to enter interest splits for a buy share transaction
* Allow deletion of transactions even if the transaction carrying the focus
is currently not selected
* Added KVPs to MyMoneySplit
* Don't write empty KVPs to the file
* Added initial implementation of TransactionMatcher
* Fixed MyMoneyTransaction::operator == to include the KVPs
* Added some testcases for that
* Try to avoid asking for an account if clear for which data is imported
(tested for HBCI, not OFX)
* No need for Fees in Reinvest Dividend transaction
* Updated About KMyMoney info dialog

2008-07-14 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added missing classes to CVS
* Added split information to Schedules report
* Added Schedule report filter by account
* Added new Schedule report without splits

2008-07-13 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Removed AccountTypeToString method from PivotTable. Have it use
KMyMoneyUtils method
* Created ListTable to handle common method and data for QueryTable
and ObjectInfoTable classes
* Modified QueryTable test cases for the new class hierarchy

2008-07-12 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added report to list all schedules

2008-07-06 Fernando Vilas &lt;fvilas@iname.com&gt;
* Fixed unit tests to be less annoying in the absence of a db
* Fixed an error in the last db code commit

2008-07-04 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Cleaned MyMoneyMoney objects init in KHomeView

2008-06-21 Fernando Vilas &lt;fvilas@iname.com&gt;
* Added database unit test
* Modified database code to pass unit test
* Corrected some documentation in mymoneyfile.h

2008-06-29 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added some information to README file on how to compile and
necessary packages

2008-06-24 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Changed KMyMoneyFrequencyCombo to get the text from KMyMoneyUtils
to avoid code duplication

2008-06-24 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Implemented creation of budget based on scheduled and future
transactions
* Cleaned up schedule forecast code
* Added unit test for scheduled forecast budget
* Removed the dependency of occurenceToString of KMyMoneyFrequencyCombo

2008-06-17 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* In summary of the home page, show scheduled transfers
on its own

2008-06-20 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed VPATH include problems
* Fixed default order of account tree columns

2008-06-17 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Modified layout of summary tab in forecast view

2008-06-18 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Pretend to be Quicken 2008 during OFX download

2008-06-17 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* In home page show maximum credit instead of minimum balance
depending on account type

2008-06-17 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added patch supplied by Colin Wright adding a missing parameter
* Updated Swedish translation from Konrad Skeri

2008-06-16 Wolfgang Rohdewald &lt;wolfgang@rohdewald.de&gt;
* fixed restoring the layout in some tree views

2008-06-15 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Simplified code
* Changed function names to follow coding standards
* Keep KMyMoneyAccountTreeBaseItem a pure virtual class
* Fix default order of columns in account tree

2008-06-14 Wolfgang Rohdewald &lt;wolfgang@rohdewald.de&gt;
* Asset/Liability accounts can now be defined as VAT accounts.
They cannot yet be used as such however.
* edit account/category: make hierarchy display work again after
refactoring the AccountTree classes

2008-06-13 Wolfgang Rohdewald &lt;wolfgang@rohdewald.de&gt;
* Accounts view, icon tab: Do not show closed accounts if not wanted
* "show closed accounts" now also shows/hides closed stock
* new base classes ...AccountTreeBase{Item}. AccountTree and
AccountTreeBudget are derived from this.
* reduced duplicate code in these classes and simplified
some things - hopefully without breaking anything.
* new helper function const QString formatMoney(...)

2008-06-12 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Improved keyboard navigation in ledger view
* Removed compiler warning for KMyMoneyView
* Cleaned code in KOfxDirectConnectDlg::slotOfxFinished

2008-06-11 Wolfgang Rohdewald &lt;wolfgang@rohdewald.de&gt;
* Edit category / account: Show old name in caption
* Fix VAT percentage and a few typos in german template file skr03.kmt
* new methods for MyMoneyAccount: accountPixmap and accountGroupPixmap

2008-06-09 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Reduce duplicate code

2008-06-08 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed investment performance calculation

2008-06-07 Wolfgang Rohdewald &lt;wolfgang@rohdewald.de&gt;
* transactions touching closed accounts can no more be deleted/duplicated
* transactions with frozen splits can no more be deleted
* warn before deleting transactions with reconciled splits
* new class KMStatus for simpler display of status messages

2008-06-05 Wolfgang Rohdewald &lt;wolfgang@rohdewald.de&gt;
* globally replaced QValueList &lt;SelectedTransaction&gt; by the new class
SelectedTransactions. Right now it is almost empty, so this patch
is mostly syntactic sugar. Exception: constructor(const Register* r)

2008-06-05 Wolfgang Rohdewald &lt;wolfgang@rohdewald.de&gt;
* In home view and account tree, rename Balance to Current Balance
resp. Total Balance

2008-06-05 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added (slightly modified) patch from Colin Wright which
adds an account hierarchy page to the new account wizard
* Added a refined patch from Colin Wright for the new account wizard
* Fixed ledger sorting in i18n-ed version of KMyMoney
* Added some debug output to OFX direct connect

2008-06-03 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Unsubtle fix for memcheck to compile on GCC4.3

2008-06-02 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Fix (some) GCC4.3 compile errors

2008-06-01 Fernando Vilas &lt;fvilas@iname.com&gt;
* Fixed compilation issue of db code on Fedora Core 9

2008-05-30 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Improved calculation of investment balances in query reports
* Calculate base currency price for investment based on
transaction date in query reports
* Fixed decimals for Return On Investment that were being cut off

2008-05-31 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed payees view when changing existing match patterns
* Added MyMoneyTransaction::isImported()
* Fixed a bug which caused the sign of splits be reverse when
entering split transactions in the register w/o using the form

2008-05-30 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Improved button signal handling for database selection dialog
* Improved encryption setup (differentiate between main key and
additional keys)

2008-05-30 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added total profit/loss to summary on home page

2008-05-29 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Remove all Sqlite2 references
* Improve usability of DB selection dialog
* Coding changes

2008-05-29 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added ability to enter negative numbers with a leading dash even
if display format is "parens around".
* Improved some code changes made by Wolfgang
* Updated German translation from Yllohy
* Updated Brazilian translation from Marcus Gama
* Don't allow editing transactions that reference closed accounts

2008-05-28 Wolfgang Rohdewald &lt;wolfgang@rohdewald.de&gt;
* report view: correctly order the report groups by number
* fix one more usage of an uninitialized variable
* category ledger: For the payee, the strings "Paid to" and "From"
were interchanged

2008-05-27 Wolfgang Rohdewald &lt;wolfgang@rohdewald.de&gt;
* fixed segfault when doubleclicking on an expense category in
the category view

2008-05-26 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Moved all OFX relevant code into the OFX plugin
* Support encryption to more than one GPG key
* Added plugin specifc tab to account editor dialog for online support
* Added array operator to MyMoneyKeyValueContainer
* Support multiple strings (actually regexp's) for payee matching
* Added capability to store multiline notes with a payee record
* Store bank routing number and account id in MyMoneyStatement
* Removed KMyMoneyImporterPlugin service type
* Improve speed for database access by using MyMoneyFile's cache
* Fixed some testcases for the above change

2008-05-26 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Remove Sqlite v2 support
* Fix Sqlite v3 detection
* Clarify dialog help text

2008-05-25 Wolfgang Rohdewald &lt;wolfgang@rohdewald.de&gt;
* add new budget did throw away changes without asking
to save them
* budgetview: hide unused accounts had problems. This
fixes bug 1962065. Toggling this no longer asks if
the user wants to save the budget. The checkbox is
now disabled if the budget has no used accounts.
* fix bug 1575585: Loan with 0% interest (division by 0)

2008-05-24 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed manual forecast in forecast view

2008-05-25 Wolfgang Rohdewald &lt;wolfgang@rohdewald.de&gt;
* Fixed segfault after deleting an investment

2008-05-24 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added missing enum item to report date enum
* Fixed date when reading a custom saved report

2008-05-24 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Reverted some changes introduced during DB integration
* Fixed reference for file inclusion in Makefile.am

2008-05-24 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Prepare sqlite3 support
* Fix create views in mysql

2008-05-24 Wolfgang Rohdewald &lt;wolfgang@rohdewald.de&gt;
* fixed keyboard handling for accelerators in transaction editor
* fixed accelerator for the Tools main menu: Was T although the o
was underlined

2008-05-23 Fernando Vilas &lt;fvilas@iname.com&gt;
* Updated database engine to support new KMM engine.

2008-05-22 Wolfgang Rohdewald &lt;wolfgang@rohdewald.de&gt;
* Summary: Fix computation of investment accounts when foreign
currencies are involved

2008-05-20 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* For budget or forecast only show actual in the charts if set to
do so
* Switch networth forecast graph in the home page to use new forecast
pivot report. Changed timeframe to current date + 90 days.
* Fixed bugdet graph configuration

2008-05-20 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Sort Summary accounts by name
* Save includesForecast setting in pivot reports

2008-05-19 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added forecast calculation to pivot reports
* Added group of forecast default reports
* Skip total calculation of budget or forecast if report does not
include them
* Added setting to not purge unused forecast accounts if required
* Fixed investment calculation in home page. Calculate deep currency
not base currency.
* Set includeForecast to false when reading a report from file

2008-05-19 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Don't show '&amp;nbsp;' in abscissa labels of charts

2008-05-19 Wolfgang Rohdewald &lt;wolfgang@rohdewald.de&gt;
* History of transactions for payees could show wrong action for
transactions entered with a pre 0.9.0 version

2008-05-18 Wolfgang Rohdewald &lt;wolfgang@rohdewald.de&gt;
* focus handling in all ledgers for keyboard users: Avoid segfault
if no item had focus and first item is not focussable like in
search window
* when a new search window is generated, select top item
* when entering CTRL-F to get to the search window, make sure that
the search window always gets focus. If it already existed, this
did not always happen
* dragging a category in the tree segfaulted when there was no icon
defined for the category. Can happen when kmymoney2 cannot find its
resources.

2008-05-18 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated German translation from yllohy

2008-05-17 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Applied patches provided by Wolfgang Rohdewald
* Added logic to deal with deleting a category still assigned
to a budget
* Fixed logic that deletes categories to work correct even if no
transaction is assigned to that category but it is referenced
by a schedule or budget

2008-05-16 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated Dutch translation from Bert Keuter
* Fixed crash when saving institution data from editor that was opened
using a double click in the institution view
* Updated Portuguese translation from José Jorge

2008-05-15 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed keyboard handling in split editor widgets
* Updated German translation from Marko Kaening
* Updated Simplified Chinese translation from Roy Qu

2008-05-14 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed forecast settings refresh

2008-05-12 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated Dutch translation from Bert Keuter
* Updated German translation from Marko Kaening
* Regenerated POT file
* Bumped version number to 0.9.1
* Bumped min KDE version to 3.4.0

2008-05-12 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated Dutch translation from Bert Keuter
* Updated German translation from Marko Kaening
* Added Sweden to stats.pl for translations

2008-05-11 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* New files will be name $HOME/$USER.kmy

2008-05-08 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Updated Argentinian translation

2008-05-10 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Make menu key work in split editor

2008-05-09 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed precision problem when opening the split transaction editor
for fees/interest in investment transactions

2008-05-08 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed selection of date range combo in filter custom dialog

        