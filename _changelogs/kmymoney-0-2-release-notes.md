---
title: 'KMyMoney 0.2 Release Notes'
date: 2000-08-27 00:00:00
layout: post
---

0.2 FINAL

- Added i18n where appropriate.  Made some of the list view code more readable.
- Tried to sort out the tab orders but KDevelop is awful in that respect.  Most dialogs
        work as expected but a couple have their quirks.
- Added some toolbar buttons and enabled them in the code.
- Added a picture to the start dialog.
- Made the tabbed dialog use less screen estate.  (The widgets are only 20
            pixels high now instead of the default 30.  This MAY make it slightly
            harder to see.  Mail me if you want the widgets returned to their
            proper heights).
  