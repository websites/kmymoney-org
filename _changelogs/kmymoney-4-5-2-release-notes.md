---
title: '        KMyMoney 4.5.2 Release Notes
      '
date: 2010-12-21 00:00:00 
layout: post
---

<p>The KMyMoney Team is pleased to announce the immediate availability of KMyMoney version
          4.5.2. This is a bugfix version from the 4.5 series.</p><p>Here is a list of the most important changes since the <a href="release-notes.php#itemKMyMoney451ReleaseNotes">4.5.1</a> release:
          <ul>
            <li>fixed several crashes like when adding transactions directly into the register (on Qt 4.7.1) <a href="https://bugs.kde.org/show_bug.cgi?id=258355">#258355</a>,
              when cancelling the QIF importer date selection dialog <a href="https://bugs.kde.org/show_bug.cgi?id=259235">#259235</a>, importing GnuCash files with zero valued prices <a href="https://bugs.kde.org/show_bug.cgi?id=256282">#256282</a></li>
            <li>fixed a problem with Finance::Quote <a href="https://bugs.kde.org/show_bug.cgi?id=257260">#257260</a></li>
            <li>it is now possible to use KMyMoney with small resolutions (1024x600) <a href="https://bugs.kde.org/show_bug.cgi?id=258460">#258460</a>
              and <a href="https://bugs.kde.org/show_bug.cgi?id=256769">#256769</a></li>
            <li>improved some small user interface issues
              <a href="https://bugs.kde.org/show_bug.cgi?id=259437">#259437</a>,
              <a href="https://bugs.kde.org/show_bug.cgi?id=258466">#258466</a>,
              <a href="https://bugs.kde.org/show_bug.cgi?id=257986">#257986</a>,
              <a href="https://bugs.kde.org/show_bug.cgi?id=257761">#257761</a>,
              <a href="https://bugs.kde.org/show_bug.cgi?id=257369">#257369</a></li>
            <li>fixed a usability issue when using the <i>Include sub-accounts</i> feature with budgets <a href="https://bugs.kde.org/show_bug.cgi?id=255135">#255135</a></li>
            </ul></p><p>For a full list of the changes please check out the <a href="changelogs/ChangeLog-4.5.2.txt">changelog</a>. As with the previous bugfix versions we recommend upgrading to 4.5.2.</p><p>Merry Christmas!</p><p>The KMyMoney Development Team</p>