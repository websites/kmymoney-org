---
title: '        KMyMoney 4.8.0 Release Notes
      '
date: 2016-06-14 00:00:00 
layout: post
---

<p><a href="http://download.kde.org/stable/kmymoney/4.8.0/src/kmymoney-4.8.0.tar.xz.mirrorlist">KMyMoney version 4.8.0</a> is now available. This version contains fixes for several bugs, here is a short list:
          <ul>
            <li>Added support for online SEPA transactions</li>
            <li>Added support for import from Weboob</li>
            <li>Improved payee matching when importing transactions</li>
            <li>Supports AqBanking versions 5.5.0 or later</li>
            <li>Updated Brazilian account templates</li>
            <li>When an account cannot be closed, a tooltip explains the reason</li>
            <li>Categories no longer have opening date, which caused annoying errors
              both during input and while running the consistency check</li>
            <li>Do not inadvertently modify start date of schedules</li>
            <li>Solved rounding problems causing reconciliation to fail and
              investment transaction to show missing assignments of 0.00</li>
            <li>Fixed some annoying consistency check errors</li>
          </ul>
        The full list of solved issues can be viewed in <a href="https://bugs.kde.org/buglist.cgi?bug_status=RESOLVED&amp;f1=cf_versionfixedin&amp;f2=cf_versionfixedin&amp;j_top=OR&amp;list_id=1316030&amp;o1=substring&amp;o2=substring&amp;product=kmymoney&amp;query_format=advanced&amp;resolution=FIXED&amp;v1=4.8&amp;v2=4.7">KDE's issue tracker</a>.</p><p>Thanks to the KDE translation teams the following new translations were added: documentation in Estonian.</p><p>For a full list of the changes please check out the <a href="changelogs/ChangeLog-4.8.0.txt">changelog</a>. We highly recommend upgrading to 4.8.0.</p><p>The KMyMoney Development Team</p>