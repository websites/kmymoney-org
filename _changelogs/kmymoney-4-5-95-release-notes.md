---
title: '        KMyMoney 4.5.95 Release Notes
      '
date: 2011-06-04 00:00:00 
layout: post
---

<p>It's been almost 9 months since the last stable version is out on the
          street. During that time, lots of bugs were uncovered, and lots were
          fixed. So much so, that the team has decided it's time to get on the
          treadmill toward another stable release.</p><p>KMyMoney 4.5.95 is now available for download. It is KMyMoney 4.6 Beta
          1, only suitable for advanced users willing to help us stabilize and
          iron out the upcoming stable version.</p><p>Your help is important. Please download it, use it, and help us find issues to make a great release.
          Keep in mind that this version will still bark your dog away. Make
          extensive backups before using it.</p><p>Our plan is to have a stable version ready in August. You can find the
          schedule <a href="http://techbase.kde.org/Projects/KMyMoney#Release_schedule">here</a>.</p><p>Some of the highlights since the latest stable version are:
          <ul>
            <li>New CSV import plugin</li>
            <li>Many fixes for the OS X and Windows platform</li>
            <li>Performance improvements (although we know there's still work to do in this regard)</li>
            <li>Many fixes in the import and online banking modules</li>
            <li>Over 100 bugs have been fixed since the latest bugfix release alone</li>
          </ul>
        </p><p>For a full list of the changes please check out the <a href="changelogs/ChangeLog-4.5.95.txt">changelog</a>.</p><p>The KMyMoney Development Team</p>