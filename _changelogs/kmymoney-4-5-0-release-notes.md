---
title: '        KMyMoney 4.5.0 Release Notes
      '
date: 2011-08-16 00:00:00 
layout: post
---

<p>The KMyMoney is pleased to announce the release of a stable version
          for KDE Platform 4. With over 15 months of development, this new
          version is the starting point of a new series of KMyMoney versions to
          leverage the stellar features offered by the KDE Platform.</p><p>Throughout this effort, our focus has been to maintain feature-parity
          with previous versions, maintaining and improving the high level of
          quality that your personal financial data deserve. This version has
          gone through an intensive testing, allowing us to find and fix many
          bugs and improve the usability of the application.</p><p>Along the way, we did manage to add some new features:
          <ul>
            <li>Better documentation</li>
            <li>Works with the latest version of AqBanking</li>
            <li>Improved usability of the online banking features</li>
            <li>Uses KWallet to store online banking passwords</li>
            <li>The consistency checks runs automatically before saving your data,
              it now checks for a wider range of problems, and it automatically
              corrects many of them.</li>
            <li>Runs in all operating systems supported under KDE</li>
          </ul>
        </p><p>Since the release candidate in May, about 70 bugs were fixed, and over
          400 commits were made to the code alone, not including documentation
          and translations. The application is now fully Qt4 native and is ready
          to accept new features.</p><p>In the process, we are also moving from Sourceforge to KDE. For this
          release, we have created a new mailing list for users only,
          kmymoney@kde.org, which replaces the kmymoney2-users list.</p><p>The release of a stable version for KDE4 also marks the beginning of
          the end for the KDE3 series. In maintenance mode since 1.0.0, it is
          progressively going into frozen state. New features will be added to
          the KDE4-series only, with the KDE3 code receiving only critical
          fixes.</p><p>All in all, we believe that the port to KDE4 has grown a better
          application, and even more important, a better team. We have asked you
          for patience while we took this much needed step, now are ready to
          start adding new features again. Right after we take some a deep
          breath and some rest, because this has been very stressful to all of
          us.</p><p>The KMyMoney Development Team</p>