---
title: 'KMyMoney 0.1-220500 Release Notes'
date: 2000-05-22 00:00:00
layout: post
---

0.1-220500
- Some more bugs fixed and introduced !.  This is a file release for the
        web.

170500
- Fixed a few bugs in the file reading/opening code.
- Removed the KQuickViewDlg class and all the bank selection code.
- This is just another interim release before major structural changes
        made to the code - again.

160500
- Reimplemented the file opening/saving code using QDataStream's and fixed
        a few bugs.

140500
- Moved the MyMoney* classes to it's own sub-dir and now uses a library.

130500
- Finished converting MyMoney*.

090500
- First steps in making MyMoney* ready for a shared library.  This
        version WON'T compile due to undefined methods and KMainWnd
        not using the new methods.

060500
- Just added the MyMoneyBank class and added rudimentary support for
        it in the code.  Added a BankListView class as well to show the
        banks.

No history before this point (Thanks to a lovely disk crash).
