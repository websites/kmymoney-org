---
title: 'KMyMoney 0.6rc2 Release Notes'
date: 2004-02-06 00:00:00
layout: post
---
2004-02-06 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* 0.6rc2 released

2004-02-05 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* updated messages for further translation

2004-02-04 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated fix for #886979

2004-02-03 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Fix #886979

2004-02-03 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed bug in 'create schedule' to include all splits of the originating
transaction
* Make a selected schedule visible when coming from the 'home' page
through a link on the schedule
* Fixed EOL coding from CRLF to LF in all relevant files

2004-01-31 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed #887044 (cannot rename categories)
* Fixed #887048 (account icons not updated after rename)
* update of previous fixes. We now allow multiple splits with
the same category for one transaction. This certainly makes sense.

2004-01-29 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Fixed #884364 (Support for variable amounts in startup code).
* Fixed config read/write in settings dialog.
* Fixed 883085 (Options for schedules stored in wrong group)
* Fixed 883260 (The schedule 'engine' doesnt honour the days in advance option)
* Fix edit schedule dialog that sets the estimate option even when not set.
(Only occurs if you never click on the checkbox).
* Fixed #880052 (Adjustment of payment date when scheduled for weekend).
(I think its a bug...)
* Call refresh properly in KScheduleView::slotNewXX.
* Set the icons for the new file dialog in both constructors.
* Make KBankListItem, KMyMoneySplitTable, KMyMoneyRegister honour the list colours option.

2004-01-26 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed #881136 (No warning when filesave fails)
* Fixed #878380 (initial font setting)
* Fixed #879317 (missing word-wrap for a label field)
* Fixed #883778 (Maximum number of editable splits)
* Fixed #884190 (Transaction containing two splits referencing same account)
* Fixed #877819 (i18n of standard account names)
* Fixed #882330 (Account button does not show correct account)
* Fixed #879002 (account button does not get updated)
* Fixed #884226 (Cannot edit loan transaction)
* Fixed #882290 (Default accounts only support ISO8859-1 charset)
* Fixed #884224 (Transaction details missing in search dialog)
* added russian homepage, default accounts and po file

        