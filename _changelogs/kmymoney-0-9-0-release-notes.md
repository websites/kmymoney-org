---
title: 'KMyMoney 0.9.0 Release Notes'
date: 2008-05-08 00:00:00
layout: post
---
2008-05-08 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Removed some old files from the repository
* Updated splash screen and titlelabel for 0.9
* Released 0.9

2008-05-07 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Removed old KDevelop project file
* Added check for interest accounts to consistency check

2008-05-05 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Modified default of Income Expense Pie Chart to not show row totals
* Fixed forecast account comparison when accounts have duplicate names

2008-05-05 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated Portuguese translation from José Jorge
* Updated Simplified Chinese translation from Roy Qu

2008-05-04 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed a problem while creating categories within the schedule editor
* Improved price search when exact date option is set in the API

2008-05-03 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed problem in schedule view when amount of transaction is 0

2008-05-03 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added account type headers to budget on home page
* Added conversion to lowest fraction to querytable
* Added budget data to charts
* Auto-select current budget in custom report dialog
* Added default Budget vs Actual graph

2008-05-02 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added budget option to home page settings
* Fixed handling of home page settings that default to off
* Updated French translation from Patrick Petit

2008-05-01 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Separated grid of pivottable into a separate file
* Added budget overrun report to Home page
* Removed unit tests for TCell
* Moved PivotGrid tests to their own class
* Do not show Budget on Home Page if budget list is empty

2008-04-30 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed sorting of value columns in account views

2008-04-29 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated Simplified Chinese translation from Roy Qu
* If there are no favorite reports, don't show section on homepage
* Fixed sorting of value columns in investment view
* Added missing #include statements for GCC 4.3 as reported by Tony Graffy

2008-04-26 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed account reference in Summary of Home Page
* Fixed fraction of deep conversion in pivottable to reference current account
* Added budget difference to report properties
* Change budget difference column to calculate on report init
* Added more comments to pivottable

2008-04-27 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Turn off total column if report is configured for running sums
* Sort numeric account view columns by their numeric value

2008-04-26 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated Brazilian translation from Marcus Gama
* Fixed a crash when a split has a zero value during autofill

2008-04-26 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added Summary to Home Page
* Added convert to investment return calculation to prevent overflow
* Added Incomes and Expenses to Summary

2008-04-23 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Regenerated POT file
* Reverted patch from Arni Ingimundarson because it breaks income/expense
reports and shows a total column

2008-04-22 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed forecast testcases due to interface change of createBudget()
* Updated Dutch translation from Bert Keuter
* Added Dutch homepage and Dutch whats_new html file

2008-04-21 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed assignment of budget for budget only reports

2008-04-21 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated Brazilian translation from Marcus Gama
* Updated Dutch translation from Bert Keuter
* Added budget forecasting
* Added sort options for budgets in GUI
* Renamed kmymoney2.kdevelop into kmymoney2.kdevelop.sample

2008-04-20 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added preliminary version of the budget calculation on historic data
* Added separate generate-messages target to Makefile.am

2008-04-19 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added Investment Worth Graph report
* Added Difference To Minimum Balance column to Home Page

2008-04-18 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Assign budget to report if Any is selected
* Select budget combo if Any is selected
* Fix pivottabletest check for total column

2008-04-18 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added another validation to the consistency check feature

2008-04-18 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* 30-day schedule interval

2008-04-17 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Hide accounts in reports if closed and no transactions for
the report timeframe
* Replaced Until today by As of today in Time Combo

2008-04-17 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Preserve date during auto transaction fill

2008-04-16 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed total column of pivot reports

2008-04-15 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed budget column when report timeframe is less than a year

2008-04-15 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated Italian translation from Sandro Bonazzola
* Date for interest and charges in reconciliation wizard follow
statement date
* Added percentage field to auto fill mode

2008-04-14 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Adjust forecast days on Home View if lower than accounts cycle
* Fixed minimum limit of begin forecast day in forecast view

2008-04-14 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated Brazilian translation from Marcus Gama
* Updated French translation from Patrick Petit

2008-04-13 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated Dutch translation from Bert Keuter

2008-04-12 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed account filter for budget report
* Enable show total columns switch on graphs - patch provided
by Arni Ingimundarson

2008-04-11 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed budget report configuration problem
* Updated simplified Chinese translation from Roy Qu

2008-04-10 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Updated Argentinian translation
* Fixed minor typo in forecast view title

2008-04-10 Robert Wadley &lt;robntina@users.sourceforge.net&gt;
* Renamed some of the old template and made some minor template
changes to the old us template descriptions

2008-04-10 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated Italian translation from Sandro Bonazzola
* Regenerated pot file, merge changes into translations
* Fixed a problem in the documentation
* Cleanup Makefile.am in template/en_US

2008-04-09 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Removed database menu items temporarily

2008-04-09 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Updates (incomplete) to New Account Wizard documentation - ideas
from Danny Scott

2008-04-08 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added Tax Transactions reports for Last Fiscal Year

2008-04-07 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added Tax Transactions by Payee report
* Added Investment Holdings Pie report
* Fixed queryTable to filter tax by Payee correctly

2008-04-07 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Ignore prior date changes when checking for autofill

2008-04-06 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added Today option to time filter combo

2008-04-06 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed an i18n string issue
* Fixed usage of KMM_DEBUG
* Fixed a bug in the QIF writer that caused a problem when
an account has no opening balance transaction

2008-04-05 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Updated Argentinian translation
* Fixed minor typo in new account wizard

2008-04-05 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Handle single-split GnuCash tx's; tidy up code

2008-04-05 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated French translation from Patrick Petit

2008-04-04 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Updated Argentinian translation

2008-04-04 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Keep the fraction of an account with the account object in the
MyMoneyObjectContainer

2008-04-03 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed some .desktop file issues reported by Andrey Cherepanov

2008-04-02 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Updated Argentinian translation

2008-04-02 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed #1931100 (Minor visibility problem)
* Respect first fiscal day in register for fiscal year group marker

2008-04-01 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Changed interface of MyMoneyAccount::adjustBalance() to
support stock splits
* Adjusted testcases
* Updated Portuguese translation from José Jorge

2008-03-31 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed forecast beginDay test when day is last day of month

2008-03-31 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Force focus to currency selection list in new user wizard
* Fixed tax report and added testcase for it

2008-03-30 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added history method settings to forecast view

2008-03-30 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated Russian translation from Eugene Morozov
* Fixed an i18n issue in the sort order dialog
* Added option 'Enter key moves between the fields'
* Fixed typo in README
* Fixed forecast view to contain a title label and reduced margin to 0

2008-03-28 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Removed duplicate code and cleaned up code in forecast
* Fixed miscalculation of total row in forecast summary and detail view

2008-03-28 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated Portuguese translation from José Jorge
* Regenerated and merged messages
* Added day field to fiscal year setting

2008-03-27 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Allow changing the name and currency of the base accounts
* Added testcases
* Change the name of the base accounts to desired language when
loading file
* Updated French translation as provided by Patrick Petit
* Fixed an attribute string in MyMoneyStatement XML presentation

2008-03-26 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added weighted moving average to history forecast as default for
history-based forecast
* Added setting to forecast to select between simple moving average and
weighted moving average
* Added test case to test empty template
* Added test cases for weighted moving average

2008-03-26 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added missing #include statements for GCC 4.3

2008-03-25 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed removal and loop operation in MyMoneyForecast::
-purgeForecastAccountsBasedOnHistory and
-purgeForecastAccountsList
which caused a lockup if no data is present

2008-03-24 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added missing #include &lt;typeinfo&gt; statements

2008-03-23 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed a string problem in a dialog when reading an OFX statement

2008-03-22 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed date locale in forecast view
* Added createBudget to forecast, to forecast budget based on history
* Added unit tests to check budget forecast
* Added budget forecast tab to forecast view
* Changed the way forecast is stored internally to date and cleaned up
code in forecast class
* Added skipOpeningDate setting to forecast

2008-03-20 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed problem with investment transaction editor due to recent changes
of 'Favorites' handling

2008-03-19 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated Portuguese translation from José Jorge
* Fixed widget update problem in schedule editor dialog

2008-03-18 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Allow Cancel option when leaving the transaction editor only when
leaving is caused by selection of another transaction in the register

2008-03-17 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed crash with OFX download when IBAN field is empty (thanks to
John Whitlock for pointing it out) now for libofx &lt; 0.9

2008-03-16 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated Portuguese translation from José Jorge
* Fixed a lock-up problem with transaction reports that contain transactions
with a single income or expense split
* Added 'Favorites' to the single selection mode account selectors
* Fixed crash with OFX download when IBAN field is empty (thanks to
John Whitlock for pointing it out)

2008-03-13 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed layout of findTransactionDialog

2008-03-12 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* New and updated German price sources (thanks to M. Zimmerman and others)

2008-03-11 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated Galician translation as provided by mvillarino

2008-03-09 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed more spelling problems as reported by mvillarino

2008-03-08 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed spelling problems as reported by mvillarino
* Fixed message catalog generation
* Regenerated and merged messages

2008-03-07 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed ROI calculation when ending balance is zero

2008-03-07 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Minor change in configure.in.in to support NetBSD's shell
* Added automatic insertion of path to perl interpreter for makekdewidgets
* Fixed a leftover problem from conversion of double to MyMoneyMoney
in reports

2008-03-06 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Restored calculation of investment performance (IRR)
* Added new column on investment performance report to show ROI
* Restored original test case for IRR
* Fixed a crash on config report when not showing Include Transfers

2008-03-06 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added checkbox to hide unused budget categories

2008-03-05 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed setup of base currency when missing in file

2008-03-04 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Last changes required to compile w/o KOffice being installed

2008-03-03 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Always show register header in bold
* Size ledger divider according to the font size
* Refactored wizard modules into their own directory
* Resolved circular dependency between widgets and dialogs

2008-03-02 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added settings box to forecast view
* Added button to run forecast on modified settings from forecast view
* Forecast settings are now only used on instantiating
* Adjusted forecast test cases due to new way of using settings

2008-03-02 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed a problem with entering budget values that end in 0 and
have no fraction (trailing zeroes were removed)
* Added some testcases to check formatMoney() in more details
* Added our own copy of libkdchart so we don't depend on KOffice anymore

2008-03-01 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Skip beginForecastDate test cases if 1st or last day of month

2008-03-01 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed alternating background colors in pivot table based reports
* Added vertical column border for budget vs. actual reports

2008-02-29 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added compounding frequency widget to new account wizard for loans
* Allow share split transactions with (almost) arbitrary precision ratio

2008-02-28 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Show all investment accounts in forecast
* Do not calculate account in forecast if there is no relevant
transaction for that account
* Adjusted forecast test cases for investment accounts

2008-02-26 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Removed unused parentId information from budget account group
* Show details of split transaction as tooltip

2008-02-25 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed performance calculation when no activity during the report timeframe
* Cleaned up test code in querytable and querytabletest

2008-02-25 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated Italian translation as provided by Vincenzo Reale

2008-02-24 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed a crash due to precision formatting in forecast view
* Change performance return to show the current return, not
annualized - #1897433
* Updated performance return unit test

2008-02-24 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added missing code changes due to interface changes
in ReportAccount::currency()

2008-02-22 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Disable Include Transfers checkbox when filtering by Category - #1523508

2008-02-21 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Require currency string and precision for all formatMoney() calls
* Fixed queryTable testcase to obey selected date formatting
* Prevent crash when hitting Ctrl-W while transaction editor is present

2008-02-20 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added test case to check for correct date and value of closing
balance in query table

2008-02-18 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added check for MyMoneyMoney constructor to avoid zero denominators
* Fixed a problem with unavailability of the 'mark transaction as' options
* Removed unused code
* Fixed persistancy of balance column setting in reports
* Fixed position of closing balance row in query table reports
* Reverted changes to unit tests made on 2008-02-15 by Alvaro

2008-02-17 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed crash when calculating recent stock prices - #157905 at bugs.kde.org
* Added warning label about excluding transfers when filtering
transactions by Category
* Updated Argentinian translation

2008-02-16 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed 'WARNING: KLocale: trying to look up "" in catalog.' at appl start

2008-02-15 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed unit tests of querytable.cpp broken when adding foreign currency fixes

2008-02-14 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added stock accounts to forecast for historic and scheduled-based methods

2008-02-13 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated account templates

2008-02-12 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Remember date of last imported transaction with account object
* Fixed a compiler warning

2008-02-09 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Created new widget for account template loading
* Integrated that into the new user wizard
* New dialog for loading account templates once file is created
* Fixed home page settings

2008-02-08 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Labels in wizards use KDE color settings
* Fix label frames in new user wizard when started multiple times

2008-02-07 Robert Wadley &lt;robntina@users.sourceforge.net&gt;
* Reorganized the old template into new, more modular category
templates and added descriptions.

2008-02-07 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Keep group membership when saving to a local file

2008-02-07 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed forecast tests for minimum, maximum and average balance methods

2008-02-06 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Changed forecast negative numbers to use KMyMoney settings
* Changed detail forecast to show negative by cell not row - #1877719
* Added negative color to advanced forecast - #1877719
* Added beginDay to calculation of accountMinimumBalanceDateList in forecast
* Removed old setting and method for trendBasedForecast from home page

2008-02-05 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed removal of categories

2008-02-04 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Ask user if last payment for schedule should be reset in case the next
due date is changed to a date prior to the last payment - patch supplied
by Colin Wright

2008-02-03 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* New schedule intervals (3, 8 wks) - patch supplied by Colin Wright

2008-02-02 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed calculation of foreign currency balances and prices in transaction reports

2008-02-02 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed the example on keeping a stable and development version
on the same machine.

2008-02-02 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed calculation of foreign currency in transaction reports - #1369048

2008-02-01 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added missing files for submissions and settings chapter

2008-02-01 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Removed extra qualifier from subAccountByName() declaration

2008-01-31 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added submissions chapter to project handbook - #1394772
* Added settings chapter to project handbook
* Added option to Makefile to preview project handbook

2008-01-31 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Don't create the same parent account if it already exists during
account creation

2008-01-30 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Don't run overdue test between 28th and 2nd
* Resolved a bunch of compiler warnings

2008-01-29 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed error calculating beginDate when currentDate plus cycle equals beginDay - #1877701

2008-01-29 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Applied documentation update provided by Colin Wright

2008-01-28 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Applied documentation update provided by Colin Wright

2008-01-28 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Modified home page forecast cycles according to begin day setting - #1877701

2008-01-28 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added begin day of forecast setting and default - #1877701
* Added calculation to start forecast according to begin day of
forecast setting - #1877701
* Modified summary and advanced to show intervals according to
begin day of forecast setting - #1877701

2008-01-27 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Block delete payee function when a payee was just deleted

2008-01-26 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed width of summary list when over the width of the screen

2008-01-25 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed off-by-one error when calculating total variation in detail forecast view

2008-01-25 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added sort indicator to payees list
* Added context menu key handling to payees, investment and budget view
* Fixed crash when using context menu key in account trees
* Handle context menu the KDE way

2008-01-24 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Swapped rows and columns in detail forecast view

2008-01-24 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added calculation of base currency balances in Investment
Performance report - bug #1699859
* Removed comments of ReportAccount::deepCurrencyPrice and
baseCurrencyPrice where it said it would return 1.0 if date was not exact

2008-01-24 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added Context Menu Key handling to ledger and account tree views

2008-01-23 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Adjusted width of forecast columns when refreshing

2008-01-22 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Preserve last selected tab of schedule view
* Use KMyMoneyGlobalSettings for autosave options

2008-01-21 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Write out id to XML via MyMoneyObject
* Adjusted testcases
* Keep modified budget values when switching to a different view
* Keep focus when changing budget values
* Hide unused budgetary categories when 'Hide unused categories' filter
is active
* Deactivate clear button if budget value is zero

2008-01-20 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Improved sorting of schedules view
* Show date in schedule view according to KDE settings
* Double click on scheduled transaction opens schedule editor
* Added two changes from the debian distribution
* Clear out the reconciliation account when closing a file

2008-01-19 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Show date field when manually updateing prices
* Don't show 'Accounts with no institution' item in institution view
if there are no such accounts
* Use date of last new transaction entered also in
investment transaction editor

2008-01-18 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Removed text from buttons on top of budget list in budget view
* Converted QMessageBox calls to use KMessageBox in GNCConverter

2008-01-17 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Yahoo Canada Online Quote Source - contributed by Danny Scott

2008-01-17 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated Chinese translation provided by Roy Qu

2008-01-16 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Added FT Quote source for UK Funds - contributed by Peter Lord

2008-01-15 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed #1866855 with patch provided by Chris Roland
* Pressing the enter key during budget value entry moves the focus to the
next (value) field.

2008-01-14 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Fix desktop file errors

2008-01-14 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed a bunch of reported budget GUI issues
* Replace KMyMoneySettings with KMyMoneyGlobalSettings
* Added searchline widget to schedules view

2008-01-13 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed a bunch of open budget GUI issues
* Adapted testcases to latest changes

2008-01-12 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added 'Not marked' as new item to status filter
* Show date field when manually updateing prices

2008-01-11 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed operator == for MyMoneyPayee to include match parameters
* Cleaned up payees view and match parameter handling
* Cleanup up budget view and introduced an OK button as in payees view
* KBudgetValues widget sends out valuesChanged() signal upon all changes

2008-01-10 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Exchanged order of test for OFX and KDChart during configure
to avoid problems with additional libraries (eg curl) as reported
here: http://sourceforge.net/mailarchive/message.php?msg_name=478558AB.1080003%40lemmons.name .
* Improved error messages when invalid transaction id/key is requested
* Improved consistency check to find invalid references in schedules

2008-01-09 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added copy budget feature

2008-01-07 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed a problem with price precision in currency calculator dialog
* Fixed a warning in kmymoneytest.cpp
* Fixed problem with keypad return key during transaction entry
* Removed some unused code
* Made some improvement to the balance calculation
* Fixed scrolling problem in ledger when moving the focus with the
cursor keys.
* Refuse loading of data file only if openingBalance is not equal zero

2008-01-06 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Show a marker in the ledger when transactions are possibly filtered
This is shown if the filter is set to any other date than 1900-1-1
* Removed widget in settings dialog that should not be visible
* Fixed loan creation when no payout transaction was selected
* Fixed popup of price editor during loan creation
* Improved detection of splits for loan payment transaction in
loan editor
* Removed all references to MyMoneyAccount::openingBalance
* Fixed a bug in the XML loader that caused the date order to be incorrect
* Fixed QIF writer to write out the opening balance as expected

2008-01-05 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed problem with creating new transactions when quick filter is active
* Save transaction also on Return pressed on status combo box
* Preset status combo box with 'Not Reconciled' for new transactions
* Got rid of some openingBalance() and setOpeningBalance() calls
* Updated the reporting testcases to construct an opening balance transaction
* Removed an automatic loan account fix. User has to use version 0.8.7
or higher in 0.8 branch to fix the problem.
* Fixed endless loop when loading a broken XML file
* Fixed a bug in processing of online statements
* Fixed MyMoneyObjectContainer handling for updated objects
* Fixed bug introduced on 2007-12-06 where share amount was loaded off
by a factor of 1/1000
* Added general method to create brokerage account name in MyMoneyAccount()
* Removed unused MyMoneyFile::updateBalances()

2008-01-04 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed a problem in LibOFX 0.9 adapter implementation that caused
statement downloads to fail
* Reduced minimum version for LibOFX to 0.8.2

2008-01-03 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Changed behavior of Return-Key in transaction editor. Acts like a TAB
key except for the amount field where it acts as the Enter button.

2008-01-02 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added patch provided by Alvaro Soliverez to fix total for yearly budgets

2008-01-02 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added option to show/hide row totals in reports
* Removed default total column from non income/expense reports

2007-12-31 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Store all OFX bank list files in ~/.kde/share/apps/kmymoney2
* Use different VER settings to retrieve information in ofxpartner
* Added webprice quote patch as provided by David Pugal
* Fixed crash when changing account limits
* Added reporting patch provided by Alvaro Soliverez
* Leave reports in their group when switching to a different translation

2007-12-30 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed sign problem when selling shares with a price per transaction setting
* LibOFX 0.9.0 now mandatory
* Added missing testcase to consistency check
* Preset brokerage account if available for investment transactions

2007-12-29 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Catch exceptions thrown in libxml++ during OFX setup
* Store OFX bank list files in ~/.kde/share/apps/kmymoney2

2007-12-28 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added feature to start KDE language settings from within KMyMoney
* Updated French translation as provided by Patrick Petit
* Fixed a crash when updating multiple transactions
* Fixed documentation to mention 19% VAT for Germany
* Added i18n support to strings in mymoneyfile.cpp
* Fixed the problem of GPG photo id's popping up
* Allow to create a brokerage account for investment accounts

2007-12-26 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed a problem when loading price information in the investment
transaction editor

2007-12-24 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added testcase for MyMoneyMoney::reduce()
* Fixed price handling when loading a transaction into the editor
to prevent rounding errors

2007-12-23 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Update balance visibility after sorting the ledger items during view load
* Added feature to either enter price/share or price/transaction
to the investment transaction editor

2007-12-22 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Applied budget report patch provided by Alvaro Soliverez

2007-12-21 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed problem with the calculator when starting to enter numbers
using the mouse
* Fixed problem with various negative sign positions
* Show negative numbers in pivot table reports in the selected color
* Removed unused filter settings on the register tab of the settings dialog
* Mark splits as cleared when accepting or matching a transaction
* Show not reconciled and cleared transactions when setting the quick
filter in the ledger view to 'Not reconciled'

2007-12-20 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added budget() to MyMoneyReport to return the budget id

2007-12-19 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed layout problem in report configuration dialog
* Fixed payee assignment when deleting a payee

2007-12-18 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Take the character pressed to start editing a split transaction
as the first character of the category
* Get rid of kMyMoneyDateInput::getQDate()
* Store real budget id with report
* Fixed the signature of a signal
* Added a budget selector widget to report configuration
* Fixed unknown signal/slot problem during startup

2007-12-17 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Double click in number field assigns next check number
* Fixed type assignment for schedules to be created
based on existing transactions
* Added split fees and interest to investment transaction editor
* Use two digit month and day values in QIF export

2007-12-16 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed a bug in transaction reports which only showed in the testcases
* Improved the balance calculation of transaction reports
* Show running balance column in 'transactions by account' reports

2007-12-15 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added patch provided by Alvaro Soliverez

2007-12-14 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Remove payee from required field for schedules

2007-12-13 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Removed unnecessary assignment to MyMoneyPayee::null
* renamed budgetsubaccounts() to budgetSubaccounts() in MyMoneyBudget
* Added more budget entry features (use previously entered value when
switching to another method (eg. from monthly to yearly)
* Added clear button to clear the budget values
* Added running balance column to transaction reports

2007-12-12 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Update default quote strings for Yahoo France

2007-12-12 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Don't select different transaction if action is cancelled

2007-12-11 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Resolved duplicate function parameter name
* Added plugin dialog to show plugin information
* Added warning message when quitting transaction dialog editor
via selection of a different transaction
* Don't show the sort column shaded in KListViews

2007-12-10 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Make GNC import error message translator-friendly

2007-12-10 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed a precision problem when entering the number of shares

2007-12-09 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated Italian translation as provided by Vincenzo Reale

2007-12-09 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Fix Finance::Quote interface to use 'last' price

2007-12-07 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Dump POT-Creation-Date and PO-Revision-Date in XML output
* Added all available language files to the po directory
* Created kmymoney.pot and merged all language files

2007-12-06 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* adapted MyMoneyStatement interface to use MyMoneyMoney objects
for monetary values instead of doubles
* Removed KBanking plugin. From now on, the plugin can be found under
http://www.aquamaniac.de/sites/download/packages.php or via SVN under
http://devel.aqbanking.de/svn/kmm_plugins/banking/trunk
* Use same font size for reports as for home page
* Renamed pt_PT.po into pt.po

2007-12-05 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added readOnly property to kMyMoneyEdit
* Fixed allowEmpty property in kMyMoneyEdit
* Revised budget value widget

2007-12-03 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed selection of all transactions with ctrl+a
* Applied patch provided by Alvaro Soliverez
* Fixed build system to include new files in tarball

2007-12-01 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Store original price with investment splits for display purposes
* Improved budget UI (except reports)

2007-11-28 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Applied patch provided by Alvaro Soliverez to fix some budget report issues
* Improved budget view

2007-11-27 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added logic to the consistency check to remove invalid payee ids
and replace them with correct ones
* Moved account balance graph into own dialog

2007-11-25 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Applied patch provided by Alvaro Soliverez with a few modifications

2007-11-23 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed a problem in KFindTransactionDlg when entering a user defined date

2007-11-22 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Don't include finished schedules in reports

2007-11-21 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added standing orders and bank transfer as payment types for schedules

2007-11-19 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Use name match if no ticker symbol is present in
security record of OFX file
* Fixed a problem when creating scheduled transactions that the
shares field of the second split was set to 0

2007-11-18 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed a problem when importing OFX statements with cash dividends

2007-11-16 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed QIF importer to work with new wizards

2007-11-11 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Don't send out data changed signal during file save operation
* Imported GnuCash account template files

2007-11-07 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added current and last fiscal year to period combo
* Added period combo widget to find transaction dialog

2007-11-06 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added settings for beginning of fiscal year and drawing of markers for it
* Added ledger markers for current and previous fiscal year

2007-11-04 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Made some adjustments to charts. Thanks to David Houlden for some of them
* Completed the loan account section in the new account wizard

2007-11-01 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added new MyMoneyMoney::formatMoney() convenience method

2007-10-27 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Don't show unused securities/currencies in update stock price dialog

2007-10-26 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed a problem in forecast module (take initial balance into account)
* Adjusted a few signatures of forecast module for better performance
* Fixed a problem with split detection in split transaction editor

2007-10-25 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added forecast patch provided by Alvaro Soliverez
* Modified layout and changed QListView* into KListView* objects in forecast
* Don't include disabled widgets in mandatory field checks
* Fixed a layout problem in enter and edit schedule dialogs when resizing
* Fixed a sizing problem with KMyMoneyCombo widgets
* Removed a spacer in the base wizard logic
* Improved new account wizard

2007-10-24 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed some problems around changing the next due date of schedules
* Added testcases to catch those problems

2007-10-23 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed a few problems around the split transaction editor

2007-10-22 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed problem during creation of schedules reported by David Houlden
* Fixed schedule XML loader to adjust old style nextDueDate information

2007-10-19 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed build problems caused by recent changes

2007-10-18 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Applied forecast patch provided by Toan Nguyen
* Applied testcase provided by Alvaro Soliverez
* Initial implementation of the new loan wizard

2007-10-16 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Improved sizeHint() of KMyMoneyCombo

2007-10-12 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Gnucash importer
- Fix problem caused by improbable zero share quantity

2007-10-09 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Allow creation of payees while editing schedules
* Fixed logic to append suffix on the home view for
accounts with the same name
* Fixed re-assigning of an account to a different institution

2007-10-09 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Preserve fraction denominator of securities in GNC importer

2007-10-08 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Implemented #1394794 (Duplicate schedule)

2007-10-07 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Get rid of varargs from GNC importer

2007-10-06 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Gnucash importer
- add new schedule intervals
- improve status reporting
- debug changes

2007-10-05 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added updated testcases for MyMoneyForecast as provided by Alvaro Soliverez
* Added introduction page to new user wizard which is shown
to first time users

2007-10-04 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added operator &lt; to MyMoneyInstitution
* Added operator &lt; to MyMoneySecurity
* Replaced new account wizard
* Started implementation of KMyMoneyPeriodCombo
* Sort entries in KMyMoneySecuritySelector
* Changed display in KMyMoneySecuritySelctor from "Symbol (Name)"
to Name (Symbol)"
* Improved KMyMoneyWizard base class
* Disable 'postpone reconciliation' and 'finish reconciliation'
buttons during transaction edit
* Updated tips file
* Applied forecast patch provided by Alvaro Soliverez

2007-10-03 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Use KDE window title font for KMyMoneyTitleLabels
* Setup default buttons for KMyMoneyWizard

2007-10-02 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Avoid display of multiple entries for single time schedules on homepage

2007-10-01 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* GnuCash importer
- correct schedule import
- handle GnuCash 2.2 files

2007-09-25 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed problem when creating a payee during schedule creation

2007-09-24 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Clear payee search filter when using the 'Goto payee' function
of the ledger view
* Don't offer creating of a schedule while entering a schedule
* Show warnings if a limit has been reached
* Fixed some schedule creation problems

2007-09-18 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Modified OFX importer to use Qt::UTC as dateformat

2007-09-17 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added functionality to help buttons in edit and enter schedule dialog
* Added quick search to accounts and categories view
* Added 'collapse all' and 'expand all' buttons to accounts
and categories view

2007-09-15 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Update developer documentation

2007-09-15 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added feature to create schedule for new transactions with a postdate
in the future
* Fixed endless loop caused by scheduled transactions with single occurence
* Fix a problem, if last payment and next due date of a schedule are
identical

2007-09-14 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Overhauled schedule editor
* Store the next due date of a schedule as the post date of the
transaction object within the schedule
* Improved MyMoneyBudget API (use references where possibly)
* Added testcase to test the next due date adjustment according to the
selected weekend option
* Added feature to create schedule based on existing transaction
* Don't allow 'skip transaction' for schedules with frequency 'once'

2007-09-10 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed problem with loading a split w/o account id
* Don't allow to add/modify a split w/o account id
* Updated testcases
* Thanks to David Walling for providing the anon file to catch that one

2007-09-05 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed crash when editing splits w/o transaction form. Thanks to
Michael Aichler for tracking it down.

2007-09-02 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Obey closed accounts in pivot table based reports

2007-09-01 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed display of first row in investment transaction form
* Use alternate background in the investment view
* Control visibility of rows in the transaction form on a per
transaction basis

2007-08-31 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed creation of schedules
* Redefined tabstops in edit schedule dialog
* Engine throws exception when the application tries to store a
transaction that contains splits not referencing an account
* Don't show closed accounts in querytable based reports
* Prevent crash when cancel out of the exchange rate editor during
the entry of a scheduled transaction
* Force usage of GPG agent if environment variable GPG_AGENT_INFO is filled

2007-08-29 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Don't show closed accounts in account query table

2007-08-28 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Removed unused code from KMyMoneyUtils

2007-08-27 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added feature to close investments (stock accounts)
Maybe, the report functions need to be adjusted as well

2007-08-26 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Improved class documentation for MyMoneyFile::attachStorage()

2007-08-25 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed layout of GNC import options dialog
* Fixed processing of ROOT type account entry in GNC importer

2007-08-24 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added option to hide finished schedules
* Added option to hide closed accounts
* Added feature to 'Select all transactions' via Ctrl+A
* Added note that closed accounts will be not shown if the option is active
whenever an account is closed by the user

2007-08-23 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Allow to create an account with the same name on the same
hierarchy level
* Allow to load 'Who am I' address from standard KDE addressbook

2007-08-20 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added release target in Makefile.dist as proposed by Tony Bloomfield
* Replaced QMessageBox with KMessageBox (except in GNC importer)

2007-08-17 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Removed duplicate entry of XFP as default currency

2007-08-16 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed graph in account balance history

2007-08-10 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Replaced "acc.accountType == Stock" with "acc.isInvest()"
* Fixed #.... (Allow change of stock type)
* Merge message files during 'make package-messages'

2007-08-09 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added account field to standard transaction editor
* Allow to change account during entry of scheduled transaction
* Removed references to KMyMoneySettings in designer plugin

2007-08-08 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added interface for reconciliation report

2007-07-25 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added "CFP franc" as currency

2007-07-22 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed typo. Thanks to David Houlden for finding it

2007-07-21 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Made MyMoneyFile a static singleton and MyMoneyFile::instance() an
inline method
* Adapted testcases to work with static singleton
* Added toolbutton for HBCI account update to toolbar

2007-07-18 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed crash when online price update for exchange rate failed
* Removed some unused engine functions (accountValue/totalAccountValue)

2007-07-13 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added framework to modify axis parameters in PivotTable::drawChart()
* Fixed creation of account hierarchies

2007-07-11 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Removed the 'oldreports' option
* Allow creation of multi currency categories
* Allow changing the base currency
* Removed default parameter of MyMoneyPrice::rate()
* Base currency activities (new, edit, delete, etc.) on KActions
* Show networth graph on home page in base currency
* Fixed a problem with ledger not displaying all transactions when
loading a new account
* Added Euro conversion rates for MTL (Maltese Lira) and CYP (Cyprus Pound)

2007-07-04 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* calendar-&gt;monthString() was the wrong candidate. use calendar-&gt;monthName()
instead. The monthName(QDate(),bool) version is broken on older KDE systems
so we use monthName(int,int,bool) instead
* Don't cycle through 'Reconciled' state during reconciliation when
left clicking into the reconciliation state column of the ledger
* Adjust ending balance when changing statement date during reconciliation

2007-07-02 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed problem while determining the last used id for accounts
* Remove all references from reports when objects are deleted
* Don't use setId() directly
* convert locale-&gt;monthName() to locale-&gt;calendar-&gt;monthString

2007-06-30 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed crash when saving user information

2007-06-29 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Removed unnecessary code in MyMoneyReport
* Made inclusion of stylesheet in HTML exported reports optional
* Enable/disable also the split-button of the category widget

2007-06-28 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Rewrote the XML parser to be based on a SAX model
* Provide readAll() with larger block sizes in KGPGFile
* Fixed problem with balance cache mishits
* Fixed crash when reading an encrypted file that does not contain
the "kmm-encryption-key" is missing in the file (could happen when
reading rather old files)
* replaced QDate::shortMonthName() with KLocale::global()-&gt;monthName()
* Only assign check numbers for scheduled payments, if payment type
is 'write check'.

2007-06-22 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed update of price editor when deleting prices

2007-06-21 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed price dialog loading
* Fixed deleting accounts that have no sub-accounts

2007-06-20 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added hook for trend based forecast on homepage
* Added some details about the account types to the file info dialog

2007-06-17 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed creation of KMyMoneyWizard buttons to work when the KDE
global setting 'icons on buttons' has been turned off

2007-06-16 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added 'Move to account ...' logic
* Keep track of all toolbar attributes

2007-06-15 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added new user wizard logic

2007-06-14 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added #1349502 (Report all categories in income/expense report)

2007-06-12 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added chinese entries to desktop files
* Improved new user wizard
* Show limits in account balance graph

2007-06-11 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Pickup new options for the homepage items

2007-06-10 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added networth forecast graph to homepage

2007-06-05 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added updated account templates as provided by mvillarino

2007-06-02 Robert Wadley &lt;robntina@users.sourceforge.net&gt;
* added a "forcast" icon

2007-05-31 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* !!! Note SQL support is broken with this version. Will be fixed later. !!!
* Move MyMoneyObjectContainer to MyMoneyFile and make it a general cache
* Added start/commit/reject Transaction to seq access mgr
* Added 'last check number used' to accounts editor
* Don't allow to move stock accounts in institution view
* Modified new account wizard to use new widgets
* Modified new loan wizard ot use new widgets and fixed a bunch
of problems with unassigned share field where a value was present
* New account wizard did not set shares on credit card payment
* Added color setting for background of ledger markers
* Moved the transaction editor code from widgets/ to dialogs/
* Reduced circular dependencies between widgets/ and dialogs/ to a single one
* Removed some unused files

2007-05-27 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Don't include splits referencing deselected categories in transaction
report by category

2007-05-22 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added new account template files for Spain (Spanish and Galician)
as provided by MVillarino
* Fixed #1723325 (Cannot quit the program)

2007-05-21 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed crash when closing the passive popup of the date edit widget
during transaction editing

2007-05-19 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Include style sheet contents in exported HTML reports
* Prepare integration of general object cache in MyMoneyFile layer

2007-05-17 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added dialog to select the transaction to be used for autofill if
multiple transactions exist for the same payee
* Show all matching splits in find transaction dialog

2007-05-16 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Use prettyURL() where appropriate
* Renamed KMergeTransactionDlgDecl into KSelectTransactionDlgDecl

2007-05-14 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Better detection of write errors in GPGFile component

2007-05-13 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added patch update to QueryTable provided by Demitrios Vassaras
* Make inclusion of specific accounts (tax, investment or loan)
in reports mutually exclusive
* Added ability to 'Save as...' to a network drive
* Check if save operation would override a file and ask the user for
confirmation

2007-05-12 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Additional database debug options
* Fix database account balance problem

2007-05-12 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added report configuration changes supplied by Demitrios Vassaras

2007-05-11 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed presetting the tabbar when starting to edit an existing transaction
* Applied remaining patches to the report logic provided by Demitrios Vassaras
* Allow investment transactions to be performed against credit card accounts

2007-05-10 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Clear some transaction attributes before entering a schedule
* Added MyMoneyAccount::isLoan() and testcases

2007-05-10 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Retain status of imported transactions
Patch supplied by Fernando Vilas

2007-05-08 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed indentation problem in reports

2007-05-07 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Add all features to auto schedule enter
* Prevent asking too many times for conversion rate during schedule entry
* Created interface class for PivotTable and QueryTable called ReportTable

2007-05-05 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Fix crash when saving new file after database activity

2007-05-05 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed rounding problem when entering investment transactions

2007-05-04 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Renamed --newreports option into --oldreports
* Use new split based report logic by default
* Fixed crash when reports were opened
* Grey out 'Create schedule' as long as it is not implemented

2007-05-02 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added support for stock splits to the pivot table based reports
* Improved performance (only create pivot table once) of report update
* Improved display of delta for changed scheduled transaction during
schedule entry
* Fixed a label display problem in the investment transaction editor

2007-04-30 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed a problem with displaying numbers with high precision
* Added testcase to check for precision

2007-04-29 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Applied patches to the report logic provided by Demitrios Vassaras
* Introduced TCell as the list member for TGridRows and initial code
to support stock splits in reports (not working yet)
* Re-enabled the value column in the investment register

2007-04-28 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed sign problem when reconciling liability accounts

2007-04-27 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added moldavian leu as currency
* Fixed an autofill problem
* Show the investment account for the 'goto account' feature

2007-04-21 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Removed all references to MyMoneyObserver and MyMoneySubject
(testcases still use it)

2007-04-20 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Support 'goto account' also to stock accounts and back

2007-04-19 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed matching transaction logic to work with investment transactions
* Allow deletion of categories even with transactions assigned on the GUI
level. Re-assign them prior to deletion.
* Fixed a problem with changing the amount in an existing VAT transaction
* Fixed disappearing text 'Split transaction' in the category widget
when opening the split transaction editor

2007-04-18 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added missing headers to option menus

2007-04-16 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Database backend - phase 2

2007-04-16 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Keep expanded items in reports view expanded when reloading
* Use new KMyMoneyPayeeCombo in the transaction reassign dialog
* Fixed moc inclusion for file info dialog
* Renamed KTransactionReassignDlg into KPayeeReassignDlg

2007-04-15 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Reactivated the automatic entry of scheduled transactions after file open

2007-04-13 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added display of investment value
* Added balance display for investments

2007-04-09 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added minimum balance and maximum credit fields to account editor
(not functional yet - storage works)
* Reworked the 'Enter schedule transaction' dialog to use new transaction
editor
* Update schedules view only if visible
* Allow individual column resize in schedules view

2007-04-08 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed a problem with the Ok button in the report configuration dialog

2007-04-06 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed a problem when creating new investment transactions
* Only update home page if visible
* Don't jump to investment view after editing investment transactions

2007-04-05 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Removed code of old kMyMoneyCategory widget

2007-04-04 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed a long pending problem with the visibility of group markers
* Always allow click on reconciliation column to change reconciliation state
* Clicking the reconciliaiont column now toggle through the possible
reconciliation states. Same applies for Ctrl-Space.
* Mark transaction cleared now has a shortcut of Alt-Ctrl-Space
* Fixed autofill problem

2007-04-03 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Schedule editor does not wipe out split transaction when amount was changed
* Cleanup (removed unused files)

2007-04-02 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed automatic VAT assignment when gross amount was selected
* Merge transaction dialog uses new register code
* Added menu entries for debug purposes to toggle timers and traces

2007-03-31 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Moved isAssetLiability and isIncomeExpense from
ReportAccount to its base class MyMoneyAccount
* Added filter option 'last 11 months' which covers the last 11 full months
* Reworked the search transaction dialog to use the new register code
* Added sort feature to search register
* Fixed the institution testcases to support KVPs

2007-03-30 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed some problems around the latest changes
* Get rid of annoying message that account with same name already exists
* Corrected items in budget time period combo box

2007-03-29 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated split transaction dialog to use new KMyMoneyCategory object
* Added budget view changes as provided by MVillarino

2007-03-28 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added BIC and IBAN fields to institution and account dialogs
* Removed manager field from institution dialog
* Add new columns to account / category view
* Mark mandatory fields in account and institution edit dialogs

2007-03-27 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Code cleanup (remove unused code)
* Don't try to reload default reports from the engine

2007-03-26 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Support empty thousand separator setting
* Added new category widget to enter/edit schedule dialogs
* Don't append currency id to account name if not foreign currency
when creating CSV export of pivottable reports

2007-03-25 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Update report if report configuration has been changed
* Avoid possible usage of invalid pointer in KMyMoneySelector

2007-03-24 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated reconciliation wizard to support
- entry of payee
- multiple currencies
- automatic calculation of start end ending balance of statement
- set the statement date to the same day of the following month if
the time span since the last statement is larger than a month
* Fixed a problem when loading non VAT transactions into the editor
* Clear the memo for the second split during autofill to allow overriding
the text by the user

2007-03-22 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated kmymoney2.desktop

2007-03-21 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Use KDE conrolled date format for kschedulebriefwidget
* Fixed a problem with category creation during transaction entry
* Added two new columns to category and accounts view

2007-03-20 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed column width of price in investment register
* Respect global price precision setting in price edit widget in
investment transaction editor
* Added some budget view patches provided by mvillarino
* Fixed a crash in KMyMoneyCheckListItem and KMyMoneyListViewItem

2007-03-19 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added some russian files as provided by Andrey Cherepanov
* Fixed title of new file dialog to be based on QString rather than char*
* Fixed a few other i18n problems reported by Andrey Cherepanov
* Fixed duplicate opening of split edit dialog in some circumstances

2007-03-18 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Preload payee widget for new schedules
* Only send out update signal from engine after list of price updates
has been processed to speed up the operation
* Modified the way splits are selected for pivot table reports back to
original code but added a filter for text and amount range.
* Fixed testcase support (global newReport variable)
* Send out MyMoneyFile::dataChanged() once a new file is loaded
* Use a non-null account id when creating InvestmentTransaction objects

2007-03-17 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added method to remove the buttons from a kMyMoneyAccountSelector
* Correctly fill the account selectors in the new loan wizard
* Replaced setId(QCString()) with clearId()
* Update reports only when required
* Provide MyMoneyFileBitArray to suppress warning when index to QBitArray
is out of bounds
* Resolved some compiler warnings
* Provide --newreports option also in non-debug versions
* Fixed a severe bug which I introduced on 03-15 with the reportAllSplits
change. Files written with this version could not be read in again.
* Fixed calculation of values in the loan wizard

2007-03-16 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added logic for auto increment of check number to transaction editor
* Rearranged the register settings dialog (added new tab)

2007-03-15 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Control character that is produced by the comma on the numeric keypad
to be the monetary decimal symbol regardless of keyboard layout
* Return all splits from MyMoneyTransactionFilter when no filter is
set but reportAllSplits is set
* Changed the way splits are selected for pivot table based reports
This is only available when KMyMoney is started with option --newreports
and has been compiled with --enable-debug=yes or --enable-debug=full
* Re-use post date of a new transaction for next new transaction
* Fixed a problem when autofilling a split transaction

2007-03-14 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed a couple problems introduced with yesterdays changes
(Thanks to Bob Ewart for spotting them)

2007-03-13 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Removed transaction tab from investment view
* Removed references to kMyMoneyPayee and replaced them with
the new widget KMyMoneyPayeeCombo
* Added option to synchronize the account in the ledger and investment view

2007-03-10 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added network transparent file access to import dialog

2007-03-09 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Improved layout of find transaction dialog

2007-03-08 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Replaced getQDate() with date() in ending balance dialog
* Fixed reconciliation of liability accounts

2007-03-04 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Include children of investment accounts (stock accounts) inthe
reports if the expert mode is not enabled
* Fixed a bunch of warnings
* Improved handling of multiple selected transactions
* Applied some (modified) patches provided by Demitrios Vassaras

2007-03-01 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed a problem detecting finished schedules

2007-02-28 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Automagically remove the yellow 'imported' background when a
transaction is modified and has a category

2007-02-27 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Don't allow deletion of a category if still referenced by schedule
* Allow creation of new transactions in new (empty) accounts

2007-02-26 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added statistics script to automaticall generate an overview
about the current status of the translations
* Don't create a scheduled transaction containing splits w/o account ref

2007-02-20 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated PHB to have correct CVS examples

2007-02-19 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Force a view reload at midnight
* Fixed a problem when adding new transactions with a number filled in
* Start the loan edit wizard when editing a loan account
* Removed some unnecessary files

2007-02-14 Robert Wadley &lt;robntina@users.sourceforge.net&gt;
* added new title label for cvs version

2007-02-15 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Switched to a newer splash screen
* Added (a bit improved) patch and testcase provided by David Houlden
* Corrected ISO code for new turkish lira and added old turkish lira
to the list of ancient currencies

2007-02-14 Robert Wadley &lt;robntina@users.sourceforge.net&gt;
* Adjusted the size of the welcome page images (I got a large monitor for
Christmas and the image breaks apart when viewed full screen.
* Added some images to the pics directory to be used, or not, when needed.

2007-02-14 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Assign the payee to category splits no matter what

2007-02-13 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Prevent creation of accounts with the same name and the same parent
* Prevent moving an account to a destination that already has an
account with the same name
* Replaced ":" with MyMoneyFile::AccountSeperator where appropriate

2007-02-12 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added account column to ledger view of categories
* Improved the 'goto account' function
* Fixed creation of account hierarchies if parts of the hierarchy already
exist
* Allow modification of check number while entering scheduled transactions
of type 'write check'

2007-02-10 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Improved handling of hidden transactions in the register
* Allow usage of enter key on numeric keypad to enter transactions

2007-02-09 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added Yahoo France web source
* Added french comment for kmymoney2.desktop
* Fixed Menu name in documentation for investment price update

2007-02-08 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added ability to print home page

2007-02-07 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed a problem with detection of VPATH environment in Makefiles
Thanks to Bob Ewart for reporting

2007-02-06 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed refreshing problem with search line widget in register

2007-02-05 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed crash in KMyMoneyListViewItem::isAlternate() and
KMyMoneyCheckListItem::isAlternate()
* Added new price source "Gielda Papierow Wartosciowych" as provided
by Piotr Adacha

2007-02-04 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Improved handling of category assignment in mulit selection
* Fixed detection of closed accounts in the account tree widget
* Use localized date format in reports

2007-02-02 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Don't modify a transaction when marking for match operation
* Eliminated usage of import verify dialog

2007-01-31 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added logic to accept imported transactions
* Added transaction menu to menu bar
* Use kde-xgettext as default and check that i18n(c,s) has been extracted
correctly during 'make package-messages'

2007-01-30 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Converted the private data d-pointers to be of type "* const d"
* Fixed a bunch of missing i18n() calls as reported by Patrick Petit

2007-01-29 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Some more fixes on the background color

2007-01-28 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Reset the status filter when pressing the clear button
* Use the full width for the filter
* Prevent a crash when clearing a register

2007-01-27 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Don't rely on the split id to generate a unique id for a ledger entry
It could change while updating a modifed transaction in the engine
and would therefor not be reselected after the update
* Setup the base currency correctly after loading a new file
* Removed unused code
* Fixed list background color usage and setting
* Fixed loading ledger for specific account from home page
* Added a status combo box to the register quick filter
* Fixed initial column width problem in accounts, categories and institutions

2007-01-26 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Don't show a balance in the ledger if an entry has been skipped due
* Optimized painting of group markers
* Balance shown underneath the ledger is the current balance as of today
not including any future transactions

2007-01-25 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* More work on invest transaction editor (should be functional now)

2007-01-24 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Fix another database record count error

2007-01-23 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* More work on invest transaction editor

2007-01-22 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Allow column resizing in accounts, categories and institution view
* Remember column sizes of these views between sessions
* Added sort indicator to the lists in the above mentioned views
* Moved investTransactionTypeE from KMyMoneyRegister to MyMoneySplit
* Fixed path to title label background image

2007-01-21 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed tab display when starting to create a new transaction via the tab

2007-01-21 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Correct record counts

2007-01-18 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Finished work on the RegisterSearchLine widget
* More work on the investment transaction editor

2007-01-17 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed problem that postpone and finish reconciliation are disabled
whenever the corresponding account object changes (e.g. due to change
of a transaction)
* More work on the RegisterSearchLine widget

2007-01-16 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed problem with actions being disabled after a transaction
has been entered
* Open ledger for investment account if one of its stock accounts
is selected

2007-01-15 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added framework for RegisterSearchLine object (not yet functional)

2007-01-13 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added new reconcile overlay icon provided by Rob
* Added display of reconcile overlay to institution and account icon view
* Fixed a problem with selecting transactions

2007-01-11 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* More investment ledger work

2007-01-10 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Support all UTF-8 characters in schedule list items and account editor
dialog caption

2007-01-08 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added detection for libxml++-2.6 in libofx.m4
* Fixed Tony's pesky payee problem

2007-01-07 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added speed search to the payees view
* Fixed prevention of using unsermake on some systems

2007-01-06 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Only allow postpone and finish reconciliation in account for
which the reconciliation has been started
* Mark that account in the accounts view

2007-01-05 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Return references to QStrings instead of copies in MyMoneySecurity
* Added sort by security for investment accounts
* Removed external visibility of sorting by 'entry date'. The user
can use 'entry order' instead. Internally we still need it.
* Fixed broken handling of Return and Escape during edit.
* Disconnect edit widgets from editor object in editor's dtor to
prevent crashes
* Added GroupMarkers for Reconciled state
* Improved auto check number handling
* Added default Ctrl+Shift+Space to mark a transaction reconciled

2007-01-04 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Use Enter/Return as shortcut to start and finish editing transactions
* Improved ledger update to avoid unnecessary resize checks
* Fixed some more spots where we need to escape special chars before
passing text as pattern to QRegExp

2007-01-03 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Escape special chars in names before using the text as pattern for QRegExp

2007-01-02 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added Slovenian Tolar to the ancient currencies

2007-01-01 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Happy New Year!!
* Changed KMyMoney2App::updateActions() into slotUpdateActions()

2006-12-31 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added reconciliation state to the ledger sort options

2006-12-30 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Use include path given with --with-extra-includes during OpenSP detection

2006-12-29 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed problem introduced with changes made on 2006-12-14 and keep
the OK button in the report configuration enabled at all times
* Added support for automake 1.10
* Use KMessageBox for questions when deleting a report

2006-12-23 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed extra qualification error

2006-12-22 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Refactoring some code modules
* More work on the transaction editor for investment transactions
(only works in ledger, crashes in form)

2006-12-04 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Improved detection of KBanking support

2006-12-01 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Don't allow to edit/create transactions in income or expense ledgers

2006-11-29 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed problem with fancy header on weekstartday set to other than Monday
Thanks to David Houlden for sending a patch

2006-11-07 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Prevent autofill if date has been changed by user in transaction editor

2006-11-04 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Implement transaction fix level, for speedier startup

2006-11-03 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Fixed a bug where an invalid price was entered into the system for
cash dividend investment transactions imported via OFX/AqBanking.
Hopefully fixes 1581788.
* Added online help for new investment wizard. Partially addresses
#1506390.
* Fixed #1327943 where the user could not abort a QIF import with bad date
formats.

2006-11-03 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed crash when adding new transactions without transaction form active
* Started work on transaction factory for register

2006-11-01 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed a problem with transaction selection

2006-10-30 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed a memory leak and crash when starting with either '--help'
or '--dump-actions'. There is still a leak with '--help' but that
remains due to the fact, that exit() is called within the constructor
and there is no way to obtain a pointer to the KApplication object
* Fixed crash when selecting a different transaction and having the
option 'keep changes when selecting different transaction' selected

2006-10-29 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Fixed a memory leak and crash when a second instance of the app
is aborted

2006-10-27 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Optimized register repainting
* Select a newly created transaction automatically once entered
* Preset From/To according to payment type when creating a new transaction

2006-10-26 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed capacity()/size() method usage mixup in the register code
* Added logic to block signal emission of the engine during transaction
matching
* Applied patch provided by Fernando Vilas to check for array boundaries

2006-10-21 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed a problem in the ledger code when loading a different file
* Fixed size problem of combo boxes in transaction form

2006-10-14 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed problem with account selector dialog
* Fixed display problem when switching to / from reconciliation mode
* Fixed flicker problem with QTable

2006-10-11 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Renamed MarkColumn in register to ReconcileFlagColumn
* Update actions when view has been changed
* Allow to toggle between cleared/not reconciled during reconciliation
by left clicking on the C column
* Added new attention marker provided by Rob

2006-10-10 Robert Wadley &lt;robntina@users.sourceforge.net&gt;
* corrected a minor spelling error.

2006-10-10 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Improved painting of fancy headers

2006-10-09 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Modified fancy markers
* Modified attention sign to use black on yellow exclamation mark
* Fixed logic that causes empty ledgers to show up

2006-10-08 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed a problem with account selection and opening the ledger view
* Fixed Tabbar::copyTabs() to use the correct ids and pointers
* Added configure check for KDChartListTableData::setProp()

2006-10-08 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Implement Finance::Quote interface

2006-10-07 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed crash when turning on the transaction form while ledger view
is visible
* Fixed problem with payee creation during transaction entry

2006-10-06 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Brought back the tabbar for the form based input
* Fixed number field appearance in transaction form

2006-10-05 Robert Wadley &lt;robntina@users.sourceforge.net&gt;
* updated titlelabel_background.png to reflect 0.9 status

2006-10-03 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed documentation problems reported by doxygen
* Changed the mark for erroneous transactions to be a white
exclamation mark inside a circle (default color is red)

2006-10-02 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Removed extra qualifiers in method definitions
* Added more changes which slipped through in yesterdays checkin
* Reduced margin in investments view
* Removed account button in ledger view
* Added transaction report to account menu
* Fixed a couple register display problems

2006-10-01 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added new ledger logic

2006-09-25 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fix the shares and values to have the correct fraction during file load

2006-09-23 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed a duplicate negation during liability account creation

2006-09-19 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Fixed a multiplication bug in budget reporting when the user specified
a yearly budget amount.

2006-09-19 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed rounding problems with automatic VAT assignment
* Fixed problem when modifying values in a transaction that has
a VAT part.

2006-09-11 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Correct spelling mistakes; patch supplied by Tom Browder

2006-09-11 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added initial chapter on documentation to PHB (provided by Tom Browder)
* Switched to dblatex for PDF file generation

2006-09-09 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added -lkdeui to the link phase of the kbanking plugin (was missing)

2006-09-06 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Removed old html documentation files

2006-09-04 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed date widget keyboard behaviour for Up/Down cursor key

2006-09-04 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Minor change to error handling for transaction matching

2006-09-03 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* UI, engine, and documentation changes to support substring
payee matching. Still not functional until the matching itself is added.

2006-08-28 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Allow for null return from online price quote source

2006-08-28 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Removed duplicate period in kmymoneygpgconfigdecl.ui

2006-08-24 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Only create the chart in the account dialog, if the resp.
tab is available

2006-08-21 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Fixed an error message in the merge transactions path.

2006-08-21 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Correct currency code for Mexican Peso to MXN

2006-08-21 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed include path to allow VPATH build environment

2006-08-20 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added a proper dialog for verifying that the user wants to match 2
transactions on the ledger.

2006-08-20 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed build environment required for debian builds
* Default for KBanking support is now enabled

2006-08-19 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Improved online stock quotes so the "Update all" continues even if
one stock is unable to be updated.
* Added a dialog to prompt the user whether he wants to disable online
quotes for this stock in this case.

2006-08-19 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Integrated new icons into the application
* Changed shortcut for 'show all accounts' to Ctrl+Shift+A

2006-08-18 Robert Wadley &lt;robntina@users.sourceforge.net&gt;
* added some icons to use in the icon bar for 'show/hide reconcile',
'show/hide unused categories', and 'update prices' (investments)

2006-08-17 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added patch provided by David Houlden to support automake and
autoheader 2.6

2006-08-11 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* GnuCash importer changes
- Support for GnuCash V2.0 files (maybe incomplete)
- Handle additional account and schedule types
- Preserve account's tax-related status
- Minor coding changes

2006-08-07 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Gnucash importer
- Add non UTF-8 support (manual selection)
- remove unnecessary "Unknown payee" references

2006-07-29 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Don't show price/value if price information is invalid

2006-07-04 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Fixed bank ID handling in QIF imports (This was the purpose of my 5/29
checkin, but that was incomplete)
* Added budget-vs-actual report logic (HTML output only, CSV still needs
to be written

2006-06-30 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed #1514522 (Zero-amounted transfer transaction leads to crash)
* Added check for minimum KDE version
* Adjusted minimum KDE and QT versions in configure.in.in

2006-06-19 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Replace parenthesis with brackets in GPG key information
* Comment modification to fix problems reported by Doxygen
* Added chapter 'Making most of KMyMoney' to manual

2006-06-15 Darren Gould &lt;darren_gould@gmx.de&gt;
* Improvements in budget implementation
(Patch applied by Ace Jones)

2006-06-14 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed #1505732 (Frequent crash on changing/opening file)

2006-06-13 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Applied patch for cppunit.m4 provided by Daniel Calvi� S�chez

2006-06-12 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed more GPG problems in KGPGFile::GPGAvailable and
KGPGFile::keyAvailable by applying the same fix as on 2006-06-05

2006-06-05 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed problem in KGPGFile::open which reported an error when a read
operation was finished before open gets around to check for the presence
of the process.

2006-06-02 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Fixed a small error with the X axis labels in charts. The label for the
first column was repeated after the last column.
* Added %mm-%dd-%yyyy QIF date format (Addresses #1449744)

2006-05-31 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Replaced the Edit Account chart generation logic with a safer
implementation that uses the public interface of MyMoneyReport.

2006-05-30 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added a "last 3 to next 3" months option for transaction filter (and
reports) date lock. This is not exposed to the user yet, so it's
only for use by generated reports.

2006-05-29 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Modified bank ID handling for QIF files so the ID's are handled the same
as other imported statements.
* Added a file filter (e.g. "*.qif") to QIF profile when an input filter is
used. (Addresses RFE #1172030)

2006-05-28 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed #1496258 (Chart doesn't repaint when configured)

2006-05-27 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Updated user manual for deleting accounts

2006-05-26 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Removed a spacer from the report configuration and find transaction
dialogs. This allows users to maximize the report config dialog,
for better selection of accounts BUT it means the ledger won't be as
large in the find transaction dialog.

2006-05-23 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Fixed tax transaction report, which was erroneously including transfers

2006-05-22 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Fix Schedule Payment History storage
* Correct account balances on database read

2006-05-19 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed usage of --enable-final configure option
* Fixed cvs example in project handbook

2006-05-18 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Removed extra qualifiers as reported by Nico Kruber

2006-05-13 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added general asset and liability accounts to be shown as payment
sources for loan schedules

2006-05-05 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Preserve file permissions for compressed and encrypted files

2006-05-02 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added Skip schedule function

2006-05-01 Ryan Buschert &lt;rbuschert@users.sourceforge.net&gt;
* Fixed #1463167 (Account change in Enter Schedule Dialog not used)
(Patch applied by Thomas Baumgart)

2006-04-29 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Fixed #1478758 (ONLINEBANKING tag not anonymized).
I just removed it from the anon file entirely.

2006-04-29 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Add better support for KOffice 1.5 libraries/includes
* Fixed a problem in MyMoneySchedule::paymentDates() and added testcase

2006-04-28 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed "Overdue schedules with a single payment cause KMM to hang
when showing the home page". Many thanks to Joel Webb for his
assistance to nail this one down.

2006-04-26 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Corrected spelling

2006-04-25 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed another location of the lockup for loan transfers

2006-04-24 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Applied patch provided by Dirk Mueller &lt;dmuell@gmx.net&gt;

2006-04-23 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed translation of messages in KBanking plugin
* Fixed pot generation again

2006-04-22 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed massive creation of temp files when saving followed by
crash of the application

2006-04-21 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed pot generation (strings in ui files in subdirs were not included)
* Added logic to extract the comment field from UI string fields
to support context translation for UI files. See
https://sourceforge.net/mailarchive/message.php?msg_id=15450159
for details
* Fixed problem with loan transfers

2006-04-16 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added lithuanian translation as provided by Donates Glodenis
* Renamed kMyMoneyAccountCombo into KMyMoneyAccountCombo
* Added KMyMoneyAccountCombo to designer widgets

2006-04-10 Robert Wadley &lt;robntina@users.sourceforge.net&gt;
* Clarified text relating to the "payout transaction" in the new loan wizard.

2006-04-10 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added backward information from split to transaction

2006-04-09 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed speed/lockup issue during startup (thanks to Markus Draeger
for providing an anonymized file to duplicate the problem)
* Improved some interfaces to avoid unnecessary object copying

2006-04-06 Robert Wadley &lt;robntina@users.sourceforge.net&gt;
* Changed text "Amortization" to "principal" in the new loan wizard.

2006-04-05 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed #1439099 (Balance entry on import messed up)

2006-03-29 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Don't show invalid smallest cash fraction value in security editor for
non currency securities entries

2006-03-25 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed #1434611 (Can't edit account to 'No Institution')

2006-03-21 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Refixed #1439701 (Deposits don't book as deposits)

2006-03-20 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Remove splits from scheduled transactions that reference not existing
accounts

2006-03-17 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated online documentation

2006-03-16 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed #1398411 (Exchange rate not used when recording a transaction)
* Disable transaction edit in context menu when account is closed
* Create new transaction when first empty entry in ledger is double clicked
* Fill buttons with securities/currencies in new price editor dialog
* Renamed widget from m_commodity in m_security in price editor dialog
* Fixed #1439701 (Deposits don't book as deposits)

2006-03-15 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed #1447764 (KMyMoney crashes when loading file)
* Fixed #1445815 (Hidden category warning when editing a category)
Thanks to Ryan Buschert for supplying a patch

2006-03-08 Martin Preuss &lt;aquamaniac@users.sourceforge.net&gt;
* kbanking.cpp: When importing transactions now also read the FIID
of the transaction (if any) and the account type.
Replaced my code for KBankingPlugin::slotAccountOnlineUpdate() with a
more elaborate version (now asks the user for the first date for which
transaction reports are to be retrieved).

2006-03-04 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Fixed a bug in statement importer where transactions imported
into an investment brokerage account were not getting auto-
filled based on payee.

2006-03-03 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Present information message, that an account must be specified
before splits can be defined while creating a schedule
* Don't override amount specified if all splits have been cleared
and the split edit dialog has been left with OK back to the schedule dialog
* Update OK button in schedule dialog when the last thing required and
added are the splits of a split transaction
* Allow manual update of KMandatoryFieldGroup
* Only show asset and liability accounts in From/To field for schedules
* Only show income and expense account in Category field for schedules
* Avoid usage of std::find with QValueLists
* Added MyMoneyTransaction::hasAutoCalcSplit() and testcase
* Fixed Doxygen problems in budget headers
* Added start of New User Wizard
* Improved KMyMoneyWizard

2006-03-02 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Fixed a subtle bug in my 2/11 checkin. When transactions were auto-filled
in based on payee, the split bank ID would get brought in, too. This
make it impossible to later match that transaction.

2006-03-01 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added MyMoneySplit::isAutoCalc()
* Optimization of MyMoneySplit return parameters
* Fixed #1441071 (Investment transaction Enter button not enabled)

2006-03-01 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Fix base currency dialog for GNC import

2006-02-26 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Reorganized layout of account editor
* Added balance history/preview to account editor
* Setup shares field for scheduled transactions
* Added KReportChartView::setProperty()
* Added new timestamp() trace function
* Removed MyMoneyObserver functionality from KMyMoneyCurrencySelector

2006-02-24 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Update developer documentation
* Speedup build process

2006-02-23 Darren Gould &lt;darren_gould@gmx.de&gt;
* Added budgeting capability on the account level
* hooked up the KMyMoneyAccountTree to the BudgetList
(Patch applied by Ace Jones)

2006-02-23 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added method to remove account from account selection widget

2006-02-23 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Fix outdated hyperlink

2006-02-22 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Sort accounts on homepage in alphabetical order
* Show number of overdue payments on homepage and sum-up payments
if more than one payment is overdue for a schedule
* Added Netherland Antillian Guilder to currency list
* Added feature to create new currency entries

2006-02-21 Bjorn Helgaas &lt;bjorn.helgaas@hp.com&gt;
* Remove needless status bar updates
(Patch applied by Thomas Baumgart)

2006-02-18 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed documentation problems in MyMoneyBudget
* Added KMyMoneyWizard framework
* Fixed build system to better support FreeBSD

2006-02-17 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Don't show closed accounts on home and institution view
* Don't allow to close accounts referenced by schedules

2006-02-15 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added feature to close/re-open accounts
* Fixed currency list in new account wizard
* Fixed layout of new account wizard opening balance page

2006-02-12 Darren Gould &lt;darren_gould@gmx.de&gt;
* Added new budget, rename budget, delete budget support
* Support for changing the budget start date

2006-02-12 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Adjusted Andreas' changes here and there a bit and checked in the
whole package

2006-02-12 Andreas Nicolai &lt;Andreas.Nicolai@gmx.net&gt;
* Added : categories menu to main menu and category specific context menu
to categories view
* Added : whole hierarchies of (unused) categories can now be deleted,
also the user can again delete an unused category and have its
child-categories moved one level up in the hierarchy
* Fixed : when account/category got deleted, the actions and currently
selected account weren't updated

2006-02-11 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added rudimentary implementation of manual transaction matching

2006-02-11 Andreas Nicolai &lt;Andreas.Nicolai@gmx.net&gt;
* Added Feature request #1395262 (Allow customization of Home Page Font Size)
* Added : font scaling (adjustable with CTRL + mouse wheel) is now
saved on exit and restored on next restart
* Added : font scaling can be directly set in "Home" configuration page,
also the "remember font size on exit" feature can be turned on
and off in the configuration dialog

2006-02-09 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added patch provided by Mathieu Ouridoux to check for a possible difference
when ending reconciliation and warn the user
* Added missing include of locale.h

2006-02-09 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Read Gnucash V2 files

2006-02-07 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Remove includehints from *.ui files
* Added support of equity group to MyMoneyFile::consistencyCheck
* Converted --notimers to --timers option
* Started adding KMyMoneyPlugin::OnlinePlugin interface

2006-02-05 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Improved encryption selection on file-by-file basis
* Bumped internal version of kmymoney2ui.rc due to recent changes

2006-02-05 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Patch to allow compile on FreeBSD
* Add (some) field descriptions to Investment Register

2006-02-04 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added feature to retrieve secret keys from GPG keyring
* Added #1280473 (Encryption on a file-by-file basis)

2006-02-03 Darren Gould &lt;darren_gould@gmx.de&gt;
* Added MyMoneyBudget write and readXML capability
* Tied XML writing/reading into KBudgetView

2006-02-03 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Extend field descriptor hint to category/memo fields

2006-02-03 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Update networth manually at the end of institution processing to get
correct numbers
* Revised mandatory field handling for KMyMoneyEdit object

2006-02-02 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Laid the groundwork for budget reports

2006-02-01 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Generalized signal connection for ledger views
* Prevent from dropping a parent account onto one of its children/grand-
children
* Support calling of configure with absolute pathname
* Fixed #1422382 (Adding payees)

2006-01-31 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added a --notimers option to get rid of my nemesis, the Timer messages.

2006-01-31 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed build system to keep po/*.gmo files from being included
in the distributioin tar-ball
* Include fixuifiles in distribution
* Do not recurse into subdirectories that are not part of the
project distribution
* Updated experimental field descriptor
* Updated tool detection logic to the latest version from the KDE repository

2006-01-31 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Experimental - add field descriptor to ledger view

2006-01-29 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Show stock accounts as children of corresponding investment account
in institutions view

2006-01-29 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Fixed #1394647 (Stock splits broken)

2006-01-29 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* A tidier implementation of mandatory fields

2006-01-28 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Automatically create securities and stock accounts for securities
found in an OFX file. Removes the requirement that the security
already be present in your file before importing.

2006-01-28 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed a translation error in the German version (thanks to Karin Capey
for reporting)
* Fixed 1400746 (Entering scheduled transaction for a loan crashes)
(at least shows a somewhat more descriptive error message)
* Removed unused code
* Show stock accounts in the correct institution if the parent
investment account is assigned to an institution
* Improved signal handling in KMyMoneyAccountTree
* Improved selection of accounts and investments
* Reactivated icon tab in accounts view
* Added #1058732 (Save Account Icon Position)

2006-01-27 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Disallow importing into a stock account.
Fixes #1395656
* Removed online banking setup options when editing a stock account

2006-01-27 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Apply required field status/color to edit schedule dialog

2006-01-26 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added fixuifiles to the admin directory and included it in Makefiles

2006-01-25 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Sort list of payees in the Reassign transactions dialog by name
* Improved RMB behaviour in some views/widgets

2006-01-25 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Modified ofx plugin to accept ofc files
* #if0'd out some broken logic in ofx importer. Some OFX files don't follow
the spec wrt totals, and this was a poor attempt to deal with that. It
ended up breaking many more common cases.

2006-01-25 Darren Gould &lt;darren_gould@gmx.de&gt;
* added XML ability and mymoneybudget class for budget feature

2006-01-24 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed #1398924 (Incorrect calculation of investment price)
* Fixed #1390918 (Can't use more than 2 decimal places in manual price update)
* Added setting capability for background of required fields

2006-01-23 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added capability to modify/add the opening balance in the account
edit dialog
* Renamed kMyMoneyCurrencySelector to KMyMoneyCurrencySelector
* Renamed kMyMoneySecuritySelector to KMyMoneySecuritySelector
* Added both of the above to Qt designer widgets
* Reworked payees view to use new KAction logic

2006-01-21 Darren Gould &lt;darren_gould@linuxmail.org&gt;
* Initial budgeting view and report mockup

2006-01-20 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Developer documentation for dialog boxes

2006-01-19 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added README.ofx to help people build with OFX support

2006-01-18 Bjorn Helgaas &lt;bjorn.helgaas@hp.com&gt;
* Remove unused and commented-out code from account, institution, money,
payee, security, and transaction

2006-01-16 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Changed definition of USE_OFX_DIRECTCONNECT to be numeric as all others
* Fixed problem with investment view context menu not poping up anymore
* Finalized integration into build system for online banking setup wizard

2006-01-16 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Updated unit tests for my last checkin

2006-01-14 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added account setup for OFX direct connect
* Revived OFX direct connect (it's been broken since the KAction changes)
* OFX direct connect is now officially supproted!
* Even added some docs

2006-01-12 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed documentation problems
* Added option to resize icons in navigation bar

2006-01-11 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Allow equity accounts in templates
* Don't modify account type while reparenting account
* Made MyMoneyFile a QObject derivative to be able to emit signals.
* Started to replace notifications with signals
* Updated some code of KMyMoneyTitleLabel
* Complete rewrite of KMyMoneyAccountTree and KMyMoneyAccountTreeItem
* Added KMyMoneyAccountTree to the Qt-Designer widget library
* Started reworking the views to be more efficient
* Removed some unused code and files

2006-01-11 Robert Wadley &lt;robntina@users.sourceforge.net&gt;
* Added 'budget' icons
* Added an alternate titlebar image with no logo.
* Added new "default_categories-template" to "C" and "en_US".
The template in "en_US" contains some additional US specific
categories (taxes, etc.)
* Renamed existing "default_accounts" to "old-default_accounts"
* Added Joel Webbs schedule E and schedule E templates to en_US

2006-01-07 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added UI for OFX online banking account setup

2006-01-04 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Fix #1393899 (Add support for new Russian Ruble - RUB)
Backport to 0.8

2005-12-28 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added a "Transaction Report" option to the Account Menu pull-down in
ledgers. This allows you to quickly get a report for the current account.
This is part of my insidious plan to weave reports throughout the app.
* Fixed a bug that prohibited reports from loading, introduced in the 12/22
checkin.

2005-12-27 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed #1390918 (Can't use more than 2 decimal places in manual price update)
* Moved maintenance logic for account's balances from MMFile to MMSeqAccessMgr
* Fixed #1318972 (Currency symbol for foreign stock mis-displayed after quote)

2005-12-26 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added #1280473 (Ecryption on a file-by-file basis)

2005-12-25 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added ability to sort report groups in pivottable reports
(e.g. Income, Expense).
* Forced Income group to come before Expenses in spending reports.
* Merry Christmas!

2005-12-24 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Handle stock accounts in forecast

2005-12-22 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Removed binary reader (left a message if someone tries to load such a file)
* Added balance to MyMoneyAccount as balance cache for all stored transactions
* Applied patches provided by Bjorn Helgaas

2005-12-22 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Fixed currency conversion in daily/weekly reports

2005-12-20 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed problem with disabled Finish button in reconcile dialog
* Added (in)equality operator to MyMoneyPrice
* Added support for new Romanian Leu
* Added reference check for price table

2005-12-17 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* changed readXML methods to a constructor of the resp. MyMoneyXXX object
* adopted testcases
* Fixed argument to qDebug call in MyMoneyStorageSQL
* Added patch supplied by Bjorn Helgaas to preset the default currency
when creating a file
* Removed some unused code

2005-12-16 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Implemented MyMoneyAccountLoan::hasReferenceTo()
* Added database documentation files to list of distributed files
* Fixed markup of database documentation to follow DTD
* Use new isReferenced() method to enable certain actions
* Applied patch provided by Bjorn Helgaas (passing references)

2005-12-16 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Document database usage
* Implement database code with timing improvements

2005-12-15 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Started to fix KDE #115863 (--enable-final and 'make check' fails)
Now we can compile but still get link errors

2005-12-12 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Applied patch provided by Andreas Nicolei to also change the payee
within scheduled transactions
* Fixed new account dialog to assign currency to returned account object

2005-12-11 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed spelling error in payees view
* Added reference check logic to objects and engine code
Calls at appropriate locations must still be added

2005-12-10 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added a Loan Transactions report. This shows loan-specific information
about transactions in loan accounts. It doesn't provide future-looking
amortization information or loan summary information.

2005-12-09 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed export of symbols missing from patch applied on 2005-11-29
* Fixed missing definition in mymoneymoneytest.cpp
* Fixed export of symbols in test environment

2005-12-09 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Trap database errors; provide timing traces

2005-12-01 Andreas Nicolai &lt;ghorwin@users.sourceforge.net&gt;
* added new dialog KTransactionReassignDlg
* new features in payees view:
- multiple payees can be selected, payee info is only shown
when a single payee is selected (otherwise info widget is disabled)
- 'in place' renaming is only possible when a single payee is selected
- renaming a payee to an existing payees name gives a warning
- whitespaces in payee names (entered via 'rename' or 'new payee'
actions) are stripped
- 'New payee' creates a unique payee name of the format 'New payee [?]'
- Action 'Delete payee' is now possible for several selected payees
- when payees are deleted and still assigned to transactions, the user
can now select an alternative payee who should be used for the
transactions
* new comparison operator for MyMoneyPayee to find payees by their id

2005-12-07 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed #1375112 (Can't configure home page)

2005-12-07 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Provide utility routine for schedule weekend option
* More database stuff (work still ongoing)

2005-12-05 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Integrate database into menu structure

2005-12-05 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added patch provided by Bjorn Helgaas entitled 'remove
superfluous QString() usage'

2005-12-04 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Reworked the category creation from within the widget
* Allow creation of complete category hierarchy in one step from
within the 'Create category' dialog

2005-11-29 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added view actions to toggle 'hide reconciled transactions' and
'hide unused categories'
* Applied patches provided by Bjorn Helgaas

2005-11-27 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Install the 48x48 application icon. Fixes KDE bug #117044.
* Inverted the sign for expenses and liabilities in reports and charts.
* Added a "7-day cashflow forecast" report under the "Net Worth Reports".
Hopefully this addresses #1238112
* Added a "year-to-month" date filter in transaction search and reports.
Addresses #1260311
* Added option to include transfers on income/expense reports. This
addresses #1297972.
* Added ability to configure reports to have days or weeks as the column.

2005-11-25 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Reworked settings dialog to be based on KConfigDialog
* Allow quit when trying to open the same file as already opened in
another instance of KMyMoney
* Install kgpgfile.h
* Removed deprecated method kMyMoneyEdit::getMoneyValue()
* Use kMyMoneyEdit::setValue() rather than setText() to load values
* Changed MyMoneyPayee to use references in constructor

2005-11-24 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added mid- and low-level support for days-based grid reports. (High-
level UI support has not been added yet.)
* Removed opening balance tests in reports, which now failed thanks to
opening balances being removed.
* Fixed a bug where duplicate securities were not getting picked up
correctly in online quotes.

2005-11-22 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Better handling of missing &amp;...; sequences in HTML returned during online
quotes.
* Added an Account button to the checking &amp; loan ledger

2005-11-19 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Fix doxygenation...

2005-11-19 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Doxygenate SQL stuff

2005-11-18 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added autosave patch provided by Marcellino Villarino with a few
modifications
* Combined multiple ways of commiting a scheduled transaction into a
single path
* Send out notification when MyMoneyFile::setUser() was called

2005-11-18 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Initial checkin of SQL backend code

2005-11-16 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added factor feature for online price updates

2005-11-15 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Converted schedules view to use new action structure
* Don't colorize lines in schedule view that do not contain a schedule

2005-11-14 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added date modification widget to KCurrencyCalculator to allow
manual price updates in a single dialog
* Converted investment view to use new action structure

2005-11-13 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed problem with opening balance transactions for investment accounts
* Fixed a problem when the current selected investment account is removed
* Added toolbar button to start KCalc

2005-11-12 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Applied patch as provided by Bjorn Helgaas to collect user
information in a MyMoneyPayee object.
* Fixed display suppression of unused categories

2005-11-11 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Moved most context menus to kmymoney2ui.rc
* Modified menu structure
* Revised logic around actions, context menus
* Adapted plugin interface to new action handling
* Print error when running 'make package-messages' in a VPATH environment
* Differentiate 'split transaction' for usage as category and caption

2005-11-08 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Moved some action slots to KMyMoney2App:
* Revised KAction handling
* Fixed date calculation in isOverdue testcase, enhanced testcase
* Restructured action handling
* Renamed some members in the new account wizard
* Changed open balance handling in new account wizard to use
'opening balance transaction'

2005-11-06 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated files for german and galician translation

2005-11-06 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Save/restore payee email address
Could backport to 0.8

2005-11-05 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed #1348087 (Save not enabled)
* Added command line option '-n' to startup without the last file opened
* Adjust the size of the 'move account to ...' selector based on the
length of the account names

2005-11-04 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added ledger sort mode for 'order of entry' (based on transaction-id)
* Fixed MyMoneyScheduled::isOverdue() to return correct information
* Added testcase for MyMoneyScheduled::isOverdue()
* Fixed calculation of net-worth in views
* Added 'Move to account ...' functionality

2005-11-03 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Make MyMoneySchedule::transaction() return a reference
* Catch exceptions during forecast processing

2005-11-02 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed #1323166 (Investment balances on homepage are the number of shares)
with patch provided by Maik Hinrichs
* Applied patch to add *.a to .cvsignore as provided by Maik Hinrichs

2005-10-31 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed MyMoneyStorageANON to write out correct data
* Added performance test feature
* Performance improvements in accounts view and engine code
* ::timetrace changes
* Write error message about thrown exception to stderr in pivottable.cpp
* Added patch required for gcc 4.1 as provided by Stephan Binner
* Added patch to remove invalid includehints from *.ui file as provided
by Stephan Binner

2005-10-27 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Updated MyMoneyReport object to use MyMoneyObject
* Change MyMoneyStorageANON to calculate the quasi-random offset
factor once and use it every time.
* Also removed some extraneous consts from returned-by-value objects

2005-10-23 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Allow entry of new categories; check Payee present
Backport to 0.8

2005-10-19 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Don't crash on schedules w/ strange transactions during forecast
* Only allow entering of schedules when all needed values are present
* More changes due to introduction of MyMoneyObject

2005-10-18 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added MyMoneyKeyValueContainer::clear()
* Added testcases and fixed a few problems here and there in the read/write
XML methods
* Added configure options for Mandriva 2006 as reported by Paul Doig

2005-10-17 Robert Wadley &lt;robntina@users.sourceforge.net&gt;
* Made corrections to the 'what's new' html page.

2005-10-17 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added writeXML and readXML to MyMoneyInstitution, MyMoneyPayee
* Added testcases for those routines
* Changed storage MyMoneyStorageXML to use these routines
* Fixed non-working quit button, shortcut and menu entry
* Don't allow negative values for 'enter schedules # days in advance'

2005-10-16 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated MyMoneyObject as discussed on mailing list
* Added writeXML and readXML methods to MyMoneyAccount and
MyMoneyKeyValueContainer
* Added testcases for those routines
* Changed storage MyMoneyStorageXML to use these routines
* Added number of stored prices to file info dialog

2005-10-16 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Fix 1323157 - Price Editor showed reciprocal value
* Gnucash reader - set shares = value where split currency = tx currency

2005-10-11 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Preliminary signals for proposed file ops plugin

2005-10-10 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Don't include todays transactions twice in the forecast

2005-10-09 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added fileinfo dialog to show object counts of current file
* Added traces for time measurement
* Added patches to Makefile.dist as provided by Bjorn Helgaas
* Incremented min automake version to 1.7.9

2005-10-09 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Fix a typo which has long annoyed me, tho' nobbut a Brit would notice

2005-10-08 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added MyMoneyObject as base object to be used for all storable
MyMoneyXXX objects.
* Converted MyMoneyAccount to use MyMoneyObject

2005-10-07 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Gnucash reader - link dialog help to online manual (requires 'make install')

2005-10-04 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed #1221979 (KMyMoney won't start)

2005-10-03 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Fix transaction types for Create Schedule

2005-09-30 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Linked help button in reports configuration dialog to online manual

2005-10-02 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Fixed a raft of broken logic in calculation of scheduled payment dates
* Added option to include scheduled transactions in pivottable reports.

2005-09-30 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Vastly simplified OFX dependency checking. Now relies on pkg-config to
sort out OFX versions.
* Enabled debugging details for all load-time fixups. I want to root out
the source of all fixups upon load.
* Added debugging information around ofx direct connect sessions, in an
attempt to troubleshoot certain problems for users in that area.

2005-09-30 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Made calculatorButtonVisible available for Qt-Designer
* Added kMyMoneyPayee to designer library
* Removed calculator button from minimum balance field in new account dialog

2005-09-30 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Avoid problem when creating schedule from Check or ATM transaction.
Unlikely, but...

2005-09-29 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed library dependency during build by including the required object file

2005-09-28 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Make sure UIC uses our own widget library
* always build widget library, install for designer upon request
* Applied patches by Erik Johansson to be Up/Down the same as +/- and
the day field receives focus when the widget is selected no matter
which date format is selected
* Fixed #1289026 (Transfer creates flashing red entry in destination account)

2005-09-27 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Removed debug output for price conversion
* Added minimum balance for asset/liability accounts
* Improved 90 day forecast to show warnings when balance drops
below the minimum balance setting or even below 0

2005-09-24 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Fixed #1289811 (Account balance incorrect when opening kmymoney) by...
* Disallow importing a QIF transaction that transfers from/to the same
account.
* Properly fix the problem where KDE currency settings would interfere with
QIF date parsing.

2005-09-24 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added 90 day forecast to home view

2005-09-22 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added MyMoneyPrice unit tests
* Fixed #1285995 (Accounts view shows wrong balance with foreign currency)
actually a problem in conversion rate extraction in MyMoneyPrice
* Added date parameter to totalValue() and accountValue() in MyMoneyFile

2005-09-22 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Fixed spelling problem reported by Jochen Rundholz

2005-09-20 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed #1264916 (Cannot edit fund details in investments)
* Set default fraction for new investments to 100
* Improved numeric validator for kMyMoneyEdit objects

2005-09-19 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Editorial changes to the project handbook
* Use correct character set for HTML version of project handbook
* Added note about usage of interest field in reconciliation wizard to docs
* Added version number to title page of online documentation

2005-09-18 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added version history for the project handbook
* Added chapter about translations provided by Jochen Rundholz to project
handbook
* Fixed problems around editing multi-currency transactions based
on a different currency than the current account.
* Balance transactions in anonymized files by using a single,
somewhat random factor per transaction

2005-09-16 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed #1288592 (Crash with Auto fill)
* Added danish translation file as provided by Daniel Sørensen
 &lt;daniel@dumazz.dk&gt; (he says only 20% are done but he does not have time
to continue)

2005-09-15 Robert Wadley &lt;robntina@users.sourceforge.net&gt;
* replaced reports icons
* added some 22x22 "chart style" icons

2005-09-14 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed #1287850 (Empty action box)
* Fixed #1286028 (Edit Account dialog shows wrong currency)
* Fixed #1286033 (KMyMoney hangs up)
* Fixed result calculation in KCurrencyCalculator to produce smaller
numerators and denominators
* Added unit testcase for problem #1291044
* Fixed #1291044 (Cannot apply $0.01US interest expense in a split)
* Fixed typos reported by Jochen Rundholz

2005-09-13 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Updated to CVS version of libOFX
* Added support for fees to OFX and Statement imports
* Fixed sign reversal on OFX dividend imports
* Support QIF imports without a "Type" line
* Added Wallstreet-Online.DE online quote source
Submitted by Marc Zahnlecker &lt;tf2k@users.sourceforge.net&gt;

2005-09-09 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Changed minimum KDE version to 3.2 in configure.in.in
* Added patch provided by Erik Johansson to display the selected date
including the weekday in a KPassivePopup
* Adjusted build system to work from in initially empty sandbox

2005-09-09 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* GnuCash Reader - fix locale-dependent problem on schedules
- stop crash on invalid price

2005-09-04 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Restrict type of accounts allowed as payment/asset accounts for loans
* Sort transactions in ledger on same date by check no and then by value
* Keep check number if already entered when autofilling a transaction
* Updated link to PDF version of project handbook
* Added local rules to make online versions for project handbook and manual
(requires a script which I currently only have on my local system)
* Changed spelling of Ruble to be consistent
* Translate currency names when loading file with different language set
* Don't override user supplied check no when loading prev. transaction
* Fixed #1264927 (check number for same day not in ascending order)
* Removed GWEN logger settings as requested by Martin
* Added hint to sort order of transactions in online manual

2005-09-04 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Update docs to say that you have to remove all the transactions from an
account before deleting it.
* Fixed a bug where in online price update, the local KDE currency was being
shown for foreign stocks, instead of the stock's own currency.
* Added VWD.DE online quote source, useful for german funds
* Added ability to handle quote sources where the decimal separator
is not period
* Added ability to handle quote sources which contain a thousands separator
* Added docs on how to find the trading symbol for a security supported
by Yahoo
* Chart documentation

2005-09-04 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added support to build widget library for Qt-Designer (needs to be
enabled via configure)
* Include new options in configure summary.
* Separated configure summary in user and developer optionsa
* Separated generation of kmymoney2.pot from merging of language files
* Allow to run in VPATH environment
* Added patch provided by Mathieu Ourioux to hide reconciled
transactions in ledger view
* Added patch provided by Erik Johansson to allow * to decrement the
date and use T to set date to today
* Updated documentation
* Include a missing string in pot file
* Use KMessageBox rather than QMessageBox

2005-09-02 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Fixed legend on circular charts
* Don't give option to display charts for querytable reports
* Don't populate default charts if charts are not compiled in
* Change all uses of "security" to "encryption" where that's what it means.
Eliminates terminology conflict with "security" where it means "stock/etc".
* Change uses of "Transfer from/to" to "Convert from/to" in currency converter.
Linguistically more accurate.

2005-08-29 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* GnuCash Reader
- correct split action types
- detect checks properly
- fix locale-dependent problem on schedules

2005-08-27 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* When importing investments via OFX into an investment account with an
associated brokerage account, use that account for the buys/sells &amp;
cash dividends.
* When importing investments via OFX into an investment account with an
associated brokerage account, allow non-investment transactions. Dump
them into the brokerage account.
* Added fees to MMStatement::Transaction struct for later use
* Commented some obscure aspects of MMStatement
* Also match imported securities by name if they have no symbol

2005-08-26 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added ability for import plugins to specify the file format filter to use
when looking for those files
* Added "*.qfx" to file format filter in OFX import plugin
* Added three new default chart-type reports
* Added a new "Charts" section to the reports summary which collects all
reports that are set to display initially as charts

2005-08-25 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Fixed crash in chart configuration
* Added stacked bar chart
* Changed ring chart to be proportional widths (not sure I like this)
* Changed pie charts to look 3D

2005-08-23 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Chart configuration dialog
* Added line, bar, pie &amp; ring charts
* Ability to configure a report to always show up as a chart
* Ability to toggle grid lines and printed data values
* These new values are saved to the KMM file.

2005-08-22 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Fixed text string problems. Thanks to Jochen Rundholz for identifying.
* Attempted to fix QIF import problem where it was hampered by having
the KDE locale's negative sign position set to "Parens Around".

2005-08-19 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Modified pivottable reports to be configurable to four levels of detail
(sub-accounts, top-accounts, groups, total)
* Charting and HTML rendering respects the new levels of detail. This
makes charting much more useful, IMO.

2005-08-19 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed #1263755 (Crash when moving account from one subgroup to another)

2005-08-18 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* updated admin/acinclude.m4.in
* Fixed KDE BUG #110851 (Wrong date formatting in forms)

2005-08-17 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed #1260732 (Reconciling multiple currencies) Thanks to
"pSmart" for reporting
* Fixed #1261797 (Manual price entry/update does not store values)
* Added complete check for KDChart availability

2005-08-16 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Modified charts to show one line per sub-account or per top-account
depending on the setting of the "Show Top Accounts" configuration option.

2005-08-15 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Initial integration of charts. For pivottable reports (income/expenses and
net worth), the "Chart" button will display a single line graph of the
total. To use it, ensure you have libkdchart.la and configure with
--enable-charts.

2005-08-15 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed #1259351 (Icons view doesn't honor single click preference)
* Fixed minor problem in build environment
* Bumped minimum required version of KBanking to new AqBanking package

2005-08-14 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Explain how to convert from another encoding to UTF8
* Fixed a problem in my last checkin where libofx was
required to build CVS version.

2005-08-13 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Fixed a recently-introduced bug where the wrong price
source was being used.
* Updated OFX Direct Connect to use libofx 0.8.0
* #if0'd OFX unit tests this will get moved to a plugin unit tester.
* #if0'd code to create OFX response files. This will get moved to
libofx.
* Added support for stock splits

2005-08-12 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Catch exceptions in QIF importer when turning back on notifications

        