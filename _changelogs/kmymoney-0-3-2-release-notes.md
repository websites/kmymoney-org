---
title: 'KMyMoney 0.3.2 Release Notes'
date: 2001-05-10 00:00:00
layout: post
---

0.3.2

Javier Campos Morales javi_c@ctv.es:
- Modified the start dialog and added some new icons.
- Modified the startup logo code and redone some of the pictures and icons.
- Changed some of the the startup code in kmymoney2.cpp to reflect the new start dialog.
- Modifed main.cpp  to reflect new KDE 2 programming idioms. 
  
Michael Edwardes mte@users.sourceforge.net
- Modified the ui resource file to make the toolbar positioning better.
- Started creating some basic documentation in textual form.
      Each directory will soon have a readme-&lt;something&lt;.txt file containing
      basic documentation for that particular directory. It is NOT an attempt
      at user documentation!
- Made the kmymoney2.desktop file work.
- Removed the start dialog designer ui file from the project.
- Remembered to remove the localised call to createGUI in kmymoney2.cpp:initActions()
- Added the new developers names and emails where needed.
- Made the tree ready for CVS and put it in the online repository.
- Moved this file to Changelog.original so the developers can now use a real Changelog!

  