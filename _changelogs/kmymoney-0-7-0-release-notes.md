---
title: 'KMyMoney 0.7.0 Release Notes'
date: 2005-05-14 00:00:00
layout: post
---
2005-05-14 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Display all values in the investment transaction form as they show
up in the edit widgets
* Display currency symbol in investment transaction's value column
* Updated language specific files
* Released 0.7.0

2005-05-13 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Gnucash Reader: allow edit of suspect scheduled transactions

2005-05-12 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated am_edit to state currently found in KDE admin directory
* Fixed am_edit to setup dependencies correctly
* Show currency symbols in investment ledger

2005-05-12 Robert Wadley &lt;robntina@users.sourceforge.net&gt;
* Changed the version number in the "about this release" page to 0.7

2005-05-09 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Update field descriptions according to tab for all fields empty
* Fixed scheduled transactions to work from non-base-currency accounts
* Fix commodity of scheduled transactions if empty when loading file

2005-05-08 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Update balances in accounts and institutio view when prices are changed
* Fixed handling of 'parens around' handling for negative values
* Fixed layout of details tab in FindTransactionDlg
* Speedup processing of page-up/page-down of register display

2005-05-07 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed flickering of register when running w/o transaction form
* Fixed crash when loading different file
* Fixed initial selection of account in investment view

2005-05-06 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Force pointers to deleted objects to point to 0x00
* Fixed update of transaction type tabs and field description strings
* Fixed update of transaction type widget in ledger entry mode
* Removed unused code in kMyMoneyCombo
* Fixed signal emition of kMyMoneyCombo

2005-05-01 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Switch to ledger view when double-clicking an account in the
institutions view
* Fixed title label of institution view

2005-04-30 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed display of positive net-worth (no more &amp;nbsp;)
* Added auto increment check number
* Fixed a bug in the build system around the man pages

2005-04-29 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed crash when adding new investment transactions via clicking
on the emtpy line at the end of the ledger.
* Fixed some problems around widget updates during entry of
investment transactions
* Fixed initial column width of transaction list in payees view
* Removed non-existant files in doc/en from distribution list

2005-04-24 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added methods checkNoUsed() and highestCheckNo() to MyMoneyFile
* Warn user if check no is already in use
* When creating a transaction based on a previous one, set
check no to 'highest number used' plus 1.
* Update check no field if scheduled bill is paid with check
(This partially fixes #972028)

2005-04-24 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added setting for color used to identify values that are based
on missing conversion rates (default=blue)
* Fixed values in the institution view when stock accounts are involved
* Fixed sign of values in institution view
* Added KGlobalLedgerView::accountId() to return id of selected account

2005-04-21 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed editing schedules with a single split
* Improved exception messeages if split is not found
* Added capability to select reconcile from menu / toolbar

2005-04-19 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Removed deprecated method from KLedgerView
* Fixed creation of categories when editing transaction with
single split

2005-04-18 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Preserve file permission when saving
* Don't store ID for splits within transaction. They will be re-assigned
during file load anyway.
* Start editing split when any printable key is pressed
* Fixed crash in split editor when quitting using ESCape
* Advance selection bar to next split when saving a split

2005-04-18 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Applied patch #1183190: Online quotes locale patch
Submitted by Andy Thaller &lt;gandy@users.sourceforge.net&gt;
* Fixes an error in online quotes on locales which use a different
comma and period placement scheme from the quote source.

2005-04-17 Robert Wadley &lt;robntina@users.sourceforge.net&gt;
* Added a background image

2005-04-15 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added template exporter
* Allow start editing in split editor with any key if table has focus

2005-04-13 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Removed config.h.in from repository because it is automatically generated
* Fixed initial setting of LED in gpg config dialog when key field is empty
* Added ability to turn off VAT handling for specific accounts

2005-04-11 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added configure switch to turn off online tests by default
* Added this switch to the respective unit tests

2005-04-08 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed generation of symbolic links to header files to support
building for different environments and using the header files
of the sandbox and not the one's installed in $KDEDIR/include/kmymoney
* Fixed buildsystem again to use top_builddir in some circumstances rather
than top_srcdir
* Make sure to remove temporary files during distclean
* Fixed distribution directory for conditional subdirectories (plugins)

2005-04-06 Robert Wadley &lt;robntina@users.sourceforge.net&gt;
* Changed the size of the title text in welcome.html and whats_new.html
to allow for languages that require more space.
* Added the capability of using a background image for the welcome and
whats_new pages.

2005-04-06 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Fix spurious re-display of split editor window

2005-04-05 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Remove old GNC reader

2005-04-05 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Tidy up converter Makefile

2005-04-03 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Fix QIF import to set shares as well as value. This bug caused reports to
be all 0's until the file was saved and reloaded. The reload ran it
through KMMView::fixTransactions(), which fixed the problem.
* Added a custom '#' field which contains the unique transaction ID. The QIF
importer will refuse to load a transaction which contains the same
transaction ID as an existing transaction.
* Separated --enable-ofx into --enable-ofxplugin and --enable-ofxbanking.
ofxplugin enables ofx support only in the plugin. It defualts ON, and
should be left on for the release. ofxbanking enables OFX direct
connect, and causes the main application to require libofx. This
is experimental and is not desired for production use.
* More thorough error checking in KEnterScheduleDialog.

2005-04-03 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Move GnuCash reader to converter directory

2005-04-01 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Fixed #937728: Opening balances sometimes not picked up in QIF files
* Set the 'finish' button to be default in the last page of the new account
wizard
* Fixed bug in account select dialog. When using it to create a loan
account, the schedule is not created, which causes the account to need
to be "fixed" when its loaded.

2005-03-31 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added support for utf8-encoded QIF files
* Cleaned up some character-based string handling in QIF imports

2005-03-31 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Set all values when entering scheduled transactions
* Don't show 'More ...' in home view, if there are no more scheduled tx

2005-03-30 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed a crash on startup when compiled w/o debug options
* Fixed behaviour of briefScheduleWidget
* Fixed layout of briefScheduleWidget
* Made widgets inside briefScheduleWidget more readable

2005-03-28 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added cancel button to new file dialog
* Hide load button for address data if no data is found for the owner
and also shows a message if the logic is started anyway.
* Fix enabling reports view
* Display currency symbol for payments

2005-03-29 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Cleaned up welcome html pages a bit, with some help from Rob.

2005-03-28 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed INT64_MIN, INT64_MAX definition problem
* Fixed initial startup behaviour
* Added tabs for personal information and transaction history in payees view
* Remove symlinks to header files before (re-)creating them during configure

2005-03-27 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added new welcome and whats new screens
(Submitted by Robert Wadley &lt;rob@robntina.fastmail.us&gt;)
* Hooked up logic for all the links and added a bunch of whats new text

2005-03-26 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Handle QIF-imported opening balances the new way
* Terminology changes to US-english-ize the base text
(Submitted by Robert Wadley &lt;rob@robntina.fastmail.us&gt;)
* Fixed #1171039 (Made spaces in value amounts non-breaking in home view)
* Fixed a bug in QIF import. When a transfer transaction was imported,
the remainder of the transactions were imported into that account.

2005-03-26 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Remove window size display from caption when compiled w/o debug support

2005-03-25 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Avoid inadvertent overwrite of foreign file types

2005-03-24 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Removed unused files
* Added missing header file to support 'make bcheck'
* Remove symlinks to header files before re-creating them

2005-03-23 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fix mymoneymoneytest.cpp to work with 64 bit systems
* Fixed description of kbanking.m4 and libofx.m4 to match behaviour

2005-03-22 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Resolved doxygen warnings in kfindtransactiondlg.h
* Added missing include stoppers in kmymoneypricedlg.h
* Fixed duplicate #defines when building with --enable-final
* Revised install/uninstall instructions in icons subdirectories to
support $(DESTDIR) setting used in e.g. RPM builds
* Revised build system to support 'make distcheck'

2005-03-22 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added "Are you sure you wish to cancel?" to import verify dialog.

2005-03-21 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Modified MMTransaction::isDuplicate to consider split numbers
* Added an option to MMQifProfile to turn off the check for duplicate
transactions
* Made QIF import account type check less brittle by ignoring case and
white space. Also added "Credit Card" as a type instead of just
"CCard".

2005-03-21 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Reworked build logic (Makefile.am)

2005-03-19 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed reparenting of stock and investment accounts in engine
* Started to setup environment for external plugins

2005-03-19 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Cleaned up small issues in reports wrt foreign stocks. These were
exposed by the fixes in Tom's last checkin.
* Edited the warning message for foreign stocks.
* Infrastructure for QIF investment imports. Doesn't actually handle them
yet, but the parsing is in place to do so. Also warns the user that
investments are not supported.
* Terminology changes to US-english-ize the base text
(Submitted by Robert Wadley &lt;rob@robntina.fastmail.us&gt;)
* Fixed all warnings
* Removed old online quotes system

2005-03-18 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Allow payment of stock transactions to be in different currency
than the stock's security

2005-03-18 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Fixed Makefile.am mkinstalldirs for kmymoney.desktop

2005-03-17 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Gnucash Importer
- Handle A/Payable and A/Receivable account types

2005-03-16 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed splash screen handling
* Fixed handling to delete investment accounts

2005-03-15 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added new titlebar background
(Submitted by Robert Wadley &lt;rob@robntina.fastmail.us&gt;)
* Removed text shadow in title label widget. I couldn't find a way to ensure
it looked good in all color schemes.

2005-03-14 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Revised install/uninstall instructions in icons subdirectories
* Improved reparenting of accounts
* Don't allow removal of accounts that have transactions assigned
* Remove reference to kapptest. KDExecutor is much better ;-)
* Removed unused icon file kfm_home.png
* Allow removal of unused stock accounts

2005-03-13 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Online quotes will now only fetch quotes for the first security using a
given trading symbol. It's assumed that multiple securities using the
same symbol is an error condition.

2005-03-12 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Removed most OFX dependencies in the main program--all that's left is the
OFX Direct Connect and the OFX importer test cases
* Reworked Web Connect to work on ANY plugin-handled file, not just
OFX.
* Removed "stripped.txt" from being output during online quote retrieval

2005-03-12 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added option to control whether selecting a different transaction/split
using the mouse should be treated as Enter (saving changes) or Cancel
(discarding changes).
* Added option to activate auto fill feature in transaction entry
* Fixed crash when scrolling payees view

2005-03-11 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Honor price precision setting in Online Quotes
* Honor price precision setting in Investment Summary

2005-03-11 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* AutoEnter Schedules
- 1105503 - avoid lockup, produce debug data

2005-03-10 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Qif Profile Editor
- Added patch provided by Tami King
- Reworked layout management to align objects

2005-03-09 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Improved future payment display in financial overview page
* Revised KDE menu integration

2005-03-09 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Gnucash import
- Fix handling of credit card accounts

2005-03-08 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Fixed a crash in checking ledger when attempting to edit the splits of
an investment transaction
* Added memos in the case of split transactions to transaction reports
(Submitted by Andy Thaller &lt;gandy@users.sourceforge.net&gt;

2005-03-07 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added duplicate split feature
* Added menu dynamics to the HBCI plugin

2005-03-04 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Gnucash import
- Fix #1151541 handle invalid value in splits causing crash
- Fix #1078850 remove dailym_f schedule which we can't currently handle

2005-03-03 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added logic to update color settings for CSS

2005-02-20 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Pulled titlelabel_background.png out of the attic
* Allow plugins to access the new title label object
* Added new title label to AqBanking plugin
* Added icon to institution list
* Removed unused icon files
* Install title label background image
* Added base class for views

2005-02-19 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added icon to institutions in institution view

2005-02-18 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added OK and Cancel buttons for the mouse users to the split widget
* Added .cvsignore to ofx plugin directory

2005-02-16 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Preload split widget when category is not known
* Assign values when moving focus from e.g. amount field to split button
* Cancel any edit activity during reconciliation when pressing finish
or postpone button
* Removed unused member variables of split widget
* Fixed abnormal termination during read of encrypted files
* Improved performance of KGPGFile::atEnd()

2005-02-16 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Changed the title bar to draw the text in at runtime. Thanks to THB.
* Robert Wadley's latest 32x32 ledger icon
* Moved reports into kmymoney2/reports directory, and exploded the reports
source files into more files.
* Moved converter unit tests into kmymoney2/converter directory.

2005-02-16 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed reported documentation inconsistencies

2005-02-16 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Gnucash import
- fix scheduled transactions &gt;999.99

2005-02-15 Ace Jones &lt;acejones@users.sourceforge.net&gt;
Latest Robert Wadley icons
* Made the colors more consistent,
* Fixed the contrast to better match icons next to them
* Changed the reconcile icons to make it more obvious that it is an account
being reconciled.
* Changed the find_transaction icon to look like its actually magnifying, and
added a white row.
* Toned down the green in the '+' symbols
* Fixed ledger icon (accidentally cut off part of the icon when I exported
the images) which causes the ledger icon to look too big.

2005-02-15 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added complete re-work of the split transaction widget and dialog
* Increased height of transaction form items
* Another s/Splitted/Split/
* Improved MyMoneyMoney::operator ==(const MyMoneyMoney&amp;)
* Added new MyMoneyTracer constructor

2005-02-15 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Gnucash import
- doxygen update
- add online quote source to imported securities

2005-02-13 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Fixed doxygen warnings
* Added new icons supplied by Robert Wadley

2005-02-13 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Gnucash import
- normalize monetary amounts
- correct Investment Account description

2005-02-12 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Refactored PivotTable::AccountDescriptor into ReportAccount, which is now
a subclass of MyMoneyAccount, with hierarchy, currency, and
other convenience methods.
* Propagated ReportAccount throughout the entire reporting engine. The
reporting engine should never ever use a MyMoneyAccount.
* Numerous object logic cleanups throughout reports. I moved each method to
the proper object upon which it acted. Should improve readability. This
involved pushing a lot of functionality into the MyMoneyReport class.
* Fixed pivot table reports to handle foreign stocks
* Fixed report unit test helpers to deal with foreign currencies better
* Fixed MyMoneyMoney multiplication overflow in account query reports
* Made MyMoneyMoney::reduce() public so I can use it in reports

2005-02-10 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Use the correct account when selecting 'edit' from within
the institution view

2005-02-07 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Gnucash import - anonymizer changes, restore Ace's credit
* Fix editing of some frequencies (e.g. Quarterly)

2005-02-05 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Fixed account query reports to handle foreign stocks
* Minor clean-ups in query reports
* Really fixed report unit tests to deal with changes in handling of
opening balances
* Updated artwork supplied by Robert Wadley
* Added Tony Bloomfield to the credits

2005-02-03 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added more colorful title labels for the views in MyMoneyView using
artwork and conceptual design supplied by Robert Wadley

2005-02-02 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed name of #define to make memory leak check feature work
* Added MyMoneyTracer trace logic
* Fixed a nasty bug in categories view
* Added 0.7 splash screen

2005-01-31 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Updated unit tests to match new handling of opening balances

2005-01-30 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Moved OFX import logic to a plugin.
* Some refinements to GNC import options dialog

2005-01-30 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Gnucash import - implement option dialog

2005-01-28 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Fixed a subtle bug in reports that caused a crash if you double-clicked on
empty areas.
* Relaxed MyMoneyStorageANON::writeSecurity to pass through the trading
symbol, trading currency, and quote source for securities. This is useful
for debugging online quotes problems.

2005-01-26 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Changed open balances handling to be transaction based
* Show balance per institution in institution view
* Added recursive option to MyMoneyFile::accountList()

2005-01-26 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Gnucash import
* Properly fix coding error picked up by stricter compiler!

2005-01-24 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Gnucash import
* Fix coding error picked up by stricter compiler

2005-01-22 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Hooked up the "online price updates" button in the price editor

2005-01-20 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Gnucash import
* Scheduled transactions default to Bills rather than Deposits
* Reinstated default currency calculation

2005-01-20 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Allow modification of end date for scheduled transactions at any time
* Allow to add splits to scheduled transfers
* Loan wizard now treats final payment as 0 if two fields are left blank
* Updated handling of calculated last payment vs. final payment
* Fixed rounding problems in financial calculator
* Added testcases to financial calculator based on user's error reports
* Enter manually added price information into engine

2005-01-18 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added MSN.CA as another canadian source
* Fixed the online quote config dialog that I broke in my last checkin

2005-01-14 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* add shared library support for external plugins
* Fixed #1074957 (Create loan accounts)
* Improved the check for a valid GPG id in the settings dialog
* Fixed crash when closeing the base currency selection dialog
* Allow changing the amount of a transaction w/o having a categroy assigned
* suppress version control of *.lo files

2005-01-12 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Fixed reports test failure caused by fixing bug #1079427 ("Checkings" now
reads "Checking")

2005-01-11 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* removed unnecessary include at the end of kmymoneyonlinequoteconfig.cpp
* Fixed #1079427 ("Checkings" should read "Checking")
* Fixed problem with generated header files
* Added general plugin structure and mechanism
* Made KBanking a real plugin

2005-01-10 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added customizable date parsing to online quotes
* Added canadian mutual funds source
* Created a general-purpose date format parsing object
* Added date-parsing unit tests

2005-01-07 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Online price quotes for foreign stocks will now automatically include a
price quote for the foreign currency

2005-01-06 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Fixed single-shot price updates
* Added functionality to retrieve single-shot currency price updates,
although this is not hooked up to the UI.
* Added right-click option in price editor to do an online price update
for a single currency pair
* Cleaned up parsing of multiple currencies passed around as a single
string in online price updating pipeline

2005-01-05 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added online quotes for currencies

2005-01-05 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Gnucash import - complete rewrite
* Uses Qt SAX2 XML Parser; simpler interface, more structured code
* Uses imymoneystorage interface rather than imymoneyserialize
* Separates XML reading and conversion code
* More user options on Schedules and Investments
(selection dialog to be written)
* Better exception handling and reporting
* Conversion code tidied up considerably

2005-01-04 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Hooked up multiple sources for online quotes
* Moved online quote logic out of KEquityPriceUpdateDlg and into the new
WebPriceQuote class.
* Moved online quote configuration logic out of kMyMoneyOnlineQuoteConfig
and into the new WebPriceQuote class.
* Fixed a test case in KReportsViewTest::testInvestment to use the correct
year (2004, not "current year").

2005-01-03 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* added currency New Turkish Lira (YTL) for newly created files
* reassigned id of New Israeli Shekel from ILS to NIS for newly created files
* added configuration dialog for data encryption
* added support for gpg-agent in libkgpgfile
* updated drag&amp;drop support
* Added README.Fileformats

2005-01-02 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Show preferred investment accounts on home page
* Added tip on anonymous save
* Fixed creation of securities - do not override information of existing
securities for new stock accounts
* Modified link order to avoid problems with unresolved symbols
* Added drag and drop to accounts, categories and institution view

2005-01-01 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Fixed tab order in KNewFileDlgDecl

2005-01-01 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Happy new year to all KMyMoney developers and users!
* Show preferred investment accounts on home page
* Added tip on anonymous save
* Fixed creation of securities - do not override information of existing
securities for new stock accounts
* Added setting for memory leak checker to configure scripts

2004-12-31 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed ChangeLog entry to show the correct author
* Added more price entry logic (manual add, delete, etc.)
* Added icon to institution view
* Removed settings logic to distinguish between old and new accounts view
* Resolved a few warnings about deprecated methods
* Display a note in accounts and categories view if categories are hidden
by settings because they are unused (are not referenced by any split)
* Enhanced logic to convert account values into the base currency
* Fixed MyMoneyFile::security(const QCString&amp; id) to return the baseCurrency
in case id is empty
* Fixed designer version problem in dialogs/keditschedtransdlgdecl.ui
* Added configure summary
* renamed kmymoneyonlinequoteconfig.ui into kmymoneyonlinequoteconfigdecl.ui

2004-12-29 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added simple category matcher for OFX import. This fills in the rest of an
imported transaction using the most recent transaction with this payee as a
model.
* Maintain backward compatibility with Qt designer 3.2
* Removed support for older versions of LibOFX
* Removed check for libuuid (not using this anymore, see 12/14 checkin)

2004-12-28 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Fixed #944780 (Schedule Brief Widget)
* Fixed # 972028 (Does not honour Scheduled Method)
* Fixed #987465 (cant edit scheduled transactions)
* Fixed #1012273 (Cant enter overdue payments at real date)
* Fixed #1035730 (No number edit in enter schedule dialog)

2004-12-28 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* 64bit fixes for MyMoneyMoney

2004-12-28 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added new admin directory from kdelibs 3.3.0

2004-12-23 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Gnucash import - Christmas crash fix
* setTradingCurrency for new regime

2004-12-20 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Fixed bug in net worth reports with investments, related to new price scheme
* Added test case to catch this bug next time

2004-12-19 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Adjustments to work with new price scheme

2004-12-18 Kevin Tambascio &lt;ktambascio@users.sourceforge.net&gt;
* Added a new institution view, so users can easily edit
institution properties.

2004-12-17 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Maintain backward compatibility with Qt designer 3.2

2004-12-17 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added some changes for KDE 3.0 backward compatibility
* Added patch supplied by Leonardo Skorianez to load one's own data
* Added definition of QT_IS_VERSION to kdecompat.h
* Remove reconciliation flag if creating a transaction from a previous one
* Reduced flickering in ledger views
* Repackaged MyMoneyEquity and MyMoneyCurrency into MyMoneySecurity
* Added new investment wizard
* Added Equity base account which will hold opening balance accounts
* Removed (int) operators from MyMoneyMoney
* Added isZero(), isNegative() and isPositive() to MyMoneyMoney
* Added --without-arts configure option to acinclude.m4.in
* Removed unused MyMoneyFile* from engine objects
* Improved status display during online price update
* Added checks for referential integrity to MyMoneyFile
* Modified KEquityPriceUpdateDlg to support multi-selections
* Perform engine notifications based on a local copy of the notification
list to allow modification of the list during update()
* Enhanced parsing of MyMoneyMoney(QString) constructor
* Added automatically generated files to widgets/.cvsignore
* Added spanish and galician translation provided by Marcelino Villarino
* ... maybe a few other things I have forgotten ...

2004-12-14 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added Investment Performance reports (by account &amp; type)
* Expanded unit tests for OFX imports
* Finished ability to create OFX files from KMyMoney data (currently only used
for unit tests, but could be expanded).
* Added unit tests for investment reports
* Added XMLandBack calls to reports that were missing it
* Added MyMoneyTransaction::splitByAccount(QCStringList&amp;,bool)
* Added MyMoneyTransactionFilter::clearAccountFilter()
* Removed use of libuuid (OFX uid's don't need to be universally unique,
locally unique is good enough.)

2004-12-10 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Gnucash import - minor bug fixes
* - preserve memo in scheduled transactions
* - correct account opening dates

2004-12-07 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Fix sloppy coding error in Edit Schedule dialog
* Sorry, will try harder next time

2004-12-05 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Gnucash import changes
* Improve exception reporting
* More comprehensive scheduled transaction processing

2004-12-05 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added unit tests for OFX import of bank statements
* Maintain libofx 0.6.6 compatability
* Separated "From/To (account)" and "From/To (date)" for translation purposes

2004-12-03 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Implement KMyMoneyAccountCombo in scheduled transaction dialogs
* Allows selecting any asset/liability account for bills, etc.

2004-11-28 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Partial correction of share price import
* Fix bug 1067499 - crash on import
* Create orphan accounts for unknown account ids

2004-11-28 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Replaced 'splitted' with 'split', which is more gramatically correct
* Added support for cash dividends in OFX importing. (Cash dividends are
dividends which are NOT reinvested).

2004-11-27 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* More test cases for transaction reports
* Added error-checking in OFX import/download
* Fixed bug in transaction reports where grand total was getting too large
when multiple levels of subtotalling were in effect
* Better handling of the memo field on transaction reports

2004-11-25 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added "Anonymous" as a file saving type, which will save off a
version of the user's data with all private information (names,
strings, amounts) masked. Hopefully this will be enough to get
users to be willing to send their data files to us when they have
a problem.
* Avoid runtime warnings concerning MyMoneyMoney::operator== in unit
tests
* Replaced some various MyMoneyMoney::operator==() calls with
MyMoneyMoney.isZero()

2004-11-21 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added support for OFX Direct Connect. Thus you can download statements
directly from your bank if they expose an OFX server. Detailed online help
file included, and posted to the dev list.
* Consolidated the work of OFX importing at the KMyMoneyApp level into a
single slot, KMyMoney2App::slotOfxStatementImport
* Better handling for online price update when selecting it for a single
stock using the right-click menu in investment summary. Automatically
starts the update, and only shows the selected stock.
* Respect KDE-wide settings for negative money sign position in reports

2004-11-08 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added support for multiple accounts in a single OFX file, based on a patch
by Chris B &lt;news@semperpax.com&gt;.

2004-10-29 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added option to obtain online stock quotes using a local shell script

2004-10-23 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Investment Transactions report
* Investment Holdings by Account report
* Investment Holdings by Type report
* Account Balances by Institution report
* Account Balances by Type report
* Added grand total row to all transaction reports
* Added underlying support for account reports, which builds on
transaction reports
* Added KMyMoneyUtils::equityTypeToString

2004-10-21 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added settings tab for online quotes

2004-10-17 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Automatically close all report tabs when the file closes
* Added right-click access to on-line price updating in investment view
summary

2004-10-17 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed crash when creating new categories while entering transactions
* Added "Don't ask again" switch to question if new category should be
created.
* Removed unused tabs in new account dialog rather than disableing them
from the KABC addressbook.

2004-10-16 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added online price updating for equities
* Added "Favorite Reports" to the home page.
* Added investment view to the global ledger view, which is needed for
investment accounts to show up in the verify import dialog.
* Fixed handling of "Sell" transactions in transaction reports. They were
still being reported as "Buy" transactions.
* Tweaked the logic for handling account filters with transfer transactions
in transaction reports. Now the transaction will be included once
if only one of the accounts is included in the filter.
* Added cash flow analysis tools for future investment reports
* Cleaned up random investment data generator (in KReportsViewTest)

2004-10-14 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added VAT handling
* Added MyMoneyMoney::isZero()

2004-10-13 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added investment fields to transaction reports (action, shares, price)
* Added 'value' field to investment summary. This displays the net value of
each holding. Renamed previous 'current value' to 'price'

2004-10-11 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Implemented "Edit..." from context menu in investment view summary

2004-10-11 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated splash screen handling. Using KSplashScreen on KDE &gt;= 3.2.0

2004-10-10 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed #1043892 (crash when changing action in checkings view)
* Fixed #1040653 (GNC import doesn't handle equity accounts correctly)
as provided by Tony Bloomfield
* Allow double click in investment view on any column

2004-10-09 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Reworked keyboard event handler
* Removed signalEnter() and signalEsc() signals
* Fixed initial size of cash account completion box
* Avoid some of the register flickering introduced in investment stuff
* Fixed a problem in reinvest dividend transactions when a fee was
assigned to an interest account
* Fixed switching to investment view when start editing an investment
transaction in a different view
* Reduced min version for autoconf to 2.53
* Maintain 3.0 backward compatibility

2004-10-09 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Fixed a bug in collapsed-column net worth reports (quarterly, yearly, etc)
where currency conversion was involved. The wrong price was being used.

2004-10-06 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* make sure widgets are destroyed when edit transaction mode is left
before any other operation is started. There used to be a race
condition somewhere which caused the program to crash
* Enable to clear the category field and save the transaction
* Added a 'use system font' switch to the configuration (Fixes #1039342)

2004-10-05 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated changes for equity price edit dialog
* Updated currency edit dialog to show a better equation for price

2004-10-04 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added importing of investment statements via OFX files. This is highly
experimental, and should not be used with any real data!!
* Added MyMoneyStatement XML files as an acceptable file format for
web connect. This is just for debugging.
* Changed equity price edit dialog to use the same widget as the currency
price edit dialog.

2004-10-04 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added patch supplied by Alexander Novitsky
* Fixed logic so that register is updated when a transaction is
modified/entered.
* Corrected error messages to point to the right method names

2004-09-26 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed memory leak in KEditEquityEntryDlg
* Added icons to buttons in KEditEquityEntryDlg
* removed compile warnings in KEditEquityEntryDlg
* disable edit/remove button when no entry is selected

2004-09-25 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Use QGuardedPtr for pointers to widgets
* Renamed hideWidgets() into destroyWidgets()
* Don't re-create edit widgets when changing type of investment
transaction. This solved the misterious crash.
* Use highest compression rate for man pages

2004-09-24 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Applied patch for FreeBSD supplied by Alec
* Added feature request #929127 (new transaction default based on payee)

2004-09-23 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* added buttons to register edit mode
* revised the string comparison in operator == of our objects
* fixed bug in MyMoneyMoney::operator -(int)
* added option to re-enable all (don't show again) messages
* added setting (not in GUI yet) do suppress display of calculator button

2004-09-21 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated investment logic to support change of action during edit
* Generalized tab order handling in ledger view
* Improved tab order handling
* Fixed focus display of account combo box
* Added more investment logic (register based input)
* Cancel investment edit when focus is lost
* Fixed logic of account combo box
* Updated interface to MyMoneyEquity::hasPrice()

2004-09-19 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Update investment summary when different account is selected
* Update ledger register when account with no transactions is selected
* Set the default smallest fraction for new equities to 100
* Check all equity parameters before accepting them

2004-09-16 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Moved m_smallestAccountFraction from MyMoneyCurrency to MyMoneyEquity
* Added fraction field to equity object
* Added icons to buttons in equity dialogs
* Added edit field for totals in investment ledger
* Revised calculation of values (e.g. total) in investment ledger

2004-09-15 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Applied latest patch from Martin Preuss: Imported data is UTF8.
* Reversed sign on OFX statement import when transactions are
investment transactions. I haven't figured out yet why this is needed.

2004-09-14 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Turned KMyMoneyBanking into a singleton object
* Reduced dependency of KBanking down to converter/mymoneybanking
* Present reasonable error message when trying to load a directory as template

2004-09-13 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Do not allow any action that requires a file if no file is open

2004-09-11 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Modified OFX Web Connect to deal with the case where the user does not
have any file open at all.
* Modified OFX Web Connect to bring the window to the forefront in KDE &lt; 3.2
* Moved "Tools | Banking Settings" to "Settings | Configure Online Banking"

2004-09-11 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed inclusion order of header files
* Include xmethod generated files prior to any other header file
* Fixed argument for QMessageBox in kmymoney2.cpp

2004-09-10 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added patch from Martin Preuss to import files via AqBanking

2004-09-10 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* More investment view work (mainly register based editing)

2004-09-09 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Changed the way the program raised itself to the forefront when
receiving an ofxWebConnect DCOP call. This method was suggested by
Lubos Lunak &lt;l.lunak@suse.cz&gt; of the KDE core development team.
* Fixed compile problems when neither HAVE_NEW_OFX or HAVE_LIB_OFX
are defined.
* Copied new "OFX not available" message to the other place it's used.
* Unified the 2 interfaces to LibOFX (pre-0.7 and post-0.7). They now
share the same code for processing the data, the only differences are
in how it interacts with the library.
* Fixed memory leak in KMyMoney2App::slotBankingSettings()

2004-09-05 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added KBanking support supplied by Martin Preuss
* Set focus to register after deleting a transaction

2004-09-04 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added support for Quicken "web connect". To enable this, set up KMM to
handle the mimetypes "application/vnd.intu.qfx", "application/x-ofx". KMM
will launch and import those transactions. If there is another instance of
KMM open at the time, it will import them into THAT instance.
* Added bankID's to MyMoneyStatement &amp; MyMoneyTransaction. This is a
unique bank-assigned ID that can be used to detect duplicate transactions.
* Modified MyMoneyStatement importer to skip transactions with BankID's that
are already in the file.
* Added type identifier to MyMoneyStatement (checkings, savings, etc)
* Cleaned up MyMoneyStatement importer

2004-09-02 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added set/get functions to MyMoneyMoney for completeness
* Added formatting testcases to MyMoneyMoneyTest
* Improved libofx autoconf logic and put into separate file
* Check for a minimum version of the automake package

2004-09-02 Martin Preuss &lt;aquamaniac@users.sourceforge.net&gt;
* added optional support for next generation of LibOFX
* Online banking: Importing transaction statements from the result of
online requests now works.

2004-08-31 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added importing of OFX files
* Added libofx to the project
* Added AC_CHECK_LIB_WPROLOGUE to check for libofx
* Added MyMoneyStatement. This class represents a paper bank statement,
and acts as an interface between providers of such data (OFX, HBCI)
and the program.
* Fixed crash in QIF import when filter was set to %d/%mmm/%yyyy and
data was actually %d/%m/%yyyy

2004-08-29 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Removed "Create Me" files from Makefile.am
* Better handling of next due date in loan wizard
* Updated README with instructions about how to get the memory leak
detection code compiled in
* Fixed #1016219 (Account creation fails after loan creation)
* Added page to loan account wizard to specifiy the loan payout transaction
* More investment view work (now can create and edit all transaction types)

2004-08-26 Martin Preuss &lt;aquamaniac@users.sourceforge.net&gt;
* added support for online banking using kbanking
(http://www.aquamaniac.de/aqbanking/)
configure now checks whether KBanking is available and simply skips
support for it if kbanking is not found.

2004-08-24 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Removed "Create Me" graphics from all dialogs

2004-08-24 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed some logic errors in KAccountComboBox and completion object
* More work on the investment view

2004-08-22 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed conflicts due to merge with changes made by Ace
* Preload the KDE default settings for monetary display
* Fixed #1012257 (Calculator does not honor localized settings)
* Added flag support to MyMoneyTemplate
* Reconverted old default category files to new format (incl. flags)
* Added display only mode to kMyMoneyCategory which does not try to
create accounts

2004-08-22 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added idlist as parameter to MyMoneyFile::accountList()
* Added testcase for the above
* Fixed MyMoneyMoney::formatMoney() to avoid displaying the decimal
separator if currency does not have a fractional part (e.g. HUF, IRR)
* Added new account template loader
* Removed old default category loader
* Removed old default account files
* Added new i18n based template directories for accounts
* Converted old default category files to new format
* Rearranged the ledger views to better support 800x600
* Added size display in caption
* Resized payees view to better support 800x600

2004-08-22 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added button to kMyMoneyEdit to start the calculator
* Fixed icon display on date and money edit fields
* Converted button of kMyMoneyDateInput to KPushButton
* Fixed account selection button to show the correct
account after switch to different account type
* Fixed initial header of empty schedule view
* Fixed default button in schedule edit dialog
* Added KGPGFile::keyAvailable()
* Made OK the default button in edit schedule dialog

2004-08-21 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Fixed a bug in transaction reports where totals did not work
with the new parentheses-around money format
* Added "Do not ask again" to "Do you want to override the opening balance
of this account currently set to %1 with %2?" in QIF reader.
* Added "Automatically create missing payees" to "Last Use Settings" in
configuration file.

2004-08-17 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Fixed a bug where creating a new category would not properly use the
currently selected account as the parent, for expense accounts.
* Implemented double-clicking in accounts and categories views.

2004-08-16 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Modified reports to use ()'s for negative values
* Added Tax notation in GB default accounts
(Thanks to Tony Bloomfield)

2004-08-14 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added an initial Tax Transactions report. Still more work needed
to make it fully-featured.
* Added "Include on Tax Reports" property in category edit dialog
* Added Tax notation in US default accounts
* Commented MyMoneyReport class
* More information on "Unexpected exception in KLedgerView::
setupPointerAndBalanceArrays"
* Inverted sign of values in Transactions reports to match standard
cash flow notation.

2004-08-11 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added stock accounts to kMyMoneyAccountSelector

2004-08-08 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added the ability to designate some reports as your favorite, and
they'll be grouped together.
* Altered handling of reports index list, so customized reports are stashed
in the same heading as their parent reports.
* Fixed a bug in running sum reports (Net Worth, et al). When either end
of the date range was QDate() the opening balance could be calculated wrong.

2004-08-07 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added MS Money date format as an option to QIF importer (%m/%d'%yyyy)
* Added some new default reports, which are permutations of the old ones
* Modified the reports index list to show a tree view, organized by type of
report.
* Modified reports to remember the plain language date range setting,
so if you set a report to "Current Month", it will ALWAYS show the
current month.
* Added rudimentary help to report configuration dialog.
* Added 'close' button to report configuration dialog.

2004-08-05 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* More changes to maintain backward compatibility with older Qt versions.
Application now compiles under KDE 3.0 again.

2004-08-04 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added more operators to reports::QueryTable::TableRow to satisfy
qHeapSort in earlier versions of QT

2004-08-04 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Maintain backward compatibility with older Qt versions
* Fixed initialization order of kgpgfile objects

2004-08-03 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added ISO date format as an option to QIF importer (%yyyy-%mm%-dd)
* Added CSV export for Transaction Reports
* Added Currency conversion for Transaction Reports

2004-08-03 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added workaround for bug in KFilterDev

2004-08-02 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added GPG encryption logic

2004-08-01 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed asset loan view activation
* Fixed new category/equity process

2004-07-31 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* First draft of transaction reports. Doesn't yet convert currency or export
to CSV, and definitely needs more testing.
* Changed the currency identifier for "don't convert to base currency" reports
from the trading symbol (e.g. $ which could mean USD, or AUD, or CAD) to the
ID (e.g. USD, AUD, CAD, etc) which is more specific
* Reworked the report configuration dialog. Now instead of bolting on an
additional frame, the report configs are additional tabs inserted at the
beginning. Looks a bit cleaner. Also brought them into QT Designer.

2004-07-29 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Removed KTabWidget for KDE &lt; 3.2 in the report tabs
* Fixed CSV report export so larger (&gt;10,000) values are not rounded
* Fixed the case where the user enters transactions before the
opening date of the account and calls for a net worth report.

2004-07-29 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Maintain backward compatibility for KDE 3.0 (it's still broken
due to usage to KTabWidget which only exists in KDE &gt;= 3.2)

2004-07-28 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Fixed bug in reporting logic. AccountDescriptor::operator&lt;() was broken,
missing a case.

2004-07-27 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Fixed CSV report export so no commas are contained in values. Values are
now saved as double's instead of formatted strings.
* Fixed spelling error in reports exception dialog.

2004-07-26 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added numerous exceptions for overflow conditions in reports

2004-07-26 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed alignment of institution dialog

2004-07-26 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Standardized formatting of headers for scheduled payments on home page

2004-07-25 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* added more investment logic (register layout)

2004-07-23 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* set id of category before emitting signal in kMyMoneyCategory
* added more investment logic (automatic form adaption)

2004-07-23 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Replaced report control hyperlinks with a new button bar widget at the
top of the tab
* Added a chart view for report charts. No actual charts yet, just stubs.

2004-07-23 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Preliminary rough implementation of transaction report

2004-07-22 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added ability to save reports as CSV
* Moved reports::AccountDescriptor into reports::PivotTable

2004-07-22 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed painting of kMyMoneyAccountCombo

2004-07-20 Felix Rodriguez &lt;frodriguez@users.sourceforge.net&gt;
* Expanded Backup Dialog so we can see the OK and Cancel Buttons.

2004-07-17 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Maintain backward compatibility with older Qt and KDE versions
* Added File/Import/Gnucash menu entry and logic to call the reader

2004-07-17 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added 'comment' field for reports, displayed in the reports list tab

2004-07-17 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Ability to delete reports
* Context menu (right click) from the reports list to access the report
management actions. (The same actions that are available as links in
the report tabs)

2004-07-16 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Interface to contain the reports, as suggested by Tom. Completely re-wrote
KReportsView in the process.

2004-07-16 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed #987465 (can't edit scheduled transactions)

2004-07-14 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Creation of new reports. Interface is pretty rudimentary.
* Center reports within tab frame

2004-07-14 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* More investment work
* Moved showWidgets() from the various ledger views to the base class
as it was the same code in all derivatives

2004-07-14 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Don't attempt to 'fix' transfers from/to a credit card account on load

2004-07-14 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* More investment view work

2004-07-14 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Created MyMoneyReport object, which replaces ReportConfigurationFilter
* Serialize MyMoneyReport objects to/from XML storage file, so users' changes
to their reports will be saved/loaded automatically
* Updated all report test cases to also test serialization

2004-07-13 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added more logic for investment view
* Fixed word wrap in split transaction dialog
* Show warning message when a category is created and the unused
categories are hidden by user settings
* Added investment register framework

2004-07-09 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Add creation of stock accounts through investment view
* Added equities to MyMoneyStorageDump
* Really remove price information from equity history
* Added new loadList method to kMyMoneyAccountSelector

2004-07-08 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added stock account type
* Added checks to make sure that stock accounts have an investment parent
* Added testcases

2004-07-07 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added possibility to update currency rate for foreign currency accounts
during account creation
* Added duplicate transaction detection and use it during QIF import

2004-07-06 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed spelling of institution
* Added test for presence of round() function
* Modified MyMoneyFinancialCalculator to use own rounding function if
round() is not available
* Fixed #979747 (Interest rate does not get updated)
* Added constant MyMoneyMoney::autoCalc used to identify automatically
calculated values.

2004-07-01 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added patch supplied by Ace Jones
* Fixed the 'all dates' option in the report generation as proposed
by Ace
* Changed currency code for Ukraine Hryvnia from UAG to UAH
* Calling kMyMoneyDateInput::setDate() with QDate() as argument sets
date to 00.00.0000 which will be returned as QDate() (the invalid date)

2004-06-29 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Don't show investment accounts in category selector widget
* Added general test environment for automated application tests
* Allow three digit fractional part for loan interest rates

2004-06-27 Kevin Tambascio &lt;ktambascio@users.sourceforge.net&gt;
* Added widgets to the online stock price update dialog
* added placeholder dialog for the config options for the online update feature.
* added a class in the engine to handle price updates from the online updater.
This will allow dialogs to expose that feature, without burying logic to update
the engine objects in the dialog.
* some work on the investment view feature

2004-06-23 Kevin Tambascio &lt;ktambascio@users.sourceforge.net&gt;
* Added new dialog (doesn't do anything yet) as a placeholder for the online
stock/currency price update features.
* Added a menu item under the Tools menu to access this. It will also be
available in the investment view.

2004-06-23 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Applied patch(es) supplied by Ace Jones
* Fixed QIF generation errors
* Added method hasAccount() to MyMoneyFile
* Added testcases for this method

2004-06-15 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Report failures of unit tests correctly
* Fixed transaction filter handling for reports
* Fixed #968755 (Unwanted ledger view switch)
* Partial cleanup of unused code
* Fixed #975134 (Finished transactions will be shown on homepage)

2004-06-14 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added patch supplied by Ace Jones
* Show preferred account after startup
* Fixed logic for KDE 3.2

2004-06-09 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Replaced KLineEditDlg with KInputDialog for KDE &gt;= 3.2.0
* Replaced some deprecated methods in KDE &gt;= 3.2.0
* Fixed #968338 (Quarterly schedules are not calculated correctly)

2004-06-06 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed problem in find transaction logic
* Fixed problem with scheduled payments when payment is on 29th-31st but
month has less days

2004-06-04 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added kMyMoneyEquity to KLedgerViewInvestment
* Removed unused members from kMyMoneyCategory
* Fixed new equity entry dialog to keep updated values in member vars
* Fixed some make issues
* Fix endless loop when using with Qt &lt; 3.2.1

2004-06-03 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Renamed newAccount() to addAccount() in the engine code to
maintain consistency
* Adapted testcases to reflect above change
* Adapted kmymoneytest.cpp to work with CPPUNIT 1.9.14

2004-06-01 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Setup minimum size of kMyMoneyAccountCombo
* Clear out text on account combo if no account present
* Disable account combo box if no account available
* Disable register if no account available
* Fixed parameter setup for call of split transaction dialog

2004-05-31 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Make sure datepicker widget is always shown completely on screen
* Fixed category/amount assignment problem
* Created kMyMoneyAccountCombo widget

2004-05-30 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added KDE compatability header file. Should be used instead of kdeversion.h
* Added global test container that can be built and run using 'make check'
* Added patch for report configuration supplied by Ace Jones

2004-05-28 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Enable more button even if transaction is not edited
* Keep the payee id for transfer transactions

2004-05-27 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Allow arbitrary category/account selection in category field
* Reworked the ledger view code to support new features
* Investmentview is still broken!!
* Fixed spelling in de.po
* Fixed an issue with XML writing on KDE 3.2
* Added setWeekendOption to MyMoneySchedule

2004-05-14 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Applied patch supplied by Ace Jones
* Fixed #952804 (marking 'cleared' in transaction entry form doesn't work)
* Started to implement equity accounts

2004-05-12 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed some issues around multiple currencies / exchange rates
* Fixed #952696 (Mis-assigned shortcut Alt-M in transaction form)

2004-05-05 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed comparison of account objects
* Added testcases for equality of MyMoneyAccount object
* Removed unused code from kmymoneycategory.*
* Keep account completion box in same position when resizing

2004-05-03 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added price precision setting to settings dialog
* Use price precision setting in price edit dialog
* Maintain backward compatability with Qt 3.1

2004-05-02 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed #944771 (Transaction form moves down the screen)
* Fixed #944778 (File/Save As not working properly)
* Fixed #944761 (Next date not calculated properly)
* Fixed #932681 (no action for esc/enter in nr. field)
* Allow to enter a '-' as first character in monetary fields without
starting the calculator
This issue only occured in the credit card and liability view
* Enhanced MyMoneyFile::balance() and totalBalance() with date parameter
* optimized MyMoneyTransactionFilter::setDateFilter()
* optimized reporting code

2004-04-24 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Close dialog after the default currency has been selected
* Allow to override opening balance of existing account when
importing QIF file

2004-04-18 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed layout of initial import dialog
* Fixed #906749 (Hang in loop on QIF import)
* Fixed #933064 (MS-Money sub-categories make QIF import hang)
* Fixed a problem with importing QIF dates using spaces
* Maintain description field when importing accounts via QIF
* Preload account types for account creation wizard in constructor

2004-04-17 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added missing layout to loan wizard

2004-04-14 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed some problems around multi currency and account/category creation
* KUpdateStockPriceDlg now uses kMyMoneyEdit for price field
* Revised interface of KUpdateStockPriceDlg
* Enabled Return/Esc handling for KUpdateStockPriceDlg
* Fixed problems with inline documentation
* Fixed layout of amount tab in find transaction dialog

2004-04-05 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Revised view activation signalling
* Added print button and file print option which is active for reports
view only
* Revised handling of cross-directory dependency to support older versions
of automake
* Removed m_file member from ReportsView::PivotTable
* Added kMyMoneyCurrencySelector widget
* Moved styles for report generation from code to kmymoney2.css
* Added support for shares to XML storage object
* Fixed a problem in MyMoneyMoney::formatMoney if precision is larger than 4
* Added KCategoryListItem to differentiate between accounts and category
view
* Added observer functionality to KInvestmentView

2004-04-03 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added improvement for reporting
* Added icon for reporting

2004-04-03 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added basic multiple currency work (currencies can be managed but
they are not used for accounts and transactions yet)
* Added reporting view supplied by Ace Jones
* Made some changes to the reporting view to support printing through KDE
* Added detection of multiple KMyMoney instances and issue warning
* Prevent loading the same file into multiple instances of KMyMoney

2004-03-24 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed #922678 (number field is not saved to XML file)

2004-03-17 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed layout of split correction dialog
* Allow transfers with partial income/expense
* Fixed display of form for investment ledger
* Added missing investmentview files to project
* Added equity tests to unit test framework

2004-03-13 Kevin Tambascio &lt;ktambascio@users.sourceforge.net&gt;
* A good chunk of functionality for investments is checked in. Includes support for
reading/writing MyMoneyEquity objects. Can add investment accounts through
the new account wizard. Can use the investment view to view all your investment
accounts. XML reader/writer will read and persist the new objects.

2004-03-10 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added investment view and started integration of it

2004-02-24 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Revised payees view
* Added validator function for kMyMoneyEdit that respects monetary
settings
* Updated project file
* Fixed sizing of widgets in ledger view
* MyMoneyFile emits more notifications for payees

2004-02-24 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed #897033 (fixed open file handling)
* Fixed #902580 (unable to create category during transaction entry)
* Fixed #902581 (unable to modify date correctly in ledger view)
* Set initial focus of new account wizard to account name

2004-02-15 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added new MyMoneyMoney implementation which is based on
numerators and denominators (based on GNUCash's gnc_numeric
and converted to our object oriented environment)

2004-02-08 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed #892954 (Creating accounts within QIF import crashes application)

        