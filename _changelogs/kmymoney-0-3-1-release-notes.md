---
title: 'KMyMoney 0.3.1 Release Notes'
date: 2001-02-28 00:00:00
layout: post
---

0.3.1
- Fixed the bank/account enabling in kmymoney2.cpp.
- Cleaned up the dialogs a little, in respect to the tab orders.
- Be aware that there are quite a few qDebug statements so expect some output whilst running.
- Cleaned up the transaction engine a bit and changed the API (so MyMoneyFile knows nothing but it's
    own banks).
- Removed the accounts view and merged it into the banks view like gnu cash et al.
- Fixed a majority of unknown bugs by making the transaction engine not use auto delete
    in its linked lists.  This may cause memory leakage but until I write my own/use STL then
    the memory leakage will have to stay !.
- Made the transaction engine comparison operators more flexible.
- Moved the create new account menu option to the bank menu.
- Changed the transaction view to use a QTable now and am getting ready to use in place editing.
- Added the ability to let the user choose whether to view the input box. (Settings menu).
- Each cell has it's own table item class with appropriate arguments/editors.
- Cleaned up the transaction view code a bit and the class no longer requests updates
    to its data, it does it itself using the supplied pointer(s).
- Now using KDE 2.1beta2 and KDevelop 1.4 and nicked some icons etc from the newly generated
    programs.
- Cleaned up the bank/account/transaction/kmymoney views.
- Each transaction really does have its own id now.  Just an unsigned long for now.
- The list views can no longer show user selected columns but they do respect the font and
    colour settings.  Small bug to be fixed in the background painting of the register view

0.3.1-281200
- MyMoneyMoney updated to be represented as a double.
- New file format to save and read new MyMoneyMoney object.
- Made the toString method add thousand and decimal separators.
- BUG: MyMoneyMoney::toString returns garbage when showSign is true ???
- The list views have been changed so that the user can't resize them.
- Added extra arguments to MyMoneyMoney::toString (formatted: true if thousand separators
        are to be included in the returned string.)
- We now display dates and money according to the current locale.  To change these settings
        use the apprpriate KDE 2 dialog. (KMenu -&gt; Preferences -&gt; Personalisation -&gt; Country &amp; Language.)
- Added a dialog to let the user choose the columns to be displayed, and to choose the fonts/colours.
- Updated the list view code to read from the user variables.  A couple of bugs exist.
- The settings are now saved in the application config file. (KMainWnd::read/saveOptions).
- Added a find dialog and added menu and toolbar items.
- The find dialog now opens a new results window to display in, and now support
        regular expressions in the description &amp; number fields.
- Cleaned up some of the reconciliation code and fixed a bug. It has also been fixed in
        the stable 0.2 tree.
- Added another program to the source tree and hand edited some Makefile.am's.
- The new program is an applet that will dock on the task bar and monitor your
        bills and deposits (when they are done !).
- Tried to modify the configure stuff to support uic.  Not trusted to work at this time, I cut and
        pasted most of the stuff from kdelibs-1.94.
- Converted the KNewBillWizard dialog to designer and all seems well.  Will create all new
        dialogs with designer now.  The older, already implemented dialogs, will be converted
        at some point in the future.
- Added hacked versions of sizePolicy and sizeHint to kMyMoneyDateInput but they don't
        really work properly.
- Completely hand edited the kdevelop project file to compile the new code and to accomodate
        the new admin directory.  God knows if it will work on other machines, but it does
        work on mine!  PLEASE DO NOT USE KDEVELOP ON THIS PROJECT AS IT WILL PROBABLY OVERWRITE
        ALL THE HAND EDITED FILES (KDEVELOP DOESN'T ON MINE BUT I CAN'T GUARANTEE IT FOR OTHER
        MACHINES).  -- I DON'T REALLY UNDERSTAND AUTOCONF ETC!!!
- Added a caption to let the user know where they are at any time.  Thanks to ...
- Moved the list views over to KListView and am now using better ordering of the items.
- Changed the API in the engine to be more robust and to disengage itself from external number ordering.
- Added an index field to MyMoneyTransaction and removed MyMoneyTransactionE from KReconcileDlg.
- KReconcileDlg is now implemented using designer, and the code has been modified to reflect this
        and the code has been simplified.
- Added == operators to the 'engine' classes.
- Removed QdbtTabular completely from the source code.  All lists now use the
        KListView class.
- Moved over to KDevelop 1.3 and am now using the new templates and admin directory.
- Made all kdevelop dialogs into designer dialogs and they now look better and the tabs behave as expected.
- Removed the dock applet from the source, I will release it once kmymoney2 becomes stable again.  (most of
        it isn't implemented anyway!).
- If the build process fails try running the mk-ui.sh script in the kmymoney2 source directory first.  (There
        are some dependency problems on my machine ?).
- Use the compile.sh script to build kmymoney2 from now on.
- Completely removed all the kdevelop dialogs and turned them into designer dialogs.
- Removed the Quick Start wizard and am thinking of a new interface to wizards/templates, maybe using some
        kind of scripting so the user can add more if they want to.
- More streamlining of the engine code to stop using QListIterators.  All that needs to be done to
        remove QT dependence from the transaction engine is to use something other than QString's and
        QLists internally.  Work will begin on a gnome interface once I have the KDE code working as expected...
- This list is getting QUITE BIG so I have called this 0.3.1-281200 and have bundled it as a distribution.
        Not too sure yet whether I'll stick it on the web, (depends if i can scp into sourceforge i suppose).

  