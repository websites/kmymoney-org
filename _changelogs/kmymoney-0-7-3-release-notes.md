---
title: 'KMyMoney 0.7.3 Release Notes'
date: 2005-07-08 00:00:00
layout: post
---
2005-07-08 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Regenerated message files
* Released 0.7.3

2005-07-07 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added quick-fix to avoid crash on schedule creation
* Applied doc patch provided by Jerry Amundsen

2005-07-06 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Save equity account to file
* Re-attach children of equity account when loading old file

2005-07-05 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Moved stockSplit() and transactionType() from KLedgerView to KMyMoneyUtils
* Improved display of investment transactions in other ledger views
* Added help button to new account wizard and linked to online doc
* Fixed some of the screenshot stuff

2005-07-05 Robert Wadley &lt;robntina@users.sourceforge.net&gt;
* screenshot stuff

2005-07-04 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added index.html to .cvsignore in doc/en
* Fixed #1231925 (Institutions view: totals do not reflect the accounts)
* Changed section tags into sect1 tags
* Changed 'KMyMoney' to &amp;kappname;
* Added missing files to doc/en/Makefile.am
* Added home.html and whats_new.html for french as provided by
duloup@gmail.com

2005-07-02 Robert Wadley &lt;robntina@users.sourceforge.net&gt;
* Added screenshots for the manual.
* Made some small changes to some of the image names in the manual.
* Added image names to the makefile

2005-06-28 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added meta tag to html reports. Apparantly this makes them show up better
on Windows. Submitted by Duloup &lt;duloup@gmail.com&gt;

2005-06-23 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Selecting a different payee during renaming of another payee will
now enter the changes if the 'Keep changes when selecting a
different transaction/split' register setting is selected.
See also ChangeLog entry dated 2005-03-12.
* New categories will be shown as sub-account of expenses (#1061790)
* Applied patch provided by Laurent Montel to kmymoney2ui.rc

2005-06-20 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed 'undefined INT_MIN, INT_MAX' problem
* Resolved compiler warnings in kofxdirectconnectdlg.cpp
* Fixed #1216779 (Fails to save with space in path)
* Added italian what's new page provided by Samuel Algisi and Andrea Nironi

2005-06-20 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Surround CSV fields with quotes in report export. Fixes #1210755

2005-06-20 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* GnuCash Reader - fix QMessageBox change/bug

2005-06-15 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* GnuCash Reader - make anonymized names more readable

2005-06-15 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Moved mymoneymoney.cpp to be the first module included when
building with --enable-final=yes, so that __STDC_LIMIT_MACROS is defined
before stdint.h is included for the first time

2005-06-14 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Bumped version number to 0.7.3
* Applied fix-compile-visibility patch by Laurent Montel
* Updated acinclude.m4.in and cvs.sh from KDE/admin
* Pick up unusual places for the KDE basedir (e.g. /usr/lib/kde/3.3)
* Allow generation of pot file even if kde-config --install include
returns NONE

2005-06-13 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added description of common widgets
* Extract path for stylesheet from $(KDE_XSL_STYLESHEET)
* Applied ui-dialog patch provided by Laurent Montel

2005-06-08 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added function to select/unselect an account subtree in the
account selector widget via the right mouse button
* Fixed nested 'if' in Makefile.am
* Fixed reported Makefile.am problems with missing $(DESTDIR)

2005-06-07 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added Roger's chapter on institutions
* Updated some documentor assignments
* Added Roger to the doc credits

2005-06-06 Ace Jones &lt;acejones@users.sourceforge.net&gt;
QIF Importer fixes for investments
* Don't create the cash side of the split if there is no cash involved
(Reinvest dividends and add/subtract shares)
* Fix Reinvest Dividends to use an income account also
* Support reinvlg, reinvsh (doing this on blind faith..I'm not really sure
what these transactions look like)
* Fix missing 'else' that caused reinvested dividends to show up as
unsupported.
* Support "sellx", "divx", etc
* Clarified the warning that !Type:Bank + NInvst isn't really supported
* Clarified the warning that Memorized Transactions are not supported

2005-06-06 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added access to online help to find transaction dialog
* Made the transaction history the default in payees view
* Added chapter about widgets to documentation
* Convert html to encoding 'utf-8' for make preview
* Added 'make show' which opens the preview in konqueror
* Updated italian translation provided by Samuel Algisi and Andrea Nironi
* Revised german pages

2005-06-05 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* GnuCash Reader - fix crash in Tools/Prices

2005-06-05 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* GnuCash reader documentation (forgot --check - sorry!)

2005-06-05 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* GnuCash reader documentation

2005-06-03 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Refined the language regarding report configuration &amp; report favorites
* Fixed the QIF import progress dialog for adding transactions to your
ledger, so now it displays properly.

2005-06-03 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed broken actions (find transactions, print, reconcile)
* Removed some recently added debug output
* Applied i18n-patch provided by Laurent Montel with slight modifications

2005-06-03 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added some i18n's in report configuration. Submitted by Laurent Montel
 &lt;montel@kde.org&gt;

2005-06-03 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Add a bit of code that got lost yesterday

2005-06-02 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Massive speed-up to QIF import. 3,000 transactions now takes less than a
minute, instead of &gt;45. The progress dialog needs help, though.
* Fixed OFX direct connect, that has been broken since I purged the non-
plugin OFX importer.
* Changes to experiment with link replacement in home.html. These are all
#if0'd out.

2005-06-02 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Don't allow GnuCash import into existing file

2005-06-02 Tony Bloomfield &lt;tonybloom@users.sourceforge.net&gt;
* Allow to open/import GnuCash files without suffix

2005-05-31 Ace Jones &lt;acejones@users.sourceforge.net&gt;
* Added Categories and Accounts help as submitted by
Roger Lum &lt;rogerlum@gmail.com&gt;
* Added Search Transactions help as submitted by
Darin Strait &lt;darin_strait@yahoo.com&gt;

        