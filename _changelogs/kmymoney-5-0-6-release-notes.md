---
title: '        KMyMoney 5.0.6 Release Notes
      '
date: 2019-08-18 00:00:00 
layout: post
---

<p>The KMyMoney development team is proud to present <a href="http://download.kde.org/stable/kmymoney/5.0.6/src/kmymoney-5.0.6.tar.xz.mirrorlist">version 5.0.6</a> of its open source Personal Finance Manager.</p><p>Here is the list of the bugs which have been fixed:
          <ul>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=408361">408361</a> Hardly distinguishable line colors in reports</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=410091">410091</a> Open sqlite database under kmymoney versions &gt;= 5.0</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=410865">410865</a> Access to german online banking requires product key</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=411030">411030</a> Attempt to move one split to new category moves all splits with same category</li>
          </ul>
        </p><p>Here are some of the new improvements found in this release:
          <ul>
            <li>The default price precision for the Indonesian Rupiah in new files has been raised to 10 decimals.</li>
          </ul>
        </p><p>A complete description of all changes can be found in the <a href="https://kmymoney.org/changelogs/ChangeLog-5.0.6.txt">changelog</a></p>.