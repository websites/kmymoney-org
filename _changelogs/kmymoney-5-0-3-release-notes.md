---
title: '        KMyMoney 5.0.3 Release Notes
      '
date: 2019-01-27 00:00:00 
layout: post
---

<p>The KMyMoney development team is proud to present <a href="http://download.kde.org/stable/kmymoney/5.0.3/src/kmymoney-5.0.3.tar.xz.mirrorlist">version 5.0.3</a> of its open source Personal Finance Manager.</p><p>Some problems have been reported by many of you and the development team worked hard
        to fix them in the meantime. The result of this effort is the new
        KMyMoney 5.0.3 release.</p><p>Despite even more extensive testing than usual, we understand that some
          bugs may have slipped past our best efforts.  If you find one of them,
          please forgive us, and be sure to report it, either to the <a href="mailto:kmymoney-devel@kde.org">mailing list</a>
          or on <a href="https://bugs.kde.org/enter_bug.cgi?product=kmymoney%26format=guided">bugs.kde.org</a>.</p><p>From here, we will continue to fix reported bugs, and working to add
          many requested additions and enhancements, as well as further improving
          performance.</p><p>Please feel free to visit our overview page of the CI builds at
          <a href="https://kmymoney.org/build.php">https://kmymoney.org/build.php</a>.</p><p>Here is the list of the bugs which have been fixed:
          <ul>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=403068">403068</a> Storing and loading customized information reports in XML is totally broken</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=347685">347685</a> Transaction contains erroneous *** UNASSIGNED *** in Tags view if no payee is given</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=392684">392684</a> Unable to remove Payee from transactions</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=396831">396831</a> 5.0 Version - Crashes when importing OFX/QFX File Using Firefox</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=397467">397467</a> Closed accounts are removed from a historical report each time the report is changed</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=398919">398919</a> Display schedule transactions with planned post date</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=399260">399260</a> title of report is truncated in graph view</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=400628">400628</a> Error Attempting Backup to Samba share - Windows 10</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=400820">400820</a> KMyMoney crashed when clicking on Forecast&gt;Chart</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=401080">401080</a> Cumbersome opening of multiple reports</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=401144">401144</a> Price in exponential format is not interpreted correctly</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=401448">401448</a> Windows build is broken - '_isinf': identifier not found</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=402101">402101</a> Allow to reuse check numbers</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=402120">402120</a> "Pay to" field gets auto filled from previous transaction even if cleared manually</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=402195">402195</a> CSV importer: Opposite signs option not read from banking profile</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=402200">402200</a> EnterSchedule dialog too narrow</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=402302">402302</a> Saving confirmation asked even for unfilled operation</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=402501">402501</a> Check/transaction number not imported via CSV plugin</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=402534">402534</a> DB password doesn't apply while trying to save</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=402547">402547</a> Unable to save data to database in presence of online jobs</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=402694">402694</a> in remove shares transaction field tab order sticks in number of shares field</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=402699">402699</a> Unknown account id mymoneystoreagemgr.cpp:142</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=402708">402708</a> Division by zero errors in investement reports</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=402750">402750</a> Rounding problems with stock split factor cause crashes and errors</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=402783">402783</a> Transaction editor is not reset</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=402794">402794</a> Automatic category assignment does not respect tax assignment</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=402814">402814</a> Initial state of "View/Show all accounts" is not correct</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=403039">403039</a> The Interest pull down field missing when trying to enter income transaction in Investment ledger</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=403156">403156</a> Cmake fails on Fedora 28 looking for LibAlkimia and Qt4 Core and DBus</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=403529">403529</a> assigned TAG isn't shown up in scheduled transaction configuration</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=403565">403565</a> Crash when importing qif</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=403608">403608</a> SaveAs defaults to "wrong" file path</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=403617">403617</a> The currency New Kwanza Angolano has been replaced</li>
          </ul>
        </p><p>Here is the list of the enhancements which have been added:
          <ul>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=398133">398133</a> Allow saving investment transaction without brokerage account if net amount is zero</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=402316">402316</a> Add action to create new transaction from any view</li>
          </ul>
        </p>