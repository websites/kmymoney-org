---
title: '        KMyMoney 4.5.3 Release Notes
      '
date: 2011-02-01 00:00:00 
layout: post
---

<p>The KMyMoney Team is happy to announce the availability of KMyMoney version
          4.5.3. This is a bugfix version from the 4.5 series.</p><p>This release only contains performance improvements since the <a href="release-notes.php#itemKMyMoney452ReleaseNotes">4.5.2</a> release and a few bugfixes:
          <ul>
            <li>Improve performance with investment account balance calculation</li>
            <li>Switch to <a href="http://www2.aquamaniac.de/sites/aqbanking/index.php" target="new">AqBanking5</a></li>
            <li>Don't select the character that starts the split editor <a href="https://bugs.kde.org/show_bug.cgi?id=263319">#263319</a></li>
            <li>Support "Gielda Papierow Wartosciowych" as price source <a href="https://bugs.kde.org/show_bug.cgi?id=261803">#261803</a></li>
          </ul>
        </p><p>For a full list of the changes please check out the <a href="changelogs/ChangeLog-4.5.3.txt">changelog</a>. As with the previous bugfix versions we recommend upgrading to 4.5.3.</p><p>The KMyMoney Development Team</p>