---
title: '        KMyMoney 4.6.1 Release Notes
      '
date: 2011-11-06 00:00:00 
layout: post
---

<p>The KMyMoney Team is pleased to announce the immediate availability of KMyMoney version
          4.6.1. This version contains several fixes for bugs found in <a href="release-notes.php#itemKMyMoney460ReleaseNotes">4.6.0</a> since it was
          released three months ago.</p><p>Here is a list of the most important changes since the last stable release:
          <ul>
            <li>fixed schedules moved to the next processing day <a href="https://bugs.kde.org/show_bug.cgi?id=262945">#262945</a></li>
            <li>fixed a crash with an uncaught exception when closing the current file before a GNUCash import <a href="https://bugs.kde.org/show_bug.cgi?id=280070">#280070</a></li>
            <li>fixed the split window redraw when resizing <a href="https://bugs.kde.org/show_bug.cgi?id=241544">#241544</a></li>
            <li>fixed a crash caused by an invalid budget <a href="https://bugs.kde.org/show_bug.cgi?id=280172">#280172</a></li>
            <li>fixed a crash when deleting an account <a href="https://bugs.kde.org/show_bug.cgi?id=280740">#280740</a></li>
            <li>the date can now be modified when editing multiple transactions <a href="https://bugs.kde.org/show_bug.cgi?id=280795">#280795</a></li>
            <li>the balance in the payees view is now computed correctly even if the payee has transactions with different currencies <a href="https://bugs.kde.org/show_bug.cgi?id=280795">#280795</a></li>
            </ul></p><p>For a full list of the changes please check out the <a href="changelogs/ChangeLog-4.6.1.txt">changelog</a>. We highly recommend upgrading to 4.6.1 as soon as possible.</p><p>The KMyMoney Development Team</p>