---
title: KMyMoney 1.0.0 Release Notes
date: 2009-08-19 00:00:00 
layout: post
---

<p>The KMyMoney development team is pleased to announce a major step forward for what has been described as "the BEST personal finance manager for FREE users". With over 3 years of development, this new stable release has a lot of new features and a refreshed user interface.</p><h3>What's new since version 0.9.3:</h3><ul>
          <li>A new look and feel, and a new set of icons</li>
          <li>Added investment price reports, investment moving average price</li>
          <li>Added GUI option for expert mode</li>
          <li>Improved scheduled occurrences support</li>
          <li>Improved support for CMake </li>
          <li>Improved text filter for transactions</li>
          <li>Improved charts look and feel</li>
          <li>Improved DB support and performance</li>
          <li>Improved QIF and OFX support</li>
          <li>Updated documentation</li>
          <li>Updated translations</li>
          <li>A lot of bug fixing and polishing rough edges to make this an outstanding release</li>
        </ul><h3>What's new since version 0.8.9:</h3><ul>
          <li>Home Page. Improved reports are available on the home page to give a quick glance on your financial situation</li>
          <li>Appearance. A refreshed home page and a new set of icons have been exclusively designed to celebrate the 1.0 release</li>
          <li>Budgets. A new budget feature allows you to specify your future expectations of income and expenditure, and then to compare your actual performance against these. You can use your historical data to have KMyMoney create a basic budget for you. Budget reports can be displayed as charts if desired. Monthly budget overruns can be displayed on your home page, but can be removed if you find it too depressing</li>
          <li>Forecasts. A forecast can be produced of the future balances of your critical accounts, based either on regular scheduled transactions or your historical data</li>
          <li>Reports. Forecast, Schedule Information, Account Information, Loan Information, Reconciliation, Cash Flow, Investment Price, and Investment Moving Average Price</li>
          <li>Charts. Many of the reports produced by KMyMoney may now be displayed in chart form. Line, bar, stacked bar, pie and ring charts are supported where appropriate</li>
          <li>Schedules. More flexible occurrence periods</li>
          <li>Importer. A totally re-written Quicken (QIF) file importer and an improved GnuCash file importer. Also added OFX support for money market accounts</li>
          <li>PGP encryption. Added support for more than one key</li>
          <li>Database. The optional use of a database for data storage is supported (MySQL, PostgreSQL and SQLite version 3 have been extensively tested)</li>
          <li>CMake. Added support for compilation and tests</li>
          <li>Translations. Added translations for Swedish, Simplified Chinese, Turkish, Romanian, and Finnish</li>
        </ul><p>Many enhancements have been made to existing functionality, to improve your user experience. Among the foremost of these are the following:</p><ul>
          <li>User interface. Several new options have been provided to enable you to configure KMyMoney to your personal requirements. Multiple user interface improvements</li>
          <li>Accounts. New 'wizards' have been produced to simplify the setting up of new accounts, and complicated situations such as loans and mortgages. One wizard in particular will guide first time users through the setting up of their new file, and template lists of frequently-used accounts and categories are supplied, often tailored to nationality. Defunct accounts may now be marked as closed, so that they no longer clutter up your screens, though they are still available for viewing if required. Allow creation of additional Equity accounts and editing of Equity transactions. Added hierarchy management to New Account wizard. Warning messages can be displayed if the balance on an account exceeds, or falls below, a given value</li>
          <li>Currencies. Added Euro support for Slovakia. Better support for multiple currencies</li>
          <li>Ledgers. The views of the various ledgers have been revamped to provide better performance, and more facilities for sorting and searching transactions, including a 'quick search' option. A new multiple transaction selection function will allow you to do some 'bulk' changing of transactions. A new, more comprehensive, transaction autofill facility, based on payee name, has been introduced</li>
          <li>Payees. Deletion of 'non-empty' payees and categories is now allowed, with the transactions being re-assigned as required</li>
          <li>Schedules. Changes have been made to simplify the entry and maintenance of your regular scheduled transactions. Allow direct editing and entering of schedules from Home page. Allow viewing and entering schedules on the Ledger View</li>
          <li>Reports. Improved text filter for transactions. Various fixes related to Loans, and Budget reports. Some new reports have been added, new levels of detail have been included, and many of them have also been made more configurable. There have also been a lot of fixes related to multi-currency reports. Improved calculation of returns and multiple currencies in investment reports</li>
          <li>Online update. If you use OFX or HBCI for online banking, your setup has been made easier, and a manual transaction matching facility should simplify reconciliation of your downloaded statements. There is support for new versions of AqBanking and libofx, providing more flexibility and reliability. Improved QIF and OFX support, with significant improvements to the OFX import process. Improvements and enhancements to matching and error handling, among others. Improved investment transaction and online update dialog</li>
          <li>Improved documentation and README files</li>
          <li>Updated translations for Spanish, Argentinian Spanish, Brazilian Portuguese, simplified Chinese, Czech, Dutch, French, Galician, German, Italian and Portuguese</li>
          <li>Lots of code cleanup</li>
          <li>A lot of bug fixing and polishing rough edges to make this an outstanding release</li>
        </ul><p>Let us know what you think. We hope that you enjoy using this version of
          KMyMoney.</p><p>Please let us know about any abnormal behavior in the program by
          selecting "Report bug..." from the help menu or by sending an e-mail to
          the <a href="mailto:kmymoney-devel@kde.org">developers mailing list</a>.</p><p>The KMyMoney Development Team</p>
<br/><br/>
<p>Detailed changelog:</p>

2009-05-11 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed cmake tests - Patch provided by Holger

2009-05-11 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added splitup-kde-chunk-online script to contrib directory
* Changed documentation build to use this script instead of a local copy

2009-05-10 Tony Bloomfield &lt;gandalf1189@users.sourceforge.net&gt;
* Don't delete all customized reports from database!

2009-05-09 Fernando Vilas &lt;fvilas@iname.com&gt;
* Updated schedule test cases to pass after Tony's patch
* Fixed a few warnings about const on primitive return types
* Made MMStorageSql inherit from KShared
* Changed the include order on reports test and pivottable test to make
  tests compile on Slackware

2009-05-09 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated Galician translation from Marce Villarino
* Show scheduled transactions during reconciliation and don't show the
  initial dialog for overdue scheduled transactions anymore

2009-05-09 Tony Bloomfield &lt;gandalf1189@users.sourceforge.net&gt;
* Fixed #2779291
- Correct precision in formatted fields
- Fix potential upgrade problem

2009-05-08 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated Romanian translation from Cristian Onet
* Fixed precision problem in transaction editor

2009-05-07 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed #1910270 (GNUCash Import Options large in PDF docs)

2009-05-07 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Updated user documentation - submitted by John Hudson

2009-05-06 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added date display for certain ledger entries

2009-05-03 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Updated Argentinian translation
* Updated Spanish translation

2009-05-03 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated German translation

2009-05-02 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Regenerated message files

2009-04-30 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added option to turn on expert mode in settings dialog

2009-04-29 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated Finnish translation from Mikael Kujanpaa

2009-04-27 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* More fixes to the online documentation generation
* Preserve access rights when saving a local file which was owned
  by another user but we had group write access

2009-04-26 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed generation and upload of PHB and online manual
* Added more output to configuration of sqlite support
* Fixed some tips
* Minor changes to the project handbook
* Added patch by Matt Blythe (payeeCategoryPatch.diff)

2009-04-18 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* For scheduled transactions with the estimate option turned on, an imported
  transaction with an amount in the range of +/- 10% of the scheduled amount
  will match

2009-04-16 Tony Bloomfield &lt;gandalf1189@users.sourceforge.net&gt;
* Add missing error check on saveAsDatabase

2009-04-15 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed balance display in ledger when all transactions of an
  investment account are filtered. Patch provided by Thomas Baumgart

2009-04-15 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed #2732207 (Unable to unmatch transaction matched by OFX import)
  for plain file based version. DB code still needs to be fixed.

2009-04-14 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added documentation of new file wizard (screen shots still missing)

2009-04-13 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed overdue scheduled transactions in pivot to display
  as happening tomorrow instead of the original date

2009-04-12 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Removed return in void setMatchData() introduced two days ago

2009-04-11 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Changed MyMoneyReport to use EDetailLevel instead of bool
* Cleaned up code in MyMoneyReport and related classes

2009-04-11 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Changed 'Schedule' --&gt; 'Scheduled transaction' (patch provided by
  Peter Hargreaves)

2009-04-11 Tony Bloomfield &lt;gandalf1189@users.sourceforge.net&gt;
* Update database structures to Version 5
- add missing fields in Payee, Split
- fix crash on duplicate report name

2009-04-10 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed permission problems during 'make distcheck'

2009-04-09 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Changed the generation of PDF versions of the documentation

2009-04-08 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed bko #188328 (Delete ransaction option is not disabled in closed
  accounts) with patch provided by Ian Neal

2009-04-07 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added line width option to charts

2009-04-07 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Improved 'Update all accounts' functionality to require less user activity

2009-04-05 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Initialize report type value when loading a custom report

2009-04-05 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated French translation from Patrick Petit

2009-04-04 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Handle invalid chart types of reports in a graceful manner
* Show only investments accounts in the account selector
  of Investment View, including when the investment account is
  a subaccount
* In the account selector, disable a parent account if it does not
  match the required type

2009-04-04 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Speedup loading of large files when filename is passed on command line

2009-04-03 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Reverted previous commit because some accounts are not showing

2009-04-02 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Show only investments accounts in the account selector
  of Investment View

2009-04-02 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Preset payee name with the previous one used for a new transaction

2009-04-01 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Enable OFX plugin by default

2009-03-31 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed the text filter when it does not contain a text and
  the main split matches that but a split does not
* Updated user documentation - submitted by John Hudson

2009-03-28 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Updated user documentation - submitted by John Hudson
* Further improvements of text filter
* Updated Spanish translation
* Updated Argentinian translation

2009-03-30 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Further improvements of text filter

2009-03-28 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Updated user documentation - submitted by John Hudson

2009-03-28 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added MyMoneyFile::referencesClosedAccount() methods

2009-03-26 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed Net Worth Today report to show current date's balance
  instead of end of the month -- bug #2714063
* Updated Argentinian translation - Provided by Sergio Minini

2009-03-25 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added patch from Cristian Onet (payees_view.patch)
* Fixed resize problem of splitter in payees view
* Added persistency to the splitter settings
* Added patch from Cristian Onet (plugins_settings.patch)
* Added missing desktop items to OFX plugin

2009-03-23 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed balance problem with DB backend
* Updated .desktop files with Romanian entries from Cristian Onet

2009-03-22 Fernando Vilas &lt;fvilas@iname.com&gt;
* Fixed bug in transaction date filter for db

2009-03-21 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Resolved some compiler warnings

2009-03-18 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Also reload OFX bank information files during setup if their
  size is less than 1024 bytes

2009-03-17 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed month-by-month budget report when fiscal year starts in a month other than January

2009-03-16 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed text matching bug in transaction reports when splits are displayed
  on its own, eg Transactions by Category reports
* Updated Spanish translation
* Updated Argentinian translation

2009-03-14 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added Investment Moving Average Price Graph

2009-03-11 Fernando Vilas &lt;fvilas@iname.com&gt;
* Removed redundant QString ctor calls in db code
* Fixed several warnings in db code
* db now remembers online banking KVPs

2009-03-11 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed overflow error in Total Rows of CashFlow Summary
* Fixed overflow error in Total Rows of Assets and Liabilities Summary
* Fixed currency conversion error in Budget reports

2009-03-11 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added patch by Holger
  0001-Added-build-types-Debug-Debugfull-Profile.patch

2009-03-09 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Added Investment Price Graph
* Added patch by Holger:
  0002-turned-bullets-off-and-simplified-the-chart-class-a.patch
* Updated Spanish translation

2009-03-09 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added option to skip stripping of online price info data

2009-03-08 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Dim update button if plugin status changed
* Added selection combo to negate text filter (not persistant yet)
* Use standard buttons for filter dialog
* Added patches by Holger:
  0001-moved-calculateAutoLoan-to-MyMoneyForecast.patch
  0002-Bugfixes-and-Preparation-for-cleaner-library-builds.patch
  0003-Fixed-last-patch.patch
  0004-OnMouseOver-in-Charts_NEW.patch
  typo-in-0001-0003.patch
  0001-added-fPIC.patch
* Show online balance for liability accounts in register as positive amount

2009-03-08 Alvaro Soliverez &lt;asoliverez@gmail.com&gt;
* Fixed Forecast reports when start date is after current date
* Fixed Budget reports for monthly budget of foreign currency categories

2009-03-03 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added patch provided by Holger (0001-USE_QT_DESIGNER-Version-number-and-minor-fixes.patch)

2009-03-02 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added patch provided by Colin Wright (briefscheduleskipiconanddisable.diff)

2009-03-01 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Return valid value from PivotGrid::rowSet(QString id)
* Added patch provided by Colin Wright (schedulecalendarskipbutton.diff)
* Fixed a few button icons

2009-02-27 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added option 'Match names from start'
* Fixed spelling of principal in account templates
* Added splitter to payees view

2009-02-25 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added patch provided by Colin Wright (editcompoundoccurence.diff)
* Fixed connection for edit widget signal in schedule editor
