---
title: 'KMyMoney 0.2pre1 Release Notes'
date: 2000-08-14 00:00:00
layout: post
---

0.2pre1

- Removed QdbtTabular from the source tree and we are now using
        a shared library.
- Added a KPayeeDlg class &amp; dialog.
- Added a KReconcileDlg class &amp; dialog.
- Added a KEndingBalanceDlg class &amp; dialog.
- Removed kdbMoneyEdit from the source tree and am now using my own class (kMyMoneyEdit).
- Extensive API changes to MyMoneyMoney.
- Copy constructors and assignment operators added to all the mymoney classes.
- Quite a few changes to the mymoney classes actually, (added consts, &amp;'s, removed a few methods etc).
- Switched to KDE2 Beta3 and QT2.2.0 beta0 (qt-copy-1.92).
- Changed the main window from KTMainWindow to KMainWindow and changed some affected code.
- Added income/expense support to categories and changed dialog and list view code to reflect these changes.
- Added a KNewCategoryDlg to edit the categories from KCategoriesDlg.
- Added code to load standard categories when a new file is created.  The data is loaded from
        $KDEDIR/share/apps/kmymoney2/default_categories.dat and the original is located in the source
        tree (./kmymoney2/default_categories.dat).
- We now prompt for the user to edit the categories when they are input into the editor.
        (This will be optional soon).
- The number field of MyMoneyTransaction has been changed to a string and all affected code has been modified.
- Removed (most of) the signed/unsigned comparison warnings.
- Almost reached a usable version with reconciliation now working.
- Made this release available on the web as almost 0.2 (stable).

180700
- Added a KQuickStartWizard class and an appropriate menu.
- Made KQuickStartWizard class work.
- Added a KStartDlg as a first contact for the user.
- Added an xpm for KStartDlg that represents the above mentioned wizard.
- Added a dialog to edit categories and sub categories. (KCategoriesDlg class).
- Added a new KCategoryListItem to support KCategoriesDlg.
- Made the tabbed input box 'remember' the major and minor categories.
- I have hidden the split button until I think I need it or someone e-mails
        asking to implement split transactions.
- Completed the KCategoryDlg editor class - apart from some interface issues.
- Fixed a long standing bug in MyMoneyMoney.
- Added a start balance to new account - should have been there a long time ago !
- Updated file format to include password and encryption fields (not used
    yet).
- Added a payToList to the file format.

050700
- Release for the web.
- Started to make the interface work better.
- Fixed loads of bugs.
- Updated the web page.
- Added new screen shots section to the web page.

230600
- When the user selects an atm bank the description field is automatically updated for them.
- The banks list is now user specified and can be added by typing the new name in the appropriate
      combo box.
- Changed editFrame to a tabbed input box to resemble MS-MONEY.
- Added first steps to create account balancing.
- Changed from for to category and changed all relevant code.
- Started to make the tabbed input box work.
- File format is well fucked up.
- Tried to get the file format working again.
- Removed the option to set the sort type and removed all respective code.
- Added calendar-0.12 into the source tree rather than create my own DateValidator.
- Changed code to use this new widget.
        See kmymoney2/calendar/README for more information.
- Removed QSplitter stuff and moved the three lists into their own widgets.
- Added a main widget to control the list view widgets.
- Added some more controls to the lists view widgets.
- Added the tabbed input box to the transaction widget.
- Removed some extranous code.
- Fixed up KGeneric transaction to display data in labels when not editing like ms-money.
- Started making the interface look like it will do in version 1.  (Most of it prints
        a message saying that it isn't working yet !).
- Removed calendar-0.12 from the source tree.  You now need to install this prior to compiling.
- Added a new tabbed widget to control the different aspects of KMyMoney2.  The Main widget ((KMainView)
        is now one of the tabs (Accounts).
- Cleaned up KMainWnd (the class inherited from KTMainWindow) and it is now lean and clean.

NOTHING WORKS AT THE MOMENT.

080600
- Changed the order and number of fields shown in transactionList.
- First attempt at fixing bugs in the list selection code.
- Fixed a couple of bugs in MyMoneyTransaction to fix the money represented as strings.
- Added ability to view balance at any time in the transactionList.
- Fixed up some code in the list viewing code.
- Made KDevelop 1.2 recognise some dialogs by hand editing the kmymoney2.kdevprj file.
- Cleaned up some of the dialogs.
- Added new variables to MyMoneyAccount and MyMoneyBank - FILE FORMAT IS NOW BROKEN.
- Updated dialogs to edit the new variables added above.
- File format now includes the new variables.  Old versions can't be read anymore.
- Added ability to right click on the transaction box to create a new transaction.
- Added ATM type to methods of transactions.  Updated file format again.


040600
- Added a settings dialog but it doesn't do much at the moment.

I considered this stable enough so I entered all my past transactions
and found about a million different bugs/wishlists, so I am now
going to implement them all for the next release.  (A list can be found
in TODO in the distribution.)

NOT been released on web page.

020600
- Removed automatic list view updating and replaced with right click option
        'view contents'.
- Added ability to track access/modify dates.
- Updated file format to reflect new variables.

NOT been released on web page.

010600
- Removed second toolbar, focus stuff, operation stuff.
- Added ability to right click on the list views.
- List views hide/show themselves appropriately.
- Removed toolbar pixmaps; edit, new, delete and the small logo.

NOT been released on web page.

  