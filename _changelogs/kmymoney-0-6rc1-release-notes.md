---
title: 'KMyMoney 0.6rc1 Release Notes'
date: 2004-01-16 00:00:00
layout: post
---
2004-01-16 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* 0.6rc1 released

2004-01-14 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated integration into KDE (desktop files)
* Fixed installation location for x-kmymoney2.desktop
* Updated home pages
* Added German home page

2004-01-13 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Make sure tips are included in pot file
* Added tips for German and French translation

2004-01-08 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* made some editorial changes to dialogs for better i18n support
* added German translation file (many thanks to Raphael Langerhorst
&lt;raphael-langerhorst@gmx.at&gt; who has done a great job
* updated kmymoney2.pot file

2004-01-06 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* added info about mandir setting in README
* added man page found in MEPIS distro
* added new application icon
* added creation of symbolic link kmymoney -&gt; kmymoney2 in $(bindir)
during 'make install'

2004-01-05 Kevin Tambascio &lt;ktambascio@users.sourceforge.net&gt;
* added support for the progress bar during read operations.
* added QStringEmpty and QCStringEmpty functions to properly handle
empty strings from the XML file.
* removed the bold when you hover over a link in the HTML view
(seemed to look strange when using it).

2004-01-04 Kevin Tambascio &lt;ktambascio@users.sourceforge.net&gt;
* removed the "nextid" attribute from some of the top-level XML attributes.
* added qWarnings and qError statements when irregularities in the file
are found.
* personal data dialog box had lower case "O" and "C" for the
ok and cancel buttons, which is inconsistent.
* added some Q_CHECK_PTR() functions to check for invalid pointer values,
which issues a warning to stderr.
* added some Q_ASSERT checks in various places in the XML Reader/Writer code.
* added a version field to keep track of different XML file versions,
in a similar fashion to the binary reader/writer.

2004-01-04 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Added button icons to KEnterScheduleDialog.
* Fixed handling of lastPayment in KEnterScheduleDialog.
* Increased vertical space to KEditScheduleDialog.
* Quick fix to make the split button work with no category.
* Check the date when entering schedules.

2004-01-03 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* added engine observer functionality to kMyMoneyAccountSelector
* correcting Mike's widget/Makefile.am change in the project file
so that it will be maintained over the next regeneration of
widgets/Makefile.am by KDevelop
* show correct icons in account's icon view
* fixed category assignment

2004-01-01 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* suppress standard F2 handling while in ledger's inline-edit-mode
* fix problem when to-account is empty and more button is pressed for
transfer transactions
* clear the transaction list in payees view if required
* overhauled some i18n stuff

2003-12-30 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Update search result when data in engine is changed
* Fixed all (but one) compiler warnings
* Regenerated kmymoney2.pot translation source

2003-12-29 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* select account for QIF import if it already exists
* fixed visibility of selected account in kMyMoneyAccountSelector
* re-arranged introduction to "import verification" message and
allow to turn it off in the future (DontShowAgain)
* removed check for C-only options (-Wbad-function-cast) in acinclude.m4.in
* added bzip2 tarball generation to Makefile.am
* added alternate background to transaction list in payees view and
account selector widget
* Added general method to extract colors respecting the user settings

2003-12-28 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* display correct action strings for credit card accounts in ledger
* support i18n of action strings in ledger
* added tip(s)
* fixed action assignment during QIF import
* changed QIF import to use KProcess instead of QProcess.
* added more automatic fixes after 'file open'

2003-12-26 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* resized starting size of split dialog
* use different icons for loan accounts in the account icon view
* added inequality operator to MyMoneyTransaction
* fixed logic of terminating notification suspending in MyMoneyFile
* added more debug output to MyMoneyStorageDump
* added default color settings to be drawn from KMyMoneyUtils
* show template page during startup if no other info was found. This
allows straight file generation for the first time user
* speedup reading of default income/expense categories
* finished work on reconciliation wizard
* added consitency check to the new account wizard (Don't allow to
proceed if data is not entered)
* allow creation of new payees from new account wizard
* added icons to KNewAccountDlg
* allow 'reverse' category for schedule, but warn if it is selected
* added button icons to KBackupDlg

2003-12-19 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* reworked KEndingBalanceDlg to be a wizard
* added function to add interest and charge transactions during
reconciliation
* renamed 'total profit' into 'net worth'
* added changes required to compile engine on MAC

2003-12-16 Kevin Tambascio &lt;ktambascio@users.sourceforge.net&gt;
* Added ability for the user's last used directory to be stored in the
save and open dialogs.
* Left old code in place pending reviews from the team.

2003-12-15 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* fixed some bugs in handling w/o inline editing (no transaction form)
* Some marketing on the about page
* Store balances and dates in a locale-neutral form with accounts
* Make Home and End button work in ledger views and find transaction dialog
* fixed #859083 (false right alignment of input widgets based on
kMyMoneyLineEdit)

2003-12-14 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* don't crash when closing file in ledger view showing loan account
* newly entered transaction becomes the current selected transaction

2003-12-13 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* show correct category underneath the ledger lens
* don't update screen while marking splits as reconciled
* delete key now enables to delete transactions in the ledger views
* optimized screen update for ledger view
* page up/page down keys now scroll correctly in ledger view

2003-12-13 Kevin Tambascio &lt;ktambascio@users.sourceforge.net&gt;
* Changed the khomeview.cpp and css file to brighten up the colors,
and use better fonts.

2003-12-11 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* respect date changes when entering schedule transactions
* create new payees during loan account creation (#859079)

2003-12-12 Kevin Tambascio &lt;ktambascio@users.sourceforge.net&gt;
* Able to read and write all elements of the storage file using XML.
* Added a US list of account categories. This file is called
'default_accounts_enUS.dat', and it requires the user to select it,
it's not the default list.

2003-12-11 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* reset the reconcile flag when creating schedules from existing
transactions
* fix existing schedules with reconciliation flag set
* added more details to dumper

2003-12-08 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* emit stateChanged in kMyMoneyAccountSelector in single selection mode
when selection changed
* added missing pages to the loan wizard
* fixed crash in settings dialog
* don't show 'unknown institution' if no account w/o institution is found
* added account's kvp to memory dump
* fixed 'file/open...' handling
* added testcases for them
* added wizard to modify loan data
* added ActionInterest
* added testcases for large values to MyMoneyMoneyTest::testFormatMoney()
* fixed MyMoneyMoney::formatMoney() for large values
* added new type for loan schedules
* updated the enter schedule dialogs to reflect loan payments
* don't show number of transactions in accountsview for standard accounts
(asset, income, etc.)
* added field to setup the number of days before the actual occurence of a
scheduled transaction it will be shown (functionality not implemented)

2003-11-26 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* added conversion to l10n free form for MyMoneyMoney objects
* removed KDE specific code from engine code
* added MyMoneySplit::ActionInterest

2003-10-23 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* move resize-grip in find transaction dialog
* fixed minor problems in transaction filter
* fixed duplicates for transalations in KMyMoneyUtils::stringToAccountType
* added investment loan to the new account wizard
* added button icons to KImportVerifyDlg
* show accounts in sorted order in the combo box of the ledger view
* make sure that kMyMoneyEdit fields always have a fractional part
* keep QIF profiles created during QIF input
* added financial calculator class MyMoneyFinancialCalculator
* force usage of new account wizard for creation of accounts
* removed selection of wizard from settings
* updated french translation file

2003-10-02 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* modify transfers to/from loan accounts to amortization payments
when a new transaction is entered into the engine
* added more logic for loan ledger view
* added logic to read categories from QIF file
* removed libxml++ references in configure stuff
* improved QIF import performance
* added method to turn off/on engine notifications for bulk updates
* send out notification if an account was moved to another institution
* if no filename is known, show 'Untitled' in caption
* allow creation of categories while entering transactions
* replaced "" with QCString() in all id handling spots
* replaced comparison against "" with call to isEmpty() method
* removed member m_file from MyMoneyTransaction
* adapted testcases to the QCString() changes
* don't popup transaction form when modifying transaction in
reconciliation phase
* removed message about file closing if no data has been changed
* added startup logo for version 0.6 (hope you like it ;-) )
* fixed crash in transaction filter
* fixed initial setting of post date for new transactions

2003-09-29 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Create a liability ledger view.
* Confirm that the user meant to enter a positive number for the
opening balance when creating liability accounts. (Should usually
enter a negative amount).

2003-09-27 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Change the end date when the start date changes while editing schedules.
* Re-implement paintFocus in KAccountListItem and KScheduledListItem.
* Use the doubleClicked() signal instead of executed() in the schedule list
view.

2003-09-26 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* smarter detection of filetype to be read (looks at file contents
not the extension)
* fixed edit/delete options in AccountIconView
* fixed listview handling with the changes for the background painting

2003-09-26 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Select the new institution in KNewAccountWizard.
* Pre-select the institution if we right click on an institution and choose
create new account.
* Remove the new, edit and delete buttons from the categories view.
* Select the payee after adding it in the payees view.
* Dont let account types not supported by engine at this time get created.
* Add a, non functional, help button to the new schedule dialog.

2003-09-25 Kevin Tambascio &lt;ktambascio@yahoo.com&gt;
* Able to write out files in XML with all the categories present in the new engine.
* Started the code for reading the XML files.

2003-09-25 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* moved KMyMoneyUtils::isCreditPayment to MyMoneyTransaction::isLoanPayment
* added testcase for MyMoneyTransaction::isLoanPayment
* removed unused code in MyMoneyTransaction
* marked addXXX methods of IMyMoneySerialize as deprecated in the docs
* added loan payment transactions (rudimentary support)

2003-09-24 Kevin Tambascio &lt;ktambascio@yahoo.com&gt;
* Made GUI changes to allow the user to save their file as an XML file.
* Removed checks for conditionally compiling XML suport.
This is possible because I am now using the QT API's, and everyone
has support for those.
* Able to now save a basic XML file. It only currently saves the user
information, and it provides placeholders for the other sections of
our file.

2003-09-24 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Draw the background colour across the _whole_ list view in schedule
view.
* Use alternating colours for the accounts and categories view.
* Start with the alternate colour in the list views.

2003-09-19 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* added new account selection widget
* find transaction dialog uses new account selection widget
* QIF import uses new account selection widget
* added convenience methods to MyMoneyTransactionFilter
* added button icons to the institution dialog
* added button icons to the category/account selection dialog
* fixed KFindTransactionDlg so that references to income and
expense accounts are not shown
* fixed transaction display in payees view
* added new reconcile icon for buttons
* fixed isValid() in kMyMoneyEdit
* added MyMoneyMoney::abs()
* added testcase for MyMoneyMoney::abs()
* fixed the amount filter to work with absolute values only and
to make sure that from&lt;= to
* removed debug output in payees view

2003-09-18 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* MyMoneySchedule::isFinished() now works for occurences of 'Once'.
* Added 'Todays payments' to the home view to make it easier to spot them.
* If the schedule has finished use the word Finished instead of the next
payment date in the schedule list view.
* Select the account type in the account wizard.

2003-09-17 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* added new fr.po supplied by Laurent
* fixed loading new account wizard widgets after loading of file
* update caption when QIF import is finished

2003-09-17 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* More validation on the KEditScheduleDialog user input.
* Add a gap to the home page just before 'Future Payments'.
* Fix creating a schedule from a transfer transaction.
* Set the right schedule type in KNewAccountWizard::accept.
* Work around backwards transfers in several places.

2003-09-16 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* You can only create schedules with a start date&lt; today if
they are not automatically entered.
* New option to the settings dialog to check schedules upon startup.
* MyMoneySeqAccessMgr::scheduleList() now calls MyMoneySchedule::isOverdue()
which takes into account the recorded payments for a schedule.
* Refresh the schedule view list items starting with the background colour.
* Take into account recorded payments when calculating the next payment
for a schedule.

2003-09-15 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Fix memory leak in new account wizard when checking if a payee exists.
* Individual occurences for a schedule can now be entered

2003-09-15 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* added MyMoneySplit::ActionAmortization
* added MyMoneySplit::isAmoritizationSplit
* added KMyMoneyUtils::isCreditPayment
* added kMyMoneyRegisterLoan
* fixed a bug in KNewAccountDialog when creating new accounts and
the accounts or category view is active
* force every account to have a parent in KNewAccountDlg
* allow to select all accounts as parents when creating an account
* fixed update of entry count in accounts/categories view
* fixed problem with date mark in ledger if all transactions are in
the future
* show matching substrings for payees in the transaction form the
same way as for categories/accounts
* added auto payee creation to QIF import
* do not allow to create account with same name
* fixed nasty bug that occured that occured during the cycle import /
verify / cancel /import and had to do with a loose observer object
* added case insensitive account names as suggested by Michael

2003-09-15 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Removed some old search code from KMyMoneyView and some compile warnings.
* Fix credit card schedule creation.
* Use StrongFocus for certain widgets in the accout wizard ui.
* Force the user to use the correct category type when creating schedules.
* Fix bug in KEditScheduleDialog that wrongly negated the entered amount.

2003-09-10 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Implemented adding a schedule for the credit card account type when
using the account wizard.
* When executing a schedule list item, set the date to the real next payment.
* Only call the slots in KEditScheduleDialog::relodFromFile() if its a new
schedule.
* Only load the 'to' accounts if the schedule's type is transfer.
* Only set the name field of the account dialog if we are editing.
* Add ability to create category hierarchy to MyMoneyFile.
* You can now create categories directly from the edit schedule dialog.
* You can now create categories directly from the enter schedule dialog.
* Masses of fixes to KEditScheduleDialog.
* Setup KEnterScheduleDialog with the correct date.
* The schedule's transaction can now be committed to the engine via a manual
enter. NOTE ONLY THE LAST PAYMENT CAN BE COMMITTED at the moment.
* Added two methods to MyMoneySchedule : isOverdue() and isFinished().
* Changed the enums for MyMoneySchedule members.
* Only show the enter button for valid schedule next payments.
* Schedules that have finished now have green text.

2003-09-10 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* use the accounts view as standard, not the institutions view
* added 'Unknown institution' as standard to institutions view
* fixed memory leak in exception handling of KAccountsView::slotListRightMouse
* create Default QIF profile if not existant during QIF import/export
* use KDE documentpath to start looking for files to be imported
* fixed missing notifications when adding/modifying/removing institutions
* removed i18n() from certain non-translatable strings in the QIF profile
editor
* improved error messages during QIF import and display QIF entry
during account/category selection for user's reference
* set focus in KAccountSelectDlg to create button
* fixed edit and delete operation of RMB menu for institutions

2003-09-07 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* fixed visibility of new institution dialog when import a QIF file
* use current date as opening date for new accounts if the date
passed from e.g. a QIF source is invalid

2003-09-06 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* added kMyMoneyEdit::isValid()
* added MyMoneyAccount::accountGroup(void)
* revised interface between KLedgerView and kMyMoneyRegister
* Added KMyMoneyTransaction which is based on MyMoneyTransaction
but also keeps additonal information required for display purposes
* renamed MyMoneyTransaction::split to splitByAccount
* added MyMoneyTransaction::splitById which returns a split by split id
* optimized space for items in search register
* added full search functionality
* fixed bug in font handling of register code
* added possibility to create scheduled transaction from ledger view
based on existing transaction
* added button icons to find transaction dialog

2003-09-04 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Fix bug in KEditScheduleDlg::slotSplitClicked() and
KEnterScheduleDialog::slotSplitClicked().
* More split related bugs fixed in KEditScheduleDialog and
KEnterScheduleDialog.
* Re-wrote most of KEditScheduleDialog. Fixes _many_ bugs.

2003-09-02 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Show overdue schedules with red text in the list view.
* Fixed stupid bug when creating schedules.
* Schedule brief widget now shows only the schedule for the date we are over.
* Added initial ui file to enter schedules manually.
* Added class for the enter dialog.
* Display overdue schedules in red in the calendar.
* Added ability to right click on schedule in the list to enter it manually.
* Added an Enter button to the Brief schedule widget.

2003-09-01 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Made the list colours in the schedule view match the home view.
* Implemented the schedule link from the home page.

2003-08-31 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* added MyMoneyTransactionFilter class
* modified engine to use MyMoneyTransactionFilter instead of dedicated code
* modified GUI code to use MyMoneyTransactionFilter
* added 'case sensitivity' and 'regular expression' switch to
KFindTransactionDlg
* added definition of MyMoneyMoney::minValue and MyMoneyMoney::maxValue
* major overhaul of the view interface

2003-08-31 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Added a default default_accounts_enC.dat file.
* Set KDialog to look for the default_accounts_enC.dat file for its
initial dir.
* Fix the mouse tracking in the calendar table.
* Popup the Schedule brief widget 'intelligently'.
* Make sure default_accounts_enGB.dat, default_accounts_enC.dat and
comptes_par_defaut_fr.dat are installed.
* Made the Schedule brief widget borderless and added a close button.

2003-08-22 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* added french files provided by Laurent Colognes

2003-08-21 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* added more logic to the find dialog
* added more output to dump
* Fixed a bug in KNewAccount Wizard when creating liability accounts
* Differentiate accounts and categories with the same name during import
* added consistency check tool
* allow to move accounts between expense/income group
* Fixed close window function, again

2003-08-10 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* backup file name is now constructed using international date format
* added progress bar to backup function
* added new find transaction dialog (no functionality yet)
* revised register class

2003-08-09 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* avoid more compiler warnings
* use last used post date for new transactions
* fix view update after loading a file
* force memo field in transaction form to be left aligned
* fixed bugs in settings dialog (grid setting was not written)
* removed color settings for transactions
* some speed improvements after loading a file

2003-08-06 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* set date fields in export QIF dialog to the posting dates of the
first and last transaction found in the selected account
* made appendCorrectFileExt() a static method of KMyMoneyUtils and
removed it in all other places
* removed unused connections in KMyMoney2App

2003-08-02 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* show full url as tooltip in start dialog
* fixed endless loop when recent file list contains abandoned files
* disable dragability of items in start dialog
* added icon to reset button in settings dialog
* added logic for customization of home page to settings dialog
it is not yet used in the home page logic
* use KDE settings for font calculation in settings dialog
* allow setting of Preferred attribute in KNewAccountWizard
* removed reference to unknown signals/slots in KMyMoney2App::slotQifExport
* added button icons to QIF export dialog
* added button icons to QIF import dialog
* cleaned up QIF import code from leftovers of old implementation
* added button icons to QIF profile editor

2003-07-31 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Fix the nasty bug i introduced into the start dialog.
* Removed the fileNew action.
* File Save/Save As default to Documents folder.
* Removed unnecessary actions.
* Fix crash when File|Close is chosen.

2003-07-30 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Removed duplicate entries from default_accounts.dat.
* Renamed default_accounts.dat to default_accounts_enGB.dat
* Display only the filename in the recent files icon view.
* Default to the 'Documents' folder in the start dialog.

2003-07-30 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* added button icons to the ledger view
* added button icons to split dialog
* removed warnings in ksplittransactiondlg.cpp
* added button icons to split correction dialog
* added french home page

2003-07-30 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Use the IconGroup User to load icons.
* Add icons to the schedule list view items.
* Use the global icon loader object.

2003-07-29 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Add icons to the edit schedule dialog.
* Added icons to the schedule dialog.
* Added icons to the new file dialog, schedule view.
* Use colours in the schedule list.

2003-07-29 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* disable save button and menu entry if data in engine is unchanged
* fix memory leak in KLedgerView
* added feature to have country and language specific home page

2003-07-29 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Fixed loading of blank icon in the startup dialog.
* Fix schedule errors.

2003-07-28 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* fixed setting of 'startup file options' in settings dialog when
the reset button is pressed
* added feature to select the last page viewed when starting the application
* use QPushButton::setPopup for the MORE button in the transaction form
* use the british english spelling Cheque
* fixed compile error in MyMoneyStorageBin
* fixed crash when exiting the startup dialog with 'Cancel'

2003-07-26 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added concept of preferred account
* remove KDE specifics from MyMoneySchedule
* MyMoneySchedule now uses QDate::isValid() to determine validity of dates
* align values to the right in the schedule list view
* re-arranged inclusion of header files to speed up compilation
* MyMoneySchedule::account() now returns MyMoneyAccount object
* Added 'dynamic home page'

2003-07-23 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Fix handling of split dialog.
* Save open state of top level items in schedule list view.

2003-07-23 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Show filename and modification mark in caption of main window
* Avoid some compiler warnings
* Fixed order when reading items from file
* Suppress false memory leakage message during autotest
* Added &lt; operator to MyMoneySchedule to allow sorting

2003-07-23 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Removed unnecessary schedule edit classes.
* Removed old unnecessary classes.
* Changing transactions remaining also changes end date in edit dialog.
* Changing end date also changes transactions remaining in edit dialog.
* Fix IEditScheduleDialog.
* Set default list view items to open.
* Save memo field in KEditScheduleDialog.

2003-07-22 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Removed unnecessary atributes from MyMoneySchedule.

2003-07-20 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* always select a transaction in the ledger view if one's available

2003-07-19 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed testcases for MyMoneySchedule related tests in
MyMoneySeqAccessMgrTest
* modified construction of transfer transactions in
keditscheduledtransferdlg.cpp

2003-07-18 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Added the equality operator to MyMoneySchedule.
* List and calendar view honour the account filter.
* Implemented file reading and saving of schedules.
* Added a transferAccount member attribute to MyMoneySchedule.
* Updated the read and save routines.

2003-07-18 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Fix MyMoneySchedule::nextPayment()
* Clear splits when KSplitTransactionDlg sets only one. (Workaround).
* Fixed occurence of schedules.
* Fixed tab order in new schedule dialogs.
* Show the accounts that can be filtered on.

2003-07-17 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Use MyMoneySchedule::validate() when adding schedules.
(validate() will be fully implemented later).
* Added some convenience methods to kMyMoneyCombo to reduce code duplication.
* Removed the MyMoneyScheduled class.
* Added an accountId member to MyMoneySchedule.
* Added a combo box to filter on accounts in the list &amp; calendar view.

2003-07-16 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Updated the UI to use MyMoneyFile for sheduled transactions.

2003-07-14 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added support for scheduled transactions to the MyMoney engine code
GUI code does not use it yet, but testcases do

2003-07-09 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* cleanup in headerfile includes
* refresh schedule view upon loading of a new file

2003-07-07 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Added a hoverDate signal to the table.
* Set mouse tracking in the table so we get all mouse move events. (Doesnt Work?)
* Added UI file for the schedule brief description widget.
* Implemented the select schedules combo box option to filter the calendar
on schedule types.
* Use contentsMouseReleaseEvent rather than contentsMousePressEvent in
the base class kMyMoneyDateTbl.
* Fix implementation of kMyMoneyScheduledDateTbl::addDayPostfix.

2003-07-05 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Fixed bug in KMyMoneyCalendar to create the private objects *before* the
initialisation.
* Draw the userButtons after the first 'styleControl' button.
* Added schedule type selection to the calendar via the new user
buttons.
* Implemented the weekly view of the calendar. (Note, always uses Monday
as the start of week, (ignores kde settings at the moment)).
* Started work on integrating the schedules into the calendar view.
* Changes to the account selection are notified to the schedule calendar
view.
* The date for the current cell to be painted is passed to the
drawCellContents method.
* Changed the list view widget in KScheduleView to a KListView so I can use
the executed signal.
* Implemented an executed slot on a schedule list item that shows the next
payment date in the calendar.
* Removed the edit and delete buttons. These are accessed via a right mouse
click on the item.
* Removed the selection signal from the schedule list view.

2003-07-04 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* More work on the calendar view, including deriving a new class from
kMyMoneyDateTbl as kMyMoneyScheduledDateTbl.
* Added a style control button to the calendar view. (Not working).
* Draw rectangles around the days. (Will be configurable in future).
* Added ability to add 'user buttons' to the calendar. This will be
used by the schedule table to display schedule options for the
calendar. Only two user buttons are currently supported.
* Fixed bug in KDateTable::setEnabled()

2003-07-03 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* fill AccountsView with standard accounts even if there are
no further accounts defined
* preset correct type of new account according to current selected
account in the accounts and categories view
* made new widgets compilable under KDE 3.0 - while loosing some
functionality :-(

2003-07-03 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Subclassed a new more specific to schedules calendar class
from kMyMoneyCalendar.
* Added a close button to kMyMoneyDateInput, (Requires &gt;= KDE-3.1).
* Imported the KDatePicker &amp; KDateTable classes into KMyMoney to use
as the basis for the calendar view. Custom implementation still to
be done.

2003-07-02 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Fixed editing the schedule's transaction splits.
* Display only positive amounts in the schedule list view.
* More work on the scheduled transfer dialog.
* Fixed schedule validation for transfers.
* Added a name property to MyMoneySchedule.
* Updated UI dialogs to handle the schedule name.
* Added the initial widget for the calendar view. (Does nothing yet).

2003-07-01 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Fixed crash in KScheduleView::refresh when no accounts exist.
* Monitor selections in KStartDlg so the user can select an item
and then press on OK and the item is executed as expected.
* Deselect all other highlights in KStartDlg when a selection occurs.
* Remember the last page in KStartDlg, (defaults to recent files page).
* Context menu can appear in the schedule view when there are no items.
* Disable schedule view if no account is available.

2003-06-30 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* updated 'home' page for next release
* differentiate between new account and new category
* cleanup of slot interface between KMyMoney2App and KMyMoneyView
* removed debug output messages in various classes
* added function to create new institution during QIF import
* fixed bugs while importing splitted transactions
* set opening date on accounts only if date passed is invalid
* fixed KNewAccountWizard to set opening date correct
* added filter location entry to QIF profile
* disable ledger view if no account is available
* added external filter support for QIF import
* prepared external filter support for QIF export
* added duplicate method to IMyMoneyStorage objects and testcase

2003-06-20 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* show liability and asset accounts when no account info is found
in the QIF source file for account selection
* KMyMoneyUtils::stringToAccountType() now works case insensitive
* Fixed the account selection dialog to allow creation of categories
* Added method to extract the parent name of a colon-seperated
hierarchical category name to MyMoneyFile
* Fixed some problems of the KNewAccountDlg with respect to categories
* Don't allow to create accounts of type income and expense in
KNewAccountDlg when not called as category editor
* Use KMyMoneyUtils to convert from textual form of account type
to numeric representation
* Cleanup of account and category editing
* Removed MyMoneyAccount::setAccountTypeByString in favor of
usage of KMyMoneyUtils::stringToAccountType
* Make sure, one cannot select an account as it's own parent account
* Show selected parent account when opening KNewAccountDlg
* Added support to suppress updates during lengthy operations to
more views
* Fixed uninitalized variable in KAccountSelectDlg
* Added date import conversion to MyMoneyQifProfile
* Added coloring for imported transactions to kMyMoneyRegister
* Added parameter 'name' to kMyMoneyCombo constructor(s)
* Cleanup of KGlobalLedgerView member variables

2003-03-06 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Moved categoryTypeE to KMyMoneyUtils
* Updated template files to contain standard text
* Added tips entry
* added MyMoneyKeyValueContainer functionality to MyMoneyTransaction
* more work on the QIF import (not done yet)

2003-02-26 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* include config.h in mymoneymoney.h
* changed the state-engine during backup
* fixed backup procedure (error report #692390)
* added "Don't ask again" to warning box about new file layout

2003-02-13 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added missing account types to utility functions
* Fixed online documentation in MyMoneyFile
* Added dialog to select an account
* Moved the MyMoneyQifProfileEditor to the dialog subdir
* Removed some circular references among classes
* More work on QIF import

2003-02-05 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added some methods to KNewAccountWizard to be capable to preset values
* Added opening date to account wizard
* Added method to set account type to new account wizard
* Added KMyMoneyUtils as container for utility functions requiring KDE
* Moved accountTypeToString and stringToAccountType to KMyMoneyUtils
* Removed some warnings
* Added some more values to the data dumper
* Added more teststeps to MyMoneySeqAccessMgrTest

2003-01-30 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* More work on the scheduled view and dialogs.
* Fixed combo box selection bug in okClicked for all dialogs.
* Disable transfer dialog until i fully understand whats needed.
* Implemented the edit schedule feature for bills and deposits.
* Implemented the delete schedule feature.
* Added context menus to the list view.

2003-01-26 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added new method MyMoneyFile::storage() and tests
* Added equality operators for MyMoneyPayee, MyMoneyAccount,
MyMoneyBalanceCacheItem
* Added test for them
* Removed necessity for KMyMoneyFile object
* Added feature to hide unused categories in lists
* Added method transactionCountMap to MyMoneyFile
* Started working of QIF import
* Removed duplicates from tips, fixed HTML code in tips
* Added missing converter directory to CVS

2003-01-23 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Started work on UI for schedules.
* Started work on the 'New Bill' schedule interface dialog.
* Updated MyMoneyScheduled for account specification, preparing it for
inclusion into MyMoneyFile.
* Updated the schedule tests to add the account references.
* Started work on the 'New Deposit' schedule interface dialog.
* Started work on the 'New Transfer' schedule interface dialog.

2003-01-21 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Added the beginnings of recurring transaction support.
* Added some test cases for the MyMoneyScheduled and
MyMoneySchedule classes.

2003-01-04 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Honor start date setting in ledger view was broken after startup
* Added QifProfile and QifProfileEditor classes
* Fixed memory leaks when editing in-register is selected
* Calculate the size required for the datepicker according to it's sizeHint()
* Fixed documentation bug in MyMoneyFile
* Allow empty separators for decimal and thousands in MyMoneyMoney
* Added QIF export functionality
* Use standard menu icons where available
* Added method to refresh the payees view after loading a file
* Moved logic to drop some progress bar updates to KMyMoney2
* Allow empty separators in MyMoneyMoney
* Added method to return current cell font to kMyMoneyRegister
* Added display of number of transactions in accounts view
* Updated menu structure
* Updated message file (POT)

2003-01-03 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added display of number of transactions in accounts list view
* Added configure.in.in to project for inclusion in source distribution
* Created Source package for version 0.5.1

2003-01-01 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added section about signing an RPM to the project handbook
* Added some more documentation to source at various places

2002-12-31 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed some inline documentation
* Payee's transaction list respects list font settings
* Reduced cell height in register and split register

2002-12-30 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added list of transactions to payee view
* Added fixup for splits in KLedgerView and MyMoneyStorageBin
* Added method to extract a split from a transaction by payee-Id
* Added class KTransactionListItem()
* Double click on transaction in payee view opens ledger view
with this transaction selected
* Added function to move from selected transaction in ledger view
to the assigned payee

2002-12-28 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added functionality to 'More' button in transaction form

2002-12-22 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Force static linking of libxml++ if present

2002-12-18 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added missing admin/am_edit to source distribution
* Added some tips
* Added some description to the 'home page'
* Added descriptive warning to the save operation

2002-12-17 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed translation problem in KReconcileDlgDecl.ui
* Remember the type of accounts view the user selected last

2002-12-16 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Dynamically allocate application object to cover more code with
memory leakage checker
* Added method to clean static objects in KAccountListItem
* Fixed progress bar when reading old format files
* Fixed memory leak in KMyMoneyFile, KNewAccountDlg, KNewFileDlg,
KEndingBalanceDlg, KBackupDlg, KChooseImportExportDlg, KCsvProgressDlg,
KExportDlg, KFindTransactionDlg, KImportDlg
* Including XML support needs special compiler option
* Updated a couple of Makefile.am for the RPM distribution

2002-12-15 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated PHB to contain information about building RPMs
* Fixed compilation with _CHECK_MEMORY defined

2002-12-10 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Removed unknown file references from the KDevelop project file
* Removed some debug output that is not needed anymore
* Modified some Makefile.am to not include unnecessary files
in the source distribution
* Updated README with up-to-date installation instructions

2002-12-09 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Speedup load operation by filling the balance cache while loading
* Added some more tips
* Replaced progress dialog with global progress bar in status line
* Added progress callback to MyMoneyStorageBin, MyMoneyStorageXML
and IMyMoneyFormat
* Corrected display of status message in status bar
* Moved tip of the day logic to main()
* Close the splash screen upon the first call to the application
(startWithDialog())
* Revised startup code in main() and KMyMoney2App()

2002-12-08 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added mark of current date in ledger view if it's sorted by posting date
* Removed a few unused member variables from kMyMoneyRegister
* Added sorttype 'Number' to ledger views
* Don't override the entry date of transactions when loading a file
* Added option to show a nr field in any transaction form
* Added option to insert the transaction type into the nr field upon
creation of a new transaction
* Terminate any edit session before starting the settings dialog
* Cancel edit session before re-sorting
* Keep the transaction selected after re-sorting
* Removed old categories file from project
* Added a german account template file
* Added a file dialog to select the initial account/category set
for a new file
* Added progress bar when reading the initial account/category set
* Update ledger view after loading a different file
* Allow to edit the number field in credit card view

2002-12-05 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added 'tip of the day' feature
* Added different images to account icon view
* Added key value container to MyMoneySeqAccessMgr, it's interface
IMyMoneyStorage and MyMoneyFile.
* Store and load the key value container with the (binary) file
* Moved account icons from kbankviewdecl.ui to kbanksview.cpp
* Converted account icon view from QIconView to KIconView
* Added KAccountIconItem class
* Reset values in new account wizard when starting
* Added account types asset and liability to account dialog
* Suppress a few options in the account context menu
* Added functionality to the Account detail button in the ledger view

2002-12-04 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added possibility to change from transfer to non-transfer and vice
versa in in-register edit mode
* Added cash and asset account types
* Added missing Nr label during entry of ATM transactions in the form
* Cancel any transaction edit session if an account is selected with
the ComboBox in the ledger view
* Added new sort types for ledger sorting (Type, Nr, Receiver).
See KTransactionPtrVector for details
* Added context menu to change sort order
* Fixed functionality of context menu in accounts view
* Added context menu to categories view

2002-12-02 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Show transaction action also in register underneath the date
* Changes to payment/deposit and transaction type are reflected
in the 'other widgets' during the edit phase of a transaction
* Updated kMyMoneyCombo to serve as widget for the ledger view
* Preset post date only, if invalid. Otherwise, take user's input
* kMyMoneyEdit will respect selected text upon entry of arithmetic
operators (e.g. +, -, *, etc.)

2002-11-28 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Balance of accounts in accounts and category view did not get updated
* Show liablities also as positive values in accounts view
* Summary line shows balance in red if negative
* Exit any pending edit activities in ledger views when switching to
a different view (e.g. payees or schedule view)
* Added Credit Card ledgerview
* Added automatic preset of from/to account if opposite is not
the current selected account
* Update split.action() when a negative amount is entered
* Handle from/to account of transfer when entering negative amounts
* Made the finish button the default for the split dialog
* Made Enter button the default button in transaction form

2002-11-27 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added RBM context menu to the ledger view
* Saving the file in binary format now clears the dirty flag
* Added option to 'jump' to the opposite split of a transfer transaction
in the context menu
* Prepared to store scheduled transactions in the binary file
* Fixed decimal problem in kMyMoneyCalculator
* Removed some debug output which I do not need anymore
* Fixed bugs in MyMoneyMoney(QString) constructor regarding fractions
Added testcases to find them the next time
* Write balance in register in red when negative
* Fixed a problem introduced by removing an entry from settings dlg
* Re-organized keyboard handling in split dialog
* Fixed a bug in register keyboard handling
* Added split handling to savings ledger view
* Don't open calculator if first character is plus or minus
* Added possibility to create transactions with an empty category
* Fixed the 'leave unassigned' feature for splitted transactions
* Incorrect transactions (less than 2 splits, sum of splits not equal 0)
will change their textcolor in the register with 1Hz
* Removed more unused code

2002-11-25 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Removed unused files from project
* Fixed bug in MyMoneyMoney::formatMoney() when value is negative and
fraction is 0
* Modifed the MyMoneyMoney stream in/out operators to support 64 bit values
Write out warnings, if old format is used
* Fixed handling of locale settings in MyMoneyMoney
* Setup locale settings in MyMoneyMoney before the first usage
* Integrated the split dialog
* Fixed visual appearance of calculator
* Removed setting for textual prompt as it is not provided anymore
* Added border around KMyMoneyView in KMyMoney2App
* Fixed minor details while going from gcc 2.95 to gcc 3.2
* Set the row height of the transaction form to a fixed value
* Corrected usage of filters during reconciliation

2002-11-18 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed filter set for reconciliation
* Added difference display in reconciliation view
* Fixed alignment of display in reconciliation view
* Added switch to turn on transaction form during reconciliation
* Fixed bug introduced with last change to kMyMoneyEdit
* Made sure that the datepicker is always shown completely on screen
* Added icon to the datepicker button

2002-11-17 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added MyMoneyKeyValueContainer class to have the ability to
add a variable number of arguments to an engine object
* Fixed reader for old format to interpret account type correctly
* Fixed testcases as well
* Fixed settings dialog to set the startdate to 1.1.1970 if not
present in kmymoney2rc file. Otherwise, it would use currentDate.
* Added MyMoneyKeyValueContainer to MyMoneyAccount
* Added testcases for MyMoneyKeyValueContainer and updated
testcases for MyMoneyAccount
* Added a widget stack to the ledger view and use it within the
the checkings and savings ledger view to switch between the
buttons and reconcile data
* Added methods to interface IMyMoneySerialize to load the internal id's
* Fixed KEndingBalanceDlg (activated code, made member vars private, etc)
* Started working on reconciliation for checkings and savings accounts
* Fixed a missing pointer assignment int kMyMoneyRegister
* Fixed resetting the next...Id values inside MyMoneySeqAccessMgr during
file reading
* Fixed flickering when redrawing the register
* Added first implementation of reconciliation code
* Modified kmymoneyview.cpp to correctly interpret the filename for
files accessed through a network-URL
* Fixed event processing in kMyMoneyEdit
* Added signalSpace() to kMyMoneyRegister

2002-11-14 Kevin Tambascio &lt;ktambascio@yahoo.com&gt;
* Modified kmymoneyview.cpp, to allow XML files to be passed to the right
file parser.
* Added mymoneyxmlparser.h, which extends the xml++.h class.
* removed mymoneystoragexmlcallback files, because they weren't needed.
* more work on the XML Reader code.

2002-11-10 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added support for network file access over the protocols
provided by KIO (ftp, http, smb etc.)

2002-11-09 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added support for compressed file I/O. The reader checks if an
uncompressed or compressed file is read. The writer always adds
compression. This can be turned off by setting the switch
'WriteDataUncompressed' in the [General Options] section of
the kmymoney2rc file. A UI in the settings dialog is not supported
as the end-user should always get a compressed file
* Therefor changed the interface to IMyMoneyStorageFormat to pass a
QIODevice* instead of a QFile*

2002-11-08 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed bug in MyMoneyMoney locale handling
* Added testcase for that
* Removed some more references to the old transaction view
* Added a summary line underneath the register in the ledger view
* Added the savings view
* Added warning when trying to edit a reconciled transaction
* Frozen transactions cannot be edited anymore
* Cleaned up code of checkings view
* Added MyMoneyTransaction::setEntryDate (no testcase yet)
* Added new binary writer/reader pair (alpha state, you have been warned)
* Removed unnecessary methods from MyMoneyStorageXML
* Modified interface to IMyMoneyStorageFormat to pass a QFile* instead
of a QDataStream&amp;
* Prepared file operations in KMyMoneyView to support network access
via e.g. HTTP / FTP.

2002-11-08 Kevin Tambascio &lt;ktambascio@yahoo.com&gt;
* Made changes in MyMoneyStorageXML to begin the support of reading
XML-based files. There is no real functionality yet, I just wanted to
archive my changes.

2002-11-05 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* kMyMoneyRegister now fully supports font changes of cell and
header Font
* Ctrl-T now switches the register detailed view on/off. The form
visibility can be changed using the settings dialog
* Setting for row count in register deprecated and removed from
settings dialog
* Fixed recursive loop in kMyMoneyDateInput widget
* Eliminated unused method slotEnterPressed()
* Fixed kMyMoneyDateInput to obey KDE's locale settings
* Added locale handling interface to MyMoneyMoney
([set]decimalSeparator() and [set]thousandSeparator()
* Modified MyMoneyMoney::formatMoney() and MyMoneyMoney(QString&amp;)
constructor to support locale settings
* Added testcases for the MyMoneyMoney locale stuff
* Added some more API documentation
* Fixed a bunch of problems introduced with the latest
check-in by Kevin

2002-11-04 Kevin Tambascio &lt;ktambascio@yahoo.com&gt;
* Added new pure virtual class to describe the file type reader classes,
IMyMoneyStorageFormat.cpp and .h.
* Updated project files to include these new files.

2002-11-01 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed return code of kMyMoneyLedgerViewCheckings::focusNextPrevChild()
* Improved KLedgerView::focusNextPrevChild to handle invisible and
disabled widgets
* Added ledger lens to show all rows of the selected transaction
in the ledger. This is the default when the transaction form
is turned off.
* Added settings options for the ledger lens and the transaction form
* Added the new icons to the settings dialog

2002-10-30 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added parameter name to constructor of kMyMoneyLineEdit
* Added kMyMoneyDateEdit::focusWidget()
* Fixed tab order handling in form based transaction entry
* Fixed calculator to return first operand as result if no second operand
has been entered after an operation was selected
* Improved keyboard support for ledger view
* Added more source code documentation
* Removed some unused code

2002-10-27 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added detection of libxml2 and libxml++ to configure
* Fixed initial loading of KAccountListItem

2002-10-25 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Changed line number in MyMoneyException to unsigned long
* Added documentation to some engine and view classes
* Removed references to MyMoneyFile within a transaction
* Renamed 'Bills &amp; Reminders' into 'Schedule' in icon list
* Added icons for the icon list to icons/48x48/apps
* Added accountTypeToString and stringToAccountType helper methods
to KMyMoneyFile
* Added KTransactionPtrVector::setSortType()

2002-10-21 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Modified name of member variable in KCategoryView
* Added checks for atoll and strtoll to configure.in.in
* Replace atoll with strtoll in mymoneymoney.h
* Reduced number of arguments for KAccountListItem constructors
* Added documentation to KAccountListItem
* Eliminated the ugly hack in KAccountsView that Michael wanted a
solution for
* Fixed resizeing of CategoryView and AccountView
* Replaced includes with forward class references where applicable
* Added kMyMoneyCalculator widget to kMyMoneyEdit
* Improved performance when building account trees
* Added missing mymoney/autotest.h file to the repository

2002-10-15 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added notification classes for payees, institutions, accounts
and account-hierarchy to MyMoneyFile
* updated test cases
* Added logic to support transfer transactions
* Updated KPayeesView to interface to new MyMoneyFile object
* Moved general logic from KLedgerViewCheckings to KLedgerView
* If an account is opened, the ledger view is selected instead of
the 'old' transactionview.
* Fixed handling of Finish-Button in Account-Wizard
* Added descriptive error messages to MyMoneyFile::addAccount()
* Fixed documentation of MyMoneyFile::attach()
* Fixed MyMoneySeqAccessMgr::totalBalance() to use totalBalance() of
subordinate accounts instead of balance().
* Delete objects in KMyMoneyFile::close()
* Added eventFilter to class kMyMoneyTransactionFormTable
* Added framework for iconlist to account view
* Allow a maximum of three rows per transaction in the settings dialog
* Fixed names of standard account names to have only two colons max

2002-09-16 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added MyMoneySeqAccessMgr::payeeByName() and MyMoneyFile::payeeByName()
* Added MyMoneyTransaction::splitSum()
* Added MyMoneyFile::categoryToAccount() and MyMoneyFile::nameToAccount()
* Added testcase for the above functions
* Fixed bug in MyMoneySplit::operator ==
* Changed interface to MyMoneyTransaction::addSplit to use reference
* Added MyMoneyTransaction::splitSum() and testcases
* Added some details to exceptions in MyMoneyTransaction
* Added central function to display info about unexpected exceptions
in autotest.cpp
* Added logic to fill checkings form and to add/modify transactions
* Fixed kmymoney2/Makefile.am to be able to extract the i18n messages
* Added new startup logo customized for version 0.5

2002-08-19 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed configure problem when CPPUNIT is not installed
* Fixed MyMoneyMoney::format() to return fractional part for value 0

2002-08-09 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Do not allow in-register editing if the form is visible
* Added payee widget with auto completion feature
* Fixed some problems showing up with GCC 3.x, thanks to Jim Blomo
for reporting them

2002-08-07 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added option to show/hide the transaction form in a ledger view in the
settings menu and through shortcut (Ctrl-T)

2002-08-06 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added reconciliation flag display in KMyMoneyRegisterCheckings
* Added MyMoneyFile::accountToCategory()
* Added enable/disable of buttons in transaction form
* Added display of category within transactions
* Display only absolute value of amount in transaction form
* Fixed initial visibility if more than 1 row per transaction is selected

2002-08-05 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added number and action handling to MyMoneySplit
* Added number and action handling to MyMoneyStorageBin
* Added kMyMoneyTransactionFrom widget
* Added transaction selection in ledger register widget with mouse
* Added form display for checking accounts
* Fixed grammer error in project handbook

2002-07-23 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added logic to support selection bar in kMyMoneyRegister
* Added payee data loading from old file format
* Catch exceptions when reading data from file

2002-07-22 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added balance cache to MyMoneySeqAccessMgr for better performance
* Added balance display to ledger view and register widget

2002-07-21 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added global function timetrace() for simple time measurement
* Added ledger view (base class and checkings implementation)
* Added function to return matching and non-matching split
for a given account from a transaction to MyMoneyTransaction::split()

2002-07-09 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed account creation with through standard dialog
* Added more logic to the Account Creation Wizard

2002-07-08 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added Account Creation Wizard
* Added configuration setting to select between wizard and dialog
for new account creation

2002-06-24 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Made KTransactionView enabled.
* Fixed transaction refresh.

2002-06-22 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* removed MyMoneyAccount::Transaction, all references and tests
* added MyMoneyTransaction::split(const QCString&amp; accountId) and tests
* added MyMoneyFile::attachStorage and MyMoneyFile::detachStorage
* check for presence of storage object prior to use it inside MyMoneyFile
* added MyMoneyFile::payeeList()
* made MyMoneyFile a Singleton object
* removed singleton behaviour from KMyMoneyFile
* filled logic for KMyMoneyView::loadDefaultCategories
* default accounts are now stored in file 'default_accounts.dat'
* 'default_categories.dat' is not used anymore. I kept it for referenc
in the repository
* show error message, if a file could not be loaded in KMyMoneyView::readFile
* made KCategoriesView an observer of the MyMoneyFile object to track
any changes to any account in the 'income' and 'expense' hierarchy

2002-06-17 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* added accountId as parameter to MyMoneyFile::transactionCount()
* added test cases for it
* removeing an account did not remove it from it's parent accountList.

2002-06-16 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed the singleton pattern
* Added double constructor for MyMoneyMoney and testcase
* Modified MyMoneyStorageBin to use the new constructor when reading old
files
* use macro supplied by CPPUNIT to detect presence of CPPUNIT &gt;= 1.8.0
* don't try to compile test code, if CPPUNIT is not present
* added chapter about unit testing and example code to the PHB
* minor spelling modifications in the PHB
* Added payeeList() method
* Fixed ambiguity error in KBanksView
* Return value for KMyMoneyView::isopen()

2002-06-14 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Improved the 'file open' logic.
* Fixed the infinite loop on exit. Can't believe we didn't spot this
before!
* Use MyMoneyFile::totalBalance() instead of MyMoneyAccount::balance() when
displaying accounts in the accounts &amp; categories view.

2002-06-13 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Right click on accounts view now works.
* Editing/deleting an institution works.
* Types and balances are shown again in the accounts view.
* Categories view updated.
* KCategoryListItem removed.
* The accounts view type is now configurable through the settings dialog
in the page 'Accounts View'. (Institution or Accounts view).

2002-06-11 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Categories view now works as before.
* Made the new account dialog also serve as a new category dialog.
* The accounts view defaults to the original institution view.
* Removed some more old classes, (KNewCategoryDlg, KFileInfoDlg).

2002-06-10 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Converted KMyMoneyView to use a singleton pattern for access
to the MyMoneyFile.
* Cleaned up the menu entries a bit.
* You can now view the categories, (its just the income and expense accounts).

2002-06-09 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Fixed the date input widget.
* New account dialog is now fully functional.
* Fixed the accounts view window. You can now use the old banks view
or the newer accounts view. Currently this can only be done
manually, by adding the following line to the "List Options"
group in ~/.kde/share/config/kmymoney2rc:
NormalAccountsView=true
Set it to true for the banks view, false for the accounts view.

2002-06-07 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Adding institution in the account dialog now works.
* The edit account dialog now loads nearly all the 'fields'
properly.
* Integrated the date input widget changes from the 0-4-kde3-branch.

2002-06-06 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Selecting the four root accounts in the parent account widget
now works.

2002-06-05 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Removed the action enabling logic.
* You can now read a file into KMyMoney2 again.
* Using MyMoneyStorageDump we can now dump the file by
choosing the menu option File|Info.
* You can now add institutions, (but not see them).
* You can also add accounts, (in a limited fashion).
* Added the ability to set a parent account when creating new
accounts.

2002-06-04 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Made some of the accounts view work with the new engine.

2002-05-31 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added initial MyMoneyPayee support

2002-05-29 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added setAccountName() method to engine
* Preparations for payee handling (not yet finished)

2002-05-28 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Moved testcase implementation into separate source files

2002-05-27 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added transactionList(const QCString&amp; acc) to engine interface

2002-05-26 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added unary minus operator to MyMoneyMoney

2002-05-24 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Converted all id references from QString to QCString
* In order to activate the memory leak checks, one must
specify the compiler option -D_CHECK_MEMORY in KDevelop

2002-05-22 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added the string 'Id' to all engine methods returning or
setting an id
* Made accountType settable

2002-05-19 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* added notification logic to the engine

2002-05-17 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* merged new engine branch
* *** THE PROJECT WILL NOT COMPILE FOR AWHILE ***

2002-04-26 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed compile and link order problem
* Updated developer list in About window

2002-04-26 Arni Ingimundarson &lt;arniing@users.sourceforge.net&gt;
* Got rid of KTempDatePicker and used the standar kde
KDatePicker. A popup problem still remains.

2002-03-27 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Fixed bug #535840. 'Annoying window refresh'.

2002-03-22 Arni Ingimundarson &lt;arniing@users.sourceforge.net&gt;
* Added support for keyboard input in kMyMoneyDateInput
+/- now works and PgDn popsup the DatePickerWidget.

2002-03-20 Arni Ingimundarson &lt;arniing@users.sourceforge.net&gt;
* Fixed Datepicker inconsistency (bug 502805)

2002-03-19 Arni Ingimundarson &lt;arniing@users.sourceforge.net&gt;
* Fixed file dialog choose-&gt;cancel bug (490427)
in kbackupdlg.cpp, kexportdlg.cpp and kimportdlg.cpp

2002-03-14 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Fixed disappearing categories bug.

2002-03-14 Arni Ingimundarson &lt;arniing@users.sourceforge.net&gt;
* Fixed a sorting bug in KReconcileDlg

2002-03-10 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Fix for autoconf 2.5 which KDE 3 now requires.

2002-03-07 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Commited Javi C's new icons. &lt;javi_c@users.sourceforge.net&gt;

2002-02-17 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* New &lt;template&gt; dialogs for scheduled transactions.
* Changed the scheduled view to use a tool button.
* Improved functionality in the dialogs.
* Changed scheduled interface in mymoney api.

2002-02-14 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Made the source fully KDE3 compatable.

2002-02-13 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Updated KDE 3 support to current KDE 3 cvs.

2002-02-12 Kevin Tambascio &lt;ktambascio@users.sourceforge.net&gt;
* more investment work.

2002-02-12 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* fixed wrong examples in the project handbook

2002-02-11 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* added section about release management to the project handbook

2002-02-09 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Improved handling of the views.

2002-02-09 Kevin Tambascio &lt;ktambascio@users.sourceforge.net&gt;
* More changes for stock transactions.
* added dialog to update stock prices.

2002-02-07 Kevin Tambascio &lt;ktambascio@users.sourceforge.net&gt;
* More changes for stock transactions.

2002-02-07 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Some views now emit activated signals.
* KMyMoneyView prepped for new toolbar/menu entry enabling code
dependant upon what the user is viewing.

2002-02-06 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Fixed deletion of transaction bug where it diddn't refrsh list properly.

2002-02-05 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added framework for automated regression testing to mymoney modules

2002-02-04 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added current date mark in transaction view
* Modified exit handling
* Setup m_viewType first and then emit signals in
KTransactionView::viewTypeActivated
* Removed locale error messages

2002-02-04 Kevin Tambascio &lt;ktambascio@users.sourceforge.net&gt;
* More stock transaction work.

2002-02-04 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated project file to include all new files
* Fixed a missing semicolon

2002-02-03 Kevin Tambascio &lt;ktambascio@users.sourceforge.net&gt;
* Added MyMoneyInvestTransaction class to hold transaction information for investments.
* Made some more progress with the stock transactions.
* Fixed problem in the transaction view, not showing any transactions (Fix from Michael's email).

2002-01-31 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Added scheduled transaction addition dialog.
* Fixed constructor of kmymoneycombo. Didn't fix the rest though.

2002-01-30 Kevin Tambascio &lt;ktambascio@users.sourceforge.net&gt;
* Fixed problem with showing the kinvestmentview and kstocktransactionview class as widgets, instead of
dialogs.

2002-01-30 Kevin Tambascio &lt;ktambascio@users.sourceforge.net&gt;
* Added a new investment view, started to add code to show investment view when an investment account is
selected.

2002-01-29 Kevin Tambascio &lt;ktambascio@users.sourceforge.net&gt;
* Added a dialog for entering information about a new stock or mutual fund.

2002-01-29 Kevin Tambascio &lt;ktambascio@users.sourceforge.net&gt;
* Added MyMoneyEquity and MyMoneyEquityList classes, to store information about stocks, mutul funds.
* Added MyMoneyUtil.cpp and .h to put global/static utility functions, typedefs.
* Added STL support for the MyMoneyUtil class. Just include "mymoneyutils.h" to access STL.

2002-01-29 Kevin Tambascio &lt;ktambascio@users.sourceforge.net&gt;
* Added stock transaction view files, no new functionality yet.

2002-01-28 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Added section in the PHB about files to be checked in and
files that should not be checked in

2002-01-27 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Added support for specifying the start date in viewing transactions.
* Added the view for recurring transactions.

2002-01-26 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed generation of PHB
* Fixed sizing problems with kMyMoneyTable widget
* Allow empty category (default)

2002-01-25 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Removed the pics and text from the payees/categories view.

2002-01-25 Kevin Tambascio &lt;ktambascio@users.sourceforge.net&gt;
* Added account types in mymoneyaccount class.
* Forgot to add loans and mortgage category in the dialog.
* Made a few other changes to display the proper account type in the bankview listview.

2002-01-25 Kevin Tambascio &lt;ktambascio@users.sourceforge.net&gt;
* Modified new account dialog for some of the new account types.

2002-01-24 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Added functionality to the categories view.
* Added an intial payees page.
* Added functionality to the payees view.
* Fixed bug on payee view initialisation.

2002-01-24 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* added tab/back-tab handling to kMyMoneyEdit and kMyMoneyLineEdit
* added keyboard handling to KSplitTransactionDlg
* added debug output if QIF import date conversion fails
* added new method setCurrentItem(QString &amp;) to kMyMoneyCombo and
use it in KTransactionView::setInputData()
* check for valid category is now done in kMyMoneyComboBox. One cannot
select the special entries like '--- Income ---' anymore.
* removed check for those special entries in KTransactionView

2002-01-22 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Created a views directory for the different views of mymoney and
added the relevant classes to that dir.
* Initial view files added.

2002-01-18 Kevin Tambascio &lt;ktambascio@users.sourceforge.net&gt;
* added newline to mymoneytransactionbase.h, because it was causing compiler
warnings all over the place.

2002-01-17 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* added more functionality to the split transaction handling
which is not yet done.

2002-01-16 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Removed some old 'recurring transactions' code.
* Removed the unneeded KMainView class and converted KMyMoneyView
to use the KBanksView and KTransactionView classes directly.
* Moved all the dialogs into the dialogs subdir.

2002-01-10 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* added setDirty() to MyMoneyTransaction, MyMoneyAccount and MyMoneyBank
* split MyMoneyTransaction into MyMoneyTransactionBase, a virtual base
class required for splits
* added MyMoneySplitTransaction framework
* update template files to include new developers
* modified library link orderto "dialog, widgets, mymoney" so that
all references can be resolved
* override KMyMoneyTable::paintFocus to avoid display of focus
* Fixed bug #502804
* Fixed bug #502803
* added color selector for color of grid in register view

2002-01-06 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* bumped version number to 0.5.0
* added PHB docbook files to the project (makefile.am is still a problem)
* clearing the text in KTransactionView::clear() before doing a
transactionsTable-&gt;setNumRows(0) is unnecessary. I removed it.
* cleaned up unused code in ktransactionview.cpp
* restructured filling of transactionsTable to speed up the operation.
I still don't know why this is so much faster now.
* fixed procedures to create new releases in PHB

2001-12-30 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed bug #490440 - extra edit boxes in register
* display all text in register with centered vertical alignment
* Fixed bug #497902 - memo text is not displayed
* Fixed bug #497903 - context menu does not open on right click

2001-12-29 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated project handbook (fixed tag naming conventions)

2001-12-28 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Updated project handbook
* Turned off editable flag of m_method in ktransactionview.

2001-12-27 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Fixed bug #490434 - Account operations (2).

2001-12-26 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Fixed bug #490019 - Toolbar icon out of sync.
* Fixed bug #490424 - New institution dialog tab order.

2001-12-23 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Added ability to compile for KDE 3 or KDE 2.

2001-12-21 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Corrected transaction view to display correctly when resizing horizontaly
* Use visibleWidth() when resizing the transaction view. This will
automatically include/exclude the vertical scroll bar into/from
the calculation of the field widths.
* suppress popup of horizontal scroll bar in transaction view,
bank view, reconciliation and categories dialog
* fixed resizing the reconciliation dialog (almost)

2001-12-20 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed bug #433660 Big numbers do not fit into the fields

2001-12-19 Kevin Tambascio &lt;ktambascio@yahoo.com&gt;
* Fixed Bug #494908 Date/Calender control is too small

2001-12-19 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* added logic to suppress display of splash screen through config setting

2001-12-18 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* removed/resolved compiler warnings

2001-12-17 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Fixed bug no 490015.
* Updated admin directory to kdelibs-2.2.2 whilst preserving
the autoconf-2.50 changes so we can use --enable-objprelink.

2001-12-17 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* fixed Bug #490442
* removed currency text from reconcile dialog elements
* fixed memory leaks in KReconcileListItem
* replaced formatNumber with formatMoney
* fixed KMyMoneyEdit::getMoneyValue() to support localized money formats
with full rounding to selected fraction size.

2001-12-16 Kevin Tambascio &lt;ktambascio@yahoo.com&gt;
* Fixed Bug #490016 File filter in open dialog

2001-12-16 Felix Rodriguez &lt;frodriguez@mail.wesleyan.edu&gt;
* fixed memory leaks in the reconcile and endingbalance dialog boxes

2001-12-14 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* fixed another memory leak bug having to do with the Transaction KPopupMenu
* revised Felix' fixes to avoid creating countless objects

2001-12-14 Felix Rodriguez &lt;frodriguez@mail.wesleyan.edu&gt;
* fixed another memory leak bug having to do with the Account KPopupMenu

2001-12-13 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* fixed debug output of QIF import (result message of date conversion)
* commented debug messages in QIF import
* read and write memo field during QIF import/export
* shrunk export dialog - this was way tooo big for my notebook
* update transaction count during QIF export every ten iterations
* added german date formats to QIF export (monetaryDecimalSymbol to be done)
* close QIF export dialog when done
* allow QIF export with only categories or account data selected
* MyMoneyMoney supports local money formats
* KBanksView honors locale settings
* removed unused variables in kcsvprogressdlg.cpp

2001-12-13 Kevin Tambascio &lt;ktambascio@yahoo.com&gt;
* Fixed export dialogs to always add the appropriate file extension.

2001-12-12 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* updated project handbook files in developer-doc/phb
* added selection of monetaryDecimalSymbol in QIF import
* collect memo during QIF import

2001-12-11 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* fixed display of transaction view for large amount of transactions
* leave more room for scroll bar on the right side of transaction view
to avoid horizontal scroll bar
* fixed loading of payment method combo-box in transaction view
* added german date handling to QIF import

2001-12-10 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* modified acinclude.m4.in to work with autoconf 2.50

2001-12-09 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Changed the csv export format so each record is on one line.
* Created the csv file import method in mymoneyaccount.
* Added a static helper method to mymoneytransaction.

2001-12-09 Kevin Tambascio &lt;ktambascio@yahoo.com&gt;
* Fixed the save as dialog to append the .kmy file extension more intelligently.

2001-12-09 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Removed the transaction number from csv export.
* Moved csv export into mymoneyaccount.
* Updated the kcsvprogressdlg to use new mymoneyaccount method.
* Added date ranges to csv export/import dialog.

2001-12-08 Kevin Tambascio &lt;ktambascio@yahoo.com&gt;
* When you double-click on an account in the bank view, you will see the transaction list.

2001-12-08 Felix Rodriguez &lt;frodriguez@mail.wesleyan.edu&gt;
* Fixed Pixmap Crash on Exit error.

2001-12-08 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Update flags when refreshing banksview
* Fixed date of previous entry

2001-12-08 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* force update of imported records on screen every ten iterations

2001-11-26 Kevin Tambascio &lt;ktambascio@yahoo.com&gt;
* New files will always have .kmy extension to them.

2001-11-24 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Incorporated Coding Rules into Project Handbook
* Added CVS examples to Project Handbook

2001-11-21 Felix Rodriguez &lt;frodriguez@mail.wesleyan.edu&gt;
* Commented out delete of widgets in KTransactionView

2001-09-30 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Update bank view after import
* Added apostrophe selection to QIF import

2001-09-27 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Changed the date input class to display either left or right.
* Fixed a couple of issues of when to show action(s).

2001-09-27 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Call resize() of base class in KCategoriesDlg, KBanksView, KReconcileDlg
* Prevent hiding of fields 'cleared balance' and 'difference' when shrinking
the reconcile dialog

2001-09-26 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed another uninitialized variable bug in KExportDlg::readConfig()
This sets the program default of QIF exports to %d/%m/%yyyy

2001-09-26 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Added pictures where-ever I think they should be. Along with the icons they
are just placeholders ready to be drawn.

2001-09-26 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Added icons where-ever I think they should be. Have a look and see if they
are placed appropriately. The icons don't actually depict anything, that
still needs to be done!
* Improved icon handling in the right menu(s) by using the KIconLoader class.
We have now just the 22x22 icons and KIconLoader does all the hardwork of
resizing to 16x16.
* Added default hi-color versions of the application icons.

2001-09-25 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Bumped version up to 0.3.8.
* Removed some old icons and added some new ones. Please be aware that
I am a useless artist!

2001-09-17 Thomas Baumgart &lt;ipwizard@users.sourceforge.net&gt;
* Fixed two uninitialized variable bugs

2001-08-29 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Made the category list be alphabetical in the two different groups.
* KTransactionView now loads an alphabetical category list.
* Removed an unnecessary class. (KTransactionTableItem).
* Fixed error in file modification logic error in app class.

2001-08-27 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Added some better default categories and slightly improved reading process
to deal with empty minor category lists.

2001-08-23 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Slight improvement to the qif date formatter code by utilising qt.
* Check whether import file exists.
* Import &amp; Export dialog logic improved, e.g buttons only available when text
is input.
* Changed mymoneyedit to use the better kfloatvalidator to accept localised numbers.
* Wrapped all user visible text in i18n.
* Removed all sprintfs to aid internationalisation.
* Removed all latin1() calls to aid in internationaliastion. latin1 calls
now only exist in qDebug calls which should be changed to kdDebug calls anyway.
* Updated kmymoney2.pot file.

2001-08-20 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Added the new *unstable* qif date formatter code to the qif read and write
methods.

2001-08-19 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Export QIF now has a progress bar.
* KExportDlg now conforms to coding standards and uses i18n where
appropriate.
* Updated version to 0.3.7.
* Small update to coding standards mentioning i18n.
* Import QIF now has a progress bar.
* KImportDlg now conforms to coding standards and uses i18n where
appropriate.

2001-08-18 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Moved the QIF read &amp; write methods into MyMoneyAccount.

2001-08-17 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Moved import and export dialogs into dialogs dir.
* Updated admin dir for kde2.2.

2001-07-29 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Improved the CSV import/export process.

2001-07-28 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Quick fix to the kmymoneyview class for Mandrake 7.2.

2001-07-27 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Fixed bug i introduced. I tried to stop the seg faults on exit by using
references, but forgot that the account needs to be accessed.

2001-07-27 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Added default text to prompt the user in the register view.
* Fixed the seg fault i introduced into the register view.
* Added an option to the settings dialog to show the textual prompt.
* Removed balance display when viewing the searched transactions.

2001-07-26 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Added a qstring operator to the mymoney class.
* Added support for editing transactions when in search mode.

2001-07-23 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* MyMoneyFile now doesn't add empty categories &amp; payees. If this is going
to become a standard financial library these sort of checks are going to
have to be made everywhere.
* Fixed two bugs in the reconcile dialog.

2001-07-23 Felix Rodriguez &lt;frodriguez@mail.wesleyan.edu&gt;
* Fixed data entry bug

2001-07-22 Felix Rodriguez &lt;frodriguez@mail.wesleyan.edu&gt;
* Placed Category and Memo in the same cell.

2001-07-22 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Added a radio button to KSettingsDlg and made it conform to
the new coding standards.

2001-07-22 Felix Rodriguez &lt;frodriguez@mail.wesleyan.edu&gt;
* Made Payee Combobox editable and enabled autocompletion

2001-07-22 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Added an apply button to the settings dialog.
* Added a reset button to the settings dialog.

2001-07-21 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Implemented the colour painting option.
* Fixed the bug when resizing the transaction view.
* Fixed the alignment bug in the transaction view.

2001-07-21 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Changed payee colunm name in reconcile dialog to payee.
* Fixed the KMyMoneyCombo class.
* Fixed bug when updating cleared &amp; unreconciled status.
* KSettingsDlg now checks that the row count is &gt;= 1 &amp;&amp;&lt;= 3.

2001-07-19 Felix Rodriguez &lt;frodriguez@mail.wesleyan.edu&gt;
* Fixed Payee display in Reconcile dialog

2001-07-19 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Added 'autoremember' for the date format in the qif dialogs.
* Changed the transaction view to speed it up.
* Added ability to change the number of rows displayed in the register
views.
* Added ability to turn on or off the grid in the register view.
* Removed some redundant classes.

2001-07-15 Felix Rodriguez &lt;frodriguez@mail.wesleyan.edu&gt;
* I think I fixed Crash on Exit Bug

2001-07-13 Felix Rodriguez &lt;frodriguez@mail.wesleyan.edu&gt;
* Added Date format option for QIF import and export

2001-07-13 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Improved 'autoremember' for most dialogs relevant e.g
KCategoriesDlg remembers the last selected category.
* Updated README.

2001-07-12 Michael Edwardes &lt;mte@users.sourceforge.net&gt;
* Implemented CSV export. Import may be disabled for CSV in the
future (will anybody use it ?).

        