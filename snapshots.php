<?php

$enableSetup = FALSE;
$verbose=0;
$product = "KMyMoney";
$obsPackageRoot="https://build.opensuse.org/package/show/";
$pathRoot="home:rhabacker:branches:windows:mingw";
$obsDownloadRoot="https://download.opensuse.org/repositories/";
$sfDownloadRoot="https://sourceforge.net/projects/kde-windows/files";
$repoRoot="openSUSE_Leap_15.6";
$useSFMirror=getIntFromRequest('usesfmirror', 0);

$refsUsed = array();

function ref($key)
{
    global $refsUsed;

    $refsUsed[$key] = 1;
    return "<a href=\"#n$key\">[$key]</a>";
}

function addRef($key, $text)
{
    global $refsUsed;

    if (isset($refsUsed[$key]))
        return "<p id=\"n$key\">[$key]&nbsp;$text</p>";
    else
        return "";
}

$kmymoneyStableProducts = array(
    array(
        "name" => "KMyMoney 5.1 (stable snapshot) (Aqbanking6, Qt5.15, KF5.103)",
        "obs" => array(
            "path" => array(
                "32" => "windows:mingw:win32",
                "64" => "windows:mingw:win64",
            ),
            "project" => ":snapshots",
            "package" => array(
                "name" => "kmymoney5",
                "version" => "5.1",
            ),
        ),
        "git" => array(
            "urlroot" => "https://invent.kde.org",
            "path" => "office/kmymoney",
            "branch" => "5.1",
        ),
        "sfurl" => "$sfDownloadRoot/kmymoney/snapshots/",
        "anchor" => "kmymoney5-stable-aq6",
    ),
);

# does not build
$kmymoneyUnstableProducts = array(
    array(
        "name" => "KMyMoney 5.1.92 (unstable snapshot) (Aqbanking6, Qt5.15, KF5.103)",
        "obs" => array(
            "path" => array(
                "32" => "windows:mingw:win32",
                "64" => "windows:mingw:win64",
            ),
            "project" => ":staging",
            "package" => array(
                "name" => "kmymoney5",
                "version" => "5.1.92",
            ),
        ),
        "git" => array(
            "urlroot" => "https://invent.kde.org",
            "path" => "office/kmymoney",
            "branch" => "master",
        ),
        "anchor" => "kmymoney5-unstable-aq6",
    ),
);

$alkimiaProducts = array(
    array(
        "name" => "Alkimia (stable snapshot)",
        "obs" => array(
            "path" => array(
                "32" => "windows:mingw:win32",
                "64" => "windows:mingw:win64",
            ),
            "project" => ":snapshots",
            "package" => array(
                "name" => "libalkimia5",
                "filepattern" => "onlinequoteseditor5",
                "version" => "8.1",
            ),
        ),
        "git" => array(
            "urlroot" => "https://invent.kde.org",
            "path" => "office/alkimia",
            "branch" => "8.1",
        ),
        "sfurl" => "$sfDownloadRoot/onlinequoteseditor/snapshots/",
        "anchor" => "alkimia5-stable",
    ),
    array(
        "name" => "Alkimia (unstable snapshot)",
        "obs" => array(
            "path" => array(
                "32" => "windows:mingw:win32",
                "64" => "windows:mingw:win64",
            ),
            "project" => ":staging",
            "package" => array(
                "name" => "libalkimia5",
                "filepattern" => "onlinequoteseditor5",
                "version" => "8.1.92",
            ),
        ),
        "git" => array(
            "urlroot" => "https://invent.kde.org",
            "path" => "office/alkimia",
            "branch" => "master",
        ),
        "sfurl" => "$sfDownloadRoot/onlinequoteseditor/snapshots/",
        "anchor" => "alkimia5-master",
    ),
);

$gdbProducts = array(
    array(
        "name" => "GDB (release) ".ref(5),
        "obs" => array(
            "path" => array(
                "32" => "windows:mingw:win32",
                "64" => "windows:mingw:win64",
            ),
            "project" => "",
            "package" => array(
                "name" => "gdb",
                "filepattern" => "gdb-",
                "version" => "",
            ),
        ),
        "git" => array(
            "url" => "https://sourceware.org/git?p=binutils-gdb.git;a=shortlog;h=refs/tags/gdb-14.2-release",
        ),
        "sfurl" => "$sfDownloadRoot/gdb/14.2/",
        "anchor" => "gdb-release",
    ),
    array(
        "name" => "GDB (stable snapshot) ".ref(5),
        "obs" => array(
            "path" => array(
                "32" => "windows:mingw:win32",
                "64" => "windows:mingw:win64",
            ),
            "project" => ":snapshots",
            "package" => array(
                "name" => "gdb",
                "filepattern" => "gdb-",
                "version" => "",
            ),
        ),
        "git" => array(
            "url" => "https://sourceware.org/git?p=binutils-gdb.git;a=shortlog;h=refs/tags/gdb-14.2-release",
        ),
        "sfurl" => "$sfDownloadRoot/gdb/14.2/",
        "anchor" => "gdb-stable-snapshot",
    ),
);

$previousProducts = array(
    array(
        "name" => "KMyMoney4 (stable) (Aqbanking6, KDE4)",
        "project" => ":kmymoney",
        "package" => "kmymoney",
        "git" => array("url" => "https://invent.kde.org/office/kmymoney/-/commits/4.8/"),
        "anchor" => "kmymoney4-aq6",
    ),
    array(
        "name" => "KMyMoney4 (stable) (Aqbanking5, KDE4)",
        "project" => ":kmymoney-aq5",
        "package" => "kmymoney",
        "git" => array("url" => "https://invent.kde.org/office/kmymoney/-/commits/4.8/"),
        "anchor" => "kmymoney4-aq5",
    ),
    array(
        "name" => "KMyMoney4 (staging branch) (Aqbanking5, KDE4)",
        "project" => ":staging",
        "package" => "kmymoney",
        "git" => array("url" => "https://github.com/rhabacker/kmymoney/commits/4.8-staging"),
        "anchor" => "kmymoney4-aq5-staging",
    ),
);

$groups = array(
    'kmymoney-stable' => array(
        "arch" => array( "32", "64" ),
        'name' => 'Stable products',
        'products' => $kmymoneyStableProducts,
    ),
    'kmymoney-unstable' => array(
        "arch" => array( "32", "64" ),
        'name' => 'Unstable products',
        'products' => $kmymoneyUnstableProducts,
    ),
    'alkimia' => array(
        "arch" => array( "32", "64" ),
        'name' => 'Alkimia products',
        'products' => $alkimiaProducts,
    ),
    'addon' => array(
        "arch" => array( "32", "64", ),
        'name' => 'Addons',
        'products' => $gdbProducts,
    ),
);

$archs = array( "32", "64");

function getStringFromRequest($key, $defaultValue = '')
{
    return isset($_REQUEST[$key]) ? $_REQUEST[$key] : $defaultValue;
}

function getIntFromRequest($key, $defaultValue = 0)
{
    return isset($_REQUEST[$key]) ? intval($_REQUEST[$key]) : $defaultValue;
}

function strEndsWith( $haystack, $needle ) {
    $length = strlen( $needle );
    if( !$length ) {
        return true;
    }
    return substr( $haystack, -$length ) === $needle;
}

function getOBSPackageName($product)
{
    $package = is_array($product['obs']['package']) ? (isset($product['obs']['package']['obsname']) ? $product['obs']['package']['obsname'] : $product['obs']['package']['name']) : $product['obs']['package'];
    return $package;
}


function getPath($arch,$product)
{
    global $pathRoot;
    return isset($product['obs']['path'][$arch]) ? $product['obs']['path'][$arch] : $pathRoot.":win$arch";
}

function getProjectPath($arch,$product)
{
    $package = getOBSPackageName($product);
    return getPath($arch,$product).$product['obs']['project']."/mingw$arch-$package";
}

function getProjectsList($prefix, $suffix)
{
    global $groups,$archs;
    $s = "";
    foreach($groups as $group) {
        foreach($group['products'] as $product) {
            foreach($archs as $arch) {
                $s .= $prefix.getProjectPath($arch,$product).$suffix;
            }
        }
    }
    return $s;
}

function getOBSDownloadUrl($arch,$product)
{
    global $obsDownloadRoot,$repoRoot;
    return $obsDownloadRoot.str_replace(":",":/",getPath($arch,$product).$product['obs']['project'])."/$repoRoot/noarch/";
}

function getOBSPackageUrl($arch,$product)
{
    global $obsPackageRoot;
    $package = getOBSPackageName($product);
    return "$obsPackageRoot".getPath($arch,$product).$product['obs']['project']."/mingw$arch-".$package;
}

function git_history_url($product,$portableLink)
{
    if (isset($product['git']['url']))
        return $product['git']['url'];

    if (!isset($product['git']['urlroot']) || !isset($product['git']['path']))
        return "";
    #echo "<!-- $portableLink -->\n";
    $info = parse_url($portableLink);
    #onlinequoteseditor5-mingw32-8.1%2B20220723%2Bgit.6bc4f81
    $path = urldecode($info['path']);

    #onlinequoteseditor5-mingw32-8.1+B20220723+git.6bc4f81
    preg_match("/mingw\d\d-(\d\.\d)/", $path, $matches1);
    preg_match("/\+git\.(\w+)-/", $path, $matches2);

    #echo "<!-- ".urldecode($portableLink)."\n".print_r($matches, true)."-->\n";
    if(sizeof($matches1) > 0) {
        if (sizeof($matches2) > 0) {
            $branch = $matches1[1];
            $rev = $matches2[1];
            return $product['git']['urlroot']."/".$product['git']['path']."/-/network/$branch?extended_sha1=$rev";
        } else {
            $branch = $matches1[1];
            return $product['git']['urlroot']."/".$product['git']['path']."/-/tree/$branch";
        }
    }
    return "";
}

$pageCache = array();

function fetchFiles($product, $archs)
{
    global $useSFMirror, $verbose, $pageCache;
    $result = array();
    if (!function_exists('curl_init')) {
        return array();
    }

    $package = $product['obs']['package'];
    $baseName = is_array($package) ? (isset($package['filepattern']) ? $package['filepattern'] : $package['name']) : $package;

    foreach($archs as $arch) {
        if ($useSFMirror && isset($product['sfurl'])) {
            $url = $product['sfurl'];
            $obs = 0;
        } else {
            $url = getOBSDownloadUrl($arch, $product);
            $obs = 1;
        }

        if (isset($pageCache[$url])) {
            $page = $pageCache[$url];
        } else {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $page = curl_exec($ch);
            $pageCache[$url] = $page;
            curl_close($ch);
            if ($verbose == 1)
                echo "<small>fetching $url</br></small>";
            if ($verbose > 1)
                echo "<small>$page</br></small>";
        }
        $lines = explode("\n", $page);
        $portableLink = "";
        $setupLink = "";
        $debugPackageLink = "";

        # sf:
        #  href="https://sourceforge.net/projects/kde-windows/files/kmymoney/snapshots/kmymoney5-5.1%2BQT512%2BKF576%2B20220812%2Bgit.5b03a22-src.7z/download"
        # obs:
        #  href="./mingw32-kmymoney-portable-4.8.7f3e468f9-75.3.noarch.rpm"
        #  href="./mingw64-kmymoney-portable-4.8.3cf78f9c1-lp151.76.16.noarch.rpm"
        foreach($lines as $line) {
            if (strstr($line, "<a href") === False)
                continue;
            preg_match('/href=["\']?([^"\'>]+)["\']?/', $line, $match);
            if (sizeof($match)  == 0 || strstr($match[1], $baseName) === False || strstr($match[1], "mingw".$arch) === False
                    || (is_array($package) && isset($package['version']) && strstr($match[1], $package['version']) === False))
                continue;
            if ($obs && strEndsWith($match[1], ".rpm") === False)
                continue;
            $info = parse_url($match[1]);
            $path = $info['path'];
            if (!isset($info['host'])) {
                if ($obs)
                    $pkgurl = $url.$info['path'];
                else
                    continue;
            } else {
                $pkgurl = $info['scheme'].'://'.$info['host'].$info['path'];
            }
            #echo "<!--- $match[1] \n ".print_r($info, True)."\n $path \n $pkgurl -->\n";
            if (strstr($path, "portable") !== FALSE)
                $portableLink = $pkgurl;
            if (strstr($path, "setup") !== FALSE)
                $setupLink = $pkgurl;
            if (strstr($path, "debug") !== FALSE)
                $debugPackageLink = $pkgurl;
        }
        $result[$arch] = array($portableLink, $setupLink, $debugPackageLink, getOBSPackageUrl($arch, $product));
        #echo "<!--" .$product['name']." ".print_r($result, true)."-->\n";
    }
    return $result;
}

function noSnapshotFound($productName, $baseName, $arch, $url)
{
    echo "<ul>"
        . "<li>choose <b>mingw$arch-$baseName-portable-....rpm</b> to get a zip file with a portable installation of $productName</li>"
        . "<li>choose <b>mingw$arch-$baseName-setup-....rpm</b> to get a setup installer for $productName</li>"
        . "<li>Debug symbols can be found in the file <b>mingw$arch-$baseName-debugpackage-....rpm</b>. They need to be unpacked in the directory above the <b>bin</b> folder
        so that gdb can find them.</li>"
        . "</ul>"
        . "<p>After clicking on the button below you will be redirected to the related download page to get a $productName snapshot</p>"
        . "<input type=\"button\" name=\"xxx\" value=\"$arch-bit snapshot\" onclick=\"javascript:window.location='$url';\">"
        ;
    return;
}

function addfootNoteRef($link)
{
    if (stripos($link, ".7z") != False)
        return ref(3)." " .ref(4);
    elseif (stripos($link, ".rpm") != False)
        return ref(1);
    else
        return "";
}

function showProductHeader($enableSetup)
{
    echo "<table>"
        ."<tr>"
        ."<th>Product</th><th>Architecture</th><th>portable package</th>".($enableSetup ? "<th>setup installer</th>" : "")."<th>debug package ".ref(2)."</th><th>OBS project</th><th>git branch history</th>"
        ."</tr>\n";
    ;
}

function showProduct($product, $archs, $header = '2')
{
    global $enableSetup, $repoRoot, $verbose;

    $productName = $product['name'];
    $package = $product['obs']['package'];
    $anchor = $product['anchor'];

    $portableText = "portable package";
    $setupText = "setup installer";
    $debugPackageText = "debug symbol package";

    $files = fetchFiles($product, $archs);
    if (sizeof($files) == 0) {
        echo "<ul><li>No package found for this product</li></ul>";
        return;
    }
    #echo "<!---" . print_r($files, true)."-->\n";
    foreach($files as $arch => $data) {
        $portableLink = $data[0];
        $setupLink = $data[1];
        $debugPackageLink = $data[2];
        $obsPackageUrl = $data[3];
        $obsPackageText = $product['obs']['path'][$arch].$product['obs']['project'].'/'.$product['obs']['package']['name'];
        echo "<tr>"
            ."<td>$productName</td>"
            ."<td>$arch bit</td>"
            ;
        if ($portableLink != '')
            echo "<td><a href=\"$portableLink\">$portableText</a> ".addfootNoteRef($portableLink)."</td>"
            .($enableSetup ? "<td><a href=\"$setupLink\">$setupText</a> ".addfootNoteRef($setupLink)."</td>" : "")
            ."<td><a href=\"$debugPackageLink\">$debugPackageText</a> ".addfootNoteRef($debugPackageLink)."</td>"
            ;
        else
            echo "<td colspan=2>No package available yet.</td>";
        echo "<td><a href=\"$obsPackageUrl\">$obsPackageText</a></td>"
            ."<td><a href=\"".git_history_url($product, $portableLink)."\"/>history</a></td>"
            ;
        echo
            "</tr>\n";
    }
}

function showProductFooter()
{
    echo "</table>\n";
}

header("Content-Type: text/html; charset=utf-8");

echo "<!DOCTYPE html>"
    . "<head>"
    . " <style>"
    . "  table, th, td { border: 1px solid lightgray; padding: 5px; } "
    . " </style>"
    . "</head>"
    . "<html><body style=\"background-color:white;\">"
    . "<h1>Download cross compiled $product snapshots for Windows</h1>"
    . "<p> The files on this page may be '.rpm' or '.7z' files. Use <a href=\"https://www.7-zip.de/\">7-zip</a> to extract the binaries.</p>"
    . "<p>The minimum supported operating system version for these binary packages is Windows 8 (Vista). They are known not to work on <a href=\"https://bugs.kde.org/show_bug.cgi?id=439776\">Windows 7</a>.</p>"
    . "<p>".($useSFMirror ? "<a href=\"snapshots.php?usesfmirror=0\">Use openSUSE mirror</a>" : "<a href=\"snapshots.php?usesfmirror=1\">Use SourceForge mirror</a>")."</p>"
    ;

showProductHeader($enableSetup);

foreach($groups as $group) {
    foreach($group['products'] as $product) {
        showProduct($product, $group['arch'], '3');
    }
}
showProductFooter();

echo
    '<h3>Footnotes</h3>'
    .addRef(1, "The rpm container can be unpacked with <a href=\"https://www.7-zip.de/\">7-zip</a>.")
    .addRef(2, "Debug symbols need to be unpacked in the directory above the <b>bin</b> folder so that gdb can find them.")
    .addRef(3, "The 7z container can be unpacked with <a href=\"https://www.7-zip.de/\">7-zip</a>.")
    .addRef(4, "This file is located on a sourceforge mirror and is synchronized daily from build.opensuse.org.")
    .addRef(5, "gdb cannot be used to see debug symbols with binaries generated with msvc; you need windbg or cdb for this case.")
    ;
echo "
    <h1 id=\"n3\">Local build instructions</h1>"
    . "<p>To build the above mentioned packages on a local openSUSE Linux distribution follow the recipe at <a href=\"https://openbuildservice.org/help/manuals/obs-user-guide/cha.obs.building.html\">Local Building</a>  for OBS.</p>"
    ."<p>Click on a link in the <b>OBS project</b> column and press <b>Checkout Package</b> to get the associated check out url for <b>osc</b>.</p>"
    ;

echo "</body></html>";
