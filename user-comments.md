---
title: Compliments
layout: page
---

# ***Compliments***

## ***These are some of the nice things we hear from users of KMyMoney***

*If you have something nice you'd like to say about KMyMoney, please let us know and send us the compliment (naturally, we love to get them.) We may even publish it here.*

*Constructive criticism is welcome too, and in fact many of these compliments came with some criticism as well.*
*You can read from the archive the complete email message for each quote, good and bad, by following the "read the full comment" link below each quote.*

[**Email us your comments**](mailto:kmymoney-devel@kde.org?subject=Comments%20about%20KMyMoney)

Note: <font color="red"><b>Some of the older <i>Read the full comment</i> links are still broken due to a change in the mail archive.</b></font>
We are sorry about that, but fixing them is a manual and time consuming process and we considered to leave them broken for now.

---

## ***markhm***

The more I get into and experiment with KMyMoney, the more I become impressed with the program. I switched to
it from Quicken and initially, I missed some of the tools I had in my version of Q (I either used Premier or
Home&Business versions). But as I changed my thinking from "how did I do something in Quicken?" to "how can
I do this even better in KMyMoney?" I began to appreciate how much thought went into the design of KMyMoney
and how powerful the program can be.

[**Read the full comment**](https://forum.kde.org/viewtopic.php?f=69&t=173339#p454767)

## ***Jesse***

I can't wait for all the work I see being built to come out! You guys are the best! Just want you all to know!

No link available because this was said on IRC

## ***Aaron Roberts***

I have to say I am really liking kmymoney so far. Lots of work to get it set up initially.
But that is true with most accounting programs. I tried gnucash, and just could not figure that out.
You guys have done a good job.

No link available because this was said on IRC

## ***Ingo Klöcker***
Thanks a lot for KMyMoney! It's awesome.

I'm using it since a bit more than two years now. Before that I used a heavily
tweaked LibreOffice Calc document, but this always got very sluggish near the
end of the year (with just 1000+ rows). So, at the start of 2018, I finally
decided to give a dedicated finance manager a try. I chose KMyMoney and I
didn't regret my decision. ...

[**Read the full comment**](https://mail.kde.org/pipermail/kmymoney/2020-February/003415.html)

## ***jimdavis***
Thanks again, and just so you know KMyMoney has the best CSV import method and functionality that I have run across so far in my effort to do this. Kudos to you and everyone.

[**Read the full comment**](https://forum.kde.org/viewtopic.php?f=69&amp;t=159784&amp;p=414162#p414145)

## ***Michael Carpino***
Thomas &amp; the KMM developement Team,

I wanted to reply to say thank you for all the hard work you put in and
continue to do regarding the release of KMM 5.0. I know there has been
some bumps but I still have faith in the product and your work. I'm
grateful for what you do and appreciate your continued effort with
developing the product. I know many have been doing some backflips
getting the bugs worked out since the release and your efforts should be
applauded.

[**Read the full comment**](https://mail.kde.org/pipermail/kmymoney/2018-March/002859.html)

## ***thomasforrester***
Having used this app for a short time, I can say it's a shoe-in as a replacement for Quicken.
Could use some usability improvements, but its super-impressive as is. I was totally blown
away when it sucked in a very robust xml data file from GnuCash and was perfectly functional
with it from the git-go.

[**Read the full comment**](https://forum.kde.org/viewtopic.php?f=69&amp;t=151173&amp;p=395127#p395127)

## ***Philip Henderson***
I've been using KMyMoney for over a dozen years now and find it the best
program for my needs. Some of the features make KMyMoney a joy to use
and are just so logical (to me anyway). The improvements that have been
made over the last few years have made it even better. A great program.
Thank you to all the developers for a great piece of work.

[**Read the full comment**](https://mail.kde.org/pipermail/kmymoney-devel/2017-June/018619.html)

## ***Frank Osborne***
I've used Quicken. MS Money, Moneydance, and others and KMyMoney is the
best. You guys did an excellent job.

[**Read the full comment**](https://bugs.kde.org/show_bug.cgi?id=376010#c2)

## ***Roger G. Rioux***
Just wanted to say "Thank you" for all your effort in developing this
excellent money management program. I have finally weaned myself off
M$Money after many years of use. The learning curve for KMyMoney was a
little steeper than that of MSMoney, however, it was well worth it. I
am now completely free from Microsoft (MSMoney was the last hold out).
Once again, thank you so very much!

<!--
https://mail.kde.org/pipermail/kmymoney-devel/2015-April/015165.html[**Read the full comment**]
-->
## ***Loic d'Anterroches***
Anyway, wonderful, having nearly 8 years of personal accounting in a
simple to use, robust, easy to use with the keyboard only, easy to
customize reports, easy to print for the wife software is really really
great!

[**Read the full comment**](https://mail.kde.org/pipermail/kmymoney-devel/2015-April/015165.html)

## ***Huw***
I just want to add that I've recently decided to really take a hold of my finances, budget properly etc., and I've found KMyMoney to be excellent so far. The interface is intuitive and helpful, there's no unnecessary jargon, I've found no bugs at all, and the reports are powerful. It's going to be a real boon and I found it just when I needed it.

[**Read the full comment**](https://mail.kde.org/pipermail/kmymoney/2014-July/001783.html)

## ***Les Turner***
I'm now starting to use Kmymoney properly and I've got to say I really like the software. You've done an excellent job. It's intuitive and easy to use. All the useful standard reports that I need are there (and more) and I'm really pleased with what I see.

[**Read the full comment**](https://mail.kde.org/pipermail/kmymoney-devel/2014-April/012436.html)

## ***Charlie Bray***
The interface is intuitive, the workflow is superb, the quality of the program
is top notch and I have to say, this is one of the most polished,
wellconceived, easiest to use yet deep enough to do what you need financial
apps I have ever seen.

[**Read the full comment**](https://mail.kde.org/pipermail/kmymoney-devel/2013-August/010723.html)

## ***steffie (on the KDE bug tracker)***
I have said this before, but am happy to say it again ... as a long-term [but now ex-] Quicken-user, &amp; a GnuCash-user [also now ex-] for ~a year, i think that KMM is just *fabulous*. To everyone who designed, built &amp; actively maintains &amp; enhances it... thank you for doing your marvellous work for the world!!!

[**Read the full comment**](https://bugs.kde.org/show_bug.cgi?id=283858#c32)

## ***currawong (on the KDE forum)***
I have evaluated most of the freely available personal
finance packages available under Linux and found the feature
that I was asking about is included in many of them.

Although there are several very good packages available, they
all seem to lack some crucial element of some type, e.g. some
don't support split transactions (an essential feature in my opinion),
some don't support fiscal years other than calendar years, some don't
have reconciliation features and so on.

KMyMoney I have found to be the most comprehensive (just lacking
this minor feature) so I will continue to use it and make use of
the cheque no. field as a payment mode field.

[**Read the full comment**](http://forum.kde.org/viewtopic.php?f=69&amp;t=95865&amp;p=199618#p199618)

## ***James Adams***
First, let me say that I am EXTREMELY impressed with KMyMoney2. The
functionality, completeness and ease of use are tremendous, and I have
thoroughly enjoyed using it.

[**Read the full comment**](https://sourceforge.net/p/kmymoney2/mailman/message/25198552/)

## ***bbroeksema (on IRC)***
[18:19] \<bbroeksema> ipwizard: Is it also possible to link (or however to call it) transaction from a bankaccount to scheduled transactions in KMymoney?

[18:19] \<bbroeksema> that is, from within the qif file

[18:20] \<ipwizard> Yes, that's what transaction matching is for. See http://kmymoney2.sourceforge.net/online-manual/details.ledgers.match.html

[18:22] \<bbroeksema> aah cool. You guys make accounting almost fun :)

<!--
http://sourceforge.net/mailarchive/message.php?msg_name=6aa230fb0910130848w5f8c5f8do92eb912a9ffc5331%40mail.gmail.com"
target="_new">[**Read the full comment**]
-->
## ***Paul Saletan***
This is one of the most polished open source applications you'll find.
The user interface and navigation is beautifully engineered. The
developers have delivered a complex double entry accounting system but
kept it easy to use. As a long-time Quickbooks user who also tried
GnuCash, I had despaired of finding a Linux personal finance app good
enough to ditch Windows. Now I'm getting my accounts balanced in half
the time and enjoying it. Kudos!
<!--
http://sourceforge.net/mailarchive/message.php?msg_name=6aa230fb0910130848w5f8c5f8do92eb912a9ffc5331%40mail.gmail.com"
target="_new">[**Read the full comment**]
-->

## ***Bruce Herdman***
Just a quick note to say how
much I appreciate the support and hand holding from Alvaro Soliverez
and the development team for Kmymoney. My problems were resolved and I
am impressed by the level of competence and care your team has shown me

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_name=4AB3CD7F.3090103%40centurytel.net)

## ***Martin Beenham***
The support in this forum is outstanding.
The application is outstanding.
I do not regret upgrading to KMyMoney from MSMoney.
I only regret not doing it sooner...

[**Read the full comment**](http://forum.kde.org/viewtopic.php?f=69&amp;t=62451&amp;start=10#p83115)

## ***Graeme Nichols***
KMyMoney is a very polished application. It works just fine and I have
no niggles at all. Much better than Quicken Personal Plus 2009 on Vista
or XP. No bringing the system down and just as many features that I use.

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_name=4A4596FA.6090005%40graemenichols.com)

## ***Dave Le Huray***
I have recently started using KMyMoney (v0.9.3) on my laptop running
PCLinuxOS2009. I am looking to replace Moneydance, for no other reason than
the data is locked in proprietary format. I have looked at KMyMoney before
and not found it suitable but now I must say that I am very pleased and it
looks like I'll be changing over very shortly.

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_name=dcb2dbe10906140450m30aa99abhdf9a872ede8c045a%40mail.gmail.com)

## ***Dorneles Treméa***
... After testing a dozen of different softwares, which didn't fit my needs, I finally discovered KMyMoney.

It was love at first sight and to make things even better, it
imported my 3 years old gnucash file without miss a single record!

That's a fantastic job guys! KMyMoney is the *only* software that
can actually do that, congratulations!

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_name=gj9rdo$dmc$1%40ger.gmane.org)

## ***David Pardue***
... Thanks for any help/advice with this. I appreciate how helpful and friendly Thomas, Alvaro and the other KMM developers are. You guys make Intuit Tech Support look like slackers!

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_name=662150.61765.qm%40web57001.mail.re3.yahoo.com)

## ***Marko Käning***
Dear developers of KMyMoney, this is just a general thanks to all of you!

I never worked with Quicken on Windows. Years back I used some tool called Dialog24 for HBCI access to Deutsche Bank, but gave up on it for various reasons... I am so happy that I found KMyMoney running on my beloved Linux!!!

&gt;&gt;&gt; This tool currently saves my ass. &lt;&lt;&lt;

:) Honestly!

Without it's features I'd be in deep trouble.

THANKS. THANKS. THANKS.

Wish I could contribute more to it than just translating or bugging you developers with little quirks or design proposals...
But I wouldn't have time for more than this, to tell the truth.

Anyway, once again I want to express my thanks to all of you. I am very happy that you guys (especially Thomas and Alvaro) are so active here and almost instantaneously respond to everything.

I hope to be able to contribute a bit more in the future when ressources and time allow that.

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_name=956300039%40web.de)

## ***Greg Darke***
Firstly, I wish to express my sincere gratitude and thanks to everybody
involved with this great project. I finally have a replacement for the
very last application that required me to keep M$FT software on my hard
drives. I have been using Microsoft Money for more than 12 years and
have a huge file containing every transaction from approx 10 open
accounts and about another 12 accounts that have been closed. M$ Money
has handled this fairly well although various problems have arisen over
the years that have always been solved by purchasing and upgrading to
the next version.

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_name=48A45670.4070000%40gmail.com)

## ***Hugh Whelan***
Let me start by saying I am a huge fan of KMyMoney (I run the CVS version) and the excellence of it's
informative reports is one of the key reasons I much prefer it to other finance programs.

[**Read the full comment**](http://sourceforge.net/tracker/?func=detail&amp;atid=354708&amp;aid=1954556&amp;group_id=4708)

## ***Martin Zaske***
... We have still not transitioned fully to Linux but for this very purpose we have a dual
boot set-up and we boot into Kubuntu just to do our bookkeeping. What
more proof could you want that we really appreciate this software.

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_name=200804082038.17192%40net-bembel.de)

## ***John Dow***
Hi Guys,

Just built the current developer version from CVS and am seriously
impressed. KMyMoney has just leapt ahead of MS Money by a fair
margin, and I can at last ditch the Windows XP VM that's cluttering up
my disk.

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_name=2a0f0ec00803240347m9efbedbl18b7b78d48eb0db9%40mail.gmail.com)

## ***David Pardue***
I would like to take a few minutes to thank you for all the work and
time you have devoted to creating KMyMoney. I recently began using
version 0.8.7 and am very pleased with it; I think KMyMoney will likely
allow me to move away from Quicken and handle my finances entirely in
Linux.&nbsp;&nbsp;...

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_name=47CE49C9.2040809%40earthlink.net)

## ***Craig Lindholm***
While I have played with Linux off and on for the past few years, This is the first time I have found it able to finally replace my windows box.... mostly thanks to you guys on KMyMoney. Quicken was really the thorn in my side. But since I've installed OpenSuse 10.3 about 3-4 weeks ago, I haven't needed to reboot once into Windows (I have however snuck in once or twice using VirtualBox to do my stock charting software)

[ Received in a private e-mail ]

## ***Kyle Rabe***
Hello!First, I'm so happy that you have created KMyMoney. I use it
for all of my personal finances (and have for a couple of years now),
and I'm glad that I don't have to buy the bloated,
commercially-available software and an OS to go with it.&nbsp;&nbsp;...

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_name=68a9c78a0802251735q720ea7b9u9c1ae58999aaacce%40mail.gmail.com)

## ***Sébastien Plc***
I've tried KMM this week-end (originally just out of curiosity...) and I have to admit that the overall ergonomic as well as the strange "accounting is simple" feeling just convinced me to do something amazing (at least for a me): manage my accounts in another way than stacking everything for months and then spending week-ends to try to reconciliate the mess (better late than never!).&nbsp;&nbsp;...

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_name=BLU119-W41B8699675F904E04083AB9F180%40phx.gbl)

## ***Mikhail Yakshin***
First, I'd like to express my kudos and cheers for generally
developing such high-class product. In fact, I've never seen any
personal finance products before and KMyMoney was the first one I've
seen. It was a bliss to explore and in fact, everything I've seen
after KMyMoney (evaluating other products, including commercial ones)
looks worse than it.

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_name=240e377b0801202054i5cebfda3k9aa8ed584b71d337%40mail.gmail.com)

## ***Walter Sams***
... FYI, once I loaded up 0.9-CVS this program, in the words of our teenagers,
really ROCKS.

It works the way I like it too ( except for my little oops with the first
reconcile.)

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_name=200801201510.36400.walter%40samsco.biz)

## ***Greg***
... Your work is wonderful and I am slowly migrating. The absolute
"cats-meow" would be that tool to instantly "beam" all my MS Money data
into KMyMoney, so I could move on. In any case, I am a fan, a
developing user and commend your efforts.

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_name=1200859552.11917.15.camel%40g-MainDesktop1)

## ***Bob Igo***
... I switched from Quicken
to KMyMoney, too, having first tried GNUcash. I really needed to escape
the forced upgrading built into Quicken for simple things like stock
price lookups.

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_name=47617E12.1000300%40Igo.name)

## ***Michael A. Schwarz***
First off, let me say "great work." I've got all of my accounts
balanced to the penny for the first time since I swore of Intuit's
products.

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_name=45991189-6BF5-4DF9-95B0-93A0A7619141%40multitool.net)

## ***Ahmad El-Gazzaz***
Dear KMyMoney Developers,

First I would like to thank you very much for this wonderful software, I
have been using it since I started using linux 2 years ago and my tries to
use any finance software under windows failed. You really help making life
easier.
[... snip ...]
Thank you for this program and keep beating the commercial software!

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_name=84d1efb40711191142kf9fcaecice07016fdec5bce4%40mail.gmail.com)

## ***Old Al' (Dr. Algis Kabaila)***
...With kind regards to all who are working on this fine program that beats the
commercial stuff 5 - 0 !

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_name=200711022008.13775.akabaila%40pcug.org.au)

## ***Peter***
Thanks, I just loaded one of the main accounts, and the closing balance
reconciles with M$ Money.

KMyMoney is a great application.

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_name=13606500.post%40talk.nabble.com)

## ***Mike Moore***
Wow! Boy does it look good. New features in the works like budgets and
investments, better usability... I'm now running the CVS code (and plan
to report any bugs I find).

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_name=200707301131.46664.mm06a%40wowway.com)

## ***David Love***
I use KMYMoney2 daily to run a small business (Yes I know it isn't targeted
at business users - but it's ideal for what I require) and am very satisfied
with it. It is very intuitive and a "breeze" to enter payments and
receipts. The ease in which one is able to make global amendments (e.g.
renaming an account and see it reflected in all receipts/payments in that
account) is astounding. Keep up the development. I cannot see me changing
to any other accounting program.

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_name=gemini.jljyra006f1wg046h.zed%40zed.net.nz)

## ***Chris Hiestand***
I just wanted to say great job on kmymoney. I just started using this
program and it seems very useful. I'll be sure to communicate with you
if I find any bugs or have any suggestions.

I particularly like how it can import different file formats, it happens
to be compatible with my bank who lets me choose from .OFX .QFX
and .CSV.

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_name=1184225545.6140.0.camel%40dimstation)

## ***David Walling***
I really like the Kmymoney software and have been using it in one form
or another for over a year now. It's the easiest to use I've found, and
comes closest to having all the featuers of Micro$oft Money that I used
prior.

It was also the very last piece of software I needed to completely do
away with Windows as a primary OS on my laptop! I already and Cinelerra
for video editing, K3b for burning, gimp for graphics, Inkscape for SVG,
and Open Office for office.

Now, if the budgeting capabilites where just a little better :-D

Thanks again for the hard work you and the rest of the Kmymoney2 devs
have put in to this software.

[ Received in a private e-mail ]

## ***Dennis Gallion***
Please add my thanks and kudos for such a **great** KDE/open source application!
I have finally been able to replace Quicken, the only Windows application I
was still using regularly. You've done a marvelous job with the interface
and ease-of-use. The QIF conversion worked well, with KMyMoney being extra
helpful at cleaning up the data. Overall, just a fantastic job!

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_name=200706271616.51118.dwgallien%40bellsouth.net)

## ***Jan van den Heuvel***
Thanks for such a great program. MS Money was the last program that
kept me booting Windows every now and then. This current version of
KMyMoney means that time is over.

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_name=460402D5.3030401%40maths.lse.ac.uk)

## ***Michael Riegert***
I love this program. The ease of use compared to MS Money or others
allows me to actually use it. Thanks for the great work."

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_name=200705291958.49289.monte48lowes%40yahoo.com)

## ***Eugene Batogov***
"KMyMoney is COOL !!!"

[ Received during an ICQ session ]

## ***Tim Jump***
"Hey there, first off I wanted to compliment you guys on an absolutely
fantastic program. I've been using MS Money for years but have always
wanted to switch to something I could use under Linux (wine has never liked
Money all that much). You guys did it and I will be eternally grateful. No
longer will I have to suffer through using an inferior operating system to
keep track of my finances!"

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_name=ffd897370704141113y4d65e4e1u47eb50b8b357d40%40mail.gmail.com)

## ***Prasenjit Kapat***
"I have been using KMyMoney2 for nearly 8 months now. Surprisingly, this is my
first money mangement sotware. And hey, did I choose the right one? Just
love it."

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_name=200703022007.07978.kapatp%40gmail.com)

## ***David Love***
"I've just started using KMyMoney2 - after trying GnuCash and giving up
in perplexity - and love it!"

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=38307733)

## ***Art Alexion***
"Thanks for all of your help. While kmm is a great program, half the
value is the wonderfully generous support that you give on this
mailto:kmymoney@kde.org">list."

[ Received as private e-mail ]

## ***Bill Streeter***
"I love this program on Linux. It's been my lifesaver in *finally*
making a full transisition over to Linux, and not having to run
wine/quicken."

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=38255481)

## ***Juan Sebastián Echeverry Grisales***
"...i'm moving from GnuCash to KMyMoney and I'm impressed with this app. Keep the good work."

http://sourceforge.net/mailarchive/message.php?msg_id=38142298[**Read the full comment**]

## ***John Iannone***
"Thanks for your time, and thanks for building such a great application.
Not having an application of this nature was previously the only thing
that tied me to the Windows box in my house. Now, it's doing what it's
best at... collecting dust!"

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=38130106)

## ***Tim Hutt***
"Way better than gnucash even if it is unfinished.

Keep it up."

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=38070724)

## ***Kyle Stevens***
"First, I'd like to say thanks for this great application. It is simple
and fun to use. It is my favorite program to use (finance or otherwise)."

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=38037927)

## ***Marius Groenewald***
"I converted from MS Money 95 (Windows) to KMyMoney (Linux). There are
nice features in KMyMoney that is not available in MS Money and vice
versa...

Thanks for a great Financial Package that is freely available. Can't
wait for Graphs to be included in the Reports."

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=37814748)

## ***Algis Kabaila***
"I confess to that one of my favourite hates is accounting and I only do it when I am forced to do it (mostly by my spouse of ... wait for it ...57 years uninterrupted marriage!).

I must say, though, the KMyMoney makes the generally unpleasant task so much easier - and to think that I put up with Quicken in my windows days..."

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=37784243)

## ***Don***
"thanks for all your hard work on this project (along with the
others on the development team!) -- I've been using KMM since last
Spring and have not wishes to change."

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=37782101)

## ***Bret Jordan***
"First off I wanted to tell you how wonderful your product is thus far.
You have done some amazing work...

Thanks for all your hard work. This product has the potential of being
the best tool on the market."

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=37757380)

***Also, from a private e-mail to Thomas Baumgart:*** "I really must applaud the work you guys have done thus far, the fact
that you are also responsive to requests and queries is a major bonus."

## ***Paul A. Sustman***
"Great work on Kmymoney. In many ways it's much more intuitive and easier
to use than Quicken. I'm looking forward to a few more features."

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=37737858)

## ***Mario Fabiano***
"You did a great job; KMyMoney is a very good and useful software."

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=37211974)

## ***Ken Castania***
"Wonderful!It's difficult to put this but . . . I'm glad there's
something between putting info in an OpenOffice spreadsheet and GNUCash.
There are many of us who want to keep and reconcile our checkbooks and
get a few reports on occasion, but don't want to become CPAs to do it.

KMyMoney is much more Quicken-esque, a welcomed addition to the open
source community.

Thanks!"

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=37080950)

## ***Nicholas Tomlin***
"Congratulations!

I've just been dabling with 0.8 and choose to commend you on your excellent
effort, its a damn shame it doesn't do business accounts."

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=36844199)

## ***S. Brunner***
"I very much love kmymoney, it makes entering
bills a joy where it has been a pain before (using some spreadsheet). Thank
you!"

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=33362073)

## ***Lorenzo Villani (Arbiter)***
"I just want to say that i'm really happy with this program.
Good work! :-D "

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=36562959)

## ***Mark A. Slaw***
"First, let me say that I think kmymoney meets almost all of my needs. I
like it! One useful feature, however, would be online banking. If I had
the ability, i could honestly say that the program met any need i could
have."***Note:** Kmymoney does support some online banking features (transaction download) via OFX if your bank supports it. Using AqBanking, it also supports HBCI, YellowNet, and DTAUS.*

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=36325654)

## ***Mark Blakeney***
"I am new to kmymoney (coming from quicken). What a fantastic mix of simplicity with nice aesthetics and functionality! I have also trialled the gtk2 gnucash but am swayed towards kmymoney and am planning to use an asset account for invoicing plus your handy VAT functionality for GST/VAT tracking. Who said kmymoney does not do invoicing!"

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=17124333)

## ***Don Jackson***
"Kudos to the developers and maintainers of KMyMoney2!Where has this
great program been hiding?You need more publicity."

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=17059047)

## ***Nathan Bradshaw***
"Hi KMyMoney guys/gals, let me start with saying thanks for the great app. I decided I had to get my finances in order, installed KMyMoney2 0.8 and it was exactly what I was looking for. Great work :) "

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=16523889)

## ***Marcin Gil***
"...Otherwise [KMyMoney] is fantastic! Easy, user friendly with
lowest learning curve I've observed in financial apps.

Keep up good work! "

[**Read the full comment**](https://sourceforge.net/mailarchive/message.php?msg_id=15762622)

## ***Adam Watkins***
"First off, great application. I have finally found one more thing that reduces my dependence on Windows.(0.8 tipped the balance for me, I am now switching from MS Money). "

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=15294751)

## ***Claudio Henrique Fortes Felix***
"I'd like to thank you for the good work done on Kmymoney. It's interface is clean and nice to use, which is a big plus comparing to some other similar programs."

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=14878676)

## ***David Price***
"First of all brilliant programme, I use to use that other one for Gnome, but this one had export
features, cut and paste, and a much more friendly interface. I have enjoyed making my own custom
reports and having the freedom to use this program with spreadsheets."

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=14607701)

## ***Tim Ziegler***
"Upon upgrading my Linux box two months ago, I switched from using Quicken (via
Win4Lin) to KMyMoney. Because of some nastiness within Quicken I had to
wrestle the date format, but otherwise the import of all my Quicken stuff
went very well.
It took a few times to get used to a different interface than Quicken, but I
now feel it is simpler and better. Reports are easily customized, and I like
the ability to click on a payee and instantly see all related transactions.
Payees (and other stuff) were buried in Quicken, so all the icons on the left
side of the screen in KMyMoney are a real treat.

Thanks for your hard work on a wonderfully useful product!"

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=14479494)

## ***Keithe J Merl, P.E.***
" First off, excellent app. Great job, all the features I want and need. I
 use this on a daily basis and also feel it is much better and easier to
 use than quicken, GnuCash, or any other ledger program."

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=14241207)

## ***Andrew Fischer***
"...I've been using [KMyMoney] for several months for personal and small business
finances, and I really appreciate how powerful yet easy to use it is.
This past weekend I helped my girlfriend setup Microsoft Money, my
first time using the program, and I was stunned how complicated it was
to use even for accessing simple information. It's default page
intimidated the user so much I apologized for suggesting it as an
alternative to a simple spreadsheet she had before. When I showed her
kmymoney she asked why such a program wasn't available on windows.

I'm sure MS Money is a very powerful and feature filled program, but
it made a very poor impression compared to kmymoney for the
inexperienced user. Thanks, all, for your hard work."

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=14424466)

## ***Howard Barbrey***
"I just installed this today and it is GREAT!
I am a fugative from MS Money and have been seeking this program for a while."

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=14264045)

***Also, from a private email response to my request for permission to post his quote:*** "As I get more comfortable with Linux I am finding myself trying new things.
Because I am retired and KMyMoney is very important to my every day living I
am currently running MS MONEY 2004 on my laptop and I have KMyMoney installed
on my desktop system. I am running them in parallel mainly to be sure that
I don't goof something up.
I have been using MS Money for about 3 years and have looked for a Linux
alternative for quite a while. I was reading over the Suse 10.0 instruction
manual that is shipped with the retail version and found a brief comment
their and then I went to google and now here I am.
I am happy with it and learning more about it all of the time."

## ***Bill Suit***
"I am an 8 year veteran user ofthe linux os. What kept me tied to m$ for the
longest time was my use of Quicken and Turbo Tax.

Several years ago I tried several of the other open source and/or commercial
accounting software programs. None of them really caught my fancy, for
whatever reasons.

About a year and a half ago I heard of Kmymoney (kmm) and started using it.
The documentation indicated that I could use OFX to download bank info into
kmm. Though that was the draw for me, kmm also fulfilled all of my simple
bookkeeping needs. BTW, I haven't used Quicken for several years now.

Over the last year and a half the continued improvements, the look, and the
functionality of the program has just made it a real pleasure to use. Your
response to problems is quick. The user community also is a big plus. I've
noticed that if the program does not currently support a particular function
that you're open to considering adding that function.

Thank you all very much for your efforts in making this a truly first class
program. I greatly appreciate it."

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=14294117)

## ***Andres Krapf***
"I just wanted to let you know that I recently
installed kmymoney2 and I think it's great ! Thank you
very much for the obvious effort you put into this
piece of software. Even though I didn't know much
about accounting, it was really simple to use and it
ate my bank's files without a blink (OFX import)."

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=13957107)

## ***David Nelson***
"I've finally switched over my bank accounts from Quicken and Wine, and am now
looking at getting the investment accounts converted.
It's great to have a native KDE replacement for Quicken at last.
Thanks for the great job you're doing."

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=14085288)

## ***Guillermo Cediel***
"I'm using kmymoney2 v0.8.1 (after testing some other similar software, like kbudget, grisbi, jgnash, etc) and I think you have made a great work..."

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=13966011)

## ***Ben Shewmaker***
"I recently made the switch to Linux, and I have to
 thank you for producing such a fantastic financial
 program. I had been using MS Money for the last 3
 years, but once I started using KMyMoney I never
 looked back. This is exactly the type of financial
 software I had been looking for. Keep up the good
 work!"

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=14080824)

## ***Mike Moore***
"I must thank you for the wonderful software you are putting
together!I've been using kmymoney since vesion 0.4 and think it is a
fantastic program that gets better by the day... thanks to your
efforts!"

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=14072099)

## ***Tom Welch -Linspire CTO (Chief Technical Officer)***
" My name is Tom Welch and I am the CTO for Linspire. I just wanted to
 drop you guys a note and say "Way to go and keep it up!"Your project
 has really made some excellent improvements recently. Anyway we can
 help out spreading the word just let me know."

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=13931088)

## ***Brenda Wolf***
"Hi,

Thank you so much for your efforts on developing this program. I use it all the time to keep record of my checks, etc. I could say is such great program that I installed linux in my computer just to be able to use it!!

Thank you!"

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=13921316)

## ***Marlon Nacario***
"I have used Quicken, MS Money and Linux version of Moneydance, I will
 tell how impressed I am with Kmymoney. It is better and stable than any
 of those that I have used in the past. I noticed Kmymoney at its earlier
 stage of development and is very surprised how the software progress
 from a very crude to the best.

 KEEP UP THE GOOD WORK. LONG LIVE OPEN SOURCE and GNU."

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=13860526)

<!--Start next block

## ***John Kent***


"MSMoney isthe final application that I use that has prevented me from switchingcompletely, and KMyMoney appears to be the best alternative (GnuCashis just to awkward to use). "

<a href=
""
target="_new">[**Read the full comment**]


 End block-->
## ***Johannes Leimbach***
"I wanted to tell you I really love Kmymoney. It's as you wished it to be, the
easiest personal money tool out there. Thank you :) "

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=13790961)

## ***Jordi Alfonso***
"Hi!

Congratulations for this good program. I've using it about a few days and
traslating all my oocalc information to it. Good."

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=13793951)

## ***Christophe Prud'homme***
"I have just started to use kmymoney2(under debian/gnu/linux), it took me a
little while to get use to it but now I am quite happy I made the effort and
I like very much your tool.
Thanks a lot!"

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=13760901)

## ***Jose Jorge***
"I switched to kmymoney after 4 years of GNUCASH, the import
tool worked flawlessly, except for some UTF-8 strings (the
gnucash file was in UTF-8).

I switched because on my old hardware (700MHz) GNUCASH opened
the 4 years full file in about 25s, when kmymoney does it in
5s. OK, it's a silly reason...


...Otherwise it's all nice, congratulations."

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=13625527)

## ***Bill Fahrenkrug***
"Just started using KMymoney 0.8 and works perfect for me -
wife doesn't much since it shows her spending patterns in easy
to understand reports ;)

...Again, THANK YOU for this great app. Haven't used my lame
old MS 2000 OS for a year because of developer's like you.
(wife was doing the budget on her MS box with lol MS money -
now I'm in control... yeehaw)"

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=13185188)

## ***Bob Ewart***
"Kmm is looking better and better. Keep up the good work."

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=12928894)

## ***Erik Johansson***
"I recently switched from gnucash to kmymoney and so far I
haven't regretted it. A big thank you to all developers."

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=12840501)

## ***Martijn Dijksterhuis***
"Keep up the good work, its already an impressive piece of
software! "

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=12783846)

## ***Maarten Th. Mulders***
"since a while I have been using KMyMoney and I really like
it."

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_id=12697756)

## ***Michael McIntyre***
"I'll chip in my two cents eventually, once I formulate an
opinion. I'm still in the tedious setting-everything-up stage
of my relationship with your application. It seems well worth
the effort to do all this though, and I haven't had to RTFM to
figure anything out yet, which is a good sign for your
usability. I figure I'll RTFM later and see what I missed."

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_name=200508160116.48163.dmmcintyr%40users.sourceforge.net)

## ***Nicholas Lee***
"KMM is on a good track. Making it very a easy to use,
powerful personal finance package."

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_name=2b6116b305081501383f9bd16c%40mail.gmail.com)

## ***Jeroen Jonk***
"Congratulations for this wonderful new release! I am using
KMyMoney to track my personal expenses almost on a daily basis
and it really does a good job. One of the benefits that
KMyMoney brings for me, is control over my financial situation.
Most banks in The Netherlands are only sending account
statements once a month and keeping track of all expenses and
the balance of the account during a month really made me
overspend.

Now, using categories, I am able to see where I am overspending
compared to last months and I can control things.

The only thing that I am missing dearly is a budgeting
function. That would really help me getting even more control
over where I should increase or reduce spending.

Like so many others, I used commercial software for many years,
but I switched to Linux last December and I have never had
regrets. Although my system is still multi-boot, I only use
Linux. My wife, who is from Turkish origin, still uses
commercial software to have webcam/sound (MSN) chat with her
relatives in Turkey. However, the guys developing Kopete
released a new roadmap in which MSN video is mentioned for the
first time. If that becomes a reality then I can get rid of all
the commercial software.

For me, a good Office suite and a program like KMyMoney really
made the difference. I used alternatives like GNUCash, but I
did not like is.

Thank you for putting so much effort in developing KMyMoney --
I am sure it is appreciated by many users."

[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_name=200508140948.19831%40net-bembel.de)

## ***Adam Treat***
"First, I want to say that you have a pretty decent
applicaition in the making. Kudos."

<!-- "http://sourceforge.net/mailarchive/message.php?msg_id=12639004" -->
[**Read the full comment**](http://sourceforge.net/mailarchive/message.php?msg_name=200508131006.52066.treat%40kde.org)

