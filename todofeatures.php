<?php
#
# @author Ralf Habacker <ralf.habacker@freenet.de>
#
# redirect to bugs.kde.org and shows a list of open feature requests
#
# syntax:
#   todofeatures.php                       - shows all open feature requests
#   todofeatures.php?[<product>/]<version> - shows open feature requests for a specific (optional) product and version
#
# for details see lib.inc:bugListFromQuery()
#
	include(dirname(__FILE__)."/lib.inc");
	$query = isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : "";
	$url = buglistFromQuery("todofeatures/$query");
	if ($url)
		Header("Location: $url");
?>
