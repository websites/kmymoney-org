---
title: 'KMyMoney 4.8.4 released'
date: 2019-08-13 00:00:00 
layout: post
---

KMyMoney version 4.8.4 for Windows is now available. This version contains several <a href="https://kmymoney.org/resolvedbugs.php?4.8.4">bug fixes</a> and a few implemented <a href="https://kmymoney.org/resolvedfeatures.php?4.8.4">feature requests</a>.

For a full list of the changes please check out the <a href="https://cgit.kde.org/kmymoney.git/log/?h=4.8.4">changelog</a>.

Binary packages for 32 and 64 bit Windows are available from the KDE <a class="piwik_link" href="http://download.kde.org/stable/kmymoney/4.8.4/">download mirror network</a>

The KMyMoney Development Team
