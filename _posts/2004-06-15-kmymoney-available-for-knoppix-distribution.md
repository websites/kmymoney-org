---
title: 'KMyMoney available for Knoppix distribution'
date: '2004-06-15 17:25:48:00 +0000'
layout: post
---
<div class="markdown_content"><p>The authors of klik (http://klik.berlios.de/) recently announced the availability of an installer for KMyMoney under the Knoppix distribution. For more details visit their KMyMoney page at <a href="http://klik.berlios.de/?view=kmymoney." rel="nofollow">http://klik.berlios.de/?view=kmymoney.</a></p></div>

_(Originally posted on [Sourceforge.net](https://sourceforge.net/p/kmymoney2/news/2004/06/kmymoney-available-for-knoppix-distribution/) by Michael Edwardes)_

            