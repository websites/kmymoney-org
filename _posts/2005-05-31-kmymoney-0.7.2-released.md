---
title: 'KMyMoney 0.7.2 released'
date: '2005-05-31 19:45:43:00 +0000'
layout: post
---
<div class="markdown_content"><p>The development team has released an updated version of the current development branch. This release contains many fixes. Please see the ChangeLog at <a href="http://sourceforge.net/project/shownotes.php?release_id=331387">http://sourceforge.net/project/shownotes.php?release_id=331387</a> for details and download the tar-ball from <a href="http://prdownloads.sourceforge.net/kmymoney2/kmymoney2-0.7.2.tar.bz2?download">http://prdownloads.sourceforge.net/kmymoney2/kmymoney2-0.7.2.tar.bz2?download</a> .</p></div>

_(Originally posted on [Sourceforge.net](https://sourceforge.net/p/kmymoney2/news/2005/05/kmymoney-072-released/) by Michael Edwardes)_

            