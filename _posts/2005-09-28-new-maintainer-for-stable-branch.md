---
title: 'New maintainer for stable branch'
date: '2005-09-28 19:45:41:00 +0000'
layout: post
---
<div class="markdown_content"><p>You might have noticed, that the recent checkins on the 0.8 stable branch were performed solely by Tony Bloomfield. Tony is the author of KMyMoney's GnuCash importer and recently joined the KMyMoney maintenance team and took over the work on the 0.8 stable branch.</p>
<p>Tony is working heavily to catch up recent bugfix changes on CVS HEAD to be ready for a first update release of 0.8.</p>
<p>Thanks again to Tony for his support on this important task.</p></div>

_(Originally posted on [Sourceforge.net](https://sourceforge.net/p/kmymoney2/news/2005/09/new-maintainer-for-stable-branch/) by Michael Edwardes)_

            