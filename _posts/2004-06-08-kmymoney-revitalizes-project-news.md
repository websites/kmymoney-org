---
title: 'KMyMoney revitalizes project news'
date: '2004-06-08 05:27:19:00 +0000'
layout: post
---
<div class="markdown_content"><p>The KMyMoney project team is in the process to make use of the SourceForge news system again. Currently, we are working on the automated synchronization of articles on the news platform and our project web-site.</p>
<p>Therefore, you will probably see some test news entries here. They will be removed once we're done.</p></div>

_(Originally posted on [Sourceforge.net](https://sourceforge.net/p/kmymoney2/news/2004/06/kmymoney-revitalizes-project-news/) by Michael Edwardes)_

            