---
title: 'KMyMoney 5.1.2 released'
date: 2021-06-23 00:00:00
layout: post
---
<a href="https://download.kde.org/stable/kmymoney/5.1.2/src/kmymoney-5.1.2.tar.xz.mirrorlist">KMyMoney version 5.1.2</a> is now available.

<p>The KMyMoney development team is proud to present the next maintenance release of its open source Personal Finance Manager.</p>

<p>Here is the list of the included bugfixes grouped by severity:</p>
<h3>major</h3>
<p>
  <ul>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=414675">414675</a> Budget not using Fiscal Year</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=436647">436647</a> Overwriting a gpg-encrypted file results in a zero byte file</li>
  </ul>
</p>
<h3>crash</h3>
<p>
  <ul>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=430176">430176</a> Crash when extending selection of filtered transactions beyond top of list</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=434605">434605</a> crash when importing csv with missing category</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=438069">438069</a> Crash when selecting views in newer Qt versions</li>
  </ul>
</p>
<h3>normal</h3>
<p>
  <ul>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=350904">350904</a> Net worth currency not updated when changing the base currency</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=388793">388793</a> No price label on editing securities</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=396016">396016</a> KMYMoney GPG option is grayed out even though GPG installed</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=396313">396313</a> rounding errors in currency conversion of investment transactions</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=399230">399230</a> KMyMoney uses incorrect path to detect presence of GPG secret keyring on Windows</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=403196">403196</a> Displaying the ledger for a category shows the wrong account</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=422561">422561</a> A half-empty PRICEPAIR causes app to crash when attempting to update prices</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=430813">430813</a> Danish account templates won't load</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=431248">431248</a> Payee Default Category not saved unless manually selected</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=431294">431294</a> Price editor: Enter on manual price has no effect</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=431482">431482</a> 1 Online Quotes does not work and can not be corrected</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=433694">433694</a> reconciliation removes the history</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=433855">433855</a> Settings for "Hide reconciled transactions" are not synced</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=434226">434226</a> Default regex to extract currency rates does not support results in exponential form</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=434573">434573</a> built-in calculator widget can extend beyond main window</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=435488">435488</a> CSV  importer does not support ISO date format</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=435512">435512</a> When matching an assigned and unassigned transaction, KMM may leave the resulting category empty</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=435752">435752</a> Selecting icon sets is not supported on Windows</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=435837">435837</a> messages from the aqbanking plugin are swallowed</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=435856">435856</a> ofx import ignores banking transactions in invenstment account since libofx upgrade to 0.10</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=436766">436766</a> Selection of security accounts does not work for some investment reports</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=437332">437332</a> We should default to yahoo for any new securities</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=437452">437452</a> QR-TAN support is not working properly</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=437810">437810</a> CSV importer should trim the column contents before importing</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=438328">438328</a> Payee name in schedule is not updated in all accounts</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=438529">438529</a> App won't fully quit on macOS</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=438973">438973</a> Cannot right click and Go To a counterparty split if it is a category</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=439006">439006</a> New account wizard always defaults to account type "Checkings"</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=426414">426414</a> Stable kmymoney package from binary factory does not provide gui translations</li>
  </ul>
</p>
<h3>minor</h3>
<p>
  <ul>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=437398">437398</a> typo in Reconciliation Wizard text</li>
  </ul>
</p>
<h3>wishlist</h3>
<p>
  <ul>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=186616">186616</a> Entering negative amounts in Split window</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=397918">397918</a> Sort the favorite reports on the home screen in alphabetical order</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=405383">405383</a> Add python3 support for weboob plugin</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=433662">433662</a> No visual representation of the reconciliation history</li>
  </ul>
</p>
<h3>translations</h3>
<p>
The Slovenian translation team provided an initial Slovenian translation which made it into the release.
</p>
<p>A complete description of all changes can be found in the <a href="/changelogs/ChangeLog-5.1.2.txt">changelog</a>.</p>

The KMyMoney Development Team
