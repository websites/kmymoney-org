---
title: '        KMyMoney 4.6.6 released
      '
date: 2014-06-20 00:00:00 
layout: post
---

<p><a class="" href="http://download.kde.org/stable/kmymoney/4.6.6/src/kmymoney-4.6.6.tar.xz.mirrorlist">KMyMoney version 4.6.6</a> is now available. A build problem was found in the previous version 4.6.5 and a new version was issued to fix it.</p><p>sha254sum of the file is 2b9e70a157ce270ab337d981145b3a79aa4533c7daadcca426299dfcd0a9d06d.</p><p>The KMyMoney Development Team</p>