---
title: 'SuSE RPMs available'
date: '2005-07-16 14:24:56:00 +0000'
layout: post
---
<div class="markdown_content"><p>RPMs for SuSE 9.1 i386 and SuSE 9.3 AMD64 have been released to the file release system. Please be aware, that they do not contain the OFX plugin. They will be released as separate RPM.</p></div>

_(Originally posted on [Sourceforge.net](https://sourceforge.net/p/kmymoney2/news/2005/07/suse-rpms-available/) by Michael Edwardes)_

            