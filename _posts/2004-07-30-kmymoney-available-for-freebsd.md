---
title: 'KMyMoney available for FreeBSD'
date: '2004-07-30 16:49:55:00 +0000'
layout: post
---
<div class="markdown_content"><p>Many thanks to Alexander Novitsky who provided a port of the recent stable KMyMoney version for FreeBSD. According to Alex, the package can be found under the name finance/kmymoney2 in the latest CVS of the FreeBSD ports tree, or on the BSD freshports site: <a href="http://www.freshports.org/finance/kmymoney2/" rel="nofollow">http://www.freshports.org/finance/kmymoney2/</a></p></div>

_(Originally posted on [Sourceforge.net](https://sourceforge.net/p/kmymoney2/news/2004/07/kmymoney-available-for-freebsd/) by Michael Edwardes)_

            