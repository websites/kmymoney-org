---
title: 'New translations'
date: '2004-11-22 22:51:11:00 +0000'
layout: post
---
<div class="markdown_content"><p>Thanks to the translation effort of Marcelino Villarino, the CVS rel-0-6 branch now contains Galician and Spanish language support. They will be part of the next maintenance release (0.6.5). A date for this release is not yet scheduled.</p></div>

_(Originally posted on [Sourceforge.net](https://sourceforge.net/p/kmymoney2/news/2004/11/new-translations/) by Michael Edwardes)_

            