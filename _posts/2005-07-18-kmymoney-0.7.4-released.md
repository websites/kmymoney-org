---
title: 'KMyMoney 0.7.4 released'
date: '2005-07-18 18:38:06:00 +0000'
layout: post
---
<div class="markdown_content"><p>The development team has released KMyMoney 0.7.4, an updated version of the current development branch. The highlights of this release besides the usual lot of bugfixes are: Improved QIF import and the online help/manual with a lot of screenshots. Also this release should be able to make it to the FreeBSD port system.</p></div>

_(Originally posted on [Sourceforge.net](https://sourceforge.net/p/kmymoney2/news/2005/07/kmymoney-074-released/) by Michael Edwardes)_

            