---
title: 'KMyMoney 0.8.3 released'
date: '2006-02-22 16:00:16:00 +0000'
layout: post
---
<div class="markdown_content"><p>KMyMoney is the Personal Finance Manager for KDE. It operates similar to MS-Money and Quicken, supports different account types, categorisation of expenses, QIF import/export, multiple currencies and initial online banking support.</p>
<p>The KMyMoney team is pleased to announce the availability of Release 0.8.3.<br />
This is an update to our latest stable release, and contains several bug fixes and some improvements to the user interface. There are also some new translations and miscellaneous changes, all of which should help to make building and using the application even easier.</p>
<p>The source tarball for this package can be downloaded from our web site at <a href="http://kmymoney2.sourceforge.net.">http://kmymoney2.sourceforge.net.</a> Should you need any further information or advice, feel free to contact us on one of our mailing lists:</p>
<p>kmymoney2-user@lists.sourceforge.net – for usage questions<br />
kmymoney2-developer@lists.sourceforge.net – for installation problems.</p></div>

_(Originally posted on [Sourceforge.net](https://sourceforge.net/p/kmymoney2/news/2006/02/kmymoney-083-released/) by Michael Edwardes)_

            