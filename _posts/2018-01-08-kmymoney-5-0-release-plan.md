---
title: '        KMyMoney 5.0 release plan
      '
date: 2018-01-08 00:00:00 
layout: post
---

<p>The KMyMoney development team recently announced the timeline for the long awaited KMyMoney 5.0 release. The dates are as follows:
        <ul>
          <li>2018-01-05: Create new stable branch 5.0 and inform kde-i18n-doc, Code freeze on 5.0 (only bug fixes from now on)</li>
          <li>2018-01-12: String-Freeze on 5.0 branch (ready for screenshots) and &#8220;What&#8217;s new&#8221; page must be finished too</li>
          <li>2018-01-26: Finish Release Notes compilation</li>
          <li>2018-02-02: Finish documentation (English) and UI translations</li>
          <li>2018-02-03: Tarball creation and tagging for 5.0.0</li>
          <li>2018-02-04: Release time for 5.0.0</li>
        </ul>
        The required Alkimia 7.0 package has been released over the last weekend.</p><p>The KMyMoney Development Team</p>