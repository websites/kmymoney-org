---
title: 'KMyMoney 0.8.4 released'
date: '2006-05-20 18:45:44:00 +0000'
layout: post
---
<div class="markdown_content"><p>The KMyMoney team is pleased to announce the availability of Release 0.8.4. </p>
<p>This is an update to our latest stable release, <br />
and contains several bug fixes and some improvements <br />
to the entry of scheduled transactions. There are also <br />
some new translations and miscellaneous changes, all <br />
of which should help to make building and using the <br />
application even easier. </p>
<p>The source tarball for this package can be downloaded <br />
from our web site at <a href="http://kmymoney2.sourceforge.net.">http://kmymoney2.sourceforge.net.</a> <br />
Should you need any further information or advice, feel <br />
free to contact us on one of our mailing lists: </p>
<p>kmymoney2-user@lists.sourceforge.net – for usage questions <br />
kmymoney2-developer@lists.sourceforge.net – for installation problems. </p></div>

_(Originally posted on [Sourceforge.net](https://sourceforge.net/p/kmymoney2/news/2006/05/kmymoney-084-released/) by Michael Edwardes)_

            