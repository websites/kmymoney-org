---
title: 'KMyMoney 0.6.4 released'
date: '2004-11-03 20:24:42:00 +0000'
layout: post
---
<div class="markdown_content"><p>The previously found problem in version 0.6.3 of the KDE personal finance manager KMyMoney has been fixed and an updated version has been released.</p></div>

_(Originally posted on [Sourceforge.net](https://sourceforge.net/p/kmymoney2/news/2004/11/kmymoney-064-released/) by Michael Edwardes)_

            