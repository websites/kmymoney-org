---
title: 'KMyMoney 5.1.0 released'
date: 2020-06-14 00:00:00
layout: post
---

<a href="https://download.kde.org/stable/kmymoney/5.1.0/src/kmymoney-5.1.0.tar.xz.mirrorlist">KMyMoney version 5.1.0</a> is now available.

Please take a look at the <a href="/changelogs/kmymoney-5-1-0-release-notes.html">release notes</a> before installing.

For a full list of the changes please check out the <a href="/changelogs/ChangeLog-5.1.0.txt">changelog</a>.

The KMyMoney Development Team
