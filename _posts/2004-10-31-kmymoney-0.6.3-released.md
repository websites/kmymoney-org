---
title: 'KMyMoney 0.6.3 released'
date: '2004-10-31 09:27:14:00 +0000'
layout: post
---
<div class="markdown_content"><p>As an ongoing service to the KMyMoney user community, the development team has released a new maintenance release of it's personal finance mananger for KDE. This maintenance release includes the fixes for some minor issues that have been found and also some functional enhancements. </p>
<p>Please visit <a href="http://sourceforge.net/project/showfiles.php?group_id=4708">http://sourceforge.net/project/showfiles.php?group_id=4708</a> to get the source tar ball of the latest version. Please see the Change Log for a detailed list of the modifications we have made.</p></div>

_(Originally posted on [Sourceforge.net](https://sourceforge.net/p/kmymoney2/news/2004/10/kmymoney-063-released/) by Michael Edwardes)_

            