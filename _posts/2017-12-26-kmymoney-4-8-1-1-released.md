---
title: '        KMyMoney 4.8.1.1 released
      '
date: 2017-12-26 00:00:00 
layout: post
---

<p><a href="http://download.kde.org/stable/kmymoney/4.8.1.1/src/kmymoney-4.8.1.1.tar.xz.mirrorlist">KMyMoney version 4.8.1.1</a> is now available.
          This version contains updated translations for several languages. Due to delays in the translation work, the translations for this version are
          unfortunally not complete for a few languages. We ask for your understanding.</p><p>Binary packages are available from related Linux distributions and for 32 and 64 bit Windows from the KDE <a class="piwik_link" href="http://download.kde.org/stable/kmymoney/4.8.1.1/">download mirror network</a>.</p><p>We highly recommend upgrading to 4.8.1.1.</p><p>The KMyMoney Development Team</p>