---
title: 'KMyMoney 5.1.3 released'
date: 2022-07-30 00:00:00
layout: post
---
<a href="https://download.kde.org/stable/kmymoney/5.1.3/src/kmymoney-5.1.3.tar.xz.mirrorlist">KMyMoney version 5.1.3</a> is now available.

<p>The KMyMoney development team is proud to present the next maintenance release of its open source Personal Finance Manager.</p>

<p>Here is the list of the included bugfixes grouped by severity:</p>
<h3>critical</h3>
<p>
  <ul>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=432380">432380</a> Appimage unable to print reports</li>
  </ul>
</p>
<h3>major</h3>
<p>
  <ul>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=426161">426161</a> Duplicating an investment transaction also duplicates a matched but not accepted transaction in the brokerage account with original not the new date on the matched transaction</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=447025">447025</a> Calculation of the balance is incorrect for future balances</li>
  </ul>
</p>
<h3>crash</h3>
<p>
  <ul>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=445458">445458</a> Investment Cap Gains by Account (Customized) crashes with my dataset. Changing the date of one ledger entry fixes the crash.</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=450016">450016</a> Attempting KMyMoney 5.0.8 "Currencies" Maintenance, Application Crash</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=451677">451677</a> crashes on new category with a double colon</li>
  </ul>
</p>
<h3>normal</h3>
<p>
  <ul>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=223708">223708</a> Closed accounts are not hidden in accounts/categories view</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=411272">411272</a> Not saving changes to Shortcuts</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=424303">424303</a> Export report as csv file gives a html file</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=425333">425333</a> no pre-defined account templates on a mac</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=428156">428156</a> OFX import goes to the wrong account</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=435866">435866</a> No ledger icon in the pane of the left side</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=439287">439287</a> Home view is missing styling</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=439722">439722</a> Equities are shown with currencies in new account dialog</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=439819">439819</a> Issue with changing credit card limits</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=439861">439861</a> Rounding error on investment transactions</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=440060">440060</a> Icons are missing on Linux if Breeze icon theme shipped by the bistro is older than 5.81</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=440111">440111</a> Tags/Payees Double Enter</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=440476">440476</a> Can not update stock price manually</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=440500">440500</a> Stock wizard shows online source that no longer exist</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=440681">440681</a> Currency list not sorted with locale awareness in the new account wizard</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=440692">440692</a> When importing OFX, the OK and Skip buttons are reversed</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=440695">440695</a> Unable to inspect the Splits when account is closed.</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=441292">441292</a> Impossible to paste into calculator widget</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=443208">443208</a> Build failure with aqbanking 6.3.2</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=444414">444414</a> Transaction notes are not imported from paypal account</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=445472">445472</a> Stock split transactions can cause rounding problems</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=446990">446990</a> Wayland: Tooltip on date input fields steals focus, prevents entering data</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=451891">451891</a> Setting the payee matching to exact name is not persistent</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=452068">452068</a> kmymoney complains about "GPG no secure keyring found"</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=452497">452497</a> Scheduled transactions: "Next due date", "Number remaining" and "Date of final" do not always update in step</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=452616">452616</a> Missing transaction information</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=452720">452720</a> Provide feature to rename existing loan accounts</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=452918">452918</a> Payee > Account Numbers > IBAN does not accept pasted content with a space at the start</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=456520">456520</a> OFX import broken upstream</li>
  </ul>
</p>
<h3>minor</h3>
<p>
  <ul>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=440736">440736</a> In "New Account" wizard, Enter key does not work on "Parent account" page</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=441296">441296</a> Fields in Exchange Rate/Price Editor misaligned</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=441937">441937</a> Default cash flow report: Name does not match date range</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=448013">448013</a> Unresponsive UI elements in "New File Setup" > "Select Accounts"</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=452863">452863</a> New file setup wizard: UK VAT Accounts produces "invalid top-level account type" error</li>
  </ul>
</p>
<h3>wishlist</h3>
<p>
  <ul>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=399685">399685</a> add match strings  as well as name from deleted payee to new payee</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=424377">424377</a> Change default matching behavior for new payees to "match on exact payee name"</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=440586">440586</a> When exporting a report, the file name (suggested) takes the report name</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=441581">441581</a> When "Amount" at ledgers get the focus by click, select the entire value</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=444262">444262</a> Date picker, frequency and process schedule at last day of the month should interact</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=447480">447480</a> Allow currencies to be divisible by more than ten decimal places</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=450965">450965</a> Please add Functionality to Scheduled Transactions</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=453922">453922</a> Decimal and thousands separators in ordinate axis labels are missing</li>
  </ul>
</p>
<p>A complete description of all changes can be found in the <a href="/changelogs/ChangeLog-5.1.3.txt">ChangeLog</a>.</p>

The KMyMoney Development Team
