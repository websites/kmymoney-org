---
title: '        KMyMoney 4.8.2 released
      '
date: 2018-06-01 00:00:00 
layout: post
---

<p>KMyMoney version 4.8.2 for Windows 7 SP1 or newer is now available. This version contains 20 <a href="https://kmymoney.org/resolvedbugs.php?4.8.2">bug fixes</a> and a few implemented <a href="https://kmymoney.org/resolvedfeatures.php?4.8.2">feature requests</a>.
          For a full list of the changes please check out the <a href="https://cgit.kde.org/kmymoney.git/log/?h=4.8.2">changelog</a>.</p><p>This release supports 29 languages (bs ca ca@valencia cs da de el en_GB es et eu fi fr gl hu it kk nds nl pl pt pt_BR ro ru sk sv tr uk zh_TW)
          and the documentation is available in 11 languages (en_US de es et fr it nl pt pt_BR sv uk) - many thanks to the translation teams for their efforts.</p><p>Binary packages are available from related Linux distributions and for 32 and 64 bit Windows from the KDE <a class="piwik_link" href="http://download.kde.org/stable/kmymoney/4.8.2/">download mirror network</a>.</p><p>The KMyMoney Development Team</p>