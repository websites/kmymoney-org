---
title: 'KMyMoney 1.0.2 released'
date: '2009-10-04 04:56:22:00 +0000'
layout: post
---
<div class="markdown_content"><p>The KMyMoney development team announces the
                release of KMyMoney 1.0.2.<br />
                This is a bug-fix release, including, among others, the following changes:<br />
                * Improve QIF importer to simply skip unknown sections<br />
                * Removed 'combine transaction' feature from menus, since it is not implemented<br />
                * Fixed #2867234 (Don't take status field when creating schedule from<br />
                ledger)<br />
                * Fixed a problem when no file was opened during program start<br />
                * Force initial size to be 800x600<br />
                * Make expand/collapse state of account and category view persistent<br />
                * Allow to modify the tab order during transaction entry<br />
                * Updated Russian translation by Andrey Cherepanov<br />
                * Updated German translation<br />
                * Fixed sizing of forecast chart<br />
                * Select all subaccounts of an investment in reports when not in expert mode - #2853193</p></div>
            

_(Originally posted on [Sourceforge.net](https://sourceforge.net/p/kmymoney2/news/2009/10/kmymoney-102-released/) by Michael Edwardes)_

            