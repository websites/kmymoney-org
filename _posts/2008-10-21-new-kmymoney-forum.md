---
title: 'New KMyMoney Forum'
date: '2008-10-21 22:14:46:00 +0000'
layout: post
---
<div class="markdown_content"><p>The KMyMoney Development Team is glad to announce
                the KMyMoney Forum <a href="http://forum.kde.org/forumdisplay.php?fid=69" rel="nofollow">http://forum.kde.org/forumdisplay.php?fid=69</a>,
                in the new KDE Forums. <a href="http://forum.kde.org/" rel="nofollow">http://forum.kde.org/</a><br
                />
                You can subscribe to the RSS feed. <a href="http://forum.kde.org/syndication.php?fid=69&amp;limit=15"
                rel="nofollow">http://forum.kde.org/syndication.php?fid=69&amp;limit=15</a><br />
                Although the mailing lists will continue to be the main line of support, the forum will monitored and
                moderated by members of the development team to provide timely help and advice. We welcome this as a new
                way of communication with all users and hope that it will become an important path of interaction not
                only with the development team but among users too.</p></div>
            

_(Originally posted on [Sourceforge.net](https://sourceforge.net/p/kmymoney2/news/2008/10/new-kmymoney-forum/) by Michael Edwardes)_

            