---
title: 'New mailing lists available'
date: '2004-06-28 19:53:52:00 +0000'
layout: post
---
<div class="markdown_content"><p>In order to allow developers to choose to receive certain automatically generated messages or not, two new mailing lists have been established for the KMyMoney project.</p>
<p>kmymoney2-cvs distributes notification e-mails about activities in the CVS repository (e.g. check-in, add, remove). kmymoney2-bugs distributes new entries and changes made to the bug tracking and feature request system. These automated mails used to be sent to the general developer list in the past.</p>
<p>Please visit <a href="https://sourceforge.net/mail/?group_id=4708">https://sourceforge.net/mail/?group_id=4708</a> to subscribe to any of the projects mailing lists.</p></div>

_(Originally posted on [Sourceforge.net](https://sourceforge.net/p/kmymoney2/news/2004/06/new-mailing-lists-available/) by Michael Edwardes)_

            