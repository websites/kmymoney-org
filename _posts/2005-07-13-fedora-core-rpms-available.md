---
title: 'Fedora Core RPMs available'
date: '2005-07-13 18:07:12:00 +0000'
layout: post
---
<div class="markdown_content"><p>Christian Nolte provided two RPMs for KMyMoney 0.7.3 for the Fedora Core 4 distribution. One is for the i386 platform, the other  is a specialized compilation for AMD athlon based systems.</p>
<p>Please visit the SourceForge File Release system to download these files. Problems regarding installation/packaging should directly be sent to Christian at noltec (at) users.sourceforge.net.</p></div>

_(Originally posted on [Sourceforge.net](https://sourceforge.net/p/kmymoney2/news/2005/07/fedora-core-rpms-available/) by Michael Edwardes)_

            