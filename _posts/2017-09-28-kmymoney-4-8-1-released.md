---
title: '        KMyMoney 4.8.1 released
      '
date: 2017-09-28 00:00:00 
layout: post
---

<p>KMyMoney version 4.8.1 is now available. This version contains about 70 fixes for <a href="https://kmymoney.org/resolvedbugs.php?4.8.1">several bugs</a> and a few implemented <a href="https://kmymoney.org/resolvedfeatures.php?4.8.1">feature requests</a>. See <a href="https://docs.kde.org/stable4/en/extragear-office/kmymoney/whatsnew.html">here</a> for a short list. For a full list of the changes please check out the <a href="https://cgit.kde.org/kmymoney.git/log/?h=4.8.1">changelog</a>.</p><p>Please note that this release does not contains any translated documentation, which affects Linux/Unix platform. Translated documentation will be available in release 4.8.1.1.</p><p>Binary packages are available from related Linux distributions and for 32 and 64 bit Windows from the KDE <a class="piwik_link" href="http://download.kde.org/stable/kmymoney/4.8.1/">download mirror network</a>.</p><p>The KMyMoney Development Team</p>