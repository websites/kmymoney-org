---
title: '        KMyMoney 4.6.1 released
      '
date: 2011-11-06 00:00:00 
layout: post
---

<p>The KMyMoney Team is pleased to announce the immediate availability of KMyMoney version 4.6.1. This version contains several fixes for bugs found in 4.6.0 since it was released three months ago.</p><p>Here is a list of the most important changes since the last stable release:</p><ul>
          <li>fixed schedules moved to the next processing day</li>
          <li>fixed a crash with an uncaught exception when closing the current file before a GNUCash import</li>
          <li>fixed the split window redraw when resizing</li>
          <li>fixed a crash caused by an invalid budget</li>
          <li>fixed a crash when deleting an account</li>
          <li>the date can now be modified when editing multiple transactions</li>
          <li>the balance in the payees view is now computed correctly even if the payee has transactions with different currencies</li>
        </ul><p>We highly recommend upgrading to 4.6.1 as soon as possible.</p>