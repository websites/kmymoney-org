---
title: 'KMyMoney 0.7.5 released'
date: '2005-08-02 13:05:49:00 +0000'
layout: post
---
<div class="markdown_content"><p>The development team has released KMyMoney 0.7.5, an updated version of the current development branch. Please expect updated installation packages for various distributions soon.  KMyMoney is the Personal Finance Manager for KDE. It operates similar to MS-Money and Quicken, supports different account types, categorisation of expenses, QIF import/export, multiple currencies and initial online banking support.</p></div>

_(Originally posted on [Sourceforge.net](https://sourceforge.net/p/kmymoney2/news/2005/08/kmymoney-075-released/) by Michael Edwardes)_

            