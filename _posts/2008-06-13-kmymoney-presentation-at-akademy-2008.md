---
title: 'KMyMoney presentation at aKademy 2008'
date: '2008-06-13 11:32:08:00 +0000'
layout: post
---
<div class="markdown_content"><p>The aKademy conference committee has accepted the
                talk to be given by Thomas Baumgart during the KDE Contributors Conference tracks to be held on Saturday
                August 9th and Sunday August 10th at the De Nayer Institute in Sint-Katelijne-Waver, Belgium.</p>
                <p>There are no details yet about the exact date and time of the talk. Details will be presented
                by the committee at <a href="http://akademy2008.kde.org/schedule.php" rel="nofollow">http://akademy2008.kde.org/schedule.php</a>
                as soon as they are available.</p>
                <p>Thomas plans to be in Belgium from Friday evening until Sunday afternoon. If you want to meet
                him, please send him an e-mail.</p></div>
            

_(Originally posted on [Sourceforge.net](https://sourceforge.net/p/kmymoney2/news/2008/06/kmymoney-presentation-at-akademy-2008/) by Michael Edwardes)_

            