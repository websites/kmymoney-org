---
title: '        KMyMoney 4.6.2 released
      '
date: 2012-02-04 00:00:00 
layout: post
---

<p>The KMyMoney Team is pleased to announce the immediate availability of KMyMoney version 4.6.2. This version contains quite a few fixes for bugs found in 4.6.1 since it was released three months ago.</p><p>Here is a list of the most important changes since the last stable release:</p><ul>
          <li>OFX files with UTF-8 data can now be imported correctly</li>
          <li>Fix displaying the 'Enter/Skip schedule' action icons in the homepage on Windows</li>
          <li>Fixed the initial size of the schedule entry dialog in some use cases</li>
          <li>Fixed a hang in reports in some scenarios</li>
          <li>Fixed some Finance::Quote related problems on Windows</li>
          <li>Allow editing the memo of multiple transactions</li>
          <li>Fix schedule handling</li>
          <li>Make the QIF import on Windows usable</li>
          <li>Fix GnuCash file import</li>
          <li>Improve item navigation using the keyboard</li>
          <li>Scheduled transactions can now be correctly skipped or ignored when automatic entry is enabled</li>
          <li>Fix the budgets that somehow still reference invalid accounts</li>
        </ul><p>For a full list of the changes please check out the changelog. We highly recommend upgrading to 4.6.2 as soon as possible. We would also recommend packagers to endorse this version as their latest stable version of KMyMoney due to the number of fixes available vs. 4.5.3. During this bugfix cycle a new release of libalkimia was also made available.</p>