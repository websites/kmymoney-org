---
title: '        KMyMoney 5.0.1 released
      '
date: 2018-03-19 00:00:00 
layout: post
---

<p><a href="https://download.kde.org/stable/kmymoney/5.0.1/src/kmymoney-5.0.1.tar.xz.mirrorlist">KMyMoney version 5.0.1</a> is now available.</p><p>Please take a look at the <a href="https://kmymoney.org/release-notes.php">release notes</a> before installing.</p><p>The KMyMoney Development Team</p>