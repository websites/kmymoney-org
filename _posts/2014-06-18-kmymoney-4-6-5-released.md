---
title: '        KMyMoney 4.6.5 released
      '
date: 2014-06-18 00:00:00 
layout: post
---

<p><a class="" href="http://download.kde.org/stable/kmymoney/4.6.6/src/kmymoney-4.6.6.tar.xz.mirrorlist">KMyMoney version 4.6.5</a> is now available. This version contains fixes for online imports using Quicken 2011 compatibility.</p><p>We have added latest Quicken versions, to be able to continue using online imports with those banks that dropped support for deprecated Quicken versions.</p><p>For a full list of the changes please check out the <a href="changelogs/ChangeLog-4.6.5.txt">changelog</a>. We only recommend upgrading if you have been having problems importing statements from your bank lately.</p><p>The KMyMoney Development Team</p>