---
title: 'KMyMoney: 0.9 released'
date: '2008-05-19 20:11:16:00 +0000'
layout: post
---
<div class="markdown_content"><p>KMyMoney is the Personal Finance Manager for KDE.
                It operates similar to MS-Money and Quicken, supports different account types, categorisation of
                expenses, QIF import/export, multiple currencies and initial online banking support.</p>
                <p>The KMyMoney development team is pleased to announce a major step forward for what has been
                described as &quot;the easiest personal money tool out there&quot;, with the first public
                availability of a new development release, version 0.9. This offers many new features and improvements
                over the existing, stable, 0.8 series; see below for just some of these.</p>
                <p>It should be emphasised however that this is a development release, and as such some of the
                newer<br />
                features may not be fully functional. Releases like this are also termed 'unstable', so as such,<br />
                it is advisable when using it to ensure that you maintain frequent backups of your files,<br />
                including your current (version 0.8) file. That being said, most of the developers themselves have,<br
                />
                of course, been entrusting their own data to this new release for some time!</p>
                <p>One aspect which (as is not uncommon for open source software) is particularly lacking<br />
                is up-to-date documentation. Be assured that we are working on this aspect, and any offers<br />
                of help will be appreciated. Now for the good news.</p>
                <p>New Features<br />
                ------------</p>
                <p>- Charts. Many of the reports produced by KMyMoney may now be displayed in chart form. Line,
                bar,<br />
                stacked bar, pie &amp; ring charts are supported where appropriate.</p>
                <p>- Budgets. A new budget feature allows you to specify your future expectations of income and
                expenditure,<br />
                and then to compare your actual performance against these. You can use your historical data to<br />
                have KMyMoney create a basic budget for you. Budget reports can be displayed as charts if desired.<br
                />
                Monthly budget overruns can be displayed on your home page, but can be removed if you find it<br />
                too depressing.</p>
                <p>- Forecasts. A forecast can be produced of the future balances of your critical accounts,<br
                />
                based either on regular scheduled transactions or your historical data. (Sadly, we are unable<br />
                to accurately predict the future value of your investments!). Forecast charts based<br />
                on historical data are under development.</p>
                <p>Major Enhancements<br />
                ------------------</p>
                <p>Many enhancements have been made to existing functionality, to improve your user experience.<br
                />
                Among the foremost of these are the following:</p>
                <p>- New 'wizards' have been produced to simplify the setting up of new accounts, and complicated<br
                />
                situations such as loans and mortgages. One wizard in particular will guide first time users<br />
                through the setting up of their new file, and template lists of frequently-used accounts<br />
                and categories are supplied, often tailored to nationality.</p>
                <p>- Several new options have been provided to enable you to configure KMyMoney to your personal
                requirements.</p>
                <p>- Defunct accounts may now be marked as closed, so that they no longer clutter up your screens,<br
                />
                though they are still available for viewing if required.</p>
                <p>- The views of the various ledgers have been revamped to provide better performance, and more<br
                />
                facilities for sorting and searching transactions, including a 'quick search' option.<br />
                A new multiple transaction selection function will allow you to do some 'bulk' changing of transactions.</p>
                <p>- A new, more comprehensive, transaction autofill facility, based on payee name, has been
                introduced.</p>
                <p>- Deletion of 'non-empty' payees and categories is now allowed, with the transactions<br />
                being re-assigned as required.</p>
                <p>- Changes have been made to simplify the entry and maintenance of your regular scheduled
                transactions.</p>
                <p>- Some new reports have been added, new levels of detail have been included, and many of them<br
                />
                have also been made more configurable. There are also been a number of fixes related to<br />
                multi-currency reports.</p>
                <p>- Warning messages can be displayed if the balance on an account exceeds, or falls below, a
                given value.</p>
                <p>- A summary of all your accounts can be seen in the home page, including current net worth,<br
                />
                income and expenses, and the total scheduled income and expenses for current month.</p>
                <p>- If you use OFX or HBCI for online banking, your setup has been made easier, and a manual<br
                />
                transaction matching facility should simplify reconciliation of your downloaded<br />
                statements. There is support for new versions of AqBanking and libofx, providing more<br />
                flexibility and reliability.</p>
                <p>- And, not least of all, new artwork and cosmetic enhancements may make your usage<br />
                of the product easier and, we hope, more pleasurable.</p>
                <p>Coming along<br />
                ------------</p>
                <p>Work on provision of the ability to store your financial data in one of the popular relational
                databases<br />
                is in an advanced state. It was only withheld from this release pending some performance improvements.</p>
                <p>As mentioned above, work on providing more comprehensive budgetting and forecasting features is
                also under way.</p>
                <p>Translations<br />
                ------------</p>
                <p>Some translations of some of the texts and messages used in this release into languages other<br
                />
                than English are already available. See the 'Help make a translation' link at<br />
                <a href="http://kmymoney2.sourceforge.net/index-devel.html">http://kmymoney2.sourceforge.net/index-devel.html</a>
                for details of progress on this.<br />
                (The status of this release will be roughly similar to that of HEAD.)</p>
                <p>If you have some fluency in any of the languages which are not yet translated, or which have<br
                />
                a low percentage of completion, your help would be welcomed. No great technical expertise is<br />
                required, though a knowledge of personal finance terminology would obviously be useful. Initial<br />
                contact should be made to the developer mailing list, see below.</p>
                <p>Technical Issues<br />
                ----------------</p>
                <p>This release is still dependent on versions 3 of the KDE Desktop Environment and the Qt
                toolkit.<br />
                Though it has been known to work in a KDE4 environment, the development (for building from source code)<br
                />
                and runtime versions of these products must also be installed.</p>
                <p>Some caveats<br />
                ------------</p>
                <p>Once you have updated your file and saved it under the new release, it may still be possible<br
                />
                to read it under the older, 0.8, versions, but this is not guaranteed. Hence the warning above,<br />
                to preserve a backup of your current file.</p>
                <p>If you are currently running a 0.8 version of this product which you built from source code
                ('cvs'),<br />
                it is highly advisable to uninstall it ('make uninstall', 'make clean') before installing this release.</p>
                <p>Bugs and Problems<br />
                -----------------</p>
                <p>We are eager for as many people as possible to help us by reporting any problems or bugs<br
                />
                which may be encountered in this release. Please report them either to the developer mailing list at
                <br />
                kmymoney2-developer@lists.sourceforge.net, or via the 'Report a bug' link at <a
                href="http://kmymoney2.sourceforge.net/index-devel.html.">http://kmymoney2.sourceforge.net/index-devel.html.</a></p>
                <p>The developer list can also be used for any offers of help in documentation, translation or
                programming.</p>
                <p>We thank you all for your support.</p></div>
            

_(Originally posted on [Sourceforge.net](https://sourceforge.net/p/kmymoney2/news/2008/05/kmymoney-09-released/) by Michael Edwardes)_

            