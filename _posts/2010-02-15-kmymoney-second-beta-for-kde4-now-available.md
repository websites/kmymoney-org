---
title: '        KMyMoney second beta for KDE4 now available
      '
date: 2010-02-15 00:00:00 
layout: post
---

<p>The KMyMoney Team is proud to announce the immediate availability of version 3.96 of KMyMoney. This is the second beta release for the KDE4 platform,
          after only 1 and a half month of hard work, stabilizing and fixing bugs found during the first beta release.</p><p>This version is still considered beta and not recommended for general use. As with the previous version, the intention is to get feedback from the
          community and find those issues that are not immediately apparent.</p><p>Some of the improvements included in this release are:</p><ul>
          <li>Compatibility with KDE SC 4.4, after an unfortunate bug in our previous release that made it impossible to compile in that setup</li>
          <li>Better integration with the Qt4 version of AqBanking, and an arrangement with AqBanking to have the Qt4 version more easily available</li>
          <li>Integration with KDE Calendar libraries, for a better local holiday support</li>
          <li>Improved handling of GPG-related errors</li>
          <li>The main view now has three different navigation modes: the traditional left navigation bar, a tree, and tabbed windows</li>
          <li>Charts have been improved and several features which were missing in the previous release have been added back, and some that were not present in the
            KDE3 version</li>
          <li>Over 30 bugs have been fixed for this release alone and only 8 bugs remain open at this point. We thank the community for the reports and we expect to
            see more reports (and hopefully more fixes) after this release</li>
          <li>Several custom widgets have been ported to be fully Qt4-native. This means not only an aesthetic improvement, but also a cleaner and more maintainable
            code, which has already helped us solve some bugs along the way. Plans are already being laid out to extend this effort to all the remaining widgets in
            subsequent releases</li>
          <li>Better build management, thanks to the feedback provided by the packagers of many distributions and platforms</li>
          <li>Documentation is now available in more languages (Italian, UK English, and Brazilian Portuguese, among others) and a Valencian Catalan translation is
            available for the user interface</li>
        </ul><p>For this and coming releases, at least until hitting a stable version, we have adopted a quick release cycle, comprised of 4 weeks devoted to the big
          changes and fixes, and 2 weeks to stabilizing and translating. We believe this will help us find bugs more quickly and improve the feedback, while still
          having enough time to work on bugs and new features. This means you can expect a new release 6 weeks from now, unless a blocking issue gets in the way.</p>