---
title: 'KMyMoney is a finalist in the SF 2007 CCA'
date: '2007-07-08 09:06:18:00 +0000'
layout: post
---
<div class="markdown_content"><p>Good news everyone! We made it!</p>
<p>KMyMoney has been nominated for &quot;best user support&quot; and<br />
is a finalist in the Sourceforge.net 2007 Community Choice Awards.</p>
<p>Final voting will be open until July 20th.<br />
So hurry up and select your favorite projects. </p>
<p>You can vote by going to <a href="http://sourceforge.net/awards/cca/vote.php">http://sourceforge.net/awards/cca/vote.php</a></p>
<p>Winners will be announced on July 26th and posted on<br />
Sourceforge before July 30th.</p></div>

_(Originally posted on [Sourceforge.net](https://sourceforge.net/p/kmymoney2/news/2007/07/kmymoney-is-a-finalist-in-the-sf-2007-cca/) by Michael Edwardes)_

            