---
title: 'KMyMoney development team welcomes a new member'
date: '2004-07-13 06:20:11:00 +0000'
layout: post
---
<div class="markdown_content"><p>The KMyMoney development team welcomes Ace Jones as a new member. Ace contributed the reporting logic available in the development branch.</p>
<p>Again: Welcome on board and keep up the good work.</p></div>

_(Originally posted on [Sourceforge.net](https://sourceforge.net/p/kmymoney2/news/2004/07/kmymoney-development-team-welcomes-a-new-member/) by Michael Edwardes)_

            