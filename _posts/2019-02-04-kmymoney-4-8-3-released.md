---
title: '        KMyMoney 4.8.3 released
      '
date: 2019-02-04 00:00:00 
layout: post
---

<p>KMyMoney version 4.8.3 for Windows 7 SP1 or newer is now available. This version contains more than 20 <a href="https://kmymoney.org/resolvedbugs.php?4.8.3">bug fixes</a>.
          For a full list of the changes please check out the <a href="https://cgit.kde.org/kmymoney.git/log/?h=4.8.3">changelog</a>.</p><p>Binary packages for 32 and 64 bit Windows are available from the KDE <a class="piwik_link" href="http://download.kde.org/stable/kmymoney/4.8.3/">download mirror network</a>.</p><p>The KMyMoney Development Team</p>