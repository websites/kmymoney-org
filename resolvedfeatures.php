<?php
#
# @author Ralf Habacker <ralf.habacker@freenet.de>
#
# redirect to bugs.kde.org and shows a list of resolved features
#
# syntax:
#   resolvedfeatures.php                       - shows all resolved features
#   resolvedfeatures.php?[<product>/]<version> - shows open feature requests for a specific product and version
#
# for details see lib.inc:bugListFromQuery()
#
	include(dirname(__FILE__)."/lib.inc");
	$query = isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : "";
	$url = buglistFromQuery("resolvedfeatures/$query");
	if ($url)
		Header("Location: $url");
?>