---
layout: page
title: Download
sources:
  - name: Linux
    icon: /assets/img/tux.png
    description: >
        KMyMoney is readily available on the majority of Linux distributions. You
        can install it using the Software Center provided by your vendor.<br><br>

        <a href="/appimage.html">AppImage packages</a> are provided as an alternative for those who
        want to use a version newer than the one available in their Software Center.<br><br>

        A <a href="https://snapcraft.io/kmymoney" target="_top">Snapcraft package</a> is available
        as another alternative for those who want to use a version newer than the one available in
        their Software Center.

  - name: Windows
    icon: /assets/img/windows.svg
    description: >

        <strong>Provided from KDE CI/CD system:</strong><br><br>

        MSVC based builds signed by KDE can be downloaded from KDE's CI/CD pipeline. [This page](windows.html)
        contains the necessary links and instructions. Note: these builds do not provide aqbanking support.<br><br>

        <strong>Provided by external sources and not maintained by KMyMoney developers:</strong><br>

        - An installation package for the latest stable release with aqbanking support can be found on the
        [KDE download server](https://download.kde.org/stable/kmymoney/latest/).

        - The same developer also provides daily generated (gcc based cross-compiled) [preview builds](https://kmymoney.org/snapshots.php)
        with aqbanking support.<br><br>

  - name: macOS
    icon: /assets/img/macOS.svg
    description: >

        <strong>Provided from KDE CI/CD system:</strong><br><br>

        DMG packages signed by KDE can be downloaded from KDE's CI/CD pipeline. [This page](macos.html)
        contains the necessary links and instructions.<br><br>

        <strong>Provided by external sources and not maintained by KMyMoney developers:</strong><br>

        - An experimental Homebrew package is provided by [Homebrew KDE project](https://invent.kde.org/packaging/homebrew-kde).

        - Legacy KMyMoney4 builds may also be available in [MacPorts](https://www.macports.org/ports.php?by=name&substr=kmymoney).

  - name: Source code archive
    icon: /assets/img/ark.svg
    description: >
        You can find the source code of the KMyMoney latest stable release
        [here](https://download.kde.org/stable/kmymoney/) and the installation
        instructions in the [KDE TechBase wiki](https://techbase.kde.org/Projects/KMyMoney#Installation)<br><br>

  - name: Git
    icon: /assets/img/git.svg
    description: >
        The KMyMoney Git repository can be found in the [KDE GitLab](https://invent.kde.org/office/kmymoney).<br><br>

        To clone KMyMoney use:<br>
        <code>git clone https://invent.kde.org/office/kmymoney.git</code>

---

<h1>Download</h1>

<table class="distribution-table">
{% for source in page.sources %}
    <tr class="title-row">
        <td rowspan="2" width="100">
            <img src="{{ source.icon }}" alt="{{ source.name }}">
        </td>
        <th>{{ source.name }}</th>
    </tr>
    <tr>
        <td>{{ source.description | markdownify }}</td>
    </tr>
{% endfor %}
</table>
