<?php
#
# @author Ralf Habacker <ralf.habacker@freenet.de>
#
# redirect to bugs.kde.org and shows a list of open bugs
#
# syntax:
#   openbugs.php                       - shows all open bugs
#   openbugs.php?[<product>/]<version> - shows open bugs for a specific (optional) product and version
#
# for details see lib.inc:bugListFromQuery()
#
	include(dirname(__FILE__)."/lib.inc");
	$query = isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : "";
	$url = buglistFromQuery("openbugs/$query");
	if ($url)
		Header("Location: $url");
?>
