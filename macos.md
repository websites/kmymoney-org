---
title: "Download and use the MacOS version"
layout: page
sources:
  - name: Stable version (arm64)
    pipelineTxt: download page showing the stable version
    pipelineUrl: https://cdn.kde.org/ci-builds/office/kmymoney/5.1/macos-arm64/
    imageName: 	kmymoney-5.1-3138-macos-clang-arm64.dmg
    description: >

    note:

  - name: Development version (x86_64)
    pipelineTxt: download page showing the development version
    pipelineUrl: https://cdn.kde.org/ci-builds/office/kmymoney/master/macos-x86_64/
    imageName: kmymoney-master-XXXX-macos-clang-x86_64.dmg
    description: >
        For the more adventurous Linux users, the development versions might be interesting. They contain the latest development state but
        might be unstable and eat your data. Make sure to have backups of your data before you use this software.
        To download it, the following steps need to be performed:

    note:

  - name: Development version (arm64)
    pipelineTxt: download page showing the development version
    pipelineUrl: https://cdn.kde.org/ci-builds/office/kmymoney/master/macos-arm64/
    imageName: kmymoney-master-XXXX-macos-clang-arm64.dmg
    description: >
        For the more adventurous Linux users, the development versions might be interesting. They contain the latest development state but
        might be unstable and eat your data. Make sure to have backups of your data before you use this software.
        To download it, the following steps need to be performed:

    note:
---

<h1>How to download and use the MacOS version</h1>

Page last updated: 2024-03-12

<div class="highlight">

    <p><strong>Important:</strong> The images referenced on this page are build as part of KDE's CI/CD process and are automatically generated when
    the repository receives changes. They contain a number in the build which is changed with each build. Please replace the placeholder #### with
    the actual number in the instructions below.</p>

<!-- MACOS stable x86_64 version -->
     <h2>Stable version (x86_64)</h2>
     Unfortunately, the MacOS build for the stable x86_64 version of KMyMoney on the KDE CI/CD infrastructure is currently broken.  We are looking for an experienced MacOS developer to help us fixing the issue. Please contact us at <a href="mailto:kmymoney-devel@kde.org">kmymoney-devel@kde.org</a> if you can help out.</p>

<!-- commented out until the MacOS builds are working again. The following needs an update (see windows.md or appimage.md)
  - name: Stable version (x86_64)
    pipelineTxt: download page showing the stable version
    pipelineUrl: https://invent.kde.org/office/kmymoney/-/pipelines?page=1&scope=all&ref=5.1&status=success
    imageName: kmymoney-5.1-XXXX-macos-clang-x86_64.dmg
    description: >

    note:
-->


{% for source in page.sources %}
    <h2>{{ source.name }}</h2>
    {{ source.description  | markdownify }}

    <ul>
    <li>Visit the <a href="{{ source.pipelineUrl }}" target="_blank">{{ source.pipelineTxt }}</a></li>
    <li>Click on the link for the package named <strong>{{ source.imageName }}</strong> to start downloading the file to your local disk.</li>
    <li>Install the downloaded <strong>{{ source.imageName }}</strong> </li>
    </ul>

    <p><em>{{ source.note | markdownify }}</em></p>
{% endfor %}
</div>
