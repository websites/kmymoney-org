How to setup an php development website on a linux host
===================================================

1. Clone required repositories

    mkdir <my-root>/websites
    cd <my-root>/websites
    git clone git@invent.kde.org:websites/kmymoney-org.git

2. Install at least
     apache2
     apache2-mod_php7

   (package names may differ on your distribution, the mentioned
    ones are from opensuse)

3. Create a apache virtual host from the provided template
   with a least the following attributes

<VirtualHost *:80>
    ServerAdmin webmaster@dummy-host.example.com
    ServerName kmymoney-dev.kde.org

    ErrorLog /var/log/apache2/kmymoney-dev.kde.org-error_log
    CustomLog /var/log/apache2/kmymoney-dev.kde.org-access_log combined

    DocumentRoot <my-root>/websites/kmymoney-kde-org
    <Directory <my-root>/websites/kmymoney-kde-org>
        Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
    </Directory>

</VirtualHost>

4. (re)start apache2 service

5. Add the following line to /etc/hosts

    127.0.0.1 kmymoney-dev.kde.org

6. Access the website from a browser with

      http://kmymoney-dev.kde.org/snapshots.php

