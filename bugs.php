<?php
#
# @author Ralf Habacker <ralf.habacker@freenet.de>
# 
# common wrapper for showing bug lists from bug.kde.org
#
# syntax:
#  bugs.php?<type>[/<product>][/<version>]
#
# for details see lib.inc:bugListFromQuery()
#
	include(dirname(__FILE__)."/lib.inc");
	$query = isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : "";
	$url = buglistFromQuery($query);
	if ($url)
		Header("Location: $url");
?>
