<?php
#
# @author Ralf Habacker <ralf.habacker@freenet.de>
#
# redirect to bugs.kde.org and shows a list of resolved bugs
#
# syntax:
#   resolvedbugs.php                       - shows all resolved bugs
#   resolvedbugs.php?<version>[/<product>] - shows resolved bugs for a specific (optional) product and version
#
# for details see lib.inc:bugListFromQuery()
#
	include(dirname(__FILE__)."/lib.inc");
	$query = isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : "";
	$url = buglistFromQuery("resolvedbugs/$query");
	if ($url)
		Header("Location: $url");
?>
